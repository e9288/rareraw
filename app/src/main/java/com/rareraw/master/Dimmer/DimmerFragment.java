package com.rareraw.master.Dimmer;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Config;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.data.Dimmer;
import com.rareraw.master.data.SimpleSceneInfo;
import com.rareraw.master.data.TestDataManager;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.ArrayList;
import java.util.List;

public class DimmerFragment extends BaseFragment implements View.OnClickListener {
    private static String TAG = "SensorFragment";
    private static Context mContext;
    private View mParent;
    private int UP = 0, DOWN = 1, POWER = 2, POWER_ON = 3;
    ArrayList<DeviceInfo> mRoomList = new ArrayList<DeviceInfo>();
    private RelativeLayout mBtnSeleectRoom, mBtnLightOnOff, mBtnLightUp, mBtnLightDown, mTextNoroom;
    private RoomListAdapter mRoomListAdapter;
    private Spinner mRoomSpinner;
    private TextView mTextRoomSize0;
    Handler mHandler = new Handler(Looper.getMainLooper());

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        //if (APService.getUserLevel()) {
        updateUi();
        onFragmentCallbackListener.onFragmentCallback(getTag(), 0);
        getMoodList();
        // } else
        //     onFragmentCallbackListener.onFragmentCallback(getTag(), -1);
    }

    private void getMoodList() {
        // 1 step, get scene list in ap
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

            }

            @Override
            public void onError(int errorCode) {
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                LogoutMain();
            }
        });
    }

    private void updateUi() {
        getRoomInfo();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_dimmer, container, false);
        mBtnSeleectRoom = (RelativeLayout) mParent.findViewById(R.id.btn_select_room);
        mTextNoroom = (RelativeLayout) mParent.findViewById(R.id.text_no_room);
        mTextRoomSize0 = (TextView) mParent.findViewById(R.id.text_room_size0);
        mBtnSeleectRoom.setOnClickListener(this);
        mBtnLightOnOff = (RelativeLayout) mParent.findViewById(R.id.btn_light_onoff);
        mBtnLightOnOff.setOnClickListener(this);
        mBtnLightUp = (RelativeLayout) mParent.findViewById(R.id.btn_light_up);
        mBtnLightUp.setOnClickListener(this);
        mBtnLightDown = (RelativeLayout) mParent.findViewById(R.id.btn_light_down);
        mBtnLightDown.setOnClickListener(this);

        //데이터
        ArrayList<DeviceInfo> data = new ArrayList<>();
        byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
        byte[] deviceType = new byte[]{(byte) 0x01};
        // 01049147330 전체조
        data.add(new DeviceInfo(alllight_mac, deviceType));
        //UI생성
        mRoomSpinner = (Spinner) mParent.findViewById(R.id.spinner1);
        //Adapter
        mRoomListAdapter = new RoomListAdapter(getContext(), data);

        //Adapter 적용
        mRoomSpinner.setAdapter(mRoomListAdapter);
        mRoomSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                getCurrentRoomStatus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    private void getTestData() {
        if (Config.DEBUG) {
            mRoomList.clear();
            DeviceInfo info1 = new DeviceInfo(1);
            info1.setIntDevType(0);
            mRoomList.add(info1);
            mRoomList.add(new DeviceInfo(1));
            mRoomList.add(new DeviceInfo(1));

            DeviceInfo info2 = new DeviceInfo(1);
            info2.setIntDevType(0);
            mRoomList.add(info2);
            DeviceInfo info3 = new DeviceInfo(1);
            info3.setIntDevType(1);
            mRoomList.add(info3);
            DeviceInfo info4 = new DeviceInfo(1);
            info4.setIntDevType(1);
            mRoomList.add(info4);

            mRoomListAdapter.setNewRoomList(mRoomList);
            mRoomListAdapter.notifyDataSetChanged();

            getCurrentRoomStatus();
        }
    }

    private void getRoomInfo() {
        ((ContainActivity) getActivity()).createDialog(900);
        if (Config.DEBUG) {
            getTestData();
        } else {
            // 1 step, load existing device list (2 type)
            APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {

                    mRoomList.clear();
                    byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
                    byte[] deviceType = new byte[]{(byte) 0x01};
                    // 01049147330 전체조
                    mRoomList.add(new DeviceInfo(alllight_mac, deviceType));

                    ArrayList<DeviceInfo> registedDev = APService.getApRegistedDevList();

                    for (DeviceInfo info : registedDev) {
                        mRoomList.add(info);
                    }
                    //mRoomList = APService.getApRegistedDevList();
                    if (mRoomList.size() <= 0) {
                        mBtnSeleectRoom.setVisibility(View.GONE);
                        mTextNoroom.setVisibility(View.VISIBLE);
                        mTextRoomSize0.setText("등록된 룸이없습니다. 룸을 등록해주세요.");
                    } else {
                        mBtnSeleectRoom.setVisibility(View.VISIBLE);
                        mTextNoroom.setVisibility(View.GONE);
                        mRoomListAdapter.setNewRoomList(mRoomList);
                        mRoomListAdapter.notifyDataSetChanged();
                        mTextRoomSize0.setText("");
                        getCurrentRoomStatus();
                    }
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Logout();
                }
            });
        }
    }

    private void getCurrentRoomStatus() {
        if (mRoomListAdapter.getCount() <= 0)
            return;

        DeviceInfo info = (DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition());
        byte[] mac = info.getDevMac();
        APService.getDeviceDiming(mac, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                Dimmer dimmer = (Dimmer) retValue;

                //android.widget.Toast.makeText(getContext(), "[TEST] 현재 룸 상태 가져오기 성공", Toast.LENGTH_SHORT).show();
                Log.d("power is ", Boolean.toString(dimmer.isPowerStatus()));
                Log.d("diming is ", Integer.toString(dimmer.getDiming()));
                ((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(dimmer.getDiming());
                ((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setPowerStat(dimmer.isPowerStatus());
            }

            @Override
            public void onError(int errorCode) {
                //int dimingValue = 20;
                try {
                    //((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(dimingValue);
                    //  android.widget.Toast.makeText(getContext(), "[TEST] 현재 룸 상태 가져오기 실패", Toast.LENGTH_SHORT).show();
                    //  android.widget.Toast.makeText(getContext(), "[TEST] 현재 룸 상태 가져오기 실패", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public static boolean delayFlag = true;
    @Override
    public void onClick(View v) {

        if(!delayFlag)
            return;
        else
            delayFlag = !delayFlag;

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                delayFlag = true;
            }
        }, 1000);
         switch (v.getId()) {
            case R.id.btn_select_room:
                break;

            case R.id.btn_light_onoff:
                //   android.widget.Toast.makeText(mContext, "룸 전원 끔", Toast.LENGTH_SHORT).show();
                setDevicePower(POWER);
                break;

            case R.id.btn_light_up:

                // android.widget.Toast.makeText(mContext, "라이트 업", Toast.LENGTH_SHORT).show();
                setDeviceDiming(UP);
                break;

            case R.id.btn_light_down:
                //  android.widget.Toast.makeText(mContext, "라이트 다운", Toast.LENGTH_SHORT).show();
                setDeviceDiming(DOWN);

                break;

        }
    }

    private void setDevicePower(int power) {

        DeviceInfo info = (DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition());

        if (info == null)
            return;

        byte[] mac = info.getDevMac();
        byte[] action = new byte[1];

        if (info.isPowerStat())
            action[0] = (byte) 0x00;
        else
            action[0] = (byte) 0x01;

        Log.d("power  is ", Byte.toString(action[0]));
        if (((ContainActivity) getActivity()) != null)
            ((ContainActivity) getActivity()).createDialog(800);

        APService.setDevicePower(mac, action, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

                Log.d("power setting ok ", "");
                //  android.widget.Toast.makeText(getContext(), "[TEST] 전원 조정완료", Toast.LENGTH_SHORT).show();
                //((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(finalDiming);
                getCurrentRoomStatus();
            }

            @Override
            public void onError(int errorCode) {
                Log.d("power setting fail ", "");
                // android.widget.Toast.makeText(getContext(), "[TEST] 전원 조정실패", Toast.LENGTH_SHORT).show();
                //((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(finalDiming);
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }

    public void setDeviceDiming(int dimingType) {

        int dimingLevel = 0;
        DeviceInfo info = (DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition());
        if (info == null)
            return;
        if (dimingType == UP) {
            dimingLevel = info.getDimingLevel() + 10;
            if (dimingLevel > 100)
                dimingLevel = 100;
        } else if (dimingType == DOWN) {
            dimingLevel = info.getDimingLevel() - 10;
            if (dimingLevel <= 0)
                dimingLevel = 0;
        }

        byte[] mac = info.getDevMac();
        byte[] level = new byte[1];
        level[0] = (byte) dimingLevel;
        final int finalDiming = dimingLevel;
        Log.d("diming is ", Integer.toString(finalDiming));

        if (((ContainActivity) getActivity()) != null)
            ((ContainActivity) getActivity()).createDialog(800);

        APService.setDeviceDiming(mac, level, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                Log.d("diming setting ok ", "");
                //  android.widget.Toast.makeText(getContext(), "[TEST] 디밍 조정완료", Toast.LENGTH_SHORT).show();
                ((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(finalDiming);
                ((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setPowerStat(true);
            }

            @Override
            public void onError(int errorCode) {
                Log.d("diming setting fail ", "");
                //  android.widget.Toast.makeText(getContext(), "[TEST] 디밍 조정 실패", Toast.LENGTH_SHORT).show();
                //((DeviceInfo) mRoomListAdapter.getItem(mRoomSpinner.getSelectedItemPosition())).setDimingLevel(finalDiming);
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });

    }
}