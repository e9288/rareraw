package com.rareraw.master.Dimmer;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neoromax.feelmaster.R;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.DeviceInfo;

import java.util.ArrayList;
import java.util.List;

public class RoomListAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    ArrayList<DeviceInfo> mRoomList;

    public RoomListAdapter(Context context, ArrayList<DeviceInfo> data) {
        this.context = context;
        this.mRoomList = data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        if (mRoomList != null) return mRoomList.size();
        else return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_spinner1_normal, parent, false);
        }

        if (mRoomList != null) {
            //데이터세팅
            // String text = mRoomList.get(position).name;
            if(position == 0 ){
                String text = "전체조명";
                ((TextView) convertView.findViewById(R.id.spinnerText)).setText(text);
            }else {
                //String text = "ROOM " + Integer.toString(position+1);
                //((TextView) convertView.findViewById(R.id.spinnerText)).setText(text);
                String name = "";
                byte[] bnmae = mRoomList.get(position).getmByteDevName();

                try {
                    for (byte singleMac : bnmae)
                        name += String.format("%02X", (((short) singleMac)) & 0xFF);
                } catch (Exception e) {
                    Log.e("error", "convert hex to string");
                }
                String text = "";
                int value = 0;
                if (bnmae != null) {
                    for (byte b : bnmae) {
                        value += (int) b;
                    }
                }
                if (value == 0) {
                    text = "ROOM " + Integer.toString(position );
                } else {
                    try {
                        text = new String(bnmae, "UTF-8");
                    }catch (Exception e){

                    }
                }

                ((TextView) convertView.findViewById(R.id.spinnerText)).setText(text);
            }


        }

        return convertView;
    }

    @Override
    public View getDropDownView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.spinner_spinner1_dropdown, parent, false);
        }

        LinearLayout body = (LinearLayout) convertView.findViewById(R.id.item_body);
    /*    body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, Integer.toString(position), Toast.LENGTH_SHORT).show();
            }
        });*/

        if (mRoomList != null) {
            //데이터세팅
            // String text = mRoomList.get(position).name;
            if (position == 0) {
                //데이터세팅
                // String text = mRoomList.get(position);
                String text = "전체조명";
                ((TextView) convertView.findViewById(R.id.spinnerText)).setText(text);
            } else {
                //데이터세팅
                // String text = mRoomList.get(position);
                String name = "";
                byte[] bnmae = mRoomList.get(position).getmByteDevName();

                try {
                    for (byte singleMac : bnmae)
                        name += String.format("%02X", (((short) singleMac)) & 0xFF);
                } catch (Exception e) {
                    Log.e("error", "convert hex to string");
                }
                String text = "";
                int value = 0;
                if (bnmae != null) {
                    for (byte b : bnmae) {
                        value += (int) b;
                    }
                }
                if (value == 0) {
                    text = "ROOM " + Integer.toString(position);
                } else {
                    try {
                        text = new String(bnmae, "UTF-8");
                    }catch (Exception e){

                    }
                }
                // String text = "ROOM " + Integer.toString(position+1);
                ((TextView) convertView.findViewById(R.id.spinnerText)).setText(text);
            }

        }


        return convertView;
    }

    @Override
    public Object getItem(int position) {
        if (position == -1) {
            if (mRoomList.size() > 0)
                return mRoomList.get(0);
        }
        if (mRoomList == null || mRoomList.size() == 0)
            return null;
        return mRoomList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setNewRoomList(ArrayList<DeviceInfo> mRoomList) {
        this.mRoomList = mRoomList;
    }

}
