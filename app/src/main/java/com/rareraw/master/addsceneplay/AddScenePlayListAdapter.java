package com.rareraw.master.addsceneplay;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.ScenePlayInfo;

/**
 * Created by Lch on 2016-12-26.
 */

public class AddScenePlayListAdapter extends BaseAdapter implements View.OnClickListener {

    private LayoutInflater mLayoutInflater;
    private ScenePlayInfo mScenePlayInfoList;
    private OnActionListener mListner;
    private Context mContext;
    private int mDeletePosition;
    public AddScenePlayListAdapter(Context context, ScenePlayInfo _sceneplaylist, OnActionListener listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mScenePlayInfoList = _sceneplaylist;
        mListner = listener;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mScenePlayInfoList.getSceneList().size();
    }

    @Override
    public Object getItem(int i) {
        return mScenePlayInfoList.getSceneList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    CheckedTextView oldcheckedTextView;
    ImageView oldImagViee;

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        AddScenePlayListHolder addScenePlayListHolder;
        if (view == null) {
            addScenePlayListHolder = new AddScenePlayListHolder();
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_add_scene_play, viewGroup, false);
            addScenePlayListHolder.mTextName = (TextView) view.findViewById(R.id.item_list_fragment_play_mood_name);

        } else {
            addScenePlayListHolder = (AddScenePlayListHolder) view.getTag();
        }


        final ImageView moodTypeImg = (ImageView) view.findViewById(R.id.list_view_mood_type);
        moodTypeImg.setTag(R.id.list_view_mood_type, false);

        final RelativeLayout item = (RelativeLayout) view.findViewById(R.id.moode_layout);
        final TextView textchecked = (TextView) view.findViewById(R.id.textchecked);
        textchecked.setTag(R.id.textchecked, false);

        final RelativeLayout selectLayout = (RelativeLayout) view.findViewById(R.id.layout_selete);
        selectLayout.setTag(R.id.layout_selete, false);

        if(mScenePlayInfoList.getSceneList().get(i).isSelectedFlag()){

            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood_act);
            moodTypeImg.setTag(R.id.list_view_mood_type, true);

            textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
            textchecked.setTag(R.id.textchecked, true);

            selectLayout.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
            selectLayout.setTag(R.id.layout_selete, true);

        }else{
            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood);
            moodTypeImg.setTag(R.id.list_view_mood_type, false);

            textchecked.setTextColor(mContext.getResources().getColor(R.color.black_color));
            textchecked.setTag(R.id.textchecked, false);

            selectLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
            selectLayout.setTag(R.id.layout_selete, false);
        }
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag = (boolean) moodTypeImg.getTag(R.id.list_view_mood_type);
                if(flag){

                    moodTypeImg.setImageResource(R.mipmap.icon_normal_mood);
                    moodTypeImg.setTag(R.id.list_view_mood_type, false);

                    textchecked.setTextColor(mContext.getResources().getColor(R.color.black_color));
                    textchecked.setTag(R.id.textchecked, false);

                    selectLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
                    selectLayout.setTag(R.id.layout_selete, false);

                    mScenePlayInfoList.getSceneList().get(i).setSelectedFlag(false);
                }else{
                    moodTypeImg.setImageResource(R.mipmap.icon_normal_mood_act);
                    moodTypeImg.setTag(R.id.list_view_mood_type, true);

                    textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
                    textchecked.setTag(R.id.textchecked, true);

                    selectLayout.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                    selectLayout.setTag(R.id.layout_selete, true);

                    mScenePlayInfoList.getSceneList().get(i).setSelectedFlag(true);
                }


               // drawMoodImage(oldImagViee, false);
              //  oldcheckedTextView = textchecked;
               // oldImagViee = moodTypeImg;

              //  textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
              //  textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
              //  drawMoodImage(moodTypeImg, true);
            }
        });


        try {
            if (mScenePlayInfoList.getSceneList().get(i).getSceneName().equals(mContext.getString(R.string.default_scene_name))) {
                //addScenePlayListHolder.mImgSub.setVisibility(View.GONE);
                //addScenePlayListHolder.mImgAdd.setVisibility(View.GONE);
                //addScenePlayListHolder.mTextHoldingTime.setVisibility(View.GONE);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.GONE);
            } else {
                //addScenePlayListHolder.mImgSub.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mImgAdd.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mTextHoldingTime.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.VISIBLE);
            }

            addScenePlayListHolder.mTextName.setText(mScenePlayInfoList.getSceneList().get(i).getSceneName());
            //addScenePlayListHolder.mTextHoldingTime.setText(String.format("%02d", mScenePlayInfoList.getSceneList().get(i).getIntHoldingTime()));

            if(!APService.getUserLevel()){
                //addScenePlayListHolder.mImgSub.setEnabled(false);
                //addScenePlayListHolder.mImgAdd.setEnabled(false);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("error", "play scene getview");
        }
        view.setTag(addScenePlayListHolder);
        return view;
    }


    public void drawMoodImage(ImageView imgView, boolean type) {

        if (imgView != null) {
            int moodType = (int) imgView.getTag(R.id.list_view_mood_type);
            if (type) {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_on);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_on);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                }
            } else {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_off);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_off);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                }
            }
        }
    }


    @Override
    public void onClick(View v) {
        TextView holdingTime = (TextView) v.getTag(R.id.id_item_list_view_add_sceneplay_hodingtime);
        int index = 0;
        switch (v.getId()) {
            case R.id.fragment_add_scene_play_image_add:
                if (Integer.parseInt(holdingTime.getText().toString()) > 9)
                    return;

                index = (int) v.getTag(R.id.id_play_scene_holding_time_add);
                mScenePlayInfoList.getSceneList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) + 1);
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) + 1));
                break;

            case R.id.fragment_add_scene_play_image_sub:
                index = (int) v.getTag(R.id.id_play_scene_holding_time_sub);
                mScenePlayInfoList.getSceneList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) - 1);
                if (Integer.parseInt(holdingTime.getText().toString()) == 1)
                    return;
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) - 1));
                break;
            case R.id.item_list_fragment_add_scene_play_image_delete:

                int pos = (int) v.getTag(R.id.id_item_list_view_add_sceneplay_delete_position);
                mDeletePosition = pos;
                FragmentManager manager = ((Activity) mContext).getFragmentManager();
                DialogDeleteConfirm delete = new DialogDeleteConfirm();
                delete.setListener(mListner );
                delete.show(manager, "delete");
                return;
        }
    }
    public int getDeletPosition(){
        return mDeletePosition;
    }
    public void updateSceneList(ScenePlayInfo mScenePlay) {
        this.mScenePlayInfoList = mScenePlay;
    }

    public void setScenePlayInfo(ScenePlayInfo _scenePlay) {
        this.mScenePlayInfoList = _scenePlay;
    }

  /*  public int getDeletePosition() {

    }*/


    static class AddScenePlayListHolder {
        private TextView mTextHoldingTime;
        private TextView mTextName;
        private ImageView mImgAdd;
        private ImageView mImgSub;
        private ImageView mImgDelete;
    }
}
