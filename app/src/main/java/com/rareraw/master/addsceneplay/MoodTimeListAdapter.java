package com.rareraw.master.addsceneplay;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.ScenePlayInfo;

/**
 * Created by Lch on 2016-12-26.
 */

public class MoodTimeListAdapter extends BaseAdapter implements View.OnClickListener {

    private LayoutInflater mLayoutInflater;
    private ScenePlayInfo mScenePlayInfoList;
    private OnActionListener mListner;
    private Context mContext;
    private int mDeletePosition;
    public MoodTimeListAdapter(Context context, ScenePlayInfo _sceneplaylist, OnActionListener listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mScenePlayInfoList = _sceneplaylist;
        this.mScenePlayInfoList.setSelectedMoodList(_sceneplaylist);
        mListner = listener;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mScenePlayInfoList.getSelectedMoodList().size();
    }

    @Override
    public Object getItem(int i) {
        return mScenePlayInfoList.getSelectedMoodList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    CheckedTextView oldcheckedTextView;
    ImageView oldImagViee;

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        MoodTimeHolder addScenePlayListHolder;
        if (view == null) {
            addScenePlayListHolder = new MoodTimeHolder();
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_mood_time, viewGroup, false);

            addScenePlayListHolder.mTextName = (TextView) view.findViewById(R.id.item_list_fragment_mood_time);
            addScenePlayListHolder.mTextHoldingTime = (TextView) view.findViewById(R.id.fragment_add_scene_play_text_value);

            addScenePlayListHolder.mImgAdd = (ImageView) view.findViewById(R.id.fragment_add_scene_play_image_add);
            addScenePlayListHolder.mImgAdd.setOnClickListener(this);

            addScenePlayListHolder.mImgAdd.setTag(R.id.id_item_list_view_add_sceneplay_hodingtime, addScenePlayListHolder.mTextHoldingTime);
            addScenePlayListHolder.mImgAdd.setTag(R.id.id_play_scene_holding_time_add, i);

            addScenePlayListHolder.mImgSub = (ImageView) view.findViewById(R.id.fragment_add_scene_play_image_sub);
            addScenePlayListHolder.mImgSub.setTag(R.id.id_item_list_view_add_sceneplay_hodingtime, addScenePlayListHolder.mTextHoldingTime);

            addScenePlayListHolder.mImgSub.setTag(R.id.id_play_scene_holding_time_sub, i);
            addScenePlayListHolder.mImgSub.setOnClickListener(this);

            addScenePlayListHolder.mImgDelete = (RelativeLayout) view.findViewById(R.id.layout_delete);
            addScenePlayListHolder.mImgDelete.setTag(R.id.id_item_list_view_add_sceneplay_delete_position, i);
            addScenePlayListHolder.mImgDelete.setOnClickListener(this);

        } else {
            addScenePlayListHolder = (MoodTimeHolder) view.getTag();
        }


        if(mScenePlayInfoList.getSelectedMoodList().get(i).isSelectedFlag()){

        }

        try {

            if (mScenePlayInfoList.getSelectedMoodList().get(i).getSceneName().equals(mContext.getString(R.string.default_scene_name))) {
                //addScenePlayListHolder.mImgSub.setVisibility(View.GONE);
                //addScenePlayListHolder.mImgAdd.setVisibility(View.GONE);
                //addScenePlayListHolder.mTextHoldingTime.setVisibility(View.GONE);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.GONE);
            } else {
                //addScenePlayListHolder.mImgSub.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mImgAdd.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mTextHoldingTime.setVisibility(View.VISIBLE);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.VISIBLE);
            }

            addScenePlayListHolder.mTextName.setText(mScenePlayInfoList.getSelectedMoodList().get(i).getSceneName());
            addScenePlayListHolder.mTextHoldingTime.setText(String.format("%02d", mScenePlayInfoList.getSelectedMoodList().get(i).getIntHoldingTime()));

            if(!APService.getUserLevel()){
                //addScenePlayListHolder.mImgSub.setEnabled(false);
                //addScenePlayListHolder.mImgAdd.setEnabled(false);
                //addScenePlayListHolder.mImgDelete.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("error", "play scene getview");
        }
        view.setTag(addScenePlayListHolder);
        return view;
    }


    @Override
    public void onClick(View v) {
        TextView holdingTime = (TextView) v.getTag(R.id.id_item_list_view_add_sceneplay_hodingtime);
        int index = 0;
        switch (v.getId()) {
            case R.id.fragment_add_scene_play_image_add:
                if (Integer.parseInt(holdingTime.getText().toString()) > 9)
                    return;

                index = (int) v.getTag(R.id.id_play_scene_holding_time_add);
                mScenePlayInfoList.getSelectedMoodList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) + 1);
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) + 1));
                break;

            case R.id.fragment_add_scene_play_image_sub:
                index = (int) v.getTag(R.id.id_play_scene_holding_time_sub);
                mScenePlayInfoList.getSelectedMoodList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) - 1);
                if (Integer.parseInt(holdingTime.getText().toString()) == 1)
                    return;
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) - 1));
                break;
            case R.id.layout_delete:

                int pos = (int) v.getTag(R.id.id_item_list_view_add_sceneplay_delete_position);
                mDeletePosition = pos;
                FragmentManager manager = ((Activity) mContext).getFragmentManager();
                DialogDeleteConfirm delete = new DialogDeleteConfirm();
                delete.setListener(mListner );
                delete.show(manager, "delete");
                return;
        }
    }
    public int getDeletPosition(){
        return mDeletePosition;
    }
    public void updateSceneList(ScenePlayInfo mScenePlay) {
        this.mScenePlayInfoList = mScenePlay;
    }

    public void setScenePlayInfo(ScenePlayInfo _scenePlay) {

        this.mScenePlayInfoList.setSelectedMoodList(_scenePlay);
        // this.mScenePlayInfoList = _scenePlay.;
       // this.mScenePlayInfoList = _scenePlay;
    }
//mScenePlayInfoList.getSelectedMoodList().size();
  /*  public int getDeletePosition() {

    }*/


    static class MoodTimeHolder {
        private TextView mTextHoldingTime;
        private TextView mTextName;
        private ImageView mImgAdd;
        private ImageView mImgSub;
        private RelativeLayout mImgDelete;
    }
}
