package com.rareraw.master.addsceneplay;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.addscene.MoodListAdapter;
import com.rareraw.master.addscene.MoodMultyListAdapter;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.ScenePlayInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Lch on 2016-12-26.
 */

public class AddScenePlayFragment extends BaseFragment implements View.OnClickListener {
    private AddScenePlayListAdapter mAddScenePlayListAdapter;
    private MoodTimeListAdapter mMoodTimeListAdapter;
    private RecyclerView mMoodTimeList2;
    private  ItemTouchHelperCallback itemTouchHelperCallbacknew;
    private MoodTimeListAdapter2 mMoodTimeListAdapter2;
    private ListView mListViewSceneList, mMoodTimeList;
    private ImageView mBtnSave, mBtnIConType;
    private RelativeLayout mBtnAddScene;
    private CheckBox mCheckBoxHide, mCheckboxRepeat;
    private EditText mPlaySceneName;
    public boolean mSaveComplete;
    private TextView mTextPlayMoodName, mTextChangeMode ;
    private Context mContext;
    private ScenePlayInfo mScenePlay = new ScenePlayInfo();
    private int mScenenoForModify;
    private boolean MODIFY_FLAG = false; // false : new create, true : modify
    private int mSelectedSceneNo = -1;

    private MoodMultyListAdapter mAdapter;
    private ListView mMoodListView;
    private ArrayList<MoodListAdapter.moodInfomation> mMoodList = new ArrayList<>();


    private LinearLayout mLayStep1, mLayStep2, mLayStep3;
    private TextView mTextDesceiption, mTextSave;
    private RelativeLayout mPrevPage, mNextPage;
    public final int STEP1 = 0, STEP2 = 1, STEP3 = 2;
    public int NOW_PAGE = STEP1;

    private OnActionListener mOnSelectIconListener = new OnActionListener() {
        // Icon type ( day, season, place )
        // Icon index depends on the Type.
        @Override
        public void onActionListener(int _iconIndex, String _IconType) {
            mScenePlay.setIntIconType(_iconIndex);
            setUserIconType(_iconIndex);
            if (Config.DEBUG)
                Toast.makeText(getActivity(), "아이콘이 변경되었습니다.", Toast.LENGTH_SHORT).show();
        }
    };

    private OnActionListener mDeleteComfirmListener = new OnActionListener() {
        @Override
        public void onActionListener(int _deletePos, String tag) {
            try {

                mScenePlay.getSceneList().remove(mAddScenePlayListAdapter.getDeletPosition());
                mAddScenePlayListAdapter.setScenePlayInfo(mScenePlay);
                mAddScenePlayListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("error", "delete scene");
            }
        }
    };

    private OnActionListener mDeleteMoodTime = new OnActionListener() {

        @Override
        public void onActionListener(int _deletePos, String tag) {
            try {
                // mScenePlay.getSceneList().remove(mMoodTimeListAdapter.getDeletPosition());
                mScenePlay.getSceneList().get(mMoodTimeListAdapter.getDeletPosition()).setSelectedFlag(false);
                mMoodTimeListAdapter.setScenePlayInfo(mScenePlay);
                mMoodTimeListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("error", "delete scene");
            }
        }
    };

    private OnActionListener mDeleteMoodTime2 = new OnActionListener() {

        @Override
        public void onActionListener(int _deletePos, String tag) {
            try {

                mScenePlay.getSceneList().get(mMoodTimeListAdapter2.getDeletPosition()).setSelectedFlag(false);
                mMoodTimeListAdapter2.setScenePlayInfo(mScenePlay);
                mMoodTimeListAdapter2.notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("error", "delete scene");
            }
        }
    };


    private OnActionListener mOnActionListner = new OnActionListener() {
        @Override
        public void onActionListener(int _sceneNo, String _name) {
            mSelectedSceneNo = _sceneNo;
            deleteDefaultScene();
            SceneInfo sceneinfo = new SceneInfo();

            if (Config.DEBUG)
                getTestdata(_name);
            else {
                /*sceneinfo = APService.getSceneSingle(_sceneNo);
                if (sceneinfo != null) {
                    sceneinfo.setIntSceneNo(_sceneNo);
                    mScenePlay.getSceneList().add(sceneinfo);
                } else{*/
                sceneinfo = new SceneInfo();
                sceneinfo.setIntSceneNo(_sceneNo);
                sceneinfo.setSceneName(_name);
                mScenePlay.getSceneList().add(sceneinfo);
                //          }

            }
            // mAddScenePlayListAdapter.updateSceneList(mScenePlay);
            mAddScenePlayListAdapter.setScenePlayInfo(mScenePlay);
            //setListViewHeightBasedOnChildren(mListViewSceneList);
            mAddScenePlayListAdapter.notifyDataSetChanged();
        }
    };

    private void getTestdata(String _name) {
        SceneInfo defaultScene = new SceneInfo();
        defaultScene.setSceneName(_name);
        defaultScene.setIntHoldingTime(11);
        mScenePlay.getSceneList().add(defaultScene);
    }

    private ScenePlayInfo getTestData() {

        ScenePlayInfo scenePlay = new ScenePlayInfo();
        scenePlay.setScenePlayName("ScenePlay" + CmdHelper.getEmptySceneNoInt());
        scenePlay.setIntScenePlayNo(CmdHelper.getEmptySceneNoInt());
        scenePlay.setIntIconType(Config.USER_ICON_AFTERNOON);
        scenePlay.setIntIconType(Cmd.NORMAL_SCENE);
        scenePlay.setBoolHideFlag(false);
        scenePlay.setIntIndexNo(CmdHelper.getEmptyIndexNoInt());
        scenePlay.setBoolRepeatFlag(false);

        ArrayList<SceneInfo> defaultSceneArray = new ArrayList<>();

        SceneInfo defaultScene = new SceneInfo();
        defaultScene.setSceneName("my scene1");
        defaultScene.setIntHoldingTime(1);

        SceneInfo defaultScene1 = new SceneInfo();
        defaultScene1.setSceneName("my scene2");
        defaultScene1.setIntHoldingTime(2);

        SceneInfo defaultScene2 = new SceneInfo();
        defaultScene2.setSceneName("my scene3");
        defaultScene2.setIntHoldingTime(3);


        defaultSceneArray.add(defaultScene);
        defaultSceneArray.add(defaultScene1);
        defaultSceneArray.add(defaultScene2);
        scenePlay.setSceneList(defaultSceneArray);
        return scenePlay;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void deleteDefaultScene() {
        for (int i = 0; i < mScenePlay.getSceneList().size(); i++) {
            if (mScenePlay.getSceneList().get(i).getSceneName().equals(getString(R.string.default_scene_name)))
                mScenePlay.getSceneList().remove(i);
        }
    }

    public void setModiftStatus(int _sceneNo) {
        if (_sceneNo == -1)
            MODIFY_FLAG = false;
        else {
            MODIFY_FLAG = true;
            mScenenoForModify = _sceneNo;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity().getApplicationContext();
        // getScenePlayInfo();
        mAddScenePlayListAdapter = new AddScenePlayListAdapter(getContext(), mScenePlay, mDeleteComfirmListener);
        NOW_PAGE = STEP1;
        mSaveComplete = false;
    }


    private void initMoodList(View view) {

        mMoodListView = (ListView) view.findViewById(R.id.listview_mood_list);
        mMoodList = APService.getMoodList();
        mAdapter = new MoodMultyListAdapter(mContext, mMoodList);
        mMoodListView.setAdapter(mAdapter);
        mAdapter.setDefaultCheck();
        // setListViewHeightBasedOnChildren(mListViewSceneList);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getScenePlayInfo() {
        if (MODIFY_FLAG)
            getExistingSceneInfo();
        else
            createSceneInfo();
    }

    public void updateUi() {
        getScenePlayInfo();
        if (mScenePlay == null)
            return;
    }

    private void updateSceneInfo() {

        setUserIconType(mScenePlay.getIntIconType());
        mCheckBoxHide.setChecked(mScenePlay.getBoolHideFlag());
        mCheckboxRepeat.setChecked(mScenePlay.getBoolRepeatFlag());
        mPlaySceneName.setText(removeNullChar(mScenePlay.getScenePlayName()));
        mTextPlayMoodName.setText(removeNullChar(mScenePlay.getScenePlayName()));
    }

    private void createSceneInfo() {
        ScenePlayInfo scenePlay = new ScenePlayInfo();
        try {
            // scenePlay.setScenePlayName("ScenePlay" + CmdHelper.getEmptySceneNoInt());
            scenePlay.setScenePlayName("");
            scenePlay.setIntScenePlayNo(CmdHelper.getEmptySceneNoInt());
            scenePlay.setIntIconType(Config.USER_DEFAULT_ICON);
            scenePlay.setBoolHideFlag(false);
            scenePlay.setIntIndexNo(CmdHelper.getEmptyIndexNoInt());
            scenePlay.setBoolRepeatFlag(true);

            // ArrayList<SceneInfo> defaultSceneArray = APService.getApSceneList();
            // defaultSceneArray.add(defaultScene);
            //scenePlay.setSceneList(defaultSceneArray);


        } catch (Exception e) {
            Log.e("eror", "add sceneplay");
        }

        mScenePlay = scenePlay;
        updateSceneInfo();
        // mListViewSceneList
        mAddScenePlayListAdapter.setScenePlayInfo(mScenePlay);

        mPlaySceneName.requestFocus();
        mPlaySceneName.setSelection(0);
        //setListViewHeightBasedOnChildren(mListViewSceneList);
        //mAddScenePlayListAdapter.notifyDataSetChanged();

    }

    private void getExistingSceneInfo() {
        ((ContainActivity) getActivity()).createDialog(1300);
        ScenePlayInfo scenePlay = new ScenePlayInfo();
        try {
            if (Config.DEBUG) {
                mScenePlay = getTestData();
                updateSceneInfo();
                mAddScenePlayListAdapter.setScenePlayInfo(mScenePlay);
                //setListViewHeightBasedOnChildren(mListViewSceneList);
                mAddScenePlayListAdapter.notifyDataSetChanged();
            } else {
                APService.getScenePlaySingle(mScenenoForModify, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        ScenePlayInfo sceneInfo = new ScenePlayInfo();
                        try {
                            sceneInfo = (ScenePlayInfo) retValue;
                            if (sceneInfo == null)
                                return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            return;
                        }
                        sceneInfo.setIntScenePlayNo(mScenenoForModify);
                        sceneInfo.setByteScenePlayNo(CmdHelper.intToByteArray(mScenenoForModify));
                        boolean oldLevel = APService.getUserLevel();
                        APService.setUserLevel(true);
                        for (int i = 0; i < sceneInfo.getSceneList().size(); i++) {
                            sceneInfo.getSceneList().get(i).setSceneName(CmdHelper.getSceneName(sceneInfo.getSceneList().get(i).getByteSceneNo()));
                            sceneInfo.getSceneList().get(i).setIntHoldingTime((int) sceneInfo.getSceneList().get(i).getIntHoldingTime());
                        }
                        APService.setUserLevel(oldLevel);

                        mScenePlay = sceneInfo;
                        setSelectSceneList(mScenePlay);
                        updateSceneInfo();
                        mAddScenePlayListAdapter.setScenePlayInfo(sceneInfo);
                        // setListViewHeightBasedOnChildren(mListViewSceneList);
                        mAddScenePlayListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();
                    }
                });
            }
        } catch (Exception e) {
            Log.e("eror", "add sceneplay");
            Logout();
        }
    }

    private void setSelectSceneList(ScenePlayInfo _scenePlay) {

        ArrayList<SceneInfo> sceneList = new ArrayList<>();
        for (int i = 0; i < _scenePlay.getSceneList().size(); i++) {

            if (_scenePlay.getSceneList().get(i).getSceneName() == null && _scenePlay.getSceneList().get(i).getIntSceneNo() == 0)
                continue;
            else {
                sceneList.add(_scenePlay.getSceneList().get(i));
                _scenePlay.getSceneList().get(i).setSelectedFlag(true);
            }
        }
        mScenePlay.setSceneList(sceneList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_scene_play, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListViewSceneList = (ListView) view.findViewById(R.id.listview_mood_list);
        mMoodTimeList = (ListView) view.findViewById(R.id.listview_mood_time_list);
        mMoodTimeList2 = (RecyclerView) view.findViewById(R.id.listview_mood_time_list2);
        //mBtnIConType = (ImageView) view.findViewById(R.id.fragment_add_scene_play_image_user_icon);
        mBtnAddScene = (RelativeLayout) view.findViewById(R.id.fragment_add_scene_play_layout_add_scene);

        mCheckBoxHide = (CheckBox) view.findViewById(R.id.fragment_add_scene_play_image_hide);
        mCheckboxRepeat = (CheckBox) view.findViewById(R.id.fragment_add_scene_play_image_repeat);
        mPlaySceneName = (EditText) view.findViewById(R.id.fragment_add_scene_play_edit_scene_name);

        mTextPlayMoodName = (TextView) view.findViewById(R.id.text_moodname);
        mTextChangeMode = (TextView) view.findViewById(R.id.btn_index_change) ;
        mTextChangeMode.setTag(R.id.btn_index_change, false);
        mTextChangeMode.setOnClickListener(this);
        mListViewSceneList.setAdapter(mAddScenePlayListAdapter);

        updateUserLevelUi();
        mBtnAddScene.setOnClickListener(this);
        //mBtnIConType.setOnClickListener(this);

        mTextDesceiption = (TextView) view.findViewById(R.id.text_description);
        mTextSave = (TextView) view.findViewById(R.id.text_play_save);

        mLayStep1 = (LinearLayout) view.findViewById(R.id.layout_step1);
        mLayStep2 = (LinearLayout) view.findViewById(R.id.layout_step2);
        mLayStep3 = (LinearLayout) view.findViewById(R.id.layout_step3);

        mPrevPage = (RelativeLayout) view.findViewById(R.id.prev_page);
        mPrevPage.setOnClickListener(this);
        mNextPage = (RelativeLayout) view.findViewById(R.id.next_page);
        mNextPage.setOnClickListener(this);

        // initMoodList(view);

       /* if (!APService.getUserLevel() && MODIFY_FLAG) {
            mBtnAddScene.setVisibility(View.GONE);
            //mBtnIConType.setEnabled(false);
            mCheckBoxHide.setEnabled(false);
            mCheckboxRepeat.setEnabled(false);
            mPlaySceneName.setEnabled(false);
        }*/
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    private void updateUserLevelUi() {
     /*   if (APService.getUserLevel()) {

        } else {*/
        setUserIconType(mScenePlay.getIntIconType());
        mCheckBoxHide.setChecked(mScenePlay.getBoolHideFlag());
        //mCheckBoxHide.setFocusable(false);
        mCheckboxRepeat.setChecked(mScenePlay.getBoolRepeatFlag());
        // mCheckboxRepeat.setFocusable(false);
        mPlaySceneName.setText(mScenePlay.getScenePlayName());
        mTextPlayMoodName.setText(mScenePlay.getScenePlayName());
        //mPlaySceneName.setFocusable(false);
        //mBtnAddScene.setFocusable(false);
        //mBtnIConType.setFocusable(false);
        //  }
    }

    private void setUserIconType(int iconType) {
        try {
            // mBtnIConType.setImageResource(getIconId(iconType));
        } catch (Exception e) {
            Log.e("error", "draw user icon");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btn_index_change:
                boolean flag = (boolean) v.getTag(R.id.btn_index_change);
                TextView text = (TextView) v;
                if (!flag) {
                    itemTouchHelperCallbacknew.setFlag(true);
                    text.setText("순서변경 완료");
                    mMoodTimeListAdapter2.showChangeMode();
                    text.setTag(R.id.btn_index_change, true);
                } else {
                    itemTouchHelperCallbacknew.setFlag(false);
                    text.setText("순서변경");
                    mMoodTimeListAdapter2.hideChangeMode();
                    text.setTag(R.id.btn_index_change, false);
                }
                break;
           /* case R.id.fragment_add_scene_play_image_user_icon:
                DialogSelectUserIcon dialogSelectUserIcon = new DialogSelectUserIcon();
                dialogSelectUserIcon.setFragmentManager(getActivity().getSupportFragmentManager());
                dialogSelectUserIcon.setLisetener(mOnSelectIconListener);
                dialogSelectUserIcon.show(getFragmentManager(), "dialogSelectUserIcon");
                break;*/

            case R.id.fragment_add_scene_play_layout_add_scene:
                DialogSelectScene dialogSelectScene = new DialogSelectScene();
                dialogSelectScene.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
                dialogSelectScene.setListener(mOnActionListner);
                dialogSelectScene.show(getActivity().getFragmentManager(), "dialogSelectScene");
                break;

            case R.id.prev_page:
                if (NOW_PAGE == STEP1)
                    getActivity().onBackPressed();
                else if (NOW_PAGE == STEP2)
                    showNowPage(STEP1);
                else if (NOW_PAGE == STEP3)
                    showNowPage(STEP2);

                break;
            case R.id.next_page:
                if (NOW_PAGE == STEP1) {
                    String name = mPlaySceneName.getText().toString();
                    if (name == null || name.equals("")) {
                        MyToast.getInstance(getActivity()).showToast("플레이무드 이름이 너무짧습니다.\n다시 시도해주세요.", com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }

                    if (!checkValidation(name))
                        return;

                    showNowPage(STEP2);
                } else if (NOW_PAGE == STEP2) {
                    mTextPlayMoodName.setText(mPlaySceneName.getText().toString());

                    int size = 0;
                    for (int i = 0; i < mScenePlay.getSceneList().size(); i++) {
                        if (mScenePlay.getSceneList().get(i).isSelectedFlag())
                            size = ++size;
                    }

                    if (size < 2) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_plz_select_morethan_scene), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }

                    showNowPage(STEP3);
                    settingMoodHoldingTIme();
                    settingMoodHoldingTIme2();
                } else if (NOW_PAGE == STEP3)
                    saveScenePlayInfo();
                break;
        }
    }

    private void settingMoodHoldingTIme() {
        mMoodTimeListAdapter = new MoodTimeListAdapter(getContext(), mScenePlay, mDeleteMoodTime);
        mMoodTimeList.setAdapter(mMoodTimeListAdapter);
        mMoodTimeListAdapter.setScenePlayInfo(mScenePlay);
        mMoodTimeListAdapter.notifyDataSetChanged();
    }


    private void settingMoodHoldingTIme2() {

        mMoodTimeList2.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mMoodTimeList2.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        mMoodTimeList2.setHasFixedSize(true);
        mMoodTimeList2.setItemAnimator(new DefaultItemAnimator());
        mMoodTimeList2.setNestedScrollingEnabled(true);
        mMoodTimeListAdapter2 = new MoodTimeListAdapter2(getContext(), mScenePlay, mDeleteMoodTime2) ;
        mMoodTimeList2.setAdapter(mMoodTimeListAdapter2);

        itemTouchHelperCallbacknew = new  ItemTouchHelperCallback(mMoodTimeListAdapter2);
        ItemTouchHelper helper = new ItemTouchHelper(itemTouchHelperCallbacknew);
        //RecyclerView에 ItemTouchHelper 붙이기
        helper.attachToRecyclerView(mMoodTimeList2);
    }


    public void forceNextPage() {
        mNextPage.performClick();
    }

    public void forcePrevPage() {
        mPrevPage.performClick();
    }

    public void showNowPage(int nowPage) {
        NOW_PAGE = nowPage;
        if (nowPage == STEP1) {
            mLayStep1.setVisibility(View.VISIBLE);
            mLayStep2.setVisibility(View.GONE);
            mLayStep3.setVisibility(View.GONE);
            mTextDesceiption.setText("플레이 무드는 기본 무드를 연결시켜 연속적으로 재생시킬 수 있습니다.");
            mTextSave.setText("다음");

            mPlaySceneName.requestFocus();
            //InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } else if (nowPage == STEP2) {
            mLayStep1.setVisibility(View.GONE);
            mLayStep2.setVisibility(View.VISIBLE);
            mLayStep3.setVisibility(View.GONE);
            mTextDesceiption.setText("플레이 무드는 기본 무드를 연결시켜 연속적으로 재생시킬 수 있습니다.");
            mTextSave.setText("다음");

            if (!MODIFY_FLAG) {
                resetSelectItem();
                // 01049147330 노말씬만되도록 수정
                // ArrayList<SceneInfo> sceneArray = APService.getApSceneList();
                ArrayList<SceneInfo> sceneArray = APService.getApNormalSceneList();
                mScenePlay.setSceneList(sceneArray);

                mAddScenePlayListAdapter.setScenePlayInfo(mScenePlay);
                //setListViewHeightBasedOnChildren(mListViewSceneList);
                mAddScenePlayListAdapter.notifyDataSetChanged();
            }
        } else if (nowPage == STEP3) {
            mLayStep1.setVisibility(View.GONE);
            mLayStep2.setVisibility(View.GONE);
            mLayStep3.setVisibility(View.VISIBLE);
            mTextDesceiption.setText("무드의 개별 재생시간을 상세조정하실 수 있습니다.");
            mTextSave.setText("저장");

        }
    }

    private void resetSelectItem() {
        for (int i = 0; i < mScenePlay.getSceneList().size(); i++) {
            mScenePlay.getSceneList().get(i).setSelectedFlag(false);
        }
    }

    private boolean checkValidation() {

        if (mScenePlay.getSceneList().size() < 2) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_plz_select_morethan_scene), com.rareraw.master.Common.Toast.LENGTH_SHORT);
            return false;
        }
        return true;
    }

    public void saveScenePlayInfo() {

        if (!checkValidation())
            return;

        String name = mPlaySceneName.getText().toString();
        name = name.trim();
        if (!checkValidation(name))
            return;

        mScenePlay.setScenePlayName(name);
        mScenePlay.setBoolHideFlag(mCheckBoxHide.isChecked());
        // mScenePlay.setBoolRepeatFlag(mCheckboxRepeat.isChecked());
        mScenePlay.setBoolRepeatFlag(true);
        ByteBuffer buffer = mScenePlay.makeBuffer();
        if (buffer == null) {
            Log.e("register secenePlay", "error(null)");
            return;
        }
        APService.registerScenePlay(buffer, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                Log.e("register seceneplay", "ok");
                if (Config.DEBUG) {
                    Toast.makeText(mContext, "씬 플레이가 추가되었습니다.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(mContext, "씬번호 : " + mScenePlay.getIntSceneNo(), Toast.LENGTH_SHORT).show();
                }
                mSaveComplete = true;
                getActivity().onBackPressed();

            }

            @Override
            public void onError(int errorCode) {
                Toast.makeText(mContext, "씬 플레이가 추가에 실패하였습니다.\n다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                Log.e("register seceneplay", "error");
                mSaveComplete = false;
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }
}
