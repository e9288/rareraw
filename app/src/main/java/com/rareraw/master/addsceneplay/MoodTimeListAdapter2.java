package com.rareraw.master.addsceneplay;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.ScenePlayInfo;

import java.util.ArrayList;
import java.util.List;

public class MoodTimeListAdapter2 extends RecyclerView.Adapter<MoodTimeListAdapter2.ViewHolder> implements View.OnClickListener, ItemTouchHelperListener {

    private OnActionListener mListner;
    private Context mContext;
    private int mDeletePosition;
    private ScenePlayInfo mScenePlayInfoList;
    public boolean mEdidtMode = false;

    @Override
    public boolean onItemMove(int from_position, int to_position) {
        //이동할 객체 저장
        SceneInfo mood = mScenePlayInfoList.getSceneList().get(from_position);
        //이동할 객체 삭제
        mScenePlayInfoList.getSceneList().remove(from_position); //이동하고 싶은 position에 추가
        mScenePlayInfoList.getSceneList().add(to_position, mood);
        //Adapter에 데이터 이동알림
        notifyItemMoved(from_position,to_position);
        return false;
    }

    @Override
    public void onItemSwipe(int position) {

    }


    // 아이템 뷰를 저장하는 뷰홀더 클래스.
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextHoldingTime;
        public TextView mTextName;
        public ImageView mImgAdd;
        public ImageView mImgSub;
        public RelativeLayout mImgDelete;
        public RelativeLayout mChangeBtn;
        // public TextView mIndexChange;

        ViewHolder(View itemView) {
            super(itemView);
            mChangeBtn = (RelativeLayout) itemView.findViewById(R.id.layout_change);

            //mIndexChange = (TextView) itemView.findViewById(R.id.btn_index_change);

            mTextHoldingTime = (TextView) itemView.findViewById(R.id.fragment_add_scene_play_text_value);
            mTextName = (TextView) itemView.findViewById(R.id.item_list_fragment_mood_time);
            mImgAdd = (ImageView) itemView.findViewById(R.id.fragment_add_scene_play_image_add);
            mImgSub = (ImageView) itemView.findViewById(R.id.fragment_add_scene_play_image_sub);
            mImgDelete = (RelativeLayout) itemView.findViewById(R.id.layout_delete);
        }
    }

    // 생성자에서 데이터 리스트 객체를 전달받음.
    public MoodTimeListAdapter2(Context context, ScenePlayInfo _sceneplaylist, OnActionListener listener ){
        this.mScenePlayInfoList = _sceneplaylist;
        this.mScenePlayInfoList.setSelectedMoodList(_sceneplaylist);
        mListner = listener;
        mContext = context;
    }

    // onCreateViewHolder() - 아이템 뷰를 위한 뷰홀더 객체 생성하여 리턴.
    @Override
    public MoodTimeListAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.item_list_fragment_mood_time, parent, false);
        MoodTimeListAdapter2.ViewHolder vh = new MoodTimeListAdapter2.ViewHolder(view);

        return vh;
    }

    public void showChangeMode() {
        mEdidtMode = true;
        notifyDataSetChanged();
    }

    public void hideChangeMode() {
        mEdidtMode = false;
        notifyDataSetChanged();
    }

    // onBindViewHolder() - position에 해당하는 데이터를 뷰홀더의 아이템뷰에 표시.
    @Override
    public void onBindViewHolder(MoodTimeListAdapter2.ViewHolder holder, int position) {


        if (mEdidtMode) {
            holder.mChangeBtn.setVisibility(View.VISIBLE);

        } else {
            holder.mChangeBtn.setVisibility(View.GONE);

        }

        holder.mChangeBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN  || MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_UP ){

                }

                return false;
            }
        });
        /*holder.mChangeBtn.setOnClickListener(this);
        holder.mChangeBtn.setTag(R.id.btn_index_change, false);
        holder.mChangeBtn.setTag(R.id.layout_change, holder.mChangeBtn);
*/

        holder.mImgAdd.setOnClickListener(this);
        holder.mImgAdd.setTag(R.id.id_item_list_view_add_sceneplay_hodingtime, holder.mTextHoldingTime);
        holder.mImgAdd.setTag(R.id.id_play_scene_holding_time_add, position);

        holder.mImgSub.setTag(R.id.id_item_list_view_add_sceneplay_hodingtime, holder.mTextHoldingTime);

        holder.mImgSub.setTag(R.id.id_play_scene_holding_time_sub, position);
        holder.mImgSub.setOnClickListener(this);

        holder.mImgDelete.setTag(R.id.id_item_list_view_add_sceneplay_delete_position, position);
        holder.mImgDelete.setOnClickListener(this);

        try {
            holder.mTextName.setText(mScenePlayInfoList.getSelectedMoodList().get(position).getSceneName());
            holder.mTextHoldingTime.setText(String.format("%02d", mScenePlayInfoList.getSelectedMoodList().get(position).getIntHoldingTime()));
        } catch (Exception e) {
            Log.e("error", "play scene getview");
        }
    }


    @Override
    public void onClick(View v) {
        TextView holdingTime = (TextView) v.getTag(R.id.id_item_list_view_add_sceneplay_hodingtime);
        int index = 0;
        switch (v.getId()) {
            case R.id.layout_change:

            /*    RelativeLayout changeLayout = (RelativeLayout) v.getTag(R.id.layout_change);
                boolean flag = (boolean) v.getTag(R.id.btn_index_change);
                TextView text = (TextView) v;
                if (flag) {
                    text.setText("순서변경 완료");
                    changeLayout.setVisibility(View.VISIBLE);
                } else {
                    text.setText("순서변경");
                    changeLayout.setVisibility(View.GONE);
                }*/

                break;

            case R.id.fragment_add_scene_play_image_add:
                if (Integer.parseInt(holdingTime.getText().toString()) > 9)
                    return;

                index = (int) v.getTag(R.id.id_play_scene_holding_time_add);
                mScenePlayInfoList.getSelectedMoodList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) + 1);
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) + 1));
                break;

            case R.id.fragment_add_scene_play_image_sub:
                index = (int) v.getTag(R.id.id_play_scene_holding_time_sub);
                mScenePlayInfoList.getSelectedMoodList().get(index).setIntHoldingTime(Integer.parseInt(holdingTime.getText().toString()) - 1);
                if (Integer.parseInt(holdingTime.getText().toString()) == 1)
                    return;
                holdingTime.setText(String.format("%02d", Integer.parseInt(holdingTime.getText().toString()) - 1));
                break;
            case R.id.layout_delete:

                int pos = (int) v.getTag(R.id.id_item_list_view_add_sceneplay_delete_position);
                mDeletePosition = pos;
                FragmentManager manager = ((Activity) mContext).getFragmentManager();
                DialogDeleteConfirm delete = new DialogDeleteConfirm();
                delete.setListener(mListner);
                delete.show(manager, "delete");
                return;
        }
    }

    public int getDeletPosition() {
        return mDeletePosition;
    }

    public void updateSceneList(ScenePlayInfo mScenePlay) {
        this.mScenePlayInfoList = mScenePlay;
    }

    public void setScenePlayInfo(ScenePlayInfo _scenePlay) {

        this.mScenePlayInfoList.setSelectedMoodList(_scenePlay);
        // this.mScenePlayInfoList = _scenePlay.;
        // this.mScenePlayInfoList = _scenePlay;
    }

    // getItemCount() - 전체 데이터 갯수 리턴.
    @Override
    public int getItemCount() {
        return mScenePlayInfoList.getSceneList().size();
    }
}