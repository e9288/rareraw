package com.rareraw.master.Preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-01-22.
 */
public class Prf {
    public final static String WIDTH_SIZE_FILE = "WIDTH_SIZE_FILE";
    public final static String WIDTH_SIZE_TAG= "WIDTH_SIZE_TAG";

    public final static String TUTORIAL_OK_FILE = "TUTORIAL_OK_FILE";
    public final static String TUTORIAL_OK_TAG= "TUTORIAL_OK_TAG";

    public final static String LOGIN_PASSWORD_FILE = "LOGIN_PASSWORD_FILE";
    public final static String LOGIN_PASSWORD_TAG = "LOGIN_PASSWORD_TAG";

    public final static String REMEMBER_PW_FILE = "REMEMBER_PW_FILE";
    public final static String REMEMBER_PW_TAG = "REMEMBER_PW_TAG";

    public final static String WIRELESS_MODE_FILE = "WIRELESS_MODE_FILE";
    public final static String WIRELESS_MODE_TAG= "WIRELESS_MODE_TAG";

    public final static String DMX_USE_FILE= "DMX_USE_FILE";
    public final static String DMX_USE_TAG= "DMX_USE_TAG";
    public static ArrayList<String> getStringArrayPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String json = prefs.getString(key, null);
        ArrayList<String> urls = new ArrayList<String>();
        if (json != null) {
            try {
                JSONArray a = new JSONArray(json);
                for (int i = 0; i < a.length(); i++) {
                    String url = a.optString(i);
                    urls.add(url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static void writeToSharedPreferences(Context mContext, String file, String tag, String value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(tag, value);
        editor.apply();
    }

    public static void writeToSharedPreferences(Context mContext, String file, String tag, boolean value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(tag, value);
        editor.apply();
    }

    public static void writeToSharedPreferences(Context mContext, String file, String tag, int value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(tag, value);
        editor.apply();
    }

    public static String readToSharedPreferences(Context mContext, String file, String tag, String err) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        return sharedPreferences.getString(tag, err);
    }

    public static int readToSharedPreferences(Context mContext, String file, String tag, int err) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        return sharedPreferences.getInt(tag, err);
    }

    public static boolean readToSharedPreferences(Context mContext, String file, String tag, boolean value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE);
        return sharedPreferences.getBoolean(tag, value);
    }
    public static void deleteToSharedPreferences(Context mContext, String file) {
        SharedPreferences.Editor editor = mContext.getSharedPreferences(file, mContext.MODE_PRIVATE).edit();
        editor.clear();
        editor.commit();

    }

}
