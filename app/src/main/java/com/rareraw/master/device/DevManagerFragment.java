package com.rareraw.master.device;

import android.Manifest;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.DialogResetDevice;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnRenameListener;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.Preference.Prf;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static com.neoromax.feelmaster.R.color.devicelist_selected_color;

public class DevManagerFragment extends BaseFragment implements OnDeviceStatusChangedListener {

    ArrayList<DeviceInfo> mConnectedDevice = new ArrayList<DeviceInfo>();
    ArrayList<DeviceInfo> mNotConnectedDevice = new ArrayList<DeviceInfo>();
    DeviceListAdapter mConnectedDeviceListAdapter, mNotConnectedDeviceListAdapter;
    private static Context mContext;
    public final static int CONNECTED_ITEM = 0;
    public final static int NOT_CONNECTED_ITEM = 1;
    private ImageView mRefresh1, mRefresh2;
    private ListView cntListView, notCntListView;
    private EditText mEditSetFeelmasterName;
    private EditText mApPassword, mApPasswordConfirm, mPassword, mPasswordConfirm;
    private RelativeLayout mResetFeelmaster, mWifiPwChange, mLoginPwChange;
    private Handler mHandler = new Handler(Looper.getMainLooper());

    public OnRenameListener mListener = new OnRenameListener() {
        @Override
        public void onActionRename(byte[] nValue, String sValue) {
            if (sValue.equals("loading")) {
                showDialog();
            } else{
                MyToast.getInstance(getActivity()).showToast(sValue, Toast.LENGTH_SHORT);
                updateUi();
            }
        }

        @Override
        public void onCompleteRename(int nValue, String sValue) {
            updateUi();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getTestData();
        mContext = getActivity();

        // 2step, setting data in adapter
        mConnectedDeviceListAdapter = new DeviceListAdapter(getActivity(), mConnectedDevice, CONNECTED_ITEM, this);
        mNotConnectedDeviceListAdapter = new DeviceListAdapter(getActivity(), mNotConnectedDevice, NOT_CONNECTED_ITEM, this);
        ((ContainActivity) getActivity()).createDialog(700);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    public String getWiFiSSID(Context mContext) {
        WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = manager.getConnectionInfo();
        String sSSID = wifiInfo.getSSID();
        return sSSID;
    }

    private void updateUi() {
        showDialog();
        getRegDeviceInfo();
    }

    public void showDialog() {
        ((ContainActivity) getActivity()).createDialog();
    }

    public void hideDialog() {
        ((ContainActivity) getActivity()).hideDialog();
    }

    private void getRegDeviceInfo() {

        if (Config.DEBUG) {
            getTestData();
        } else {
            // 1 step, load existing device list (2 type)
            APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    getRegistedSwitchInfo();
                }

                @Override
                public void onError(int errorCode) {
                    hideDialog();
                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    hideDialog();
                    Logout();
                }
            });

            APService.getUnRegistedDeviceInfo(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    mNotConnectedDevice.clear();
                    mNotConnectedDevice = APService.getApUnRegistedDevList();
                    mNotConnectedDeviceListAdapter.setNewDeviceList(mNotConnectedDevice);
                    notCntListView.setAdapter(mNotConnectedDeviceListAdapter);
                    setListViewHeightBasedOnChildren(notCntListView);
                    mNotConnectedDeviceListAdapter.notifyDataSetChanged();
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Logout();
                }
            });
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void getRegistedSwitchInfo() {
        try {
            APService.getSwitchInfo(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {

                    mConnectedDevice.clear();
                    mConnectedDevice = APService.getApRegistedDevList();

                    mConnectedDeviceListAdapter.setNewDeviceList(mConnectedDevice);
                    cntListView.setAdapter(mConnectedDeviceListAdapter);
                    setListViewHeightBasedOnChildren(cntListView);
                    mConnectedDeviceListAdapter.notifyDataSetChanged();
                    hideDialog();
                }

                @Override
                public void onError(int errorCode) {
                    Log.e("onError", "");
                    hideDialog();
                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    hideDialog();
                    Log.e("onErrorConnectionId", "");
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getTestData() {
        if (Config.DEBUG) {
            DeviceInfo info1 = new DeviceInfo(1);
            info1.setIntDevType(0);
            mConnectedDevice.add(info1);
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));
            mConnectedDevice.add(new DeviceInfo(1));

            DeviceInfo info2 = new DeviceInfo(1);
            info2.setIntDevType(0);
            mNotConnectedDevice.add(info2);
            DeviceInfo info3 = new DeviceInfo(1);
            info3.setIntDevType(1);
            mNotConnectedDevice.add(info3);

            DeviceInfo info4 = new DeviceInfo(1);
            info4.setIntDevType(1);
            mNotConnectedDevice.add(info4);
            DeviceInfo info5 = new DeviceInfo(1);
            info4.setIntDevType(1);
            mNotConnectedDevice.add(info5);
            DeviceInfo info6 = new DeviceInfo(1);
            info4.setIntDevType(1);
            mNotConnectedDevice.add(info6);
            DeviceInfo info7 = new DeviceInfo(1);
            info4.setIntDevType(1);
            mNotConnectedDevice.add(info7);

            mConnectedDeviceListAdapter.setNewDeviceList(mConnectedDevice);
            cntListView.setAdapter(mConnectedDeviceListAdapter);
            setListViewHeightBasedOnChildren(cntListView);
            mConnectedDeviceListAdapter.notifyDataSetChanged();

            mNotConnectedDeviceListAdapter.setNewDeviceList(mNotConnectedDevice);
            notCntListView.setAdapter(mNotConnectedDeviceListAdapter);
            setListViewHeightBasedOnChildren(notCntListView);
            mNotConnectedDeviceListAdapter.notifyDataSetChanged();

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_device_manager, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cntListView = (ListView) view.findViewById(R.id.fragment_device_manager_list_connect_devices);
        cntListView.setAdapter(mConnectedDeviceListAdapter);

        notCntListView = (ListView) view.findViewById(R.id.fragment_device_manager_list_not_connect_devices);
        notCntListView.setAdapter(mNotConnectedDeviceListAdapter);

        mEditSetFeelmasterName = (EditText) view.findViewById(R.id.apmode_ap_name);
        mApPassword = (EditText) view.findViewById(R.id.ap_mode_password);
        mApPasswordConfirm = (EditText) view.findViewById(R.id.ap_mode_password_confirm);

        mPassword = (EditText) view.findViewById(R.id.change_new_password1);
        mPasswordConfirm = (EditText) view.findViewById(R.id.change_new_password2);

        mRefresh1 = (ImageView) view.findViewById(R.id.btn_refresh1);
        mRefresh1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateUi();
            }
        });
        mRefresh2 = (ImageView) view.findViewById(R.id.btn_refresh2);
        mRefresh2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUi();
            }
        });

        mWifiPwChange = (RelativeLayout) view.findViewById(R.id.btn_wifi_pw_change);
        mWifiPwChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // android.widget.Toast.makeText(getContext(), "
                // 와이파이 체인지", Toast.LENGTH_SHORT).show();
                if (!checkInvalideParam())
                    return;

                ByteBuffer buffer = makeBuffer();
                if (buffer != null) {
                    APService.setNetworkApMode(buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_change_wifi), Toast.LENGTH_SHORT);
                            APService.getNetworkMode(new NetworkResultLisnter() {
                                @Override
                                public void onSuccress(Object retValue) {
                                }

                                @Override
                                public void onError(int errorCode) {

                                }

                                @Override
                                public void onErrorConnectionId(int errorCode) {

                                }
                            });
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Logout();
                        }
                    });
                }
            }
        });
        mLoginPwChange = (RelativeLayout) view.findViewById(R.id.btn_login_pw_change);
        mLoginPwChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    String sNewPassword = mPassword.getText().toString();
                    String sConfirmPassword = mPasswordConfirm.getText().toString();


                    if (!sConfirmPassword.equals(sNewPassword)) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_password), Toast.LENGTH_SHORT);
                        return;
                        //Toast.makeText(getActivity(), "The new password does not match.", Toast.LENGTH_SHORT).show();
                    }

                    if (sNewPassword.getBytes().length < 4) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }

                    if (sNewPassword.getBytes().length > Config.PASSWORD_MAX_LENGTH) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }
                    if (sConfirmPassword.getBytes().length > Config.PASSWORD_MAX_LENGTH) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }


                    byte[] nullPoint = new byte[]{(byte) 0x00, (byte) 0x00};

                    String sCurrentPassword = Prf.readToSharedPreferences(mContext, Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, "12345");
                    int length = 8 - sCurrentPassword.getBytes().length;
                    if (length != 0) {
                        byte[] empty = new byte[length];
                        sCurrentPassword = sCurrentPassword + new String(empty);
                    }

                    int length2 = 8 - sNewPassword.getBytes().length;
                    if (length2 != 0) {
                        byte[] empty = new byte[length2];
                        sNewPassword = sNewPassword + new String(empty);
                    }

                    final String finalPassword = sNewPassword;
                    APService.changePassword(sCurrentPassword + new String(nullPoint) + sNewPassword + new String(nullPoint), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {

                            String pw = finalPassword.trim();
                            Prf.writeToSharedPreferences(getActivity(), Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, pw);

                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_complete_pw_change), Toast.LENGTH_SHORT);
                            Prf.writeToSharedPreferences(mContext, Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, "");
                        }

                        @Override
                        public void onError(int errorCode) {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_pw), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        mResetFeelmaster = (RelativeLayout) view.findViewById(R.id.reset_feelmaster);
        mResetFeelmaster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogResetDevice dialogResetDevice = new DialogResetDevice();
                dialogResetDevice.show(getActivity().getFragmentManager(), "dialogResetDevice");

            }
        });

        TedPermission.with(mContext)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {


                        String ssid = getWiFiSSID(getContext().getApplicationContext());
                        ssid.replace("\"", "");
                        if (ssid.length() > 2) {
                            ssid = ssid.substring(1, ssid.length() - 1);
                        }
                        final String strSsid = ssid;
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (!strSsid.isEmpty() || !strSsid.contains("unknown ssid")) {
                                    mEditSetFeelmasterName.setText(strSsid);
                                }
                            }
                        }, 400);
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                        // MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_ip_add), Toast.LENGTH_SHORT);
                    }
                })
                .setPermissions(Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_MULTICAST_STATE, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    private ByteBuffer makeBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(Cmd.AP_MODE_BUFFER_SIZE);

        String fullName = mEditSetFeelmasterName.getText().toString();
        int length = 16 - mEditSetFeelmasterName.getText().toString().getBytes().length;
        byte[] empty = new byte[length];
        fullName = fullName + new String(empty);

        String fullPassword = mApPassword.getText().toString();
        int length2 = 16 - mApPassword.getText().toString().getBytes().length;
        byte[] empty2 = new byte[length2];
        fullPassword = fullPassword + new String(empty2);

        buffer.put(fullName.getBytes());
        buffer.put(fullPassword.getBytes());
        buffer.put(APService.getIpadress().getBytes());

        return buffer;
    }

    private boolean checkInvalideParam() {

        if (mEditSetFeelmasterName.getText().length() <= 0) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mEditSetFeelmasterName.getText().toString().getBytes().length > 16) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_name_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mApPassword.getText().length() < 8 || mApPasswordConfirm.getText().length() < 8) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mApPassword.getText().length() < 8 || mApPasswordConfirm.getText().length() < 8) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (!mApPassword.getText().toString().equals(mApPasswordConfirm.getText().toString())) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_pw), Toast.LENGTH_SHORT);
            return false;
        }

        return true;
    }


    @Override
    public void onDeviceStatusChangedListener(final int index, int type) {
        switch (type) {
            case CONNECTED_ITEM:
                try {
                    APService.deleteDevice(mConnectedDevice.get(index).getDevMac(), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            showDialog();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    updateUi();
                                }
                            }, 800);

                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Logout();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Logout();
                }
                break;
            case NOT_CONNECTED_ITEM:
                try {
                    APService.registerDevice(mNotConnectedDevice.get(index).getDevMac(), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            Vibrator vibrator = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(1150); // 1초간 진동
                            showDialog();
                            mHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {

                                    byte[] mac = mNotConnectedDevice.get(index).getDevMac();
                                    DialogDeviceNameInit deviceChangeDialog = DialogDeviceNameInit.newInstance(mac);
                                    deviceChangeDialog.setLisetener(mListener);
                                    deviceChangeDialog.setOldname("");
                                    deviceChangeDialog.show(getFragmentManager(), "");
                                    // updateUi();
                                }
                            }, 800);
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.e("delete device", "error");
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Log.e("delete device", "error");
                            Logout();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("delete device", "error");
                    Logout();
                }
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private static int prevIndicatorPos = 0;
    private static View prevIndicatorView = null;

    public class DeviceListAdapter extends BaseAdapter implements View.OnClickListener {
        private int roomIndex = 0, dimmerIndex = 0, notroomIndex = 0, notdimmerIndex = 0;
        private OnDeviceStatusChangedListener onDeviceStatusChangedListener;
        private LayoutInflater mLayoutInflater;
        private ArrayList<DeviceInfo> dataSet;
        private int type;

        private void resetIndex() {
            roomIndex = 0;
            dimmerIndex = 0;
            notroomIndex = 0;
            notdimmerIndex = 0;
        }

        DeviceListAdapter(Context context, ArrayList<DeviceInfo> data, int type, OnDeviceStatusChangedListener onDeviceStatusChangedListener) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.dataSet = data;
            this.type = type;
            this.onDeviceStatusChangedListener = onDeviceStatusChangedListener;
        }

        public void setNewDeviceList(ArrayList<DeviceInfo> _dataSet) {
            resetIndex();
            dataSet = _dataSet;
        }

        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_view_device_manager, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.item_list_view_device_manager_text_name);
            ImageView indicator = (ImageView) convertView.findViewById(R.id.item_list_view_device_manager_indicator);
            convertView.setTag(R.id.item_list_view_device_manager_indicator, position);
            convertView.setTag(R.id.item_list_view_device_manager_text_name, textView);

            if (dataSet.get(position).getIndicatorFlag()) {
                // indicator.setVisibility(View.VISIBLE);

                textView.setTextColor(mContext.getResources().getColor(devicelist_selected_color));
                if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                    indicator.setImageResource(R.mipmap.icon_device_blink_on);
                } else if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_SWITCH) {
                    indicator.setImageResource(R.mipmap.icon_dimmer_twinkle);
                }
            } else {
                if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                    indicator.setImageResource(R.mipmap.icon_device_blink_off);
                } else if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_SWITCH) {
                    indicator.setImageResource(R.mipmap.icon_dimmer_off);
                }
            }
            convertView.setTag(indicator);

            if (type == NOT_CONNECTED_ITEM) {
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageView indicator = (ImageView) v.getTag();
                        TextView textBlink = (TextView) v.getTag(R.id.item_list_view_device_manager_text_name);
                        int position = (int) v.getTag(R.id.item_list_view_device_manager_indicator);
                        boolean stat = dataSet.get(position).getIndicatorFlag();

                        // 이전에 눌럿던 인디케이터 초기화

                        if (prevIndicatorView != null) {
                            ImageView prevIndicator = (ImageView) prevIndicatorView.getTag();
                            prevIndicator.setVisibility(View.GONE);
                            APService.checkBlinkDevice(dataSet.get(prevIndicatorPos).getDevMac(), Cmd.CMD_CHECK_LIGHT_OFF);
                            dataSet.get(prevIndicatorPos).setIndicatorFlag(false);
                            notifyDataSetChanged();
                        }

                        if (stat) {
                            // indicator.setVisibility(View.GONE);
                            APService.checkBlinkDevice(dataSet.get(position).getDevMac(), Cmd.CMD_CHECK_LIGHT_OFF);
                            dataSet.get(position).setIndicatorFlag(false);
                            textBlink.setTextColor(mContext.getResources().getColor(R.color.whit_color));
                            notifyDataSetChanged();
                        } else {
                            // indicator.setVisibility(View.VISIBLE);
                            APService.checkBlinkDevice(dataSet.get(position).getDevMac(), Cmd.CMD_CHECK_LIGHT_ON);
                            dataSet.get(position).setIndicatorFlag(true);
                            textBlink.setTextColor(mContext.getResources().getColor(R.color.indicator_color));
                            notifyDataSetChanged();
                        }
                        prevIndicatorPos = position;
                        prevIndicatorView = v;
                    }
                });
            } else {
                if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                    indicator.setImageResource(R.mipmap.icon_device_blink_on);
                } else if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_SWITCH) {
                    indicator.setImageResource(R.mipmap.icon_dimmer_twinkle);
                }
            }

            byte[] bMac = dataSet.get(position).getDevMac();
            String sMacName = "";

            byte[] bDevName = dataSet.get(position).getmByteDevName();
            String sDevName = "";

            try {
                for (byte singleMac : bMac)
                    sMacName += String.format("%02X", (((short) singleMac)) & 0xFF);
            } catch (Exception e) {
                Log.e("error", "convert hex to string");
            }

            try {
                for (byte singleMac : bDevName)
                    sDevName += String.format("%02X", (((short) singleMac)) & 0xFF);
            } catch (Exception e) {
                Log.e("error", "convert hex to string");
            }

            String text = "";
            int value = 0;
            if (bDevName != null) {
                for (byte b : bDevName) {
                    value += (int) b;
                }
            }

            TextView deviceStatus = (TextView) convertView.findViewById(R.id.item_list_view_device_manager_image_button);
            RelativeLayout deviceLayer = (RelativeLayout) convertView.findViewById(R.id.item_list_view_device_manager_layer);
            RelativeLayout layerNameEdit = (RelativeLayout) convertView.findViewById(R.id.item_list_view_device_manager_layer_edit);

            layerNameEdit.setTag(R.id.item_list_view_device_manager_layer_edit, position); // device mac
            if (position == 0)
                resetIndex();
            switch (type) {
                case CONNECTED_ITEM:
                    deviceStatus.setText(R.string.delete);
                    layerNameEdit.setVisibility(View.VISIBLE);
                    //indicator.setVisibility(View.GONE);
                    if (value == 0) {
                        if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                            roomIndex = ++roomIndex;
                            sDevName = "ROOM " + Integer.toString(roomIndex);
                        } else {
                            dimmerIndex = ++dimmerIndex;
                            sDevName = "DIMMER " + Integer.toString(dimmerIndex);
                        }

                    } else {
                        try {
                            sDevName = new String(bDevName, "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case NOT_CONNECTED_ITEM:
                    layerNameEdit.setVisibility(View.GONE);
                    deviceStatus.setText(R.string.frag_dev_manager_dev_register);
                    if (value == 0) {
                        if (dataSet.get(position).getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                            notroomIndex = ++notroomIndex;
                            sDevName = "ROOM " + Integer.toString(notroomIndex);
                        } else {
                            notdimmerIndex = ++notdimmerIndex;
                            sDevName = "DIMMER " + Integer.toString(notdimmerIndex);
                        }

                    } else {
                        try {
                            sDevName = new String(bDevName, "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
            }
            textView.setText(sDevName);
            Log.e("mac : ", sDevName);

            layerNameEdit.setOnClickListener(this);
            //  layerNameEdit.setVisibility(View.GONE);
            deviceLayer.setOnClickListener(this);
            deviceLayer.setTag(R.id.id_item_list_view_device_manager_index, position);

            return convertView;
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.item_list_view_device_manager_layer)
                onDeviceStatusChangedListener.onDeviceStatusChangedListener(Integer.valueOf(String.valueOf(v.getTag(R.id.id_item_list_view_device_manager_index))), type);
            if (v.getId() == R.id.item_list_view_device_manager_layer_edit) {

                int position = (int) v.getTag(R.id.item_list_view_device_manager_layer_edit);

                String oldName = dataSet.get(position).getDevName();
                String msg = "";
                int type = dataSet.get(position).getIntDevType();
                if (type == Cmd.DEVICE_TYPE_LIGHT)
                    msg = "룸이름을 입력하세요.";
                else
                    msg = "디머 이름을 입력하세요.";
                if (oldName == null || oldName.isEmpty()) {
                    oldName = msg;
                }

                byte[] mac = dataSet.get(position).getDevMac();
                DialogDeviceNameChange deviceChangeDialog = DialogDeviceNameChange.newInstance(mac);
                deviceChangeDialog.setLisetener(mListener);
                deviceChangeDialog.setOldname(oldName);
                deviceChangeDialog.show(getFragmentManager(), oldName);
            }
        }
    }
}

interface OnDeviceStatusChangedListener {
    void onDeviceStatusChangedListener(int index, int type);
}