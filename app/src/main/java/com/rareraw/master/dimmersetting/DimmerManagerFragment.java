package com.rareraw.master.dimmersetting;

import android.Manifest;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.DialogResetDevice;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnRenameListener;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Config;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.Preference.Prf;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.device.DialogDeviceNameChange;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.SwitchInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import static com.neoromax.feelmaster.R.color.devicelist_selected_color;


public class DimmerManagerFragment extends BaseFragment implements OnDimmerStatusChangedListener {
    ArrayList<SwitchInfo> mConnectedDevice = new ArrayList<SwitchInfo>();
    ArrayList<DeviceInfo> mNotConnectedDevice = new ArrayList<DeviceInfo>();
    DeviceListAdapter mConnectedDeviceListAdapter, mNotConnectedDeviceListAdapter;
    private static Context mContext;
    public final static int CONNECTED_ITEM = 0;
    public final static int NOT_CONNECTED_ITEM = 1;
    private ImageView mRefresh1, mRefresh2;
    private ListView cntListView;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private byte[] mMac;
    private String mSceneName = "";
    public DialogSelectMood.OnActionListener2 mOnActionListner = new DialogSelectMood.OnActionListener2() {
        @Override
        public void onActionListener(byte[] mac, String _name, int positiomn) { // device 값 넘어옴.

            mMac = mac;
            mSceneName = _name;
            SwitchInfo info = (SwitchInfo) mConnectedDeviceListAdapter.getItem(positiomn);

            setDimmerRoom(mMac, info.getByteSwitchMac());
        }
    };

    private void setDimmerRoom(byte[] devMac, byte[] switchMac) {

        APService.setDimmerRoom(switchMac, devMac, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                MyToast.getInstance(getActivity()).showToast("디머 룸설정이 완료되었습니다.", Toast.LENGTH_SHORT);
                Log.d("setDimmerRoom ok ", "");
                updateUi();
            }

            @Override
            public void onError(int errorCode) {
                Log.d("setDimmerRoom fail ", "");
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }

    public OnRenameListener mListener = new OnRenameListener() {
        @Override
        public void onActionRename(byte[] nValue, String sValue) {
            if (sValue.equals("loading")) {
                showDialog();
            } else
                MyToast.getInstance(getActivity()).showToast(sValue, Toast.LENGTH_SHORT);
        }

        @Override
        public void onCompleteRename(int nValue, String sValue) {
            updateUi();
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // getTestData();
        mContext = getActivity();
        // 2step, setting data in adapter
        mConnectedDeviceListAdapter = new DeviceListAdapter(getActivity(), mConnectedDevice, CONNECTED_ITEM, this);
        // mNotConnectedDeviceListAdapter = new DeviceListAdapter(getActivity(), mNotConnectedDevice, NOT_CONNECTED_ITEM, this);
        ((ContainActivity) getActivity()).createDialog(700);
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    private void updateUi() {
        showDialog();
        getDimmerInfo();
    }

    public void showDialog() {
        ((ContainActivity) getActivity()).createDialog(700);
    }

    private void getDimmerInfo() {

        if (Config.DEBUG) {
            getTestData();
        } else {
            try {
                APService.getSwitchInfo(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        ArrayList<SwitchInfo> arrInfo = (ArrayList<SwitchInfo>) retValue;
                        ArrayList<DeviceInfo> devInfo = new ArrayList<>();
                        mConnectedDevice.clear();

                   /* for (int i = 0; i < arrInfo.size(); i++) {
                        DeviceInfo info = new DeviceInfo(arrInfo.get(i).getByteSwitchMac(), arrInfo.get(i).getSwitchType());
                        // mRegistedDevList.add(info);
                        devInfo.add(info);
                    }*/


                        mConnectedDevice = arrInfo;

                        mConnectedDeviceListAdapter.setNewDeviceList(mConnectedDevice);
                        setListViewHeightBasedOnChildren(cntListView);
                        mConnectedDeviceListAdapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });

            } catch (Exception e) {

            }
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    private void getTestData() {
        if (Config.DEBUG) {
            byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
            byte[] dev_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11};

            SwitchInfo temp = new SwitchInfo();
            temp.setByteSwitchMac(alllight_mac);
            temp.setmByteDevMac(dev_mac);
            mConnectedDevice.add(temp);
            mConnectedDevice.add(new SwitchInfo());
            mConnectedDevice.add(new SwitchInfo());

            mConnectedDeviceListAdapter.setNewDeviceList(mConnectedDevice);
            setListViewHeightBasedOnChildren(cntListView);
            mConnectedDeviceListAdapter.notifyDataSetChanged();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dimmer_manager, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cntListView = (ListView) view.findViewById(R.id.fragment_device_manager_list_connect_devices);
        cntListView.setAdapter(mConnectedDeviceListAdapter);

        // notCntListView = (ListView) view.findViewById(R.id.fragment_device_manager_list_not_connect_devices);
        // notCntListView.setAdapter(mNotConnectedDeviceListAdapter);

        mRefresh1 = (ImageView) view.findViewById(R.id.btn_refresh1);
        mRefresh1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                updateUi();
            }
        });

    }

    @Override
    public void onDimmerStatusChangedListener(int index, int type) {
        switch (type) {
            case CONNECTED_ITEM:
                break;
            case NOT_CONNECTED_ITEM:
                break;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    private static int prevIndicatorPos = 0;
    private static View prevIndicatorView = null;

    public class DeviceListAdapter extends BaseAdapter implements View.OnClickListener {
        private OnDimmerStatusChangedListener onDeviceStatusChangedListener;
        private LayoutInflater mLayoutInflater;
        public ArrayList<SwitchInfo> dataSet;
        private int type;

        DeviceListAdapter(Context context, ArrayList<SwitchInfo> data, int type, OnDimmerStatusChangedListener onDeviceStatusChangedListener) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.dataSet = data;
            this.type = type;
            this.onDeviceStatusChangedListener = onDeviceStatusChangedListener;
        }

        public void setNewDeviceList(ArrayList<SwitchInfo> _dataSet) {
            dataSet = _dataSet;
        }

        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = mLayoutInflater.inflate(R.layout.item_list_view_dimmer_manager, parent, false);
            TextView textView = (TextView) convertView.findViewById(R.id.item_list_view_device_manager_text_name);
            ImageView indicator = (ImageView) convertView.findViewById(R.id.item_list_view_device_manager_indicator);
            convertView.setTag(R.id.item_list_view_device_manager_indicator, position);
            convertView.setTag(R.id.item_list_view_device_manager_text_name, textView);

            indicator.setImageResource(R.mipmap.icon_dimmer_twinkle);
            convertView.setTag(indicator);


            byte[] bDevName = dataSet.get(position).getmByteswitchName();
            byte[] bDevMac = dataSet.get(position).getByteDevMac();
            String sDevName = "";

            try {
                for (byte singleMac : bDevName)
                    sDevName += String.format("%02X", (((short) singleMac)) & 0xFF);
            } catch (Exception e) {
                Log.e("error", "convert hex to string");
            }

            String text = "";
            int value = 0;
            if (bDevName != null) {
                for (byte b : bDevName) {
                    value += (int) b;
                }
            }
            if (value == 0) {
                sDevName = "DIMMER " + Integer.toString(position + 1);
            } else {
                try {
                    sDevName = new String(bDevName, "UTF-8");
                } catch (Exception e) {

                }
            }
            String roomName = getDeviceName(bDevMac, position);
            if(roomName.isEmpty()){
                sDevName = sDevName + " (전체조명)";
            }else
                sDevName = sDevName + " (" + roomName + ")";
            // textView.setText(sMacName);
            // Log.e("mac : ", sMacName);

            textView.setText(sDevName);
            Log.e("mac : ", sDevName);

            TextView deviceStatus = (TextView) convertView.findViewById(R.id.item_list_view_device_manager_image_button);
            RelativeLayout deviceLayer = (RelativeLayout) convertView.findViewById(R.id.item_list_view_device_manager_layer);
            RelativeLayout layerNameEdit = (RelativeLayout) convertView.findViewById(R.id.item_list_view_device_manager_layer_edit);

            layerNameEdit.setTag(R.id.item_list_view_device_manager_layer_edit, position); // device mac

            switch (type) {
                case CONNECTED_ITEM:
                    deviceStatus.setText(R.string.delete);
                    layerNameEdit.setVisibility(View.VISIBLE);
                    //indicator.setVisibility(View.GONE);
                    break;
                case NOT_CONNECTED_ITEM:
                    layerNameEdit.setVisibility(View.GONE);
                    deviceStatus.setText(R.string.frag_dev_manager_dev_register);
                    break;
            }
            layerNameEdit.setOnClickListener(this);
            //  layerNameEdit.setVisibility(View.GONE);
            deviceLayer.setOnClickListener(this);
            deviceLayer.setTag(R.id.id_item_list_view_device_manager_index, position);

            return convertView;
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.item_list_view_device_manager_layer)
                onDeviceStatusChangedListener.onDimmerStatusChangedListener(Integer.valueOf(String.valueOf(v.getTag(R.id.id_item_list_view_device_manager_index))), type);
            //if (v.getId() == R.id.item_list_view_device_manager_layer)
            //onDeviceStatusChangedListener.onDeviceStatusChangedListener(Integer.valueOf(String.valueOf(v.getTag(R.id.id_item_list_view_device_manager_index))), type);
            if (v.getId() == R.id.item_list_view_device_manager_layer_edit) {

                int position = (int) v.getTag(R.id.item_list_view_device_manager_layer_edit);

                DialogSelectMood dialogSelectScene = new DialogSelectMood();
                // dialogSelectScene.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
                dialogSelectScene.setListener(mOnActionListner);
                dialogSelectScene.setposition(position);
                dialogSelectScene.show(getActivity().getFragmentManager(), "dialogSelectScene");
            }
        }
    }

    private String getDeviceName(byte[] bDevMac, int pos) {
        String sDevName = "";
        ArrayList<DeviceInfo> infos = APService.getApRegistedDevList();

        for (DeviceInfo info : infos) {
            String mac1 = "";
            String mac2 = "";
            try{
                mac1 = new String(info.getDevMac());
                mac2 = new String(bDevMac);
            }catch (Exception e){
                e.printStackTrace();
            }
            if (mac1.equals(mac2)) {
                byte[] tempName = info.getmByteDevName();

                try {
                    for (byte singleMac : tempName)
                        sDevName += String.format("%02X", (((short) singleMac)) & 0xFF);
                } catch (Exception e) {
                    Log.e("error", "convert hex to string");
                }

                String text = "";
                int value = 0;
                if (tempName != null) {
                    for (byte b : tempName) {
                        value += (int) b;
                    }
                }

                if (value == 0) {
                    sDevName = "ROOM " + Integer.toString(pos+1);
                } else {
                    try {
                        sDevName = new String(tempName, "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return sDevName;
            }
        }

        return sDevName;
    }
}

interface OnDimmerStatusChangedListener {
    void onDimmerStatusChangedListener(int index, int type);
}
