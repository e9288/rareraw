package com.rareraw.master.dimmersetting;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.OnRenameListener;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Config;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;

/**
 * Created by Lch on 2016-10-21.
 */

public class DialogDimmerNameChange extends DialogFragment {

    private LinearLayout mBtnOk, mBtnCCancel;
    private EditText mEditTextChannelName;
    private static byte[] mMac;
    private String oldName;
    private TextView mTextErrMsg;
    OnRenameListener mListener;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    public static DialogDimmerNameChange newInstance(byte[] _mac) {
        DialogDimmerNameChange channelChangeDialog = new DialogDimmerNameChange();
        mMac= _mac;
        return channelChangeDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return inflater.inflate(R.layout.dialog_change_device_name, container, false);
    }

    public void setLisetener(OnRenameListener lisetener) {
        mListener = lisetener;
    }

    public void setOldname(String name) {
        oldName = name;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditTextChannelName = (EditText) view.findViewById(R.id.change_channel_name);
        mTextErrMsg = (TextView) view.findViewById(R.id.text_err_msg);
        mEditTextChannelName.setHint(oldName);
        mBtnCCancel = (LinearLayout) view.findViewById(R.id.dialog_change_name_cancel);
        mBtnCCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (LinearLayout) view.findViewById(R.id.dialog_change_name_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = mEditTextChannelName.getText().toString();
                mTextErrMsg.setText("");
                if (name.getBytes().length > Config.NAME_MAX_LENGTH) {
                    mTextErrMsg.setText(getString(R.string.msg_overflow_channel_name_length));
                    return;
                }

                if (name.length() <= 0) {
                    mTextErrMsg.setText(getString(R.string.msg_underflow_channel_name_length));
                    return;
                }

                String tempName = name;
                int length = 16 - name.getBytes().length;
                byte[] empty = new byte[length];
                tempName = tempName + new String(empty);
                ByteBuffer buffer  = ByteBuffer.allocate(24);
                // buffer.put(mMac);
                buffer.put( mMac);
                buffer.put(tempName.getBytes());
                mListener.onActionRename(new byte[1], "loading");
                APService.setDeviceName(buffer, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Toast.makeText(getActivity(), "디머 이름이 변경되었습니다.", Toast.LENGTH_SHORT).show();
                        mListener.onCompleteRename(1, getTag());
                        dismiss();
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.e("error", "change ChannelName");
                        dismiss();
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Log.e("error", "change ChannelName");
                        dismiss();
                    }
                }) ;


            }
        });
    }


}