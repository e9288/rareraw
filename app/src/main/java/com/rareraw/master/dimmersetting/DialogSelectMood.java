package com.rareraw.master.dimmersetting;

import android.app.DialogFragment;
import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.ArrayList;

import javax.xml.transform.Templates;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogSelectMood extends DialogFragment {
    ArrayList<DeviceInfo> mConnectedDevice = new ArrayList<DeviceInfo>();
    private OnActionListener2 mListner;
    private LinearLayout mBtnOk;
    private ListView mListviewSceneList;
    private SceneListAdapter mAdapter;
    private View mParent;
    private int mSelectedPosition;

    public DialogSelectMood() {
    }

    public void setListener(OnActionListener2 _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }


    @Override
    public void onStart() {
        super.onStart();
        initSceneList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_select_mood, container, false);

        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnOk = (LinearLayout) mParent.findViewById(R.id.btn_confirm);
        ArrayList<DeviceInfo> temp = new ArrayList<>();
        mListviewSceneList = (ListView) mParent.findViewById(R.id.listview_mood_list);
        mAdapter = new SceneListAdapter(getActivity(), temp);
        mListviewSceneList.setAdapter(mAdapter);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DeviceInfo info = mAdapter.getSelectedScene();

                if (info != null) {
                    mListner.onActionListener(info.getDevMac(), info.getDevName(), mSelectedPosition);
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), "선택된 룸이없습니다.룸을 선택해주세요.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private void initSceneList() {
        getRegDeviceInfo();

    }

    private void getRegDeviceInfo() {

        if (Config.DEBUG) {
            getTestData();
        } else {
            // 1 step, load existing device list (2 type)
            APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    mConnectedDevice.clear();
                    ArrayList<DeviceInfo> tempList = APService.getApRegistedDevList();
                    byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
                    byte[] deviceType = new byte[]{(byte) 0x01};
                    // 01049147330 전체조
                    mConnectedDevice.add(new DeviceInfo(alllight_mac, deviceType));
                    for (DeviceInfo info : tempList) {
                        if (info.getIntDevType() == Cmd.DEVICE_TYPE_LIGHT) {
                            mConnectedDevice.add(info);
                        }
                    }


                    mAdapter.setNewDeviceList(mConnectedDevice);
                    setListViewHeightBasedOnChildren(mListviewSceneList);
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                }
            });
        }
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getTestData() {
        if (Config.DEBUG) {
            byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
            byte[] text_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11};
            byte[] deviceType = new byte[]{(byte) 0x01};
            // 01049147330 전체조

            DeviceInfo info1 = new DeviceInfo(1);
            info1.setIntDevType(0);
            mConnectedDevice.add(info1);
            mConnectedDevice.add(new DeviceInfo(text_mac, deviceType));
            mConnectedDevice.add(new DeviceInfo(alllight_mac, deviceType));
            mConnectedDevice.add(new DeviceInfo(alllight_mac, deviceType));
            mConnectedDevice.add(new DeviceInfo(alllight_mac, deviceType));

            mAdapter.setNewDeviceList(mConnectedDevice);
            setListViewHeightBasedOnChildren(mListviewSceneList);
            mAdapter.notifyDataSetChanged();
        }
    }


    public void setposition(int _pos) {
        this.mSelectedPosition = _pos;
    }

    public class SceneListAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<DeviceInfo> mSceneList;
        private Context mContext;

        SceneListAdapter(Context context, ArrayList<DeviceInfo> _scenelist) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mSceneList = _scenelist;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return mSceneList.size();
        }

        @Override
        public Object getItem(int position) {
            return mSceneList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null)
                view = mLayoutInflater.inflate(R.layout.item_list_view_mood_name, parent, false);

            RelativeLayout layout = view.findViewById(R.id.device_layout);
            TextView devName = view.findViewById(R.id.text_mood_name);

            if (mSceneList.get(position).getCheckStat()) {
                layout.setBackgroundColor(mContext.getResources().getColor(R.color.devicelist_selected_color));
                devName.setTextColor(mContext.getResources().getColor(R.color.whit_color));
            } else {
                layout.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
                devName.setTextColor(mContext.getResources().getColor(R.color.black_color));
            }

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (DeviceInfo info : mSceneList) {
                        info.setCheckStat(false);
                    }

                    mSceneList.get(position).setCheckStat(true);
                    notifyDataSetChanged();
                }
            });

            byte[] bMac = mSceneList.get(position).getDevMac();
            String sMacName = "";

            byte[] bDevName = mSceneList.get(position).getmByteDevName();
            String sDevName = "";

            try {
                for (byte singleMac : bMac)
                    sMacName += String.format("%02X", (((short) singleMac)) & 0xFF);
            } catch (Exception e) {
                Log.e("error", "convert hex to string");
            }

            try {
                for (byte singleMac : bDevName)
                    sDevName += String.format("%02X", (((short) singleMac)) & 0xFF);
            } catch (Exception e) {
                Log.e("error", "convert hex to string");
            }

            String text = "";
            int value = 0;
            if (bDevName != null) {
                for (byte b : bDevName) {
                    value += (int) b;
                }
            }

            if (value == 0) {
                sDevName = "ROOM " + Integer.toString(position + 1);
            } else {
                try {
                    sDevName = new String(bDevName, "UTF-8");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // textView.setText(sMacName);
            // Log.e("mac : ", sMacName);
            mSceneList.get(position).setDevName(sDevName);
            devName.setText(sDevName);
            Log.e("mac : ", sDevName);

            return view;
        }

        public DeviceInfo getSelectedScene() {

            for (DeviceInfo info : mSceneList) {
                if (info.mCheckStat)
                    return info;
            }
            return null;
        }

        public void setNewDeviceList(ArrayList<DeviceInfo> mConnectedDevice) {
            mSceneList = mConnectedDevice;
        }
    }

    public interface OnActionListener2 {
        void onActionListener(byte[] nValue, String sValue, int pos);
    }
}

