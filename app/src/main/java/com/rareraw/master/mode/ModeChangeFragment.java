package com.rareraw.master.mode;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

/**
 * Created by Lch on 2016-12-14.
 */

public class ModeChangeFragment extends BaseFragment {

    public final static int PAGE_VALUE = 108;
    private CheckBox mCheckBoxRemember;
    private EditText mEditTextPassword;
    private RelativeLayout mBtnLogin;
    private TextView  mTextVersion;
    private RelativeLayout mLayerSave;
    private LinearLayout mLayoutEnterPassword, mLayoutImgUserInfo;
    private final boolean ADMIN_LOGIN = true, NORMAL_LOGIN = false;
    private Context mContext;
    private View mParent;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_login, container, false);
        getViewInlayout();
        setViewListener();
        //setVersionInfo();

        boolean bRememberCheck = Prf.readToSharedPreferences(mContext, Prf.REMEMBER_PW_FILE, Prf.REMEMBER_PW_TAG, false);
        if (bRememberCheck) {
            mCheckBoxRemember.setChecked(true);
            String password = Prf.readToSharedPreferences(mContext, Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, "12345");
            mEditTextPassword.setText(password);
            drawRememberMe(true);
        }
        mCheckBoxRemember.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    drawRememberMe(true);
                }else{
                    drawRememberMe(false);
                }
            }
        });

        return mParent;
    }

    private void drawRememberMe(boolean type) {
        if(type) {
            //mLayerSave.setBackgroundColor(getResources().getColor(R.color.select_item));
            mCheckBoxRemember.setTextColor(getResources().getColor(R.color.whit_color));
            mCheckBoxRemember.setBackgroundColor(getResources().getColor(R.color.select_item));
        }else{
            // mLayerSave.setBackgroundColor(getResources().getColor(R.color.transparent));
            mCheckBoxRemember.setTextColor(getResources().getColor(R.color.black_color));
            mCheckBoxRemember.setBackgroundColor(getResources().getColor(R.color.transparent));
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void getViewInlayout() {
        mEditTextPassword = (EditText) mParent.findViewById(R.id.login_edittext_password);
        mBtnLogin = (RelativeLayout) mParent.findViewById(R.id.login_ok_btn);
        mCheckBoxRemember = (CheckBox) mParent.findViewById(R.id.login_checkbox_remember);
        // mTextVersion = (TextView) mParent.findViewById(R.id.text_version);
        mLayerSave = (RelativeLayout) mParent.findViewById(R.id.layer_save_btn);
    }

    private void setVersionInfo() {
        PackageInfo pi = null;

        try {
            String version = getActivity().getPackageName();
            pi = getActivity().getPackageManager().getPackageInfo(version, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String verSion = "V " + pi.versionName;
        mTextVersion.setText(verSion);
    }

    private void setViewListener() {

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mEditTextPassword.getText().length() < 4){
                    MyToast.getInstance(mContext).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT );
                    return;
                }
                if (mCheckBoxRemember.isChecked())
                    Prf.writeToSharedPreferences(mContext, Prf.REMEMBER_PW_FILE, Prf.REMEMBER_PW_TAG, true);
                else
                    Prf.writeToSharedPreferences(mContext, Prf.REMEMBER_PW_FILE, Prf.REMEMBER_PW_TAG, false);

                Prf.writeToSharedPreferences(mContext, Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, "12345");
                byte[] nullPoint = new byte[]{(byte) 0x00, (byte) 0x00};

                String pw = mEditTextPassword.getText().toString();
                String fullPassword = pw;
                int length = 6 - pw.getBytes().length;
                if (length != 0) {
                    byte[] empty = new byte[length];
                    fullPassword = fullPassword + new String(empty);
                }
                APService.connectAdminRequst(fullPassword, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                        if (mCheckBoxRemember.isChecked()) {
                            Prf.writeToSharedPreferences(mContext, Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, mEditTextPassword.getText().toString());
                        }
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(getActivity()!= null)
                                    getActivity().onBackPressed();
                            }
                        }, 400);
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_change_admin_mode), Toast.LENGTH_LONG);
                        APService.setUserLevel(ADMIN_LOGIN);
                    }

                    @Override
                    public void onError(int errorCode) {
                        try {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_pw), Toast.LENGTH_SHORT);
                        } catch (Exception e){

                        }
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();
                    }
                });
            }
        });
    }

}