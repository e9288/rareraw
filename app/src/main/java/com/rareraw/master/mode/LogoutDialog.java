package com.rareraw.master.mode;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;

/**
 * Created by Lch on 2016-10-21.
 */

public class LogoutDialog extends Activity {

    private TextView mBtnOk, mBtnCCancel;
    private OnActionListener mListener;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_mode_change_logout);
        mBtnCCancel = (TextView) findViewById(R.id.dialog_logout_cancel);
        mBtnCCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mBtnOk = (TextView) findViewById(R.id.dialog_logout_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}