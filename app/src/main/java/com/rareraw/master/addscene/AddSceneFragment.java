package com.rareraw.master.addscene;

import android.bluetooth.BluetoothClass;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.NestedScrollingChildHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.Dimmer.RoomListAdapter;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.data.Channel;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Lch on 2016-12-26.
 */

public class AddSceneFragment extends BaseFragment implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private AddSceneListAdapter mAddSceneListAdapter;
    private RoomDimingListAdapter mRoomListAdapter;
    private ListView mRoomListView;
    private ArrayList<DeviceInfo> mRoomInfolist = new ArrayList<>();
    private SceneInfo mScene = new SceneInfo();
    private int mScenenoForModify;
    private boolean MODIFY_FLAG = false; // false : new create, true : modify
    private ImageView mBtnIConType;
    public static final int USER_ICON = 1;
    private SeekBar mMasterSeekbar;
    private RelativeLayout mBtnDmxSetting;
    private Context mContext;
    private EditText mEditName;
    private CheckBox mCheckHide, mCheckAll;
    private TextView mTextFadeTime;
    private RelativeLayout mPrevPage, mNextPage;
    private final int STEP1 = 0, STEP2 = 1;
    private LinearLayout mLayoutStep1, mLayoutStep2;
    private int NOW_PAGE = STEP1;
    private OnActionListener mOnActionListener = new OnActionListener() {
        // Icon type ( day, season, place )
        // Icon index depends on the Type.
        @Override
        public void onActionListener(int _iconIndex, String _IconType) {
            mScene.setIntIconType(_iconIndex);
            setUserIconType(_iconIndex, _IconType);
        }
    };
    private OnDmxSettingValueListener mDmxValueListener = new OnDmxSettingValueListener() {
        @Override
        public void onDmxSettingValueListener(DmxInfo[] list) {
            mScene.setDmxList(list);
        }
    };

    public AddSceneFragment() {
    }

    public void setModiftStatus(int _sceneNo) {
        if (_sceneNo == -1)
            MODIFY_FLAG = false;
        else {
            MODIFY_FLAG = true;
            mScenenoForModify = _sceneNo;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity().getApplicationContext();
        //getSceneInfo();
        mAddSceneListAdapter = new AddSceneListAdapter(getActivity().getBaseContext(), mScene);
    }

    private void getSceneInfo() {
        if (MODIFY_FLAG)
            getExistingSceneInfo();
        else
            createSceneInfo();
    }

    public void updateUi() {
        // getDeviceInfo();
        //setDmxUseUi();
        getSceneInfo();
        if (mScene == null)
            return;
    }

//    private void getDeviceInfo() {
//        if (Config.DEBUG) {
//            DeviceInfo info1 = new DeviceInfo(1);
//            info1.setIntDevType(0);
//
//            DeviceInfo info2 = new DeviceInfo(1);
//            info1.setIntDevType(0);
//
//            DeviceInfo info3 = new DeviceInfo(1);
//            info1.setIntDevType(0);
//
//            ArrayList<Channel> chList1 = new ArrayList<>();
//            chList1.add(new Channel("ch1",1));
//
//            ArrayList<Channel> chList2 = new ArrayList<>();
//            chList2.add(new Channel("ch1",1));
//
//            ArrayList<Channel> chList3 = new ArrayList<>();
//            chList3.add(new Channel("ch1",1));
//
//            info1.setChannelList(chList1);
//            info1.setChannelList(chList2);
//            info1.setChannelList(chList3);
//            mRoomInfolist.add(info1);
//            mRoomInfolist.add(info2);
//            mRoomInfolist.add(info3);
//
//            mRoomListAdapter.setRoomList(mRoomInfolist);
//            mRoomListAdapter.notifyDataSetChanged();
//
//        } else {
//            APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
//                @Override
//                public void onSuccress(Object retValue) {
//                    String tempChannelName = "";
//                    mRoomInfolist = APService.mRegistedDevList;
//                    ArrayList<Channel> channels = new ArrayList<>();
//                    for (DeviceInfo deviceInfo : mRoomInfolist) {
//                        for (int i = 0; i < deviceInfo.getChannelNo().size(); i++) {
//
//                            tempChannelName = deviceInfo.getChannelName().get(i);
//                            if (deviceInfo.getIntChannelNo().get(i) == 0)
//                                continue;
//                            if (CmdHelper.checkNullName(tempChannelName) == 0) {
//                                channels.add(new Channel("CH" + String.format("%02d", deviceInfo.getIntChannelNo().get(i)), 0, false, deviceInfo.getIntChannelNo().get(i)));
//                            } else {
//                                channels.add(new Channel(tempChannelName, 0, false, deviceInfo.getIntChannelNo().get(i)));
//                            }
//                        }
//                    }
//
//                    mRoomListAdapter.setRoomList(mRoomInfolist);
//                    mRoomListAdapter.notifyDataSetChanged();
//                }
//
//                @Override
//                public void onError(int errorCode) {
//
//                }
//
//                @Override
//                public void onErrorConnectionId(int errorCode) {
//                    Logout();
//                }
//            });
//        }
//
//    }

    private void updateSceneUiInfo() {
        // setting default value
        //setUserIconType(mScene.getIntIconType(), "");
        mEditName.setText(removeNullChar(mScene.getSceneName()));
        //mCheckHide.setChecked(mScene.getHideFlag());
        mTextFadeTime.setText(Integer.toString(mScene.getIntFadeTime()));
    }

    private void setDmxUseUi() {
        if (APService.getDmxUseFlag())
            mBtnDmxSetting.setVisibility(View.VISIBLE);
        else
            mBtnDmxSetting.setVisibility(View.GONE);

        if (Config.DEBUG)
            mBtnDmxSetting.setVisibility(View.VISIBLE);
    }

    private void createSceneInfo() {

        // TODO get chanel list 2type, modify or new create
        SceneInfo scene = new SceneInfo();

        ArrayList<Channel> channels = new ArrayList<>();

        //scene.setSceneName("Scene" + CmdHelper.getEmptySceneNoInt());
        scene.setSceneName(""); // modify delete default scene name
        scene.setIntSceneNo(CmdHelper.getEmptySceneNoInt());
        scene.setIntIconType(Config.USER_DEFAULT_ICON);
        scene.setHideFlag(false);
        scene.setIntIndex(CmdHelper.getEmptyIndexNoInt());
        scene.setIntFadeTime(Config.DEFAULT_FADE_TIME);

        if (Config.DEBUG)
            testGetdata();

        String tempChannelName;
        try {
            ArrayList<DeviceInfo> devList = new ArrayList<>();
            for (DeviceInfo deviceInfo : APService.getApRegistedDevList()) {
                deviceInfo.clearChannelList();
                deviceInfo.clearSelectCheck();
                deviceInfo.setRoomDimingLevel(0);
                for (int i = 0; i < deviceInfo.getIntChannelNo().size(); i++) {
                    /*
                    tempChannelName = deviceInfo.getChannelName().get(i);
                    if (deviceInfo.getIntChannelNo().get(i) == 0)
                        continue;
                    if (CmdHelper.checkNullName(tempChannelName) == 0) {
                        // channels.add(new Channel("CH" + String.format("%02d", deviceInfo.getIntChannelNo().get(i)), 0, false, deviceInfo.getIntChannelNo().get(i)));
                        deviceInfo.addChannelList(new Channel("CH" + String.format("%02d", deviceInfo.getIntChannelNo().get(i)), 0, false, deviceInfo.getIntChannelNo().get(i)));
                    } else {
                        // channels.add(new Channel(tempChannelName, 0, false, deviceInfo.getIntChannelNo().get(i)));
                        deviceInfo.addChannelList(new Channel("", 0, false, deviceInfo.getIntChannelNo().get(i)));
                    }*/
                    deviceInfo.addChannelList(new Channel("", 0, false, deviceInfo.getIntChannelNo().get(i)));

                }
                if (deviceInfo.getChannelList().size() > 0)
                    devList.add(deviceInfo);
            }
            scene.setDevList(devList);
            //scene.setChannelList(channels);
        } catch (Exception e) {
            Log.e("eror", "add scene");
        }

        mScene = scene;
        updateSceneUiInfo();

        mRoomListAdapter.setScene(mScene);
        mRoomListAdapter.notifyDataSetChanged();

        // mAddSceneListAdapter.setSceneInfo(mScene);
        // mAddSceneListAdapter.notifyDataSetChanged();

    }

    private void testGetdata() {
        if (Config.DEBUG) {
            ArrayList<DeviceInfo> device = new ArrayList<>();
            DeviceInfo d1 = new DeviceInfo();
            ArrayList<String> name = new ArrayList<>();
            name.add("");
            name.add("");
            name.add("");
            name.add("myname1");
            name.add("myname2");
            d1.setChannelName(name);

            ArrayList<Integer> no = new ArrayList<>();
            no.add(1);
            no.add(2);
            no.add(3);
            no.add(4);
            no.add(5);
            d1.setIntChannelNo(no);

            device.add(d1);
            APService.mRegistedDevList = device;
        }
    }

    private void getExistingSceneInfo() {
        showDialog();
        APService.getSceneSingle(mScenenoForModify, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                SceneInfo scene = new SceneInfo();
                scene = (SceneInfo) retValue;

                if (scene == null)
                    return;

                String tempChannelName = "";

                scene.setIntSceneNo(mScenenoForModify);
                scene.setByteSceneNo(CmdHelper.intToByteArray(mScenenoForModify));
                if (Config.DEBUG)
                    testGetdata();
                // 채널리스트중에서 null 인값은 채널이름을 ch00으로 기본설정해서 보여준다.
                for (int i = 0; i < scene.getChannelList().size(); i++) {
                    tempChannelName = scene.getChannelList().get(i).getName();

                    //if (CmdHelper.checkNullName(tempChannelName) == 0) {
                    //    scene.getChannelList().get(i).setName("CH" + String.format("%02d", scene.getIntChannelNo().get(i)));
                   // }
                }

                // TODO must have test
                // if admin level, Include additional channels that are not selected.
                /*if (APService.getUserLevel()) {
                    for (DeviceInfo deviceInfo : APService.getApRegistedDevList())
                        for (int i = 0; i < deviceInfo.getIntChannelNo().size(); i++) {

                            if (!scene.compareChannelNo(deviceInfo.getIntChannelNo().get(i))) {
                                tempChannelName = deviceInfo.getChannelName().get(i);

                                if (CmdHelper.checkNullName(tempChannelName) == 0) {
                                    tempChannelName = "CH" + String.format("%02d", deviceInfo.getIntChannelNo().get(i));
                                }

                                Channel newChannel = new Channel(tempChannelName, 0, false, deviceInfo.getIntChannelNo().get(i));
                                scene.getChannelList().add(newChannel);
                            }
                        }
                }*/


                mScene = scene;

                ArrayList<DeviceInfo> deviceInfos = getChannelListInRoom(scene);

                mScene.setDevList(deviceInfos);

                updateSceneUiInfo();
                mRoomListAdapter.setScene(mScene);
                mRoomListAdapter.notifyDataSetChanged();

                //mAddSceneListAdapter.setSceneInfo(mScene);
                //mAddSceneListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode) {
                hideDialog();
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                hideDialog();
                Logout();

            }
        });

    }
    public void showDialog() {
        ((ContainActivity) getActivity()).createDialog();
    }

    public void hideDialog() {
        ((ContainActivity) getActivity()).hideDialog();
    }

    private ArrayList<DeviceInfo> getChannelListInRoom(SceneInfo info) {

        ArrayList<DeviceInfo> deviceInfos = new ArrayList<>();
        for (DeviceInfo deviceInfo : APService.getApRegistedDevList()) {

            if(deviceInfo.getIntChannelNo().size() <= 0)
                continue;

            int chNo = deviceInfo.getIntChannelNo().get(0);

            for (int h = 0; h < info.getChannelList().size(); h++) {
                if (chNo == info.getChannelList().get(h).getChannelNo()) {
                    String tempChannelName = "";
                    for (int j = 0; j < deviceInfo.getIntChannelNo().size(); j++) {
                        //tempChannelName = info.getChannelName().get(j);
                        if (info.getIntChannelNo().get(j) == 0)
                            continue;
                       /* if (CmdHelper.checkNullName(tempChannelName) == 0) {
                            // channels.add(new Channel("CH" + String.format("%02d", deviceInfo.getIntChannelNo().get(i)), 0, false, deviceInfo.getIntChannelNo().get(i)));
                            deviceInfo.addChannelList(new Channel("CH" + String.format("%02d", info.getIntChannelNo().get(h)), info.getIntChannelDiing().get(h), true, info.getIntChannelNo().get(h)));
                        } else {
                            // channels.add(new Channel(tempChannelName, 0, false, deviceInfo.getIntChannelNo().get(i)));
                            deviceInfo.addChannelList(new Channel(tempChannelName,info.getIntChannelDiing().get(h) , true, info.getIntChannelNo().get(h)));
                        }*/
                        deviceInfo.addChannelList(new Channel("",info.getIntChannelDiing().get(h) , true, info.getIntChannelNo().get(h)));
                    }
                    int value = 0;
                    if(info.getIntChannelDiing().get(h) != null)
                        value = info.getIntChannelDiing().get(h);

                    deviceInfo.setRoomDimingLevel(value);
                    deviceInfo.setCheckStat(true);
                    deviceInfos.add(deviceInfo);
                    break;
                }
            }
        }
        return deviceInfos;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_scene, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mMasterSeekbar = (SeekBar) view.findViewById(R.id.fragment_add_scene_seek_bar_master_dimming);

        // resizeSeekbarthumb(mMasterSeekbar, 1);
        //mBtnIConType = (ImageView) view.findViewById(R.id.fragment_add_scene_image_user_icon);
        ImageView imageSub = (ImageView) view.findViewById(R.id.fragment_add_scene_image_fade_sub);
        mEditName = (EditText) view.findViewById(R.id.fragment_add_scene_edit_scene_name);
        ImageView imageAdd = (ImageView) view.findViewById(R.id.fragment_add_scene_image_fade_add);
        mCheckHide = (CheckBox) view.findViewById(R.id.fragment_add_scene_hide_image);
        mTextFadeTime = (TextView) view.findViewById(R.id.fragment_add_scene_text_fade_value);

        mCheckAll = (CheckBox) view.findViewById(R.id.fragment_add_scene_layout_select_all);
        mCheckAll.setOnClickListener(this);
        ListView listView = (ListView) view.findViewById(R.id.fragment_add_scene_list_channel);
        mMasterSeekbar.setTag(R.id.id_fragment_add_scene_list, listView);
        mBtnDmxSetting = (RelativeLayout) view.findViewById(R.id.fragment_add_scene_dmx_setting);
        mBtnDmxSetting.setOnClickListener(this);
        //mBtnIConType.setOnClickListener(this);
        imageSub.setOnClickListener(this);
        imageAdd.setOnClickListener(this);
        mMasterSeekbar.setOnSeekBarChangeListener(this);
        // layoutSelectAll.setOnClickListener(this);
        //imageSave.setOnClickListener(this);
        listView.setAdapter(mAddSceneListAdapter);
        mRoomListView = (ListView) view.findViewById(R.id.listview_room_list);

        mRoomListAdapter = new RoomDimingListAdapter(getContext(), new SceneInfo());
        mRoomListView.setAdapter(mRoomListAdapter);

        mCheckHide.setTag(true);
        imageSub.setTag(mTextFadeTime);
        imageAdd.setTag(mTextFadeTime);
        // mBtnIConType.setTag(mScene.getIntIconType());
        mMasterSeekbar.setTag(view.findViewById(R.id.fragment_add_scene_text_master_dimming));

        mPrevPage = (RelativeLayout) view.findViewById(R.id.prev_page);
        mPrevPage.setOnClickListener(this);
        mNextPage = (RelativeLayout) view.findViewById(R.id.next_page);
        mNextPage.setOnClickListener(this);

        mLayoutStep1 = (LinearLayout) view.findViewById(R.id.layout_step1);
        mLayoutStep2 = (LinearLayout) view.findViewById(R.id.layout_step2);
        // setting default value
/*        setUserIconType(mScene.getIntIconType(), "");
        mEditName.setText(mScene.getSceneName());
        mEditName.setHint(mScene.getSceneName());
        mCheckHide.setChecked(mScene.getHideFlag());
        mTextFadeTime.setText(Integer.toString(mScene.getIntFadeTime()));*/

//        if (!APService.getUserLevel() && MODIFY_FLAG) {
//            //mBtnDmxSetting.setEnabled(false); 수정함. 볼수는 잇는데 저장은못하게 변경함.
//            // mBtnIConType.setEnabled(false);
//            imageSub.setEnabled(false);
//            imageAdd.setEnabled(false);
//            // mMasterSeekbar.setEnabled(false);
//            //layoutSelectAll.setEnabled(false);
//            mCheckAll.setEnabled(false);
//            mCheckHide.setEnabled(false);
//            mEditName.setEnabled(false);
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    private void resizeSeekbarthumb(final SeekBar mySeekbar, final double size) {
        ViewTreeObserver vto = mySeekbar.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Resources res = mContext.getResources();
                Drawable thumb = res.getDrawable(R.mipmap.img_master_seekbar_thumb);
                int h = (int) (mySeekbar.getMeasuredHeight() * size);
                int w = h;
                Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                mySeekbar.setThumb(newThumb);
                mySeekbar.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    private void setUserIconType(int iconindex, String _iconType) {
        try {
            // mBtnIConType.setImageResource(getIconId(iconindex));
        } catch (Exception e) {
            Log.e("error", "set user icon");
        }
    }

    boolean allSelect = false;


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fragment_add_scene_dmx_setting:
                DialogDmxSetting dmxsetting = new DialogDmxSetting();
                dmxsetting.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
                dmxsetting.setLisetner(mDmxValueListener);

                /*DmxInfo[] mDmxList = new DmxInfo[MAX_DMX_SIZE];
                for(int i = 0; i < MAX_DMX_SIZE; i++){
                    mDmxList[i] = new DmxInfo();
                    mDmxList[i].mIntValue = -22;
                }

                mScene.setDmxList(mDmxList);*/
                dmxsetting.setDmxInfo(mScene.getDmxList());
                dmxsetting.show(getActivity().getFragmentManager(), "dmxsetting");
                break;

            case R.id.fragment_add_scene_hide_image:
                boolean value = !(boolean) view.getTag();
                ((ImageView) view).setImageResource(value ? R.mipmap.icon_check_box_type_b_on : R.mipmap.icon_check_box_type_b_off);
                view.setTag(value);
                break;

            case R.id.fragment_add_scene_image_fade_add:
                if (Integer.parseInt(((TextView) view.getTag()).getText().toString()) > 9)
                    return;

                ((TextView) view.getTag()).setText(String.valueOf(Integer.parseInt(((TextView) view.getTag()).getText().toString()) + 1));
                break;

            case R.id.fragment_add_scene_image_fade_sub:
                if (Integer.parseInt(((TextView) view.getTag()).getText().toString()) == 0)
                    return;

                if (Integer.parseInt(((TextView) view.getTag()).getText().toString()) >= 0)
                    ((TextView) view.getTag()).setText(String.valueOf(Integer.parseInt(((TextView) view.getTag()).getText().toString()) - 1));
                break;

            case R.id.fragment_add_scene_layout_select_all:

                allSelect = !allSelect;
                // ((ImageView) view).setImageResource(allSelect ? R.mipmap.icon_check_box_type_b_on : R.mipmap.icon_check_box_type_b_off);
                mAddSceneListAdapter.onSelectChanged(allSelect);
                break;
            case R.id.prev_page:
                if (NOW_PAGE == STEP1)
                    getActivity().onBackPressed();
                /*else if (NOW_PAGE == STEP2)
                    showNowPage(STEP1);*/

                break;
            case R.id.next_page:
                if (NOW_PAGE == STEP1) {
                    String name = mEditName.getText().toString();
                    if (!checkValidation(name))
                        return;

                    if(makeChannelList() <= 0){
                        MyToast.getInstance(mContext).showToast("선택 된 룸이 없습니다.\n1개 이상의 룸을 선택해주세요.", com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }
                    saveSceneInfo();
                }
                    /*showNowPage(STEP2);
                else if (NOW_PAGE == STEP2)
                    saveSceneInfo();*/
                break;


        }
    }

    private int makeChannelList() {
        List<Channel> channels = new ArrayList<>();
        for (int i = 0; i < mScene.getDevList().size(); i++) {
            for (int j = 0; j < mScene.getDevList().get(i).getChannelList().size(); j++) {
                if (mScene.getDevList().get(i).getChannelList().get(j).getSelect())
                    channels.add(mScene.getDevList().get(i).getChannelList().get(j));
            }
        }
        mScene.setChannelList(channels);
        return channels.size();
    }

    public void showNowPage(int nowPage) {
        NOW_PAGE = nowPage;
        if (nowPage == STEP1) {
            mLayoutStep1.setVisibility(View.VISIBLE);
            mLayoutStep2.setVisibility(View.GONE);

            mEditName.requestFocus();
            //InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            //imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

        } else if (nowPage == STEP2) {
            mLayoutStep1.setVisibility(View.GONE);
            mLayoutStep2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        ((TextView) seekBar.getTag()).setText(i + "%");
        mAddSceneListAdapter.onProgressChanged(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        ListView listView = (ListView) seekBar.getTag(R.id.id_fragment_add_scene_list);
        int count = listView.getCount();
        for (int i = 0; i < count; i++)
            mScene.getChannelList().get(i).setDimming(seekBar.getProgress());

        int val = mMasterSeekbar.getProgress();
        byte[] bVal = new byte[1];
        bVal[0] = (byte) val;
        APService.setMasterDiming(new String(bVal), new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                mAddSceneListAdapter.setSceneInfo(mScene);
                mAddSceneListAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }

    public void saveSceneInfo() {
        String name = mEditName.getText().toString();
        name = name .trim();
        if (!checkValidation(name))
            return;

        if (mScene.getSelectCount() <= 0 && !APService.getDmxUseFlag()) {
            MyToast.getInstance(mContext).showToast(getString(R.string.msg_non_select_channel), com.rareraw.master.Common.Toast.LENGTH_SHORT);
            return;
        }

        // boolean dmxUserFlag = APService.getDmxUseFlag();
        boolean dmxUserFlag = false;
        mScene.setSceneName(name);
        mScene.setDmxUseFlag(dmxUserFlag);
        mScene.setHideFlag(mCheckHide.isChecked());
        mScene.setIntFadeTime(Integer.parseInt(mTextFadeTime.getText().toString()));
        ByteBuffer buffer = mScene.makeBuffer();
        if (buffer == null) {
            Log.e("register secene", "error(null)");
            return;
        }
        APService.registerScene(buffer, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                Log.e("register secene", "ok");
                if (Config.DEBUG)
                    Toast.makeText(mContext, "씬이 추가되었습니다.", Toast.LENGTH_SHORT).show();
                getActivity().onBackPressed();
            }

            @Override
            public void onError(int errorCode) {
                Log.e("register secene", "error");
                try {
                    if (Config.DEBUG)
                        Toast.makeText(mContext, "씬 추가에 실패하였습니다. : " + mScene.getIntSceneNo(), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                }
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });

    }
}
