package com.rareraw.master.addscene;

/**
 * Created by Lch on 2016-10-04.
 */
public interface OnDmxSettingValueListener {
    void onDmxSettingValueListener(DmxInfo[] list);
}
