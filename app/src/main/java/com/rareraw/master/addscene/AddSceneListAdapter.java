package com.rareraw.master.addscene;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.data.Channel;

import java.nio.ByteBuffer;

/**
 * Created by Lch on 2016-12-26.
 */

public class AddSceneListAdapter extends BaseAdapter implements View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private LayoutInflater mLayoutInflater;
    private SceneInfo mScene;
    private Context mContext;

    public AddSceneListAdapter(Context context, SceneInfo scene) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mScene = scene;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mScene.getChannelList().size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        AddSceneListHolder addSceneListHolder;
        if (view == null) {
            addSceneListHolder = new AddSceneListHolder();
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_add_scene, viewGroup, false);

            //addSceneListHolder.imageSelect = (ImageView) view.findViewById(R.id.item_list_fragment_add_scene_text_name);
            //addSceneListHolder.imageSelect.setOnClickListener(this);

            // addSceneListHolder.textName = (TextView) view.findViewById(R.id.item_list_fragment_add_scene_text_name);
            addSceneListHolder.chName = (CheckBox) view.findViewById(R.id.item_list_fragment_add_scene_text_name);

            addSceneListHolder.seekDimming = (SeekBar) view.findViewById(R.id.item_list_fragment_add_scene_seek_bar_dimming);
            addSceneListHolder.seekDimming.setOnSeekBarChangeListener(this);
            addSceneListHolder.seekDimming.setTag(R.id.id_item_list_add_scene_text_dimming, view.findViewById(R.id.item_list_fragment_add_scene_text_dimming));

            resizeSeekbarthumb(addSceneListHolder.seekDimming, 1.4);

        } else {
            addSceneListHolder = (AddSceneListHolder) view.getTag();
        }
        addSceneListHolder.chName.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mScene.getChannelList().get(i).setSelect(isChecked);
            }
        });

        Channel channel = mScene.getChannelList().get(i);
        addSceneListHolder.seekDimming.setTag(R.id.id_item_list_add_scene_index, i);


    /*  addSceneListHolder.imageSelect.setImageResource(channel.getSelect() ? R.mipmap.icon_select_type_b_on_large : R.mipmap.icon_select_type_b_off);
        addSceneListHolder.imageSelect.setTag(channel.getSelect());
        addSceneListHolder.imageSelect.setTag(R.id.id_item_list_add_scene_select_index , i);*/
        addSceneListHolder.chName.setChecked(channel.getSelect());


        // addSceneListHolder.textName.setText(channel.getName());
        addSceneListHolder.chName.setText(channel.getName());
        addSceneListHolder.seekDimming.setProgress(channel.getDimming());

        if (!APService.getUserLevel()) {
            // addSceneListHolder.imageSelect.setImageResource(channel.getSelect() ? R.mipmap.icon_select_type_b_on_large : R.mipmap.icon_select_type_b_off);
            addSceneListHolder.chName.setChecked(channel.getSelect());
            //addSceneListHolder.imageSelect.setEnabled(false);
            addSceneListHolder.chName.setEnabled(false);

        }
        view.setTag(addSceneListHolder);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
    /*        case R.id.item_list_fragment_add_scene_image_select:
                boolean value = !(boolean) view.getTag();
                int index = (int) view.getTag(R.id.id_item_list_add_scene_select_index);
                ((ImageView) view).setImageResource(value ? R.mipmap.icon_select_type_b_on_large : R.mipmap.icon_select_type_b_off);
                view.setTag(value);
                mScene.getChannelList().get(index).setSelect(value);
                break;*/
        }
    }

    public void onSelectChanged(boolean value) {
        for (int i = 0; i < mScene.getChannelList().size(); i++)
            mScene.getChannelList().get(i).setSelect(value);
        notifyDataSetChanged();
    }

    public void onProgressChanged(int value) {
        for (int i = 0; i < mScene.getChannelList().size(); i++)
            mScene.getChannelList().get(i).setDimming(value);
        notifyDataSetChanged();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        ((TextView) seekBar.getTag(R.id.id_item_list_add_scene_text_dimming)).setText(i + "%");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        ((TextView) seekBar.getTag(R.id.id_item_list_add_scene_text_dimming)).setText(seekBar.getProgress() + "%");
        int index = (int) seekBar.getTag(R.id.id_item_list_add_scene_index);
        int value = seekBar.getProgress();
        mScene.getChannelList().get(index).setDimming(value);
        // notifyDataSetChanged();
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.put((byte) mScene.getChannelList().get(index).getChannelNo());
        buffer.put((byte) seekBar.getProgress());

        APService.setSingleDiming(buffer);
    }

    public void resizeSeekbarthumb(final SeekBar mySeekbar, final double size) {
        ViewTreeObserver vto = mySeekbar.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Resources res = mContext.getResources();
                Drawable thumb = res.getDrawable(R.mipmap.img_single_seekbar_thumb);
                int h = (int) (mySeekbar.getMeasuredHeight() * size);
                int w = h;
                Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                mySeekbar.setThumb(newThumb);
                mySeekbar.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    public void setSceneInfo(SceneInfo _sceneInfo) {
        this.mScene = _sceneInfo;
    }

    static class AddSceneListHolder {
        ImageView imageSelect;
        TextView textName;
        CheckBox chName;
        SeekBar seekDimming;
    }
}
