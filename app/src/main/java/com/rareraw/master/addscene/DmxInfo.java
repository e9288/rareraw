package com.rareraw.master.addscene;

/**
 * Created by lch on 2017-01-25.
 */

public class DmxInfo {
    public int mNo = 0;
    public String mSVaue = "0x00";
    public int mIntValue = 0x00;
    public int mUseFlag = 0; // status define , 0: default, 1: select not save, 2: save
}
