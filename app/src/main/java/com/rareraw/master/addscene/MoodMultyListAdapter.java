package com.rareraw.master.addscene;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neoromax.feelmaster.R;
import com.rareraw.master.network.Cmd;

import java.util.ArrayList;

public class MoodMultyListAdapter extends BaseAdapter {

    private ArrayList<MoodListAdapter.moodInfomation> mSceneInfoList = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ArrayList<MoodListAdapter.moodInfomation> mMoodList;
    private Context mContext;

    public MoodMultyListAdapter(Context context, ArrayList<MoodListAdapter.moodInfomation> _scenelist) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mMoodList = _scenelist;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mMoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMoodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    CheckedTextView oldcheckedTextView;
    ImageView oldImagViee;
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null)
            view = mLayoutInflater.inflate(R.layout.item_list_view_scene_list, parent, false);

        //RadioButton sceneName = (RadioButton) view.findViewById(R.id.radio_scenename);
        //sceneName.setText(mMoodList.get(position).mSceneName);
        //sceneName.setChecked(mMoodList.get(position).mCheckStat);
        final ImageView moodTypeImg = (ImageView) view.findViewById(R.id.list_view_mood_type);
        moodTypeImg.setTag(R.id.list_view_mood_type, mMoodList.get(position).type);

        final RelativeLayout item = (RelativeLayout) view.findViewById(R.id.moode_layout);
        final CheckedTextView textchecked = (CheckedTextView) view.findViewById(R.id.textchecked);

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawMoodImage(oldImagViee, false);
                oldcheckedTextView = textchecked;
                oldImagViee = moodTypeImg;

                textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
                drawMoodImage(moodTypeImg, true);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMoodList.get(position).mCheckStat = true;
                notifyDataSetChanged();
            }
        });
        return view;
    }

    public void drawMoodImage(ImageView imgView, boolean type) {

        if (imgView != null) {
            int moodType = (int) imgView.getTag(R.id.list_view_mood_type);
            if (type) {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_on);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_on);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                }
            } else {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_off);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_off);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                }
            }
        }
    }

    public MoodListAdapter.moodInfomation getSelectedScene() {

        for (MoodListAdapter.moodInfomation info : mMoodList) {
            if (info.mCheckStat)
                return info;
        }
        return null;
    }

    public void setDefaultCheck() {
        try {
            mMoodList.get(0).mCheckStat = true;
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e("error", "empty list");
        }
    }
}
