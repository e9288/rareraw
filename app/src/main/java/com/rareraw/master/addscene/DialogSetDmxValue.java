package com.rareraw.master.addscene;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.Toast;

import android.app.DialogFragment;

/**
 * Created by Lch on 2016-10-12.
 */

public class DialogSetDmxValue extends DialogFragment {
    private EditText mTextValue;
    private TextView mBtnOk, mBtnCancel;
    private OnActionListener mListener;
    private int mIndex = 0;
    public DialogSetDmxValue() {
    }


    public void setListener(OnActionListener _listner){
        mListener = _listner;
    }

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return inflater.inflate(R.layout.dialog_set_dmx_value, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnOk = (TextView) view.findViewById(R.id.dialog_dmx_setting_ok) ;
        mBtnCancel = (TextView) view.findViewById(R.id.dialog_dmx_setting_cancel) ;
        mTextValue = (EditText)view.findViewById(R.id.text_content);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTextValue.getText().toString().length() == 0) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_input_null), Toast.LENGTH_SHORT);
                    return;
                }

                int value = Integer.parseInt(mTextValue.getText().toString());
                if(value > Config.DMX_MAX_VALUE){
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_input_overflow), Toast.LENGTH_SHORT );
                    return;
                }
                mListener.onActionListener(mIndex, Integer.toString(value));
                dismiss();
            }
        });
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setSelectedIndex(int indexNo) {
        mIndex = indexNo;
    }
}