package com.rareraw.master.addscene;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.neoromax.feelmaster.R;
import com.rareraw.master.data.Scene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import org.jsoup.nodes.TextNode;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class RoomDimingListAdapter extends BaseAdapter {

    private ArrayList<MoodListAdapter.moodInfomation> mSceneInfoList = new ArrayList<>();
    private LayoutInflater mLayoutInflater;
    private ArrayList<DeviceInfo> mRoomList;
    private Context mContext;
    private SceneInfo sceneInfo;
    public RoomDimingListAdapter(Context context, SceneInfo sceneInfo) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.sceneInfo = sceneInfo;
        this.mRoomList = sceneInfo.getDevList();
        this.mContext = context;
    }

    public void setRoomList(ArrayList<DeviceInfo> _scenelist){
        this.mRoomList = _scenelist;
    }
    @Override
    public int getCount() {
        return sceneInfo.getDevList().size();
    }

    @Override
    public Object getItem(int position) {
        return sceneInfo.getDevList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    CheckedTextView oldcheckedTextView;
    ImageView oldImagViee;
    RelativeLayout oldLayoutSelect;
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null)
            view = mLayoutInflater.inflate(R.layout.item_list_view_room_diming_list, parent, false);


        final ImageView moodTypeImg = (ImageView) view.findViewById(R.id.list_view_room_type);
        moodTypeImg.setTag(R.id.list_view_room_type, false);

        final TextView mTextDimingVal = (TextView) view.findViewById(R.id.text_diming);
        int dimiglevel = sceneInfo.getDevList().get(position).getRoomDimingLevel();
        mTextDimingVal.setText(Integer.toString(dimiglevel)+ "%");

        final RelativeLayout layoutSelect = (RelativeLayout) view.findViewById(R.id.layout_selete);
        final RelativeLayout item = (RelativeLayout) view.findViewById(R.id.room_layout);
        final TextView textchecked = (TextView) view.findViewById(R.id.textchecked);
        textchecked.setTag(R.id.textchecked, false);

        final TextView name  = (TextView) view.findViewById(R.id.list_view_room_name);

        String text = "";
        byte[] bDevName = sceneInfo.getDevList().get(position).getmByteDevName();
        int value = 0;
        if (bDevName != null) {
            for (byte b : bDevName) {
                value += (int) b;
            }
        }

        if (value == 0) {
            text = "ROOM " + Integer.toString(position +1);
        } else {
            try {
                text = new String(bDevName, "UTF-8");
            }catch (Exception e){

            }
        }
        name.setText(text);

        final SeekBar seeker = (SeekBar) view.findViewById(R.id.item_list_fragment_add_scene_seek_bar_dimming);

        seeker.setProgress(sceneInfo.getDevList().get(position).getRoomDimingLevel());
        seeker.setTag(R.id.id_item_list_add_scene_index, position);
        seeker.setTag(R.id.text_diming, mTextDimingVal);
        seeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                TextView textView = (TextView) seekBar.getTag(R.id.text_diming);
                textView.setText(String.valueOf(progress) + "%");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // ((TextView) seekBar.getTag(R.id.fragment_add_scene_seek_bar)).setText(seekBar.getProgress() + "%");
                int index = (int) seekBar.getTag(R.id.id_item_list_add_scene_index);
                int value = seekBar.getProgress();

                for(int i = 0; i < sceneInfo.getDevList().get(index).getChannelList().size() ; i++){
                    sceneInfo.getDevList().get(index).getChannelList().get(i).setDimming(value);
                }

              /*  if(sceneInfo.getDevList().get(index).getChannelList().size() >= 1){
                    for(int i = 0; i< sceneInfo.getDevList().get(index).getChannelList().size()  ; i++){
                        ByteBuffer buffer = ByteBuffer.allocate(2);
                        buffer.put((byte) sceneInfo.getDevList().get(index).getChannelList().get(i).getChannelNo());
                        //buffer.put((byte) mScene.getChannelList().get(index).getChannelNo());
                        buffer.put((byte) seekBar.getProgress());
                        APService.setSingleDiming(buffer);
                    }
                }*/

                byte[] mac = sceneInfo.getDevList().get(index).getDevMac();
                byte[] level = new byte[1];
                level[0] = (byte) seekBar.getProgress();
                final int finalDiming = seekBar.getProgress();
                Log.d("diming is ", Integer.toString(finalDiming));

                APService.setDeviceDiming(mac, level, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Log.d("diming setting ok ", "");
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d("diming setting fail ", "");
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                    }
                });

            }
        });

        if(sceneInfo.getDevList().get(position).getCheckStat()){

            moodTypeImg.setImageResource(R.mipmap.icon_device_blink_on);
            moodTypeImg.setTag(R.id.list_view_mood_type, true);

            textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
           // textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
            layoutSelect.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
            textchecked.setTag(R.id.textchecked, true);

        }else{
            moodTypeImg.setImageResource(R.mipmap.icon_device_blink_off);
            moodTypeImg.setTag(R.id.list_view_mood_type, false);

            textchecked.setTextColor(mContext.getResources().getColor(R.color.black_color));
            layoutSelect.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
          //  textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
            textchecked.setTag(R.id.textchecked, false);
        }


        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flag = (boolean) moodTypeImg.getTag(R.id.list_view_mood_type);
                if(flag){

                    moodTypeImg.setImageResource(R.mipmap.icon_device_blink_off);
                    moodTypeImg.setTag(R.id.list_view_mood_type, false);

                    textchecked.setTextColor(mContext.getResources().getColor(R.color.black_color));
                    // textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
                    layoutSelect.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
                    textchecked.setText("선택");
                    textchecked.setTag(R.id.textchecked, false);

                    for(int i = 0 ; i < sceneInfo.getDevList().get(position).getChannelList().size(); i++){
                        sceneInfo.getDevList().get(position).getChannelList().get(i).setSelect(false);
                    }
                    sceneInfo.getDevList().get(position).setCheckStat(false);
                }else{
                    moodTypeImg.setImageResource(R.mipmap.icon_device_blink_on);
                    moodTypeImg.setTag(R.id.list_view_mood_type, true);

                    textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
                    textchecked.setText("해제");
                    // textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                    layoutSelect.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                    textchecked.setTag(R.id.textchecked, true);
                    for(int i = 0 ; i < sceneInfo.getDevList().get(position).getChannelList().size(); i++){
                        sceneInfo.getDevList().get(position).getChannelList().get(i).setSelect(true);
                    }
                    sceneInfo.getDevList().get(position).setCheckStat(true);
                }
            }
        });

        return view;
    }


    public void setScene(SceneInfo sceneInfo) {
        this.sceneInfo = sceneInfo;
      //  this.mRoomList = sceneInfo.getDevList();
    }

/*

    public void onSelectChanged(boolean value) {
        for (int i = 0; i < mScene.getChannelList().size(); i++)
            mScene.getChannelList().get(i).setSelect(value);

        notifyDataSetChanged();
    }

    public void onProgressChanged(int value) {
        for (int i = 0; i < mScene.getChannelList().size(); i++)
            mScene.getChannelList().get(i).setDimming(value);
        notifyDataSetChanged();
    }
*/

}
