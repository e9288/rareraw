package com.rareraw.master.addscene;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.Toast;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class DialogDmxSetting extends DialogFragment {

    private final int DMX_MAX_SIZE = 42;
    private final int DMX_ARRAY_SIZE = 546;
    private final int START_PAGE = 1, END_PAGE = 13;
    private int CURRENT_PAGE = 1;
    private int DMX_NO = 0;
    private SeekBar mDmxSeeker;
    private DmxInfo[] mDmxInfoArray = new DmxInfo[Cmd.MAX_DMX_SIZE];
    private ArrayList<DmxInfo> mDmxList = new ArrayList<>();
    private AdapterDmx512 mAdapter;
    private GridView mGridView;
    private EditText mDmxValue;
    private ImageView mBtnPreview, mBtnNext;
    private TextView mTextCurPage, mBtnDmxSave, mBtnSave;
    private OnDmxSettingValueListener mListener;
    private View mParent;
    private View mGridViewHeight;
    private int mHeightSize = 0;
    private Context mContext;
    private final int NOT_SELECT = 0, SELECT_NOT_SAVE = 1, SAVED = 2;

    public DialogDmxSetting() {

    }

    private OnActionListener mOnDmxReturnListener = new OnActionListener() {
        @Override
        public void onActionListener(int _arrayIndex, String _value) {
            try {
                // Because dmx real start no at -1
                mDmxInfoArray[_arrayIndex - 1].mUseFlag = SAVED;
                mDmxInfoArray[_arrayIndex - 1].mIntValue = Integer.parseInt(_value);
                updateDmxUi();
            } catch (IndexOutOfBoundsException e) {
                Log.e("error", "indexoutofbound");
            } catch (Exception e) {
                Log.e("error", "error");
            }
        }
    };

    public static DialogDmxSetting newInstance() {
        DialogDmxSetting fragment = new DialogDmxSetting();
        return fragment;
    }

    public void setLisetner(OnDmxSettingValueListener _listener) {
        this.mListener = _listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.fragment_scene_dmx512, container, false);
        initDmxInfo();
        initView();
        return mParent;
    }

    private void initDmxInfo() {
        if (mDmxInfoArray[0] == null) {
            for (int index = 0; index < Cmd.MAX_DMX_SIZE; index++) {
                mDmxInfoArray[index] = new DmxInfo();
                mDmxInfoArray[index].mNo = index + 1;
            }
        }
    }

    private void updateDmxUi() {
        mAdapter.setDmxList(getDmxInitValue());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initView() {
        mGridView = (GridView) mParent.findViewById(R.id.grieview_dmxinfo);
        mTextCurPage = (TextView) mParent.findViewById(R.id.textview_curpage);
        mBtnPreview = (ImageView) mParent.findViewById(R.id.btn_prev);
        mDmxValue = (EditText) mParent.findViewById(R.id.etext_dmx_value);
        mBtnDmxSave = (TextView) mParent.findViewById(R.id.text_dmx_val_save);
        mDmxSeeker = (SeekBar) mParent.findViewById(R.id.setting_dmx_seekter);

        mDmxSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
           /*     TextView textView = (TextView) seekBar.getTag(R.id.id_fragment_channelcontrol_text_master_value);
                textView.setText(String.valueOf(progress) + "%");

                adapter.onProgressChanged(progress);*/
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                int value = seekBar.getProgress();
                String hexText = Integer.toString(value, 16);
                hexText = hexText.toUpperCase();
                mDmxValue.setText(hexText);

            }
        });

        if (!APService.getUserLevel()) {
            mDmxValue.setEnabled(false);
            mBtnDmxSave.setEnabled(false);
            mDmxSeeker.setEnabled(false);
            MyToast.getInstance(getActivity()).showToast(getString(R.string.disenable_dmx), Toast.LENGTH_SHORT);
        }
        mAdapter = new AdapterDmx512(getActivity(), mOnDmxReturnListener);
        mAdapter.setDmxList(getDmxInitValue());
        mGridView.setAdapter(mAdapter);

        mBtnDmxSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDmxValue.getText().toString().length() <= 0) {

                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_plz_enter_dmx_value), Toast.LENGTH_SHORT);
                    return;
                }
                int intValue = Integer.parseInt(mDmxValue.getText().toString(), 16);
                if (intValue > 255) {

                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_value), Toast.LENGTH_SHORT);
                    return;
                }

                intValue = Integer.parseInt( mDmxValue.getText().toString(), 16 );

                mAdapter.setDmxValue(intValue);

                //mListener.onDmxSettingValueListener(mDmxInfoArray);

            }
        });
        mBtnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("current page : ", Integer.toString(CURRENT_PAGE));
                if (CURRENT_PAGE == START_PAGE)
                    return;

                CURRENT_PAGE--;
                mTextCurPage.setText(Integer.toString(CURRENT_PAGE));
                updateDmxUi();
            }
        });

        mBtnNext = (ImageView) mParent.findViewById(R.id.btn_next);
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("current page : ", Integer.toString(CURRENT_PAGE));
                if (CURRENT_PAGE == END_PAGE)
                    return;
                CURRENT_PAGE++;
                mTextCurPage.setText(Integer.toString(CURRENT_PAGE));
                updateDmxUi();
            }
        });

        mBtnSave = (TextView) mParent.findViewById(R.id.btn_save);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!APService.getUserLevel())
                    dismiss();

                mListener.onDmxSettingValueListener(mDmxInfoArray);
                MyToast.getInstance(mContext).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT);
                dismiss();
            }
        });
    }

    private ArrayList<DmxInfo> getDmxInitValue() {
        int startNo = (CURRENT_PAGE * 42) - 41;
        mDmxList.clear();
        try {
            for (int i = startNo - 1; i < (DMX_MAX_SIZE + startNo) - 1; i++) {
                mDmxList.add(mDmxInfoArray[i]);
            }
        } catch (IndexOutOfBoundsException e) {
            return mDmxList;
        }
        return mDmxList;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     /*   if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void setDmxInfo(DmxInfo[] dmxList) {
        try {
            for (int i = 0; i < Cmd.MAX_DMX_SIZE; i++) {
                mDmxInfoArray[i] = dmxList[i];
            }
        } catch (Exception e) {
            Log.e("error", "setdmxinfo");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public class AdapterDmx512 extends BaseAdapter implements View.OnClickListener {

        private LayoutInflater mLayoutInflater;
        private ArrayList<DmxInfo> mDmxInfoList = new ArrayList<>();
        private OnActionListener mLinstener;

        public AdapterDmx512(Context context, OnActionListener _listner) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mLinstener = _listner;
            //mDmxInfoList = _dmxlist;
        }

        @Override
        public int getCount() {
            return mDmxInfoList.size();
        }

        @Override
        public Object getItem(int i) {

            return mDmxInfoList.get(1);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            DmxHolder holder = new DmxHolder();

            if (view == null) {
                view = mLayoutInflater.inflate(R.layout.item_gridview_dmx512, viewGroup, false);

                holder.no = (TextView) view.findViewById(R.id.text_no);
                holder.value = (TextView) view.findViewById(R.id.text_value);
                holder.background = view;

            } else
                holder = (DmxHolder) view.getTag();

            try {
                holder.no.setText(Integer.toString(mDmxInfoList.get(i).mNo));
                String sValue = String.format("%02d", mDmxInfoList.get(i).mIntValue);
                //String sValue = String.format("%02X", mDmxInfoList.get(i).mIntValue);

                holder.value.setTypeface(null, Typeface.NORMAL);

                holder.value.setText(sValue);
                if (mDmxInfoList.get(i).mIntValue > 0)
                    holder.value.setTypeface(null, Typeface.BOLD);
                if (mDmxInfoList.get(i).mUseFlag == SAVED) {
                    holder.background.setBackgroundResource(R.drawable.background_all_round_accent3);
                   // holder.background.setBackgroundColor(getResources().getColor(R.color.background_accent));

                    // holder.value.setTypeface(null, Typeface.BOLD);
                    mDmxInfoList.get(i).mUseFlag = NOT_SELECT;
                } else if (mDmxInfoList.get(i).mUseFlag == SELECT_NOT_SAVE)
                    holder.background.setBackgroundResource(R.drawable.background_all_round_white);
                    //holder.background.setBackgroundColor(getResources().getColor(R.color.whit_color));
                else if (mDmxInfoList.get(i).mUseFlag == NOT_SELECT)
                    holder.background.setBackgroundResource(R.drawable.background_all_round_accent3);
                   // holder.background.setBackgroundColor(getResources().getColor(R.color.background_accent));



                view.setTag(R.id.text_no, mDmxInfoList.get(i).mNo);
                holder.background.setOnClickListener(this);
            } catch (Exception e) {
                e.printStackTrace();
                return view;
            }
            view.setTag(holder);
            view.setTag(R.id.text_no, mDmxInfoList.get(i).mNo);
            return view;
        }

        public void setDmxList(ArrayList<DmxInfo> _dmxInitValue) {
            mDmxInfoList = _dmxInitValue;
        }

        @Override
        public void onClick(View view) {

            try {
                int indexNo = (int) view.getTag(R.id.text_no);
                int remain = (indexNo % 42 != 0) ? indexNo % 42 : 42;

                if (mDmxInfoList.get(remain - 1).mUseFlag == SELECT_NOT_SAVE)
                    mDmxInfoList.get(remain - 1).mUseFlag = NOT_SELECT;
                else if (mDmxInfoList.get(remain - 1).mUseFlag == NOT_SELECT)
                    mDmxInfoList.get(remain - 1).mUseFlag = SELECT_NOT_SAVE;
                else if (mDmxInfoList.get(remain - 1).mUseFlag == SAVED)
                    mDmxInfoList.get(remain - 1).mUseFlag = SELECT_NOT_SAVE;

           /*     int index = ((CURRENT_PAGE -1) * 42) + (remain - 1);
                if(mDmxInfoArray[index ].mUseFlag  == NOT_SELECT || mDmxInfoArray[index ].mUseFlag  == SAVED)
                    mDmxInfoArray[index ].mUseFlag = SELECT_NOT_SAVE;
                else if (mDmxInfoArray[index ].mUseFlag  == SELECT_NOT_SAVE)
                    mDmxInfoArray[index ].mUseFlag = NOT_SELECT;*/
            } catch (Exception e) {
                e.printStackTrace();
            }
            notifyDataSetChanged();
        }

        public void setDmxValue(int value) {
            for (int i = 0; i < mDmxInfoList.size(); i++) {
                if (mDmxInfoList.get(i).mUseFlag == SELECT_NOT_SAVE) {
                    mDmxInfoList.get(i).mUseFlag = SAVED;
                    mDmxInfoList.get(i).mIntValue = value;
                }
            }

            for (int i = 0; i < mDmxInfoArray.length; i++) {
                if (mDmxInfoArray[i].mUseFlag == SELECT_NOT_SAVE) {
                    mDmxInfoArray[i].mUseFlag = SAVED;
                    mDmxInfoArray[i].mIntValue = value;
                }
            }

            ByteBuffer buffer = ByteBuffer.allocate(Cmd.MAX_DMX_SIZE);
            ArrayList<DmxInfo> infose = getDmxInitValue();


            for(int i = 0; i < Cmd.MAX_DMX_SIZE;  i++){

                buffer.put((byte) mDmxInfoArray[i].mIntValue);
            }

            APService.setDmxVal( buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            notifyDataSetChanged();
                            Log.e("setDmx", "ok");
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.e("setDmx", "fail");
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Log.e("setDmx", "fail");
                        }
                    });

        }

    }

    public class DmxHolder {
        TextView no;
        TextView value;
        View background;
    }
}

