package com.rareraw.master.addscene;

import android.content.Context;
import android.graphics.Color;
import android.nfc.Tag;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.SceneInfo;

import java.util.ArrayList;

public class MoodListAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private ArrayList<moodInfomation> mMoodList;
    private Context mContext;
    private OnActionListener mListener;
    public MoodListAdapter(Context context, ArrayList<moodInfomation> _scenelist, OnActionListener listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mMoodList = _scenelist;
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return mMoodList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMoodList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    CheckedTextView oldcheckedTextView;
    ImageView oldImagViee;
    RelativeLayout oldSelectBtn;

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        if (view == null)
            view = mLayoutInflater.inflate(R.layout.item_list_view_scene_list, parent, false);

        //RadioButton sceneName = (RadioButton) view.findViewById(R.id.radio_scenename);
        //sceneName.setText(mMoodList.get(position).mSceneName);
        //sceneName.setChecked(mMoodList.get(position).mCheckStat);
        final ImageView moodTypeImg = (ImageView) view.findViewById(R.id.list_view_mood_type);
        moodTypeImg.setTag(R.id.list_view_mood_type, mMoodList.get(position).type);
        if (mMoodList.get(position).type == Cmd.NORMAL_SCENE) {
            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood_off);
        } else if (mMoodList.get(position).type == Cmd.PLAY_SCENE) {
            moodTypeImg.setImageResource(R.mipmap.icon_play_mood_off);
        } else if (mMoodList.get(position).type == Cmd.SCHEDULE_SCENE) {
            moodTypeImg.setImageResource(R.mipmap.icon_schedule_mood_off);
        } else {
            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood_off);
        }

        final TextView textName = (TextView) view.findViewById(R.id.text_mood_name);
        textName.setText(mMoodList.get(position).mSceneName);

        final RelativeLayout item = (RelativeLayout) view.findViewById(R.id.moode_layout);
        final CheckedTextView textchecked = (CheckedTextView) view.findViewById(R.id.textchecked);
        final RelativeLayout layoutSelect = (RelativeLayout) view.findViewById(R.id.layer_select_btn);

        if(mMoodList.get(position).mCheckStat){

            oldcheckedTextView = textchecked;
            oldImagViee = moodTypeImg;
            oldSelectBtn = layoutSelect;

            textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
            layoutSelect.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
            drawMoodImage(moodTypeImg, true);
            mListener.onActionListener(mMoodList.get(position).mSceneNo, mMoodList.get(position).mSceneName);
          //  mMoodList.get(position).mCheckStat = true;
        }
        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetSelectAll();
                drawMoodImage(oldImagViee, false);
                oldcheckedTextView = textchecked;
                oldImagViee = moodTypeImg;
                oldSelectBtn = layoutSelect;

                textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));

                layoutSelect.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                drawMoodImage(moodTypeImg, true);
                mMoodList.get(position).mCheckStat = true;
                // textchecked.setChecked(true);
                mListener.onActionListener(mMoodList.get(position).mSceneNo, mMoodList.get(position).mSceneName);
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(mContext, "position" + Integer.toString(position), Toast.LENGTH_SHORT).show();
                for (moodInfomation info : mMoodList) {
                    info.mCheckStat = false;
                }
                mMoodList.get(position).mCheckStat = true;
                notifyDataSetChanged();
            }
        });
        return view;
    }

    private void resetSelectAll() {
        for (moodInfomation info : mMoodList)
            info.mCheckStat = false;

        if (oldSelectBtn != null)
            oldSelectBtn.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.blue_selector_left));

        if (oldcheckedTextView != null) {
            //oldcheckedTextView.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
            oldcheckedTextView.setTextColor(mContext.getResources().getColor(R.color.black_color));
        }

        if (oldImagViee != null) {
            int moodType = (int) oldImagViee.getTag(R.id.list_view_mood_type);
            if (moodType == Cmd.NORMAL_SCENE) {
                oldImagViee.setImageResource(R.mipmap.icon_normal_mood_off);
            } else if (moodType == Cmd.PLAY_SCENE) {
                oldImagViee.setImageResource(R.mipmap.icon_play_mood_off);
            } else if (moodType == Cmd.SCHEDULE_SCENE) {
                oldImagViee.setImageResource(R.mipmap.icon_schedule_mood_off);
            } else {
                oldImagViee.setImageResource(R.mipmap.icon_normal_mood_off);
            }
        }
    }

    public void drawMoodImage(ImageView imgView, boolean type) {

        if (imgView != null) {
            int moodType = (int) imgView.getTag(R.id.list_view_mood_type);
            if (type) {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_on);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_on);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_on);
                }
            } else {
                if (moodType == Cmd.NORMAL_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                } else if (moodType == Cmd.PLAY_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_play_mood_off);
                } else if (moodType == Cmd.SCHEDULE_SCENE) {
                    imgView.setImageResource(R.mipmap.icon_schedule_mood_off);
                } else {
                    imgView.setImageResource(R.mipmap.icon_normal_mood_off);
                }
            }
        }
    }

    public moodInfomation getSelectedScene() {

        for (moodInfomation info : mMoodList) {
            if (info.mCheckStat)
                return info;
        }
        return null;
    }

    public void setMoodList(ArrayList<moodInfomation> _moodList){
        mMoodList = _moodList;
    }

    public void setDefaultCheck() {
        try {
            mMoodList.get(0).mCheckStat = true;
            notifyDataSetChanged();
        } catch (Exception e) {
            Log.e("error", "empty list");
        }
    }


    public static class moodInfomation {
        public int mSceneNo;
        public String mSceneName;
        public int type;
        public boolean mCheckStat = false;

        public moodInfomation(int _sceneno, String _name, int type) {
            this.mSceneNo = _sceneno;
            this.mSceneName = _name;
            this.type = type;
        }
    }
}
