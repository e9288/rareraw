package com.rareraw.master.network.DataClass;

import android.util.Log;

import com.rareraw.master.addscene.DmxInfo;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.data.Channel;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lch on 2016-10-08.
 */

public class SceneInfo {
    private final static String TAG = "SceneInfo";
    private DmxInfo[] mDmxList = new DmxInfo[Cmd.MAX_DMX_SIZE];

    public boolean isSelectedFlag() {
        return selectedFlag;
    }

    public void setSelectedFlag(boolean selectedFlag) {
        this.selectedFlag = selectedFlag;
    }

    public ArrayList<DeviceInfo> getDevList() {
        return devList;
    }

    public void setDevList(ArrayList<DeviceInfo> devList) {
        this.devList = devList;
    }

    private ArrayList<DeviceInfo> devList = new ArrayList<>();
    private boolean selectedFlag = false;
    public byte[] mByteSceneNo;
    public int mIntSceneNo;

    public String mSceneName;
    private boolean mDmxUseFlag;

    public byte[] mByteSceneName;

    public int mIntSceneType;
    public byte[] mByteSceneType;

    public byte[] mByteIconType;
    public int mIntIconType;

    public byte[] mByteHideFlag;
    public boolean mBoolHideFlag;

    public byte[] mByteChannelCnt;
    public int mIntChannelCnt;

    public int mIntIndex;
    public byte[] mByteIndex;

    public int mIntFadeTime = 1;
    public byte[] mByteFadeTime;

    public int mIntHoldingTime = 1;
    public byte[] mByteHoldintTime;

    public ArrayList<byte[]> mByteChannelNo = new ArrayList<>();
    public ArrayList<Integer> mIntChannelNo = new ArrayList<>();

    public ArrayList<byte[]> mByteChannelName = new ArrayList<>();

    public ArrayList<byte[]> getByteChannelName() {
        return mByteChannelName;
    }

    public ArrayList<String> mChannelName = new ArrayList<>();
    public ArrayList<Boolean> mChannelUse = new ArrayList<>();

    public ArrayList<byte[]> mByteChannelDiming = new ArrayList<>();
    public ArrayList<Integer> mIntChannelDiing = new ArrayList<>();

    public List<Channel> mChannelList = new ArrayList<>();

    public DmxInfo[] getDmxList() {
        return mDmxList;
    }
    public boolean getDmxUseStat(){
        return mDmxUseFlag;
    }
    public void setDmxUseFlag(boolean stat){
        mDmxUseFlag = stat;
    }
    public void setDmxList(DmxInfo[] _dmxList) {
        this.mDmxList = _dmxList;
    }
    public void setByteChannelName(ArrayList<byte[]> _byteChannelName) {
        this.mByteChannelName = _byteChannelName;
    }


    public byte[] getByteSceneName() {
        return mByteSceneName;
    }

    public void setByteSceneName(byte[] _byteSceneName) {
        this.mByteSceneName = _byteSceneName;
    }

    public byte[] getByteSceneNo() {
        return mByteSceneNo;
    }

    public void setByteSceneNo(byte[] _byteSceneNo) {
        this.mByteSceneNo = _byteSceneNo;
    }

    public int getIntSceneNo() {
        return mIntSceneNo;
    }

    public void setIntSceneNo(int _intSceneNo) {
        this.mIntSceneNo = _intSceneNo;
    }

    public String getSceneName() {
        return mSceneName;
    }

    public void setSceneName(String _sceneName) {
        this.mSceneName = _sceneName;
    }

    public int getIntSceneType() {
        return mIntSceneType;
    }

    public void setIntSceneType(int _intSceneType) {
        this.mIntSceneType = _intSceneType;
    }

    public byte[] getByteSceneType() {
        return mByteSceneType;
    }

    public void setByteSceneType(byte[] _byteSceneType) {
        this.mByteSceneType = _byteSceneType;
    }

    public byte[] getByteIconType() {
        return mByteIconType;
    }

    public void setByteIconType(byte[] _byteIconType) {
        this.mByteIconType = _byteIconType;
    }

    public int getIntIconType() {
        return mIntIconType;
    }

    public void setIntIconType(int _intIconType) {
        this.mIntIconType = _intIconType;
    }

    public byte[] getByteHideFlag() {
        return mByteHideFlag;
    }

    public void setmByteHideFlag(byte[] _byteHideFlag) {
        this.mByteHideFlag = _byteHideFlag;
    }

    public boolean getHideFlag() {
        return mBoolHideFlag;
    }

    public void setHideFlag(boolean _boolHideFlag) {
        this.mBoolHideFlag = _boolHideFlag;
    }

    public byte[] getByteChannelCnt() {
        return mByteChannelCnt;
    }

    public void setByteChannelCnt(byte[] _byteChannelCnt) {
        this.mByteChannelCnt = _byteChannelCnt;
    }

    public int getIntChannelCnt() {
        return mIntChannelCnt;
    }

    public void setIntChannelCnt(int _intChannelCnt) {
        this.mIntChannelCnt = _intChannelCnt;
    }

    public int getIntIndex() {
        return mIntIndex;
    }

    public void setIntIndex(int _intIndex) {
        this.mIntIndex = _intIndex;
    }

    public byte[] getByteIndex() {
        return mByteIndex;
    }

    public void setByteIndex(byte[] _byteIndex) {
        this.mByteIndex = _byteIndex;
    }

    public int getIntFadeTime() {
        return mIntFadeTime;
    }

    public void setIntFadeTime(int _intFadeTime) {
        this.mIntFadeTime = _intFadeTime;
    }

    public byte[] getByteFadeTime() {
        return mByteFadeTime;
    }

    public void setByteFadeTime(byte[] _byteFadeTime) {
        this.mByteFadeTime = _byteFadeTime;
    }

    public int getIntHoldingTime() {
        return mIntHoldingTime;
    }

    public void setIntHoldingTime(int _intHoldingTime) {
        this.mIntHoldingTime = _intHoldingTime;
    }

    public byte[] getByteHoldintTime() {
        return mByteHoldintTime;
    }

    public void setByteHoldintTime(byte[] _byteHoldintTime) {
        this.mByteHoldintTime = _byteHoldintTime;
    }

    public ArrayList<byte[]> getByteChannelNo() {
        return mByteChannelNo;
    }

    public void setByteChannelNo(ArrayList<byte[]> _byteChannelNo) {
        this.mByteChannelNo = _byteChannelNo;
    }

    public ArrayList<Integer> getIntChannelNo() {
        return mIntChannelNo;
    }

    public void setIntChannelNo(ArrayList<Integer> _intChannelNo) {
        this.mIntChannelNo = _intChannelNo;
    }

    public ArrayList<String> getChannelName() {
        return mChannelName;
    }

    public void setChannelName(ArrayList<String> _channelName) {
        this.mChannelName = _channelName;
    }

    public ArrayList<Boolean> getChannelUse() {
        return mChannelUse;
    }

    public void setChannelUse(ArrayList<Boolean> _channelUse) {
        this.mChannelUse = _channelUse;
    }

    public ArrayList<byte[]> getByteChannelDiming() {
        return mByteChannelDiming;
    }

    public void setByteChannelDiming(ArrayList<byte[]> _byteChannelDiming) {
        this.mByteChannelDiming = _byteChannelDiming;
    }

    public ArrayList<Integer> getIntChannelDiing() {
        return mIntChannelDiing;
    }

    public void setIntChannelDiing(ArrayList<Integer> _intChannelDiing) {
        this.mIntChannelDiing = _intChannelDiing;
    }

    public void setChannelList(List<Channel> channels) {
        this.mChannelList = channels;
    }

    public List<Channel> getChannelList() {
        return this.mChannelList;
    }

    public ByteBuffer makeBuffer() {
        int count = 0;
        ByteBuffer buffer = null;
        String fullName = this.getSceneName();
        int length = 16 - this.getSceneName().getBytes().length;
        byte[] empty = new byte[length];
        fullName = fullName + new String(empty);


        for(int i = 0; i < mChannelList.size(); i++){
            count += (mChannelList.get(i).getSelect() ? 1 : 0);
        }

        try {
            buffer = ByteBuffer.allocate(Cmd.SCENE_DATA_SIZE + count * Cmd.SCENE_REPEAT_DATA_SIZE);
            buffer.put(CmdHelper.intToByteArray(this.getIntSceneNo()));
            buffer.put(fullName.getBytes());
            buffer.put(CmdHelper.intToByteArray(this.getIntIndex()));

            buffer.put((byte) this.getIntIconType());
            buffer.put((byte) ((this.getHideFlag()) ? 1 : 0));
            buffer.put((byte) this.getIntFadeTime());
            buffer.put((byte) count);
            for (Channel channel : this.getChannelList()) {
                if (channel.getSelect()) {
                    buffer.put((byte) channel.getChannelNo());
                    buffer.put((byte) channel.getDimming());
                }
            }
            buffer.put((APService.getDmxUseFlag() == true)? (byte)0x01:(byte)0x00);
            if( mDmxList[0] == null){
                byte[] emptyDmxInfo = new byte[512];
                buffer.put(emptyDmxInfo);
            }else{
                for(int i = 0; i < Cmd.MAX_DMX_SIZE; i++)
                    buffer.put((byte)mDmxList[i].mIntValue);
            }

        } catch (Exception e) {
            Log.e("error", "make scene buffer");
            return null;
        }
        return buffer;
    }

    public int getSelectCount() {
        int count= 0;
        for(int i = 0; i < mChannelList.size(); i++) {
            count += (mChannelList.get(i).getSelect() ? 1 : 0);
        }
        return count;
    }

    public boolean isCheckedChannel(int _channelNo) {
        List<Channel> list = getChannelList();
        for(int i = 0;  i < getChannelList().size();i++){
            int channelNo = list.get(i).getChannelNo();
            if(channelNo == _channelNo )
                return true;

        }
        return false;
    }

    public boolean compareChannelNo(int channelNo) {
        for(Channel ch : getChannelList())
            if(ch.getChannelNo() == channelNo)
                return true;

        return false;
    }
}