package com.rareraw.master.network;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;

/**
 * Created by lch on 2016-10-06.
 */

public class ApPoster extends AsyncTask<ByteBuffer, Integer, ByteBuffer> {
    private Socket mSocket;
    private String mIpAddress = "";

    private int mPortNo =  Cmd.PORT_NO;
    private boolean mConnected = false;
    private DataOutputStream mBuffOut = null;
    private DataInputStream mBufferIn = null;
    private byte[] mReadBuffer = new byte[Cmd.READ_BUFFER_SIZE];
    private ByteBuffer mReturnData = null;
    private int mTimeout = 3500; // ms
    private Context mContext;
    private boolean mApMode;
    private byte[] CMD = new byte[]{(byte) 0x00, (byte) 0x00};
    private Handler handler = null;
    private NetworkResultLisnter lisnter;
    public ApPoster() {

    }

    public ApPoster(Context _context) {
        mContext = _context;
    }

    public ApPoster(Handler handler, String ipAddress ){
        this.handler = handler;
        this.mIpAddress = ipAddress;
    }


    @Override
    protected void onPreExecute() {
     //   mDlg = new ProgressCircle(mContext);
     //   mDlg.show();

        super.onPreExecute();
    }

    public ApPoster(Context _context, byte[] cmd) {
        mContext = _context;
        CMD = cmd;
    }

    public ApPoster(Context _context, byte[] cmd, NetworkResultLisnter lisnter) {
        mContext = _context;
        CMD = cmd;
        this.lisnter = lisnter;
    }


    private boolean connect(String addr, int port) {
        try {
            InetSocketAddress socketAddress = new InetSocketAddress(InetAddress.getByName(addr), port);

            mSocket = new Socket();
            mSocket.connect(socketAddress, mTimeout);
            Log.d("sokect : ", "connect try");
        } catch (IOException e) {

            e.printStackTrace();
            Log.d("sokect error: ", "connect try fail");
            Message message = Message.obtain(handler, Cmd.FAIL_CONNECT);
            message.obj = Cmd.FAIL_CONNECT;
            message.sendToTarget();
            return false;
        }
        return true;
    }

    synchronized public boolean isConnected() {
        return mConnected;
    }

    @Override
    protected synchronized ByteBuffer doInBackground(ByteBuffer... ByteBuffer) {

        if (!connect(mIpAddress, mPortNo)) {
            Log.e("sokect error", "connection err");
            return null;
        }
        if (mSocket == null) {
            Log.e("sokect error", "connection null");
            return null;
        }
        mConnected = true;
        try {
            mBuffOut = new DataOutputStream(mSocket.getOutputStream());
            mBufferIn = new DataInputStream(mSocket.getInputStream());
            Log.d("sokect", "socket_thread loop started");

            // send to ap
            mBuffOut.write(ByteBuffer[0].array());
            mBuffOut.flush();

            // read data
            int vaildSize = mBufferIn.read(mReadBuffer);

            mReturnData = allocateBuffer(vaildSize);
            //mReturnData.allocate(vaildSize);
            mReturnData.put(mReadBuffer, 0, vaildSize); // from zero position to valid data size !

            Log.d("socket", "socket_thread loop terminated");
            return mReturnData;

        } catch (Exception e) {

            Log.e("sokect error", "buffer write,read error");
            e.printStackTrace();
            Message message = Message.obtain(handler, Cmd.FAIL_CODE);
            message.obj = Cmd.FAIL_CODE;
            message.sendToTarget();
        }
        try {
            mBuffOut.close();
            mBufferIn.close();
        } catch (IOException e) {
            Log.e("sokect error", "buffer close error");
            e.printStackTrace();
            Message message = Message.obtain(handler, Cmd.FAIL_CODE);
            message.obj = Cmd.FAIL_CODE;
            message.sendToTarget();
        }
        mConnected = false;
        return null;
    }


    public synchronized ByteBuffer allocateBuffer(int size) {
        // ByteBuffer is not sychronization type in thread
        ByteBuffer buf = ByteBuffer.allocate(size);
        return buf;
    }

    /*    @Override
        protected void onPostExecute(ByteBuffer s) {
            super.onPostExecute(s);
        }*/
    @Override
    protected void onPostExecute(ByteBuffer result) {
        if(result != null) {
            Log.d("post success: ", "post ok");
            Message message = Message.obtain(handler, Cmd.OK_CODE);
            message.obj = result;
            message.sendToTarget();
        }
    }

}
