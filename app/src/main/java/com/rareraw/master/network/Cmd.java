package com.rareraw.master.network;

/**
 * Created by lch on 2016-10-06.
 */

public class Cmd {

    /*network define*/
    public static int PORT_NO = 40100;
    // public static String DEFAULT_IP = "10.10.10.254"; // feelmaster
    public static String DEFAULT_IP = "192.168.1.1"; // rareraw

    /*common*/
    public static byte[] PRTC_SOP = new byte[]{(byte) 0xfe, (byte) 0x02}; // start bit
    public static byte[] PRRC_EOP = new byte[]{(byte) 0xfa, (byte) 0x0d}; // end bit
    public static int READ_BUFFER_SIZE = 12000;
    public static String EMPTY_DATA = "\0";

    /*command connect*/
    public static byte[] CMD_CONNECT        = new byte[]{(byte) 0xc1, (byte) 0xa1};            // connect cmd
    public static byte[] CMD_BROADCASR       = new byte[]{(byte) 0xc1, (byte) 0xa4};            // connect broadcase
    public static byte[] CMD_CONNECT_ADMIN  = new byte[]{(byte) 0xc1, (byte) 0xa0};      // admin connect cmd

    /*command device*/
    public static byte[] CMD_UNREG_DEV_LIST = new byte[]{(byte) 0xc2, (byte) 0xd0};    // get unregisterd device list
    public static byte[] CMD_REG_DEVICE     = new byte[]{(byte) 0xc2, (byte) 0xd1};    // register device
    public static byte[] CMD_DELETE_DEVICE  = new byte[]{(byte) 0xc2, (byte) 0xd2};    // register device
    public static byte[] CMD_REG_DEV_LIST   = new byte[]{(byte) 0xc2, (byte) 0xd3};    // get registerd device list
    public static byte[] CMD_DEVICE_RESET   = new byte[]{(byte) 0xc2, (byte) 0xd7};    // device in reset
    public static byte[] CMD_CHECK_DEVICE   = new byte[]{(byte) 0xc2, (byte) 0xd8};    // check device channel ( twinkle led )
    public static byte[] CMD_SET_DEVICE_NAME   = new byte[]{(byte) 0xc2, (byte) 0xd4};    // check device channel ( twinkle led )


    /*command diming*/
    public static byte[] CMD_SET_SINGLE_DIMING  = new byte[]{(byte) 0xc3, (byte) 0xc1};    // check device channel ( twinkle led )
    public static byte[] CMD_CURTAIN_OPEN_CLOSE  = new byte[]{(byte) 0xc3, (byte) 0xc7};    // curtain control
    public static byte[] CMD_SET_MASTER_DIMING  = new byte[]{(byte) 0xc3, (byte) 0xc8};    // check device channel ( twinkle led )
    public static byte[] CMD_SET_CHANNEL_NAME   = new byte[]{(byte) 0xc3, (byte) 0xca};    // change channel name
    public static byte[] CMD_SET_SCENE_DIMING_LEVEL   = new byte[]{(byte) 0xc3, (byte) 0xcb};    // set scene diming level
    public static byte[] CMD_GET_SCENE_DIMING_LEVEL   = new byte[]{(byte) 0xc3, (byte) 0xc9};    // request scene diming level
    // public static byte[] CMD_SET_RGB_COLOR      = new byte[]{(byte) 0xc3, (byte) 0xcc};    // set rgb color for smarthome

    /*command scene*/
    public static byte[] CMD_SET_SCENE    = new byte[]{(byte) 0xc4, (byte) 0x20};         // setting scene
    public static byte[] CMD_REG_SCENE    = new byte[]{(byte) 0xc4, (byte) 0x21};         // register scene
    public static byte[] CMD_REG_SCENE_PLAY= new byte[]{(byte) 0xc4, (byte) 0x22};        // register scene play
    public static byte[] CMD_DELETE_SCENE = new byte[]{(byte) 0xc4, (byte) 0x23};         // delete scene
    public static byte[] CMD_RESET_SCENE  = new byte[]{(byte) 0xc4, (byte) 0x24};         // reset scene
    public static byte[] CMD_SCENE_LIST   = new byte[]{(byte) 0xc4, (byte) 0x25};         // get all scene normal user
    public static byte[] CMD_ADMIN_SCENE_LIST   = new byte[]{(byte) 0xc4, (byte) 0x2b};   // get all scene admin user
    public static byte[] CMD_GET_SCENE_PLAY = new byte[]{(byte) 0xc4, (byte) 0x26};       // get scene play single
    public static byte[] CMD_GET_SCENE = new byte[]{(byte) 0xc4, (byte) 0x28};            // get scene play single
    public static byte[] CMD_REG_SCENE_SCHE = new byte[]{(byte) 0xc4, (byte) 0x29};       // register scene schedule
    public static byte[] CMD_GET_SCENE_SCHE = new byte[]{(byte) 0xc4, (byte) 0x2a};       // get scene schedule
    public static byte[] CMD_MODIFY_INDEX       = new byte[]{(byte) 0xc4, (byte) 0x2c};   // modify scene index no
    public static byte[] CMD_SCENE_ACTION_CHANGE       = new byte[]{(byte) 0xc4, (byte) 0x2d};   //change a current scene

    /*command system*/
    public static byte[] CMD_TIME_SETTING      = new byte[]{(byte) 0xc5, (byte) 0x70};       // time setting
    public static byte[] CMD_SET_CLIENT_MODE   = new byte[]{(byte) 0xc5, (byte) 0x71};       // setting CMD_SET_CLIENT_MODE mpde
    public static byte[] CMD_SET_AP_MODE       = new byte[]{(byte) 0xc5, (byte) 0x71};       // setting ap mpde
    public static byte[] CMD_SIZGBEE_SETTING   = new byte[]{(byte) 0xc5, (byte) 0x73};       // zigbee panid + channel id
    public static byte[] CMD_SET_SWITCH        = new byte[]{(byte) 0xc5, (byte) 0x74};       // zigbee panid + channel id
    public static byte[] CMD_PASSWORD_CHANGE   = new byte[]{(byte) 0xc5, (byte) 0x75};       // admin password change
    public static byte[] CMD_GET_NETWORK_INFO = new byte[]{(byte) 0xc5, (byte) 0x77};       // network info, (mode, ssid, password)

    // public static byte[] CMD_GET_SIWTCH_LIST   = new byte[]{(byte) 0xc5, (byte) 0x78};       // get switch list
    public static byte[] CMD_GET_DMX_USE = new byte[]{(byte) 0xc5, (byte) 0x79};       // get dmx use
    public static byte[] CMD_SET_DMX_USE = new byte[]{(byte) 0xc5, (byte) 0x7a};       // set dmx use
    public static byte[] CMD_SET_DMX_VAL = new byte[]{(byte) 0xc3, (byte) 0xcd};       // set dmx val

    /*command alarm*/
    public static byte[] CMD_REG_ALARM     = new byte[]{(byte) 0xc6, (byte) 0xa0};        // register alarm
    public static byte[] CMD_DELETE_ALARM  = new byte[]{(byte) 0xc6, (byte) 0xa1};        // delete alarm
    public static byte[] CMD_ALARM_LIST    = new byte[]{(byte) 0xc6, (byte) 0xa2};        // get all alarm
    public static byte[] CMD_RESET_ALARM   = new byte[]{(byte) 0xc6, (byte) 0xa3};        // reset all alarm
    public static byte[] CMD_MODIFY_ALARM   = new byte[]{(byte) 0xc6, (byte) 0xa4};        // modify alarm

    /*command sensor*/
    public static byte[] CMD_SET_DETECT_SENSOR_ON     = new byte[]{(byte) 0xc9, (byte) 0x01};        // detect sensor on
    public static byte[] CMD_SET_DETECT_SENSOR_OFF    = new byte[]{(byte) 0xc9, (byte) 0x02};        // detect sensor off
    public static byte[] CMD_SET_LIGHT_SENSOR_ON      = new byte[]{(byte) 0xc9, (byte) 0x03};        // light sensor on
    public static byte[] CMD_SET_LIGHT_SENSOR_OFF     = new byte[]{(byte) 0xc9, (byte) 0x04};        // light sensor off
    public static byte[] CMD_GET_SENSOR_STATUS        = new byte[]{(byte) 0xc9, (byte) 0x05};        // get sensor value ( status )

    /*command switch*/
    public static byte[] CMD_GET_SIWTCH_LIST = new byte[]{(byte) 0xc8, (byte) 0x03};        // get siwtch list
    public static byte[] CMD_SET_SIWTCH_BUTTON_SECNE = new byte[]{(byte) 0xc8, (byte) 0x01};      // set switch button with scene
    public static byte[] CMD_SET_SIWTCH_BUTTON = new byte[]{(byte) 0xc8, (byte) 0x02};      // set switch button

    /*sub command*/
    public static byte[] CMD_CHECK_LIGHT_ON       = new byte[]{(byte) 0x10};    // twinkle led on
    public static byte[] CMD_CHECK_LIGHT_OFF      = new byte[]{(byte) 0x01};    // twinkle led off
    public static byte[] CMD_CHECK_LIGHT_OFF_ALL  = new byte[]{(byte) 0x00};    // twinkle led all off

    /*status*/
    public static byte[] CMD_CURRENT_SCENE  = new byte[]{(byte) 0xc7, (byte) 0x10};     // current scene

    /*dimmer*/
    /*public static byte[] CMD_ROOM_CONTROL  = new byte[]{(byte) 0xc3, (byte) 0xc2};
    public static byte[] CMD_GET_ROOM_STATUS  = new byte[]{(byte) 0xc3, (byte) 0xc3};*/
    public static byte[] CMD_SET_DEVICE_DIMING= new byte[]{(byte) 0xc2, (byte) 0xd9};
    public static byte[] CMD_GET_DEVICE_DIMING = new byte[]{(byte) 0xc2, (byte) 0xdc};
    public static byte[] CMD_SET_DEVICE_POWER  = new byte[]{(byte) 0xc2, (byte) 0xda};
    public static byte[] CMD_SET_DIMMER_ROOM = new byte[]{(byte) 0xc8, (byte) 0x01};

    /*common size*/
    public static int LENGTH_SIZE = 2;
    public static int SOP_SIZE = 2;
    public static int EOP_SIZE = 2;
    public static int ACK_POSITION = 10;
    public static int ACK_SESSION_POSITION = 7;
    public static int ACK_MININUM_SIZE = 13;
    public static int CMD_SIZE = 2;
    public static int CHANNEL_SIZE = 1; // depencency firmware
    public static int CHANNEL_MAX_CNT = 5; // depencency firmware // 20170710 lch max count 8 ->5 modified
    public static int CHANNEL_NAME_SIZE = 16; // depencency firmware
    public static int SCENE_DATA_SIZE = 537; // 24 + 512  ( added dmx flag + dmx info)
    public static int SCENE_REPEAT_DATA_SIZE = 2;

    public static int SCENE_PLAY_DATA_SIZE = 24;
    public static int SCENE_PLAY_REPEAT_DATA_SIZE = 3;

    public static int SCENE_SCHEDULE_DATA_SIZE = 30;
    public static int SCENE_SCHEDULE_REPEAT_DATA_SIZE = 7;

    public static int ALARM_DATA_SIZE = 12;
    public static int ALARM_MODIFY_DATA_SIZE = 14;

    public static final int MIN_SCENE_NO = 16;

    public static int EXCEPT_DATE_LENGTH = LENGTH_SIZE + SOP_SIZE + EOP_SIZE + CMD_SIZE; // remaining length, excluding the date length


    /* Response define */
    public final static int CONNECT_RESPONSE_SIZE = 4;
    public final static byte FAIL_CODE = (byte) 0xee;
    public final static byte FAIL_CONNECT= (byte) 0xed;
    public final static byte OK_CODE = (byte) 0xc8;
    public final static byte CONNECT_ID_NOT_MATCH = (byte)0xAC;

    public static byte UNKWON_ERROR = (byte)0xAC;
    public static byte  DEFERENCE_DATA_ERR = (byte) 0xc4; // added 20161013 lch connection id deference case
    public final static int NORMAL_SCENE = 33;
    public final static int PLAY_SCENE = 39;
    public final static int SCHEDULE_SCENE = 44;

    /*default scene define*/
    public final static int DEFAULT_SCENE_0 = 1;
    public final static int DEFAULT_SCENE_50 = 3;
    public final static int DEFAULT_SCENE_100 = 4;

    public final static int AP_MODE_BUFFER_SIZE = 47;
    public final static int CLIENT_MODE_BUFFER_SIZE = 48;
    public final static int MAX_DMX_SIZE = 512;

    public final static int SIWTCH_MAX_CNT = 4;
    public final static int SIWTCH__INFO_SIZE = 3;

    /* device type */
    public final static int DEVICE_TYPE_LIGHT  = 17;
    public final static int DEVICE_TYPE_SWITCH = 34;
}



