package com.rareraw.master.network.DataClass;

import android.content.Context;

import com.neoromax.feelmaster.R;

/**
 * Created by lch on 2016-12-13.
 */

public class AlarmItem {
    public int mIndex;
    public String mAmPm;
    public int mHour;
    public int mMin;
    public int mSceneNo;
    public String mSceneName;
    public String mWeek;
    public boolean mAction;
    public byte[] mbKey;
    public byte[] mBweek;
    public AlarmItem(int _index, String _ampm, int _hour, int _min, int _sceneno,
    String _name, String _week, byte[] _bweek, boolean _action, byte[] _key){
        mIndex = _index;
        mAmPm = _ampm;
        mHour = _hour;
        mMin = _min;
        mSceneNo = _sceneno;
        mSceneName = _name;
        mWeek = _week;
        mBweek = _bweek;
        mAction = _action;
        mbKey = _key;
    }
    public static String getAmPm(int hour, Context _cxt){
        if(hour <= 12)
            return _cxt.getString(R.string.frag_add_alarm_am);
        else
            return _cxt.getString(R.string.frag_add_alarm_pm);

    }

    public static String getWeek(byte[] alarmWeek, Context cxt) {
        String sWeekInfo = "";
        if(alarmWeek[0] == 1)
            sWeekInfo += cxt.getString(R.string.monday) + " ";
        if(alarmWeek[1] == 1)
            sWeekInfo += cxt.getString(R.string.tueday) + " ";
        if(alarmWeek[2] == 1)
            sWeekInfo += cxt.getString(R.string.wenday) + " ";
        if(alarmWeek[3] == 1)
            sWeekInfo += cxt.getString(R.string.thuday) + " ";
        if(alarmWeek[4] == 1)
            sWeekInfo += cxt.getString(R.string.friday) + " ";
        if(alarmWeek[5] == 1)
            sWeekInfo += cxt.getString(R.string.satday) + " ";
        if(alarmWeek[6] == 1)
            sWeekInfo += cxt.getString(R.string.sunday);
        return sWeekInfo;
    }
}
