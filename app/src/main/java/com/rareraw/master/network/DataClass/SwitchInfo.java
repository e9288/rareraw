package com.rareraw.master.network.DataClass;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by lch on 2016-10-08.
 */

public class SwitchInfo {

    private final static String TAG = "SwitchInfo";
    public byte[] mByteDevMac ;
    public byte[] mByteswitchName ;


    public byte[] mByteSwitchMac  = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x00};;
    public byte[] mByteSwitchType ;

    public byte[] getmByteswitchName() {
        return mByteswitchName;
    }

    public void setmByteswitchName(byte[] mByteswitchName) {
        this.mByteswitchName = mByteswitchName;
    }

    public byte[] getByteDevMac() {
        return mByteDevMac;
    }

    public void setmByteDevMac(byte[] _byteDevMac) {
        this.mByteDevMac = _byteDevMac;
    }

    public String getRoonName() {
        return roonName;
    }

    public void setRoonName(String roonName) {
        this.roonName = roonName;
    }

    public String roonName;

    private int mSeqNo ;
    private int mSceneNo ;
    private int mIndexNo;
    public ArrayList<byte[]> mSceneList = new ArrayList<>();
    public ArrayList<byte[]> getSceneList() {
        return mSceneList;
    }

    public void setSceneList(ArrayList<byte[]> _sceneList) {
        this.mSceneList = _sceneList;
    }


    public int getIndexNo() {
        return mIndexNo;
    }

    public void setIndexNo(int mIndexNo) {
        this.mIndexNo = mIndexNo;
    }


    public String getSwitchMac() {
        String sMacName = "";
        try {
            for (byte singleMac : mByteSwitchMac)
                sMacName += String.format("%02X", (((short) singleMac)) & 0xFF);
        } catch (Exception e) {
            Log.e("error", "convert hex to string");
        }

        return sMacName;
    }

    public void setByteSwitchMac(byte[] _byteSwitchMac) {
        this.mByteSwitchMac = _byteSwitchMac;
    }

    public byte[] getByteSwitchMac() {
        return  mByteSwitchMac ;
    }
    public byte[] getSwitchType() {
        return mByteSwitchType;
    }

    public void setByteSwitchType(byte[] _byteSwitchType) {
        this.mByteSwitchType = _byteSwitchType;
    }

    public int getSeqNo() {
        return mSeqNo;
    }

    public void setSeqNo(int mSeqNo) {
        this.mSeqNo = mSeqNo;
    }

    public int getSceneNo() {
        return mSceneNo;
    }

    public void setSceneNo(int mSceneNo) {
        this.mSceneNo = mSceneNo;
    }
}
