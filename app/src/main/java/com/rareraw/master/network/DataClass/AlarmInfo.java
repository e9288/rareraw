package com.rareraw.master.network.DataClass;

/**
 * Created by lch on 2016-10-09.
 */

public class AlarmInfo {
    private final static String TAG = "AlarmInfo";
    public byte[] mByteAlarmKey;
    public byte[] mByteAlarmTimeHour;
    public byte[] mByteAlarmTimeMin;

    public byte[] mByteAlarmWeek;
    public byte[] mByteSceneNo;

    public byte[] mByteAlarmAction;
    public int mIntAlarmAction;

    public boolean getBooleanAction() {
        return mBooleanAction;
    }

    public void setBooleanAction(boolean _booleanAction) {
        this.mBooleanAction = _booleanAction;
    }

    public boolean mBooleanAction;

    public int mIntSceneNo;
    public int mIntAlarmTimeHour;
    public int mIntAlarmTimeMin;

    public int getIntAlarmAction() {
        return mIntAlarmAction;
    }

    public void setIntAlarmAction(int _intAlarmAction) {
        this.mIntAlarmAction = _intAlarmAction;
    }

    public int getNSceneNo() {
        return mIntSceneNo;
    }

    public void setNSceneNo(int _sceneNo) {
        this.mIntSceneNo = _sceneNo;
    }

    public int getIntAlarmTimeHour() {
        return mIntAlarmTimeHour;
    }

    public void setIntAlarmTimeHour(int _alarmTimeHour) {
        this.mIntAlarmTimeHour = _alarmTimeHour;
    }

    public int getIntAlarmTimeMin() {
        return mIntAlarmTimeMin;
    }

    public void setIntAlarmTimeMin(int _alarmTimeMin) {
        this.mIntAlarmTimeMin = _alarmTimeMin;
    }

    public byte[] getAlarmKey() {
        return mByteAlarmKey;
    }

    public void setAlarmKey(byte[] _alarmKey) {
        this.mByteAlarmKey = _alarmKey;
    }

    public byte[] getAlarmTimeHour() {
        return mByteAlarmTimeHour;
    }

    public void setAlarmTimeHour(byte[] _alarmTimeHour) {
        this.mByteAlarmTimeHour = _alarmTimeHour;
    }

    public byte[] getAlarmTimeMin() {
        return mByteAlarmTimeMin;
    }

    public void setAlarmTimeMin(byte[] _alarmTimeMin) {
        this.mByteAlarmTimeMin = _alarmTimeMin;
    }

    public byte[] getAlarmWeek() {
        return mByteAlarmWeek;
    }

    public void setAlarmWeek(byte[] _alarmWeek) {
        this.mByteAlarmWeek = _alarmWeek;
    }

    public byte[] getmBSceneNo() {
        return mByteSceneNo;
    }

    public void setBSceneNo(byte[] _sceneNo) {
        this.mByteSceneNo = _sceneNo;
    }

    public byte[] getmBAlarmAction() {
        return mByteAlarmAction;
    }

    public void setmBAlarmAction(byte[] _alarmAction) {
        this.mByteAlarmAction = _alarmAction;
    }
}

