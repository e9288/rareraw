package com.rareraw.master.network;

/**
 * Created by lch on 2017-02-10.
 */

public interface NetworkResultLisnter <T1>{
    void onSuccress(T1 retValue);
    void onError(int errorCode);
    void onErrorConnectionId(int errorCode);
}
