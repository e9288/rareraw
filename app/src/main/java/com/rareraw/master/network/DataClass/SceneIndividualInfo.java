package com.rareraw.master.network.DataClass;

/**
 * Created by lch on 2017-01-07.
 */

public class SceneIndividualInfo {

    private int mIntSceneNo ;
    private byte[] mByteSceneNo;

    private String mSceneName;

    private int mIntStartHour;
    private byte mByteStartHour;

    private int mIntStartMin;
    private byte mByteStartMin;

    private int mIntEndHour;
    private byte mByteEndHour;

    private int mIntEndMin;
    private int mByteEndMin;

    private int mIntFadeTime;
    private byte mByteFadeTime;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected;
    public int getIntSceneNo() {
        return mIntSceneNo;
    }

    public void setIntSceneNo(int mIntSceneNo) {
        this.mIntSceneNo = mIntSceneNo;
    }

    public String getmSceneName() {
        return mSceneName;
    }

    public void setmSceneName(String mSceneName) {
        this.mSceneName = mSceneName;
    }

    public byte[] getByteSceneNo() {
        return mByteSceneNo;
    }

    public void setByteSceneNo(byte[] mByteSceneNo) {
        this.mByteSceneNo = mByteSceneNo;
    }

    public int getIntStartHour() {
        return mIntStartHour;
    }

    public void setIntStartHour(int mIntStartHour) {
        this.mIntStartHour = mIntStartHour;
    }

    public byte getByteStartHour() {
        return mByteStartHour;
    }

    public void setByteStartHour(byte mByteStartHour) {
        this.mByteStartHour = mByteStartHour;
    }

    public int getIntStartMin() {
        return mIntStartMin;
    }

    public void setIntStartMin(int mIntStartMin) {
        this.mIntStartMin = mIntStartMin;
    }

    public byte getByteStartMin() {
        return mByteStartMin;
    }

    public void setByteStartMin(byte mByteStartMin) {
        this.mByteStartMin = mByteStartMin;
    }

    public int getIntEndHour() {
        return mIntEndHour;
    }

    public void setIntEndHour(int mIntEndHour) {
        this.mIntEndHour = mIntEndHour;
    }

    public byte getByteEndHour() {
        return mByteEndHour;
    }

    public void setByteEndHour(byte mByteEndHour) {
        this.mByteEndHour = mByteEndHour;
    }

    public int getIntEndMin() {
        return mIntEndMin;
    }

    public void setIntEndMin(int mIntEndMin) {
        this.mIntEndMin = mIntEndMin;
    }

    public int getByteEndMin() {
        return mByteEndMin;
    }

    public void setByteEndMin(int mByteEndMin) {
        this.mByteEndMin = mByteEndMin;
    }

    public int getIntFadeTime() {
        return mIntFadeTime;
    }

    public void setIntFadeTime(int mIntFadeTime) {
        this.mIntFadeTime = mIntFadeTime;
    }

    public byte getByteFadeTime() {
        return mByteFadeTime;
    }

    public void setByteFadeTime(byte mByteFadeTime) {
        this.mByteFadeTime = mByteFadeTime;
    }

}
