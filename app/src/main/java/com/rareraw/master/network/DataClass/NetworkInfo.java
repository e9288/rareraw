package com.rareraw.master.network.DataClass;

/**
 * Created by lch on 2017-02-10.
 */

public class NetworkInfo {
    public boolean mNetworkMode; // false : apmode , true : client mode
    public String mSSid;
    public String mPassword;

    public NetworkInfo(boolean mode, String ssidName, String password){
        mNetworkMode = mode;
        mSSid = ssidName;
        mPassword = password;
    }
    public boolean getNetworkMode() {
        return mNetworkMode;
    }

    public void setNetworkMode(boolean mNetworkMode) {
        this.mNetworkMode = mNetworkMode;
    }

    public String getSSid() {
        return mSSid;
    }

    public void setSSid(String mSSid) {
        this.mSSid = mSSid;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }
}
