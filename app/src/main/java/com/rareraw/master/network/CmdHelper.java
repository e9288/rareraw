package com.rareraw.master.network;


import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import com.rareraw.master.Config;
import com.rareraw.master.network.DataClass.AlarmInfo;
import com.rareraw.master.network.DataClass.SceneInfo;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static com.rareraw.master.network.Cmd.EOP_SIZE;
import static com.rareraw.master.network.Cmd.EXCEPT_DATE_LENGTH;
import static com.rareraw.master.network.Cmd.PRRC_EOP;
import static com.rareraw.master.network.Cmd.PRTC_SOP;
import static com.rareraw.master.network.Cmd.SOP_SIZE;


public class CmdHelper {
    private final static String TAG = "CmdHelper";
    private static ByteBuffer mOutput = null;
    private final static int MIN_SCENE_NO = 16;
    private final static int MIN_INDEX_NO = 1;

    public static byte[] intToByteArray(int totalSize) {
        // To prepare more than one byte
        byte[] byteArray = new byte[2];
        try {
            byteArray[0] = (byte) (totalSize >> 8);
            byteArray[1] = (byte) (totalSize);
        } catch (Exception e) {

        }
        return byteArray;
    }

    // Using only two byte
    public static int byteArrayToInt(byte[] bSize) {
        int newValue = 0;
        try {
            if (bSize.length == 1) {
                newValue |= (((short) bSize[0])) & 0xFF;
            }
            if (bSize.length == 2) {
                newValue |= (((short) bSize[0]) << 8) & 0xFF00;
                newValue |= (((short) bSize[1])) & 0xFF;
            }

        } catch (Exception e) {
//            Log.e(TAG, bSize.length + " | " + bSize[0]);
        }
        return newValue;
    }

    public static String GetDevicesUUID(Context _context) {
        String android_id = Settings.Secure.getString(_context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static ByteBuffer makeMessage(byte[] command, ByteBuffer sessionId, String _data) {
        int totalSize = sessionId.capacity() + _data.getBytes().length + EXCEPT_DATE_LENGTH;
        mOutput = ByteBuffer.allocate(totalSize);
        try {
            mOutput.put(PRTC_SOP);
            mOutput.put(intToByteArray(totalSize - (SOP_SIZE + EOP_SIZE)));
            mOutput.put(sessionId.array());
            mOutput.put(command);
            mOutput.put(_data.getBytes());
            mOutput.put(PRRC_EOP);
        } catch (Exception e) {
            Log.e("make cmd error : ", "connect request");
            e.printStackTrace();
        }
        return mOutput;
    }

    public static ByteBuffer makeMessage(byte[] command, ByteBuffer sessionId, ByteBuffer _data) {
        int totalSize = sessionId.capacity() + _data.capacity() + EXCEPT_DATE_LENGTH;
        mOutput = ByteBuffer.allocate(totalSize);
        try {
            mOutput.put(PRTC_SOP);
            mOutput.put(intToByteArray(totalSize - (SOP_SIZE + EOP_SIZE)));
            mOutput.put(sessionId.array());
            mOutput.put(command);
            mOutput.put(_data.array());
            mOutput.put(PRRC_EOP);
        } catch (Exception e) {
            Log.e("make cmd error : ", "connect request");
            e.printStackTrace();
        }
        return mOutput;
    }

    public static String getSceneName(byte[] sceneNo) {

        for (SceneInfo scene : APService.mSceneList) {
            if (scene.mIntSceneNo == byteArrayToInt(sceneNo)) {
                return scene.getSceneName();
            }
        }
        if(Config.DEBUG)
            return "TEST_SCENE";
        return null;
    }

    public static int getHoldingTime(byte[] sceneNo) {
        for (SceneInfo scene : APService.mSceneList) {
            if (Arrays.equals(scene.mByteSceneNo, sceneNo)) {
                return scene.getIntHoldingTime();
            }
        }
        return 1;
    }

    public static int getEmptySceneNoInt() {
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {

            }
        });

        // 0번 사용못함.
        int index = 1;
        if (APService.mSceneList.size() == 0)
            return MIN_SCENE_NO;

        Collections.sort(APService.mSceneList, new AscScene());
        int j = 16;
        for (int i = 0; i < APService.mSceneList.size(); i++) {

            if (Integer.parseInt(String.valueOf(APService.mSceneList.get(i).getIntSceneNo())) < 16)
                continue;

            if (Integer.parseInt(String.valueOf(APService.mSceneList.get(i).getIntSceneNo())) != j) {
                return j;
            } else
                j++;
        }
        Log.e("empty scene no", Integer.toString(j));
        return j;
    }

    public static int getEmptyIndexNoInt() {
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {

            }
        });

        Collections.sort(APService.mSceneList, new AscIndex());
        boolean sameFlag = false;
        for (int i = 1; i < APService.mSceneList.size() + 1; i++) {
            for (int j = 0; j < APService.mSceneList.size(); j++) {
                if (i == APService.mSceneList.get(j).getIntIndex()) {
                    sameFlag = true;
                    break;
                }
            }
            if (sameFlag) {
                sameFlag = false;
                continue;
            } else
                return i;
        }

        return APService.mSceneList.size() + 1;
    }

    public static byte[] getIndexNo() {
        byte[] buffer = new byte[]{(byte) 0x00, (byte) 0x00};
        return buffer;
    }

    public static byte[] makeScenNameFullLength(String name) {

        String tempName = name;
        int length = 16 - name.getBytes().length;
        byte[] empty = new byte[length];
        tempName = tempName + new String(empty);
        return tempName.getBytes();

    }

    public static int checkNullName(String tempChannelName) {
        int totalCnt = 0;
        byte[] bTotalVal = tempChannelName.getBytes();

        for (int i = 0; i < bTotalVal.length; i++) {
            totalCnt += bTotalVal[i];
        }

        return totalCnt;
    }

    public static void sortByIndexSceneList() {
        if (Config.DEBUG) {
            SceneInfo d1 = new SceneInfo();
            d1.setIntIndex(10);

            SceneInfo d2 = new SceneInfo();
            d2.setIntIndex(5);

            SceneInfo d3 = new SceneInfo();
            d3.setIntIndex(20);

            SceneInfo d4 = new SceneInfo();
            d4.setIntIndex(1);

            APService.mSceneList.add(d1);
            APService.mSceneList.add(d2);
            APService.mSceneList.add(d3);
            APService.mSceneList.add(d4);
        }

        Collections.sort(APService.mSceneList, new AscIndex());
    }

    public static AlarmInfo getAlarmSingle(byte[] key) {
        for(AlarmInfo info:APService.getApAlramList()){
            if(Arrays.equals(key, info.getAlarmKey())){
                return info;
            }
        }
        if(Config.DEBUG){
            AlarmInfo info = new AlarmInfo();
            info.setAlarmKey(intToByteArray(5));
            info.setIntAlarmTimeHour(14);
            info.setIntAlarmTimeMin(44);
            info.setNSceneNo(1);
            byte[] b = new byte[]{(byte)0x01,(byte)0x01,(byte)0x01,(byte)0x00,(byte)0x00,(byte)0x00,(byte)0x00};
            info.setAlarmWeek(b);
            return info;
        }

        return null;
    }


    static class AscScene implements Comparator<SceneInfo> {
        @Override
        public int compare(SceneInfo s1, SceneInfo s2) {
            return s1.getIntSceneNo() < s2.getIntSceneNo() ? -1 : s1.getIntSceneNo() > s2.getIntSceneNo() ? 1 : 0;
        }
    }

    static class AscIndex implements Comparator<SceneInfo> {
        @Override
        public int compare(SceneInfo s1, SceneInfo s2) {
            return s1.getIntIndex() < s2.getIntIndex() ? -1 : s1.getIntIndex() > s2.getIntIndex() ? 1 : 0;
        }
    }

}
