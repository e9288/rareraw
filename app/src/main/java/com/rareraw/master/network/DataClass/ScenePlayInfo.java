package com.rareraw.master.network.DataClass;

import android.util.Log;

import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by lch on 2016-10-18.
 */

public class ScenePlayInfo {

    private final static String TAG = "ScenePlayInfo";
    public int mIntScenePlayNo;
    public byte[] mByteScenePlayNo;

    public String mScenePlayName;

    public byte[] mByteScenePlayName;

    public int mIntIndexNo;
    public byte[] mByteIndexNo;

    public int mIntIconType;
    public byte[] mByteIconType;

    public boolean mBoolHideFlag;
    public byte[] mByteHideFlag;

    public boolean mBoolRepeatFlag;
    public byte[] mByteRepeatFlag;

    public ArrayList<Integer> mIntSceneNo = new ArrayList<>();
    public ArrayList<byte[]> mByteSceneNo = new ArrayList<>();

    public ArrayList<Integer> mIntHoldingTime = new ArrayList<>();
    public ArrayList<byte[]> mByteHoldingTime = new ArrayList<>();

    public ArrayList<SceneInfo> getSceneList() {
        return mSceneList;
    }

    public void setSceneList(ArrayList<SceneInfo> mSceneList) {
        if(this.mSceneList  != null)
            this.mSceneList.clear();

        this.mSceneList = mSceneList;
    }

    public ArrayList<SceneInfo> mSceneList = new ArrayList<>();

    public ArrayList<SceneInfo> getSelectedMoodList() {
        return mSelectedMoodList;
    }

    public void setSelectedMoodList(ScenePlayInfo mSelectedMoodList) {

        this.mSelectedMoodList.clear();
        for (SceneInfo info : mSelectedMoodList.getSceneList()) {
            if (info.isSelectedFlag())
                this.mSelectedMoodList.add(info);
        }

    }

    public ArrayList<SceneInfo> mSelectedMoodList = new ArrayList<>();

    public byte[] getByteScenePlayName() {
        return mByteScenePlayName;
    }

    public void setByteScenePlayName(byte[] _byteScenePlayName) {
        this.mByteScenePlayName = _byteScenePlayName;
    }

    public int getIntScenePlayNo() {
        return mIntScenePlayNo;
    }

    public void setIntScenePlayNo(int _intScenePlayNo) {
        this.mIntScenePlayNo = _intScenePlayNo;
    }

    public byte[] getByteScenePlayNo() {
        return mByteScenePlayNo;
    }

    public void setByteScenePlayNo(byte[] _byteScenePlayNo) {
        this.mByteScenePlayNo = _byteScenePlayNo;
    }

    public String getScenePlayName() {
        return mScenePlayName;
    }

    public void setScenePlayName(String _scenePlayName) {
        this.mScenePlayName = _scenePlayName;
    }

    public int getIntIndexNo() {
        return mIntIndexNo;
    }

    public void setIntIndexNo(int _intIndexNo) {
        this.mIntIndexNo = _intIndexNo;
    }

    public byte[] getByteIndexNo() {
        return mByteIndexNo;
    }

    public void setByteIndexNo(byte[] _byteIndexNo) {
        this.mByteIndexNo = _byteIndexNo;
    }

    public int getIntIconType() {
        return mIntIconType;
    }

    public void setIntIconType(int _intIconType) {
        this.mIntIconType = _intIconType;
    }

    public byte[] getByteIconType() {
        return mByteIconType;
    }

    public void setByteIconType(byte[] _byteIconType) {
        this.mByteIconType = _byteIconType;
    }

    public boolean getBoolHideFlag() {
        return mBoolHideFlag;
    }

    public void setBoolHideFlag(boolean _boolHideFlag) {
        this.mBoolHideFlag = _boolHideFlag;
    }

    public byte[] getByteHideFlag() {
        return mByteHideFlag;
    }

    public void setByteHideFlag(byte[] _byteHideFlag) {
        this.mByteHideFlag = _byteHideFlag;
    }

    public boolean getBoolRepeatFlag() {
        return mBoolRepeatFlag;
    }

    public void setBoolRepeatFlag(boolean _boolRepeatFlag) {
        this.mBoolRepeatFlag = _boolRepeatFlag;
    }

    public byte[] getByteRepeatFlag() {
        return mByteRepeatFlag;
    }

    public void setByteRepeatFlag(byte[] _byteRepeatFlag) {
        this.mByteRepeatFlag = _byteRepeatFlag;
    }

    public ArrayList<Integer> getIntSceneNo() {
        return mIntSceneNo;
    }

    public void setIntSceneNo(ArrayList<Integer> _intSceneNo) {
        this.mIntSceneNo = _intSceneNo;
    }

    public ArrayList<byte[]> getByteSceneNo() {
        return mByteSceneNo;
    }

    public void setByteSceneNo(ArrayList<byte[]> _byteSceneNo) {
        this.mByteSceneNo = _byteSceneNo;
    }

    public ArrayList<Integer> getIntHoldingTime() {
        return mIntHoldingTime;
    }

    public void setIntHoldingTime(ArrayList<Integer> _intHoldingTime) {
        this.mIntHoldingTime = _intHoldingTime;
    }

    public ArrayList<byte[]> getByteHoldingTime() {
        return mByteHoldingTime;
    }

    public void setByteHoldingTime(ArrayList<byte[]> _byteHoldingTime) {
        this.mByteHoldingTime = _byteHoldingTime;
    }


    public ByteBuffer makeBuffer() {
        int count = this.getSceneList().size();
        ByteBuffer buffer = ByteBuffer.allocate(Cmd.SCENE_DATA_SIZE + count * Cmd.SCENE_PLAY_REPEAT_DATA_SIZE);
        try {
            String fullName = this.getScenePlayName().trim();
            int length = 16 - this.getScenePlayName().getBytes().length;
            byte[] empty = new byte[length];
            fullName = fullName + new String(empty);

            buffer.put(CmdHelper.intToByteArray(this.getIntScenePlayNo()));
            buffer.put(fullName.getBytes());
            buffer.put(CmdHelper.intToByteArray(this.getIntIndexNo()));

            buffer.put((byte) this.getIntIconType());
            buffer.put((this.getBoolHideFlag()) ? (byte) 0x01 : (byte) 0x00);

            // 20191110 무조건 반복
            //buffer.put((this.getBoolRepeatFlag()) ? (byte) 0x01 : (byte) 0x00);

            buffer.put( (byte) 0x01);

            buffer.put((byte) count);
            /*for (int i = 0; i < this.getSceneList().size(); i++) {
                if()
                buffer.put(CmdHelper.intToByteArray(this.getSceneList().get(i).getIntSceneNo()));
                buffer.put((byte) this.getSceneList().get(i).getIntHoldingTime());
            }*/
            for (int i = 0; i < this.getSelectedMoodList().size(); i++) {
                buffer.put(CmdHelper.intToByteArray(this.getSelectedMoodList().get(i).getIntSceneNo()));
                buffer.put((byte) this.getSelectedMoodList().get(i).getIntHoldingTime());
            }

        } catch (Exception e) {
            Log.e("error", "make scene buffer");
            return null;
        }
        return buffer;
    }

}
