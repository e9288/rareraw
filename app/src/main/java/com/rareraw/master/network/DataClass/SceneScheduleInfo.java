package com.rareraw.master.network.DataClass;

import android.util.Log;

import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by lch on 2016-10-18.
 */

public class SceneScheduleInfo {

    public ArrayList<SceneInfo> getMoodList() {
        return moodInfo;
    }

    public void setMoodInfo(ArrayList<SceneInfo> moodInfo) {
        this.moodInfo = moodInfo;
    }

    public ArrayList<SceneInfo> moodInfo = new ArrayList<>();
    public String mSceneScheduleName;
    public byte[] mByteSceneScheduleName;

    public int mIntIndexNo;
    public byte[] mByteIndexNo;

    public int mIntScheduleSceneNo;
    public byte[] mByteScheduleSceneNo;

    public byte[] mByteIconType;

    public int mIntIconType;
    public boolean mBoolHideFlag;
    public byte[] mByteHideFlag;

    public byte[] mByteWeek = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};

    public ArrayList<byte[]> mByteSceneNo = new ArrayList<>();
    public ArrayList<Integer> mIntSceneNo = new ArrayList<>();

    public ArrayList<byte[]> mByteFadeTime = new ArrayList<>();
    public ArrayList<Integer> mIntFadeTime = new ArrayList<>();

    public ArrayList<byte[]> mByteStartTime = new ArrayList<>();
    public ArrayList<Integer> mIntStartTime = new ArrayList<>();

    public ArrayList<byte[]> mByteEndTime = new ArrayList<>();
    public ArrayList<Integer> mIntEndTime = new ArrayList<>();

    public ArrayList<SceneIndividualInfo> mSceneList = new ArrayList<>();

    public ArrayList<SceneIndividualInfo> getSceneList() {
        return mSceneList;
    }

    public void setSceneList(ArrayList<SceneIndividualInfo> _sceneList) {
        this.mSceneList = _sceneList;
    }

    public void addSceneList(SceneIndividualInfo _scene) {
        this.mSceneList.add(_scene);
    }
    public int getIntIconType() {
        return mIntIconType;
    }

    public int getIntScheduleSceneNo() {
        return mIntScheduleSceneNo;
    }

    public void setIntScheduleSceneNo(int mIntScheduleSceneNo) {
        this.mIntScheduleSceneNo = mIntScheduleSceneNo;
    }

    public byte[] getByteScheduleSceneNo() {
        return mByteScheduleSceneNo;
    }

    public void setByteScheduleSceneNo(byte[] mByteScheduleSceneNo) {
        this.mByteScheduleSceneNo = mByteScheduleSceneNo;
    }

    public void setIntIconType(int _intIconType) {
        this.mIntIconType = _intIconType;
    }

    public byte[] getByteSceneScheduleName() {
        return mByteSceneScheduleName;
    }

    public void setByteSceneScheduleName(byte[] _byteSceneScheduleName) {
        this.mByteSceneScheduleName = _byteSceneScheduleName;
    }

    public String getSceneScheduleName() {
        return mSceneScheduleName;
    }

    public void setSceneScheduleName(String _sceneScheduleName) {
        this.mSceneScheduleName = _sceneScheduleName;
    }

    public int getIntIndexNo() {
        return mIntIndexNo;
    }

    public void setIntIndexNo(int m_intIndexNo) {
        this.mIntIndexNo = m_intIndexNo;
    }

    public byte[] getByteIndexNo() {
        return mByteIndexNo;
    }

    public void setByteIndexNo(byte[] _byteIndexNo) {
        this.mByteIndexNo = _byteIndexNo;
    }

    public byte[] getByteIconType() {
        return mByteIconType;
    }

    public void setByteIconType(byte[] _byteIconType) {
        this.mByteIconType = _byteIconType;
    }

    public boolean getBoolHideFlag() {
        return mBoolHideFlag;
    }

    public void setBoolHideFlag(boolean _boolHideFlag) {
        this.mBoolHideFlag = _boolHideFlag;
    }

    public byte[] getByteHideFlag() {
        return mByteHideFlag;
    }

    public void setByteHideFlag(byte[] _byteHideFlag) {
        this.mByteHideFlag = _byteHideFlag;
    }

    public byte[] getByteWeek() {
        return mByteWeek;
    }

    public void setByteWeek(byte[] _byteWeek) {
        this.mByteWeek = _byteWeek;
    }

    public ArrayList<byte[]> getByteSceneNo() {
        return mByteSceneNo;
    }

    public void setByteSceneNo(ArrayList<byte[]> _byteSceneNo) {
        this.mByteSceneNo = _byteSceneNo;
    }

    public ArrayList<Integer> getIntSceneNo() {
        return mIntSceneNo;
    }

    public void setIntSceneNo(ArrayList<Integer> _intSceneNo) {
        this.mIntSceneNo = _intSceneNo;
    }

    public ArrayList<byte[]> getByteFadeTime() {
        return mByteFadeTime;
    }

    public void setByteFadeTime(ArrayList<byte[]> _byteFadeTime) {
        this.mByteFadeTime = _byteFadeTime;
    }

    public ArrayList<Integer> getIntFadeTime() {
        return mIntFadeTime;
    }

    public void setIntFadeTime(ArrayList<Integer> _intFadeTime) {
        this.mIntFadeTime = _intFadeTime;
    }

    public ArrayList<byte[]> getByteStartTime() {
        return mByteStartTime;
    }

    public void setByteStartTime(ArrayList<byte[]> _byteStartTime) {
        this.mByteStartTime = _byteStartTime;
    }

    public ArrayList<Integer> getIntStartTime() {
        return mIntStartTime;
    }

    public void setIntStartTime(ArrayList<Integer> _intStartTime) {
        this.mIntStartTime = _intStartTime;
    }

    public ArrayList<byte[]> getByteEndTime() {
        return mByteEndTime;
    }

    public void setByteEndTime(ArrayList<byte[]> _byteEndTime) {
        this.mByteEndTime = _byteEndTime;
    }

    public ArrayList<Integer> getIntEndTime() {
        return mIntEndTime;
    }

    public void setIntEndTime(ArrayList<Integer> _intEndTime) {
        this.mIntEndTime = _intEndTime;
    }

    public ByteBuffer makeBuffer() {
        ByteBuffer buffer = null;
        int count = this.getSceneList().size();
        String fullName = this.getSceneScheduleName();
        int length = 16 - this.getSceneScheduleName().getBytes().length;
        byte[] empty = new byte[length];
        fullName = fullName + new String(empty);

        try {
            buffer = ByteBuffer.allocate(Cmd.SCENE_SCHEDULE_DATA_SIZE + count * Cmd.SCENE_SCHEDULE_REPEAT_DATA_SIZE);
            buffer.put(CmdHelper.intToByteArray(this.getIntScheduleSceneNo()));
            buffer.put(fullName.getBytes());
            buffer.put(CmdHelper.intToByteArray(this.getIntIndexNo()));

            buffer.put((byte) this.getIntIconType());
            buffer.put((byte) ((this.getBoolHideFlag()) ? 1 : 0));
            buffer.put(this.getByteWeek());
            buffer.put((byte) count);
            for (SceneIndividualInfo info : this.getSceneList()) {
                buffer.put(CmdHelper.intToByteArray(info.getIntSceneNo()));
                // buffer.put((byte)info.getIntFadeTime());

                buffer.put((byte) info.getIntStartHour());
                buffer.put((byte) info.getIntStartMin());
                buffer.put((byte) info.getIntEndHour());
                buffer.put((byte) info.getIntEndMin());
            }
        } catch (Exception e) {
            Log.e("error", "make scene schedule buffer");
            return null;
        }
        return buffer;
    }
}
