package com.rareraw.master.network.DataClass;

import com.rareraw.master.data.Channel;
import com.rareraw.master.network.CmdHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lch on 2016-10-08.
 */

public class DeviceInfo {

    private final static String TAG = "DeviceInfo";
    public byte[] mByteDevMac ;
    public byte[] mByteDevName ;

    public byte[] getmByteDevName() {
        return mByteDevName;
    }

    public void setmByteDevName(byte[] mByteDevName) {
        this.mByteDevName = mByteDevName;
    }

    public byte[] mByteDevType ;

    public String getDevName() {
        return devName;
    }

    public void setDevName(String devName) {
        this.devName = devName;
    }

    public String devName ;
    public int mIntDevType;
    public int dimingLevel;

    public boolean isPowerStat() {
        return powerStat;
    }

    public void setPowerStat(boolean powerStat) {
        this.powerStat = powerStat;
    }

    public boolean powerStat;
    public int getRoomDimingLevel() {
        return roomDimingLevel;
    }

    public void setRoomDimingLevel(int roomDimingLevel) {
        this.roomDimingLevel = roomDimingLevel;
    }

    public int roomDimingLevel;
    public ArrayList<Channel> getChannelList() {
        return channelList;
    }

    public void setChannelList(ArrayList<Channel> channelList) {
        this.channelList = channelList;
    }
    public void addChannelList(Channel channel) {
        this.channelList.add( channel);
    }

    public void clearChannelList() {
        this.channelList.clear();
    }

    public void clearSelectCheck() {
        this.setCheckStat(false);
    }

    public ArrayList<Channel> channelList = new ArrayList<>();

    public int getDimingLevel() {
        return dimingLevel;
    }

    public void setDimingLevel(int dimingLevel) {
        this.dimingLevel = dimingLevel;
    }

    public boolean mIndicatorFlag;
    public ArrayList<byte[]> mByteChannelNo = new ArrayList<>();
    public ArrayList<Integer> mIntChannelNo = new ArrayList<>();
    public ArrayList<String> mChannelName = new ArrayList<>();

    public ArrayList<byte[]> mByteChannelName = new ArrayList<>();
    public boolean mCheckStat = false;
    public boolean getIndicatorFlag() {
        return mIndicatorFlag;
    }

    public void setIndicatorFlag(boolean mIndicatorFlag) {
        this.mIndicatorFlag = mIndicatorFlag;
    }

    public ArrayList<byte[]> getByteChannelName() {
        return mByteChannelName;
    }
    public DeviceInfo(byte[] _byteDevMac , byte[] _byteDevType){

        mByteDevMac = _byteDevMac;
        mByteDevType = _byteDevType;
        mIntDevType = CmdHelper.byteArrayToInt(_byteDevType);

    }
    public DeviceInfo(byte[] _byteDevMac , byte[] _byteDevType, byte[] _name){

        mByteDevMac = _byteDevMac;
        mByteDevType = _byteDevType;
        mIntDevType = CmdHelper.byteArrayToInt(_byteDevType);
        mByteDevName = _name;

    }
    public DeviceInfo(){
    }
    public DeviceInfo(int a){ // for test
        mByteDevMac = new byte[]{(byte)0xfa,(byte)0x11, (byte)0x01};
    }
    public void setByteChannelName(ArrayList<byte[]> _byteChannelName) {
        this.mByteChannelName = _byteChannelName;
    }


    public int getIntDevType() {
        return mIntDevType;
    }

    public void setIntDevType(int _intDevType) {
        this.mIntDevType = _intDevType;
    }


    public ArrayList<Integer> getIntChannelNo() {
        return mIntChannelNo;
    }

    public void setIntChannelNo(ArrayList<Integer> _intChannelNo) {
        this.mIntChannelNo = _intChannelNo;
    }


    public byte[] getDevMac() {
        return mByteDevMac;
    }

    public void setDevMac(byte[] _devMac) {
        this.mByteDevMac = _devMac;
    }


    public byte[] getDevType() {
        return mByteDevType;
    }

    public void setDevType(byte[] _devType) {
        this.mByteDevType = _devType;
    }

    public ArrayList<byte[]> getChannelNo() {
        return mByteChannelNo;
    }

    public void setChannelNo(ArrayList<byte[]> _channelNo) {
        this.mByteChannelNo = _channelNo;
    }

    public ArrayList<String> getChannelName() {
        return mChannelName;
    }

    public void setChannelName(ArrayList<String> _channelName) {
        this.mChannelName = _channelName;
    }

    public boolean getCheckStat() {
        return mCheckStat;
    }

    public void setCheckStat(boolean _checkStat) {
        this.mCheckStat = _checkStat;
    }


}
