package com.rareraw.master.network;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;


import com.rareraw.master.Config;
import com.rareraw.master.addscene.MoodListAdapter;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.network.DataClass.AlarmInfo;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.NetworkInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.SwitchInfo;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import static android.content.Context.WIFI_SERVICE;

import static com.rareraw.master.network.Cmd.CMD_ADMIN_SCENE_LIST;
import static com.rareraw.master.network.Cmd.CMD_ALARM_LIST;
import static com.rareraw.master.network.Cmd.CMD_CHECK_DEVICE;
import static com.rareraw.master.network.Cmd.CMD_CONNECT;
import static com.rareraw.master.network.Cmd.CMD_CONNECT_ADMIN;
import static com.rareraw.master.network.Cmd.CMD_CURRENT_SCENE;
import static com.rareraw.master.network.Cmd.CMD_DELETE_ALARM;
import static com.rareraw.master.network.Cmd.CMD_DELETE_DEVICE;
import static com.rareraw.master.network.Cmd.CMD_DELETE_SCENE;
import static com.rareraw.master.network.Cmd.CMD_DEVICE_RESET;
import static com.rareraw.master.network.Cmd.CMD_GET_DEVICE_DIMING;
import static com.rareraw.master.network.Cmd.CMD_GET_DMX_USE;
import static com.rareraw.master.network.Cmd.CMD_GET_NETWORK_INFO;
import static com.rareraw.master.network.Cmd.CMD_GET_SCENE;
import static com.rareraw.master.network.Cmd.CMD_GET_SCENE_DIMING_LEVEL;
import static com.rareraw.master.network.Cmd.CMD_GET_SCENE_PLAY;
import static com.rareraw.master.network.Cmd.CMD_GET_SCENE_SCHE;
import static com.rareraw.master.network.Cmd.CMD_GET_SENSOR_STATUS;
import static com.rareraw.master.network.Cmd.CMD_GET_SIWTCH_LIST;
import static com.rareraw.master.network.Cmd.CMD_MODIFY_ALARM;
import static com.rareraw.master.network.Cmd.CMD_MODIFY_INDEX;
import static com.rareraw.master.network.Cmd.CMD_PASSWORD_CHANGE;
import static com.rareraw.master.network.Cmd.CMD_REG_ALARM;
import static com.rareraw.master.network.Cmd.CMD_REG_DEVICE;
import static com.rareraw.master.network.Cmd.CMD_REG_DEV_LIST;
import static com.rareraw.master.network.Cmd.CMD_REG_SCENE;
import static com.rareraw.master.network.Cmd.CMD_REG_SCENE_PLAY;
import static com.rareraw.master.network.Cmd.CMD_REG_SCENE_SCHE;
import static com.rareraw.master.network.Cmd.CMD_RESET_ALARM;
import static com.rareraw.master.network.Cmd.CMD_SCENE_ACTION_CHANGE;
import static com.rareraw.master.network.Cmd.CMD_SCENE_LIST;
import static com.rareraw.master.network.Cmd.CMD_SET_AP_MODE;
import static com.rareraw.master.network.Cmd.CMD_SET_CHANNEL_NAME;
import static com.rareraw.master.network.Cmd.CMD_SET_CLIENT_MODE;
import static com.rareraw.master.network.Cmd.CMD_SET_DETECT_SENSOR_OFF;
import static com.rareraw.master.network.Cmd.CMD_SET_DETECT_SENSOR_ON;
import static com.rareraw.master.network.Cmd.CMD_SET_DEVICE_DIMING;
import static com.rareraw.master.network.Cmd.CMD_SET_DEVICE_NAME;
import static com.rareraw.master.network.Cmd.CMD_SET_DEVICE_POWER;
import static com.rareraw.master.network.Cmd.CMD_SET_DIMMER_ROOM;
import static com.rareraw.master.network.Cmd.CMD_SET_DMX_USE;
import static com.rareraw.master.network.Cmd.CMD_SET_DMX_VAL;
import static com.rareraw.master.network.Cmd.CMD_SET_LIGHT_SENSOR_OFF;
import static com.rareraw.master.network.Cmd.CMD_SET_LIGHT_SENSOR_ON;
import static com.rareraw.master.network.Cmd.CMD_SET_MASTER_DIMING;
import static com.rareraw.master.network.Cmd.CMD_SET_SCENE;
import static com.rareraw.master.network.Cmd.CMD_SET_SCENE_DIMING_LEVEL;
import static com.rareraw.master.network.Cmd.CMD_SET_SINGLE_DIMING;
import static com.rareraw.master.network.Cmd.CMD_SET_SIWTCH_BUTTON;
import static com.rareraw.master.network.Cmd.CMD_SET_SIWTCH_BUTTON_SECNE;
import static com.rareraw.master.network.Cmd.CMD_SET_SWITCH;
import static com.rareraw.master.network.Cmd.CMD_SIZGBEE_SETTING;
import static com.rareraw.master.network.Cmd.CMD_TIME_SETTING;
import static com.rareraw.master.network.Cmd.CMD_UNREG_DEV_LIST;
import static com.rareraw.master.network.Cmd.EMPTY_DATA;
import static com.rareraw.master.network.Cmd.FAIL_CODE;
import static com.rareraw.master.network.Cmd.PORT_NO;


/**
 * Created by Lch on 2016-10-04.
 */

public class APService {
    private static Analyzers mAnalyze = new Analyzers();
    private static APService UNIQUE_INSTANCE;
    public static String mIpAddress = Cmd.DEFAULT_IP; // default
    private static int mPort = PORT_NO;
    private static Context mContext;
    private static ByteBuffer mByteConnectionId = ByteBuffer.allocate(4);
    private static ByteBuffer mDefaultConnectionId = ByteBuffer.allocate(4);
    private static boolean mUserLevel = false; // false : normal user, true : admin user
    private static boolean mDmxUseFlag;
    private static String mInternalIp;
    // data class
    public static ArrayList<DeviceInfo> mRegistedDevList = new ArrayList<>();
    public static ArrayList<DeviceInfo> mUnRegistedDevList = new ArrayList<>();
    public static ArrayList<SceneInfo> mSceneList = new ArrayList<>();
    public static ArrayList<SceneInfo> mNormalSceneList = new ArrayList<>();
    public static ArrayList<AlarmInfo> mAlramList = new ArrayList<>();
    public static ArrayList<SwitchInfo> mSwitchInfoList = new ArrayList<>();
    public static NetworkInfo mNetworkInfo;

    public static ArrayList<SwitchInfo> getApSwitchInfoList() {
        return mSwitchInfoList;
    }

    public static void setApSwitchInfoList(ArrayList<SwitchInfo> SwitchInfoList) {
        mSwitchInfoList.clear();
        mSwitchInfoList = SwitchInfoList;
    }

    public static ArrayList<DeviceInfo> getApRegistedDevList() {
        if (Config.DEBUG) {
            ArrayList<DeviceInfo> temp = new ArrayList<>();
            byte[] alllight_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0xff, (byte) 0xff, (byte) 0xff,(byte) 0xff};
            byte[] text_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11};
            byte[] deviceType = new byte[]{(byte) 0x01};


            temp.add(new DeviceInfo(text_mac, deviceType));
            temp.add(new DeviceInfo(alllight_mac, deviceType));
            temp.add(new DeviceInfo(alllight_mac, deviceType));
            temp.add(new DeviceInfo(alllight_mac, deviceType));

            return temp;
        }
        return mRegistedDevList;
    }

    public static void setApRegistedDevList(ArrayList<DeviceInfo> RegistedDevList) {
        mRegistedDevList.clear();
        mRegistedDevList = RegistedDevList;
    }

    public static void addApRegistedDevList(ArrayList<SwitchInfo> RegistedDevList) {


        for (int i = 0; i < RegistedDevList.size(); i++) {
            DeviceInfo info = new DeviceInfo(RegistedDevList.get(i).getByteSwitchMac(), RegistedDevList.get(i).getSwitchType(), RegistedDevList.get(i).getmByteswitchName());
            mRegistedDevList.add(info);
        }
    }

    public static ArrayList<DeviceInfo> getApUnRegistedDevList() {
        return mUnRegistedDevList;
    }

    public static void setApUnRegistedDevList(ArrayList<DeviceInfo> UnRegistedDevList) {
        mUnRegistedDevList.clear();
        mUnRegistedDevList = UnRegistedDevList;
    }


    public static ArrayList<SceneInfo> getApSceneList() {

        if (Config.DEBUG) {
            ArrayList<SceneInfo> defaultSceneArray = new ArrayList<>();

            SceneInfo defaultScene = new SceneInfo();
            defaultScene.setSceneName("my scene1");
            defaultScene.setIntHoldingTime(1);

            SceneInfo defaultScene1 = new SceneInfo();
            defaultScene1.setSceneName("my scene2");
            defaultScene1.setIntHoldingTime(2);

            SceneInfo defaultScene2 = new SceneInfo();
            defaultScene2.setSceneName("my scene3");
            defaultScene2.setIntHoldingTime(3);


            defaultSceneArray.add(defaultScene);
            defaultSceneArray.add(defaultScene1);
            defaultSceneArray.add(defaultScene2);
            return defaultSceneArray;
        }

        return mSceneList;
    }


    public static ArrayList<SceneInfo> getApNormalSceneList() {

        if (Config.DEBUG) {
            ArrayList<SceneInfo> defaultSceneArray = new ArrayList<>();

            SceneInfo defaultScene = new SceneInfo();
            defaultScene.setSceneName("my scene1");
            defaultScene.setIntHoldingTime(1);

            SceneInfo defaultScene1 = new SceneInfo();
            defaultScene1.setSceneName("my scene2");
            defaultScene1.setIntHoldingTime(2);

            SceneInfo defaultScene2 = new SceneInfo();
            defaultScene2.setSceneName("my scene3");
            defaultScene2.setIntHoldingTime(3);


            defaultSceneArray.add(defaultScene);
            defaultSceneArray.add(defaultScene1);
            defaultSceneArray.add(defaultScene2);
            return defaultSceneArray;
        }

        return mNormalSceneList;
    }


    public static void setApSceneList(ArrayList<SceneInfo> SceneList) {
        mSceneList.clear();
        mSceneList = SceneList;

        ArrayList<SceneInfo> tempList = new ArrayList<>();
        for(int i = 0; i < SceneList.size(); i++){
            if(SceneList.get(i).getIntSceneType() == Cmd.NORMAL_SCENE)
                tempList.add(SceneList.get(i));
        }
        mNormalSceneList = tempList;
    }

    public static ArrayList<AlarmInfo> getApAlramList() {
        return mAlramList;
    }

    public static void setApAlramList(ArrayList<AlarmInfo> AlramList) {
        mAlramList.clear();
        mAlramList = AlramList;
    }


    public static void setDmxUseFlag(boolean stat) {
        mDmxUseFlag = stat;
    }

    public static boolean getDmxUseFlag() {
        return mDmxUseFlag;
    }

    public static ByteBuffer getConnectionId() {
        //return mConnectionId;
        return mByteConnectionId;
    }

    public static NetworkInfo getNetworkInfo() {
        return mNetworkInfo;
    }

    public static void setNetworkInfo(NetworkInfo networkMode) {
        mNetworkInfo = networkMode;
    }

    public static void setConnectionId(byte[] _sessionId) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.put(_sessionId);
        mByteConnectionId = buffer;
    }

    public static boolean getUserLevel() {
        return mUserLevel;
    }

    public static void setUserLevel(boolean _userlevel) {
        mUserLevel = _userlevel;
    }

    private APService(Context _context) {
        this.mContext = _context;
    }

    private APService() {
    }

    public static APService getInstance(Context _context) {
        if (UNIQUE_INSTANCE == null)
            synchronized (APService.class) {
                if (UNIQUE_INSTANCE == null)
                    UNIQUE_INSTANCE = new APService(_context);
            }
        return UNIQUE_INSTANCE;
    }

    public static ArrayList<MoodListAdapter.moodInfomation> getMoodList() {
        ArrayList<MoodListAdapter.moodInfomation> list = new ArrayList<>();

        // scene
        for (SceneInfo info : APService.getApSceneList()) {
            if (info.getIntSceneType() != Cmd.NORMAL_SCENE)
                continue;

            list.add(new MoodListAdapter.moodInfomation(info.getIntSceneNo(), info.getSceneName(), info.getIntSceneType()));
        }
        // scene play list delete
        // for (ScenePlayInfo info : APService.mScenePlayList)
        //    list.add(new SceneInfomation(info.getIntScenePlayNo(), info.getScenePlayName()));

        if (Config.DEBUG) {
            byte[] test = new byte[]{(byte) 0x00, (byte) 0x00};
            list.add(new MoodListAdapter.moodInfomation(1, "name1", Cmd.NORMAL_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name2", Cmd.NORMAL_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name3", Cmd.PLAY_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name5", Cmd.PLAY_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name6", Cmd.SCHEDULE_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name7", Cmd.SCHEDULE_SCENE));
            list.add(new MoodListAdapter.moodInfomation(1, "name8", Cmd.SCHEDULE_SCENE));

        }
        return list;
    }


    public void getWifipAddr() {
        WifiManager wm = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        DhcpInfo dhcpInfo = wm.getDhcpInfo();
        int nIpAddress = dhcpInfo.serverAddress;
        mIpAddress = Formatter.formatIpAddress(nIpAddress);
    }

    public static String getIpadress() {
        return mIpAddress;
    }

    public static void connectRequst(NetworkResultLisnter lisnster) {

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CONNECT, mDefaultConnectionId, EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_CONNECT, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "connect requst");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void pingRequst(NetworkResultLisnter lisnster, String internalIp) {

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CONNECT, mDefaultConnectionId, EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsgForPing(CMD_CONNECT, lisnster, sendMsg, internalIp);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "connect requst");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void connectAdminRequst(String password, NetworkResultLisnter lisnster) {

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CONNECT_ADMIN, getConnectionId(), password);
        try {
            NetworkManager.getInstance().postMsg(CMD_CONNECT_ADMIN, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "connect connectAdminRequst");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void synchronizeTime(String _currenTime, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_TIME_SETTING, getConnectionId(), _currenTime);
        try {
            NetworkManager.getInstance().postMsg(CMD_TIME_SETTING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "syncho time");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getRegistedDeviceInfo(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_DEV_LIST, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_DEV_LIST, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "get registed dev");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getUnRegistedDeviceInfo(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_UNREG_DEV_LIST, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_UNREG_DEV_LIST, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "get unregisted dev");
            lisnster.onError(FAIL_CODE);
        }

    }

    // It is recommended that Apservice object be used rather than object copy (reference).
    public static void getSceneList(NetworkResultLisnter lisnster) {
        byte[] CMD = null;
        if (APService.getUserLevel())
            CMD = CMD_ADMIN_SCENE_LIST;
        else
            CMD = CMD_SCENE_LIST;

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "get scene list");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getAlramList(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_ALARM_LIST, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_ALARM_LIST, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "get alram list");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void registerDevice(byte[] _deviceMac, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(_deviceMac);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_DEVICE, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_DEVICE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "register device");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void deleteDevice(byte[] _deviceMac, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(_deviceMac);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_DELETE_DEVICE, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_DELETE_DEVICE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "delte device");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void checkBlinkDevice(byte[] _deviceMac, byte[] type) {

        ByteBuffer buffer = ByteBuffer.allocate(9);
        buffer.put(_deviceMac);
        buffer.put(type);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CHECK_DEVICE, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_CHECK_DEVICE, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "check device on");
        }
    }


    public static void resetDevice(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_DEVICE_RESET, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_DEVICE_RESET, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "device reset");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setScene(int _sceneNo, NetworkResultLisnter lisnster) {
        ByteBuffer bufferSceneNo = ByteBuffer.allocate(2);
        bufferSceneNo.put(CmdHelper.intToByteArray(_sceneNo));
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SCENE, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SCENE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "set scene");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void deleteScene(int _sceneNo, NetworkResultLisnter lisnster) {
        ByteBuffer bufferSceneNo = ByteBuffer.allocate(2);
        bufferSceneNo.put(CmdHelper.intToByteArray(_sceneNo));
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_DELETE_SCENE, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_DELETE_SCENE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "delete scene");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void setZigBeeChannel(String _zigbeeSetValue, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SIZGBEE_SETTING, getConnectionId(), _zigbeeSetValue);
        try {
            NetworkManager.getInstance().postMsg(CMD_SIZGBEE_SETTING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setZigBeeChannel");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void changePassword(String _password_pair, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_PASSWORD_CHANGE, getConnectionId(), _password_pair);
        try {
            NetworkManager.getInstance().postMsg(CMD_PASSWORD_CHANGE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "changePassword");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void resetAlram(NetworkResultLisnter lisnster) {

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_RESET_ALARM, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_RESET_ALARM, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "resetAlram");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void deleteAlram(ByteBuffer _alramKey, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_DELETE_ALARM, getConnectionId(), _alramKey);
        try {
            NetworkManager.getInstance().postMsg(CMD_DELETE_ALARM, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "deleteAlram");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void registerAlram(ByteBuffer _alraminfo, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_ALARM, getConnectionId(), _alraminfo);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_ALARM, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "registerAlram");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setMasterDiming(String _dimingLevel, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_MASTER_DIMING, getConnectionId(), _dimingLevel);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_MASTER_DIMING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setMasterDiming");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setSingleDiming(ByteBuffer _channelNo) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SINGLE_DIMING, getConnectionId(), _channelNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SINGLE_DIMING, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSingleDiming");

        }
    }

    public static void setChannelName(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_CHANNEL_NAME, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_CHANNEL_NAME, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setChannelName");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void setDeviceName(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DEVICE_NAME, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DEVICE_NAME, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDeviceName");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void registerScene(ByteBuffer _scene, NetworkResultLisnter lisnster) {

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_SCENE, getConnectionId(), _scene);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_SCENE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "registerScene");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void registerScenePlay(ByteBuffer _sceneplay, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_SCENE_PLAY, getConnectionId(), _sceneplay);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_SCENE_PLAY, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "registerScenePlay");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void getScenePlaySingle(int _sceneNo, NetworkResultLisnter lisnster) {
        ByteBuffer bufferSceneNo = ByteBuffer.allocate(2);
        bufferSceneNo.put(CmdHelper.intToByteArray(_sceneNo));
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SCENE_PLAY, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SCENE_PLAY, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "get scene list");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void registerSceneSchedule(ByteBuffer _sceneSche, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_REG_SCENE_SCHE, getConnectionId(), _sceneSche);
        try {
            NetworkManager.getInstance().postMsg(CMD_REG_SCENE_SCHE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "registerSceneSchedule");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getSceneScheduleSingle(int _sheduleNo, NetworkResultLisnter lisnster) {

        ByteBuffer bufferSceneNo = ByteBuffer.allocate(2);
        bufferSceneNo.put(CmdHelper.intToByteArray(_sheduleNo));

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SCENE_SCHE, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SCENE_SCHE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getSceneScheduleSingle");
            lisnster.onError(FAIL_CODE);
        }

    }

    public static void getSceneSingle(int _sceneNo, NetworkResultLisnter lisnster) {
        ByteBuffer bufferSceneNo = ByteBuffer.allocate(2);
        bufferSceneNo.put(CmdHelper.intToByteArray(_sceneNo));
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SCENE, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SCENE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getSceneSingle");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setZigBeeChannel(ByteBuffer _zigbeeSetValue, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SIZGBEE_SETTING, getConnectionId(), _zigbeeSetValue);
        try {
            NetworkManager.getInstance().postMsg(CMD_SIZGBEE_SETTING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setZigBeeChannel");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getCurrentActionScene(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CURRENT_SCENE, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_CURRENT_SCENE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getSceneSingle");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void modifySceneIndexNo(int sNo, int eNo, NetworkResultLisnter lisnster) {

        ByteBuffer bufferSceneNo = ByteBuffer.allocate(4);
        bufferSceneNo.put(CmdHelper.intToByteArray(sNo));
        bufferSceneNo.put(CmdHelper.intToByteArray(eNo));

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_MODIFY_INDEX, getConnectionId(), bufferSceneNo);
        try {
            NetworkManager.getInstance().postMsg(CMD_MODIFY_INDEX, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "modifySceneIndexNo");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setNetworkApMode(ByteBuffer _data, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_AP_MODE, getConnectionId(), _data);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_AP_MODE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setNetworkApMode");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setNetworkClientMode(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_CLIENT_MODE, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_CLIENT_MODE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setNetworkClientMode");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getCurrentSceneDimingLevel(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SCENE_DIMING_LEVEL, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SCENE_DIMING_LEVEL, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getCurrentSceneDimingLevel");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setCurrentSceneDimingLevel(int _sceneNo, int dimingLevel, NetworkResultLisnter lisnster) {

        ByteBuffer buffer = ByteBuffer.allocate(3);
        buffer.put(CmdHelper.intToByteArray(_sceneNo));
        buffer.put((byte) dimingLevel);

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SCENE_DIMING_LEVEL, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SCENE_DIMING_LEVEL, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setCurrentSceneDimingLevel");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void modifyAlram(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_MODIFY_ALARM, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_MODIFY_ALARM, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "modifyAlram");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setSwitchInfo(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SWITCH, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SWITCH, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSwitchInfo");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getSwitchInfo(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SIWTCH_LIST, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SIWTCH_LIST, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getSwitchInfo");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setDmxUsage(boolean useStat, NetworkResultLisnter lisnster) {
        byte[] cmd = new byte[]{(byte) 0x00};
        cmd[0] = (useStat) ? (byte) 0x01 : (byte) 0x00;

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DMX_USE, getConnectionId(), new String(cmd));
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DMX_USE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDmxUsage");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getDmxUsage(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_DMX_USE, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_DMX_USE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getDmxUsage");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void changeSceneAction(boolean status, NetworkResultLisnter lisnster) {

        byte[] cmd = new byte[]{(byte) 0x00};
        cmd[0] = (status) ? (byte) 0x01 : (byte) 0x00;

        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SCENE_ACTION_CHANGE, getConnectionId(), new String(cmd));
        try {
            NetworkManager.getInstance().postMsg(CMD_SCENE_ACTION_CHANGE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "changeSceneAction");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getNetworkMode(NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_NETWORK_INFO, getConnectionId(), EMPTY_DATA);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_NETWORK_INFO, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getNetworkMode");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setSortedSceneList(ByteBuffer buffer, NetworkResultLisnter lisnter) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_MODIFY_INDEX, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_MODIFY_INDEX, lisnter, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getNetworkMode");
            lisnter.onError(FAIL_CODE);
        }
    }

    public static void setSwitchButtonWithScene(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SIWTCH_BUTTON_SECNE, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SIWTCH_BUTTON_SECNE, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSwitchButtonScene");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setSwitchButton(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_SIWTCH_BUTTON, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_SIWTCH_BUTTON, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSwitchButton");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setDmxVal(ByteBuffer buffer, NetworkResultLisnter lisnster) {
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DMX_VAL, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DMX_VAL, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDmxVal");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getSensorInfo(NetworkResultLisnter lisnster) {
        byte[] data = new byte[]{(byte) 0x00};
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put(data);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_SENSOR_STATUS, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_SENSOR_STATUS, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getSensorInfo");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setSensorDetectStatus(NetworkResultLisnter lisnster, boolean status) {
        byte[] data = new byte[]{(byte) 0x00};
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put(data);

        byte[] Cmd = status == true ? CMD_SET_DETECT_SENSOR_ON : CMD_SET_DETECT_SENSOR_OFF;

        ByteBuffer sendMsg = CmdHelper.makeMessage(Cmd, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(Cmd, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSensorDetectStatus");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void setSensorLightStatus(NetworkResultLisnter lisnster, boolean status) {
        byte[] data = new byte[]{(byte) 0x00};
        ByteBuffer buffer = ByteBuffer.allocate(1);
        buffer.put(data);

        byte[] Cmd = status == true ? CMD_SET_LIGHT_SENSOR_ON : CMD_SET_LIGHT_SENSOR_OFF;

        ByteBuffer sendMsg = CmdHelper.makeMessage(Cmd, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(Cmd, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setSensorLightStatus");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void getDeviceDiming(byte[] _deviceMac, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(8);
        buffer.put(_deviceMac);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_GET_DEVICE_DIMING, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_GET_DEVICE_DIMING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "getDeviceDiming");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void setDeviceDiming(byte[] _deviceMac, byte[] _level, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(9);
        buffer.put(_deviceMac);
        buffer.put(_level);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DEVICE_DIMING, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DEVICE_DIMING, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDeviceDiming");
            lisnster.onError(FAIL_CODE);
        }
    }


    public static void setDevicePower(byte[] _deviceMac, byte[] _level, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(9);
        buffer.put(_deviceMac);
        buffer.put(_level);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DEVICE_POWER, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DEVICE_POWER, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDevicePower");
            lisnster.onError(FAIL_CODE);
        }
    }

    public static void setDimmerRoom(byte[] _deviceMac, byte[] dimmerMac, NetworkResultLisnter lisnster) {
        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.put(_deviceMac);
        buffer.put(dimmerMac);
        ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_SET_DIMMER_ROOM, getConnectionId(), buffer);
        try {
            NetworkManager.getInstance().postMsg(CMD_SET_DIMMER_ROOM, lisnster, sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("send error : ", "setDevicePower");
            lisnster.onError(FAIL_CODE);
        }
    }
}


