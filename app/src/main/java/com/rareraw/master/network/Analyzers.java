package com.rareraw.master.network;

import android.util.Log;

import com.rareraw.master.addscene.DmxInfo;
import com.rareraw.master.data.Dimmer;
import com.rareraw.master.data.SensorInfo;
import com.rareraw.master.network.DataClass.AlarmInfo;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.DataClass.NetworkInfo;
import com.rareraw.master.network.DataClass.SceneIndividualInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.ScenePlayInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;
import com.rareraw.master.data.Channel;
import com.rareraw.master.network.DataClass.SwitchInfo;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lch on 2016-10-06.
 */

public class Analyzers {

    private static Analyzers _instance;
    /*public in class*/
    private byte[] mResultBuff = new byte[1];

    /*connect device*/
    private byte[] mSessionBuf = new byte[4];

    /*device list*/
    private byte[] mMac = new byte[8];
    private byte[] mDevName = new byte[16];
    private byte[] mDevType = new byte[1];
    private byte[] mChannelNo = new byte[Cmd.CHANNEL_SIZE];
    private byte[] mChannelName = new byte[Cmd.CHANNEL_NAME_SIZE];

    /*scene list*/
    private byte[] mSceneCnt = new byte[2];
    private byte[] mCnt = new byte[1];
    private byte[] mButtonNo = new byte[1];
    private byte[] mSceneNo = new byte[2];
    private byte[] mSceneType = new byte[1];
    private byte[] mSceneIconType = new byte[1];
    private byte[] mSceneHideType = new byte[1];
    private byte[] mSceneName = new byte[16];
    private byte[] mSceneIndex = new byte[2];
    private byte[] mSceneFadeTime = new byte[1];
    private byte[] mRepeat = new byte[1];
    private byte[] mScenePlayTotalCnt = new byte[1];
    private byte[] mHoldingTime = new byte[1];
    private byte[] mWeek = new byte[7];
    private byte[] mStartTime = new byte[2];
    private byte[] mEndtime = new byte[2];
    private byte[] mChannelCnt = new byte[1];
    private byte[] mDimingLevel = new byte[1];

    /*alarm list*/
    private byte[] malarmCnt = new byte[2];
    private byte[] malarmKey = new byte[2];
    private byte[] malarmHour = new byte[1];
    private byte[] malarmMin = new byte[1];
    private byte[] malarmAction = new byte[1];

    /*dmx or network*/
    private byte[] mDmxUseflag = new byte[1];
    private byte[] mDmxArrayInfo = new byte[Cmd.MAX_DMX_SIZE];

    private byte[] mNetworkMode = new byte[1];
    private byte[] mSsidName = new byte[16];
    private byte[] mNetPassword= new byte[16];

    // dimmer
    private byte[] mPowerStatus = new byte[1];
    private byte[] mDIming = new byte[1];
    public static Analyzers getInstance() {
        if (_instance == null) {
            synchronized (Analyzers.class) {
                _instance = new Analyzers();
            }
        }
        return _instance;
    }


    public int ErrorChceker(ByteBuffer response) {
        try {
            if (response == null)
                return Cmd.UNKWON_ERROR;

            // ack mininum size is ACK_MININUM_SIZE
            if (response.capacity() < Cmd.ACK_MININUM_SIZE)
                return Cmd.UNKWON_ERROR;

            response.position(Cmd.ACK_POSITION);
            response.get(mResultBuff);

            if (mResultBuff[0] == Cmd.FAIL_CODE)
                return Cmd.FAIL_CODE;

            if (mResultBuff[0] == Cmd.CONNECT_ID_NOT_MATCH)
                return Cmd.CONNECT_ID_NOT_MATCH;

            if (mResultBuff[0] == Cmd.OK_CODE)
                return Cmd.OK_CODE;


        } catch (Exception e) {
            Log.e("exception: ", "erroChecker");
            e.printStackTrace();
            return Cmd.UNKWON_ERROR;
        }
        return Cmd.UNKWON_ERROR;
    }

    public boolean nullChecker(ByteBuffer response) {
        if (response == null)
            return false;
        else
            return true;
    }

    public ArrayList<DeviceInfo> setRegistedDevList(ByteBuffer response) {

        ArrayList<DeviceInfo> devices = new ArrayList<>();
        //response.position(ACK_POSITION);
        response.get(mResultBuff);

        for (int cnt = 0; cnt < (int) mResultBuff[0]; cnt++) {
            DeviceInfo dev = new DeviceInfo();
            response.get(mMac);
            dev.setDevMac(mMac);
            mMac = new byte[8];

            response.get(mDevName);
            dev.setmByteDevName(mDevName);
            mDevName = new byte[16];

            response.get(mDevType);
            dev.setDevType(mDevType);
            dev.setIntDevType(mDevType[0]);
            mDevType = new byte[1];

            ArrayList<Integer> intChannelNo = new ArrayList<>();
            ArrayList<byte[]> byteChannelNo = new ArrayList<>();

            ArrayList<String> sChannelName = new ArrayList<>();
            ArrayList<byte[]> byteChannelName = new ArrayList<>();
            for (int chCnt = 0; chCnt < Cmd.CHANNEL_MAX_CNT; chCnt++) {
                response.get(mChannelNo);
                //response.get(mChannelName);

                // 0x00 not using channel no
                if (mChannelNo[0] == 0)
                    continue;

                intChannelNo.add((int) mChannelNo[0]);
                byteChannelNo.add(mChannelNo);

                //sChannelName.add(new String(mChannelName));
                //byteChannelName.add(mChannelName);

                mChannelNo = new byte[Cmd.CHANNEL_SIZE];
                //mChannelName = new byte[Cmd.CHANNEL_NAME_SIZE];
            }
            dev.setIntChannelNo(intChannelNo);
            dev.setChannelNo(byteChannelNo);

            //dev.setChannelName(sChannelName);
            //dev.setByteChannelName(byteChannelName);

            devices.add(dev);
        }
        APService.setApRegistedDevList(devices);
        return devices;
    }

    public ArrayList<DeviceInfo> setUnRegistedDevList(ByteBuffer response) {

        ArrayList<DeviceInfo> devices = new ArrayList<>();
        //response.position(ACK_POSITION);
        response.get(mResultBuff);

        for (int cnt = 0; cnt < (int) mResultBuff[0]; cnt++) {
            DeviceInfo dev = new DeviceInfo();
            response.get(mMac);

            dev.setDevMac(mMac);
            mMac = new byte[8];

            //response.get(mDevName);
            //dev.setmByteDevName(mDevName);
            //mDevName = new byte[16];

            response.get(mDevType);
            dev.setDevType(mDevType);
            dev.setIntDevType(mDevType[0]);
            mDevType = new byte[1];

            dev.setChannelName(null);
            dev.setByteChannelName(null);
            devices.add(dev);
        }
        APService.setApUnRegistedDevList(devices);
        return devices;
    }

    public ArrayList<SceneInfo> setSceneList(ByteBuffer response) {
        ArrayList<SceneInfo> scenes = new ArrayList<>();
        //response.position(ACK_POSITION);
        response.get(mSceneCnt);

        int totalCnt = CmdHelper.byteArrayToInt(mSceneCnt);
        for (int cnt = 0; cnt < totalCnt; cnt++) {
            SceneInfo scene = new SceneInfo();
            response.get(mSceneNo);

            scene.setByteSceneNo(mSceneNo);
            scene.setIntSceneNo(CmdHelper.byteArrayToInt(mSceneNo));
            mSceneNo = new byte[2];

            response.get(mSceneName);
            scene.setSceneName(new String(mSceneName));
            scene.setByteSceneName(mSceneName);
            mSceneName = new byte[16];

            response.get(mSceneIndex);
            scene.setByteIndex(mSceneIndex);
            scene.setIntIndex(CmdHelper.byteArrayToInt(mSceneIndex));
            mSceneIndex = new byte[2];

            response.get(mSceneType);
            scene.setByteIconType(mSceneType);
            scene.setIntSceneType(mSceneType[0]);
            mSceneType = new byte[1];

            response.get(mSceneIconType);
            scene.setByteIconType(mSceneIconType);
            scene.setIntIconType(mSceneIconType[0]);
            mSceneIconType = new byte[1];

            response.get(mSceneHideType);
            scene.setHideFlag((mSceneHideType[0] == 0) ? false : true);
            scene.setmByteHideFlag(mSceneHideType);
            mSceneHideType = new byte[1];
            scenes.add(scene);
        }
        APService.setApSceneList(scenes);
        return scenes;
    }

    public ArrayList<AlarmInfo> setAlarmList(ByteBuffer response) {
        ArrayList<AlarmInfo> alarmList = new ArrayList<>();
        //response.position(ACK_POSITION);
        response.get(malarmCnt);

        int totalCnt = CmdHelper.byteArrayToInt(malarmCnt);
        for (int cnt = 0; cnt < totalCnt; cnt++) {
            AlarmInfo alarm = new AlarmInfo();

            response.get(malarmKey);

            alarm.setAlarmKey(malarmKey);
            malarmKey = new byte[2];

            response.get(malarmHour);
            alarm.setAlarmTimeHour(malarmHour);
            alarm.setIntAlarmTimeHour(malarmHour[0]);
            malarmHour = new byte[1];

            response.get(malarmMin);
            alarm.setAlarmTimeMin(malarmMin);
            alarm.setIntAlarmTimeMin(malarmMin[0]);
            malarmMin = new byte[1];

            response.get(mWeek);
            alarm.setAlarmWeek(mWeek);
            mWeek = new byte[7];

            response.get(mSceneNo);
            alarm.setBSceneNo(mSceneNo);
            alarm.setNSceneNo(CmdHelper.byteArrayToInt(mSceneNo));
            mSceneNo = new byte[2];

            response.get(malarmAction);
            alarm.setmBAlarmAction(malarmAction);
            alarm.setIntAlarmAction(malarmAction[0]);
            alarm.setBooleanAction((malarmAction[0] == 0) ? false : true);
            malarmAction = new byte[1];
            alarmList.add(alarm);
        }
        APService.setApAlramList(alarmList);
        return alarmList;
    }

    //public DeviceInfo setSingleDevice(ByteBuffer response, byte[] _mac) {
    public DeviceInfo setSingleDevice(ByteBuffer response) {
        //response.position(ACK_POSITION);
        DeviceInfo devices = new DeviceInfo();

        response.get(mMac);
        devices.setDevMac(mMac);
        response.get(mDevType);
        devices.setDevType(mDevType);
        devices.setIntDevType(mDevType[0]);

        ArrayList<byte[]> bChannelNoList = new ArrayList<>();
        ArrayList<Integer> nChannelNoList = new ArrayList<>();
        for (int chCnt = 0; chCnt < Cmd.CHANNEL_MAX_CNT; chCnt++) {
            response.get(mChannelNo);
            bChannelNoList.add(mChannelNo);
            nChannelNoList.add((int) mChannelNo[0]);
            mChannelNo = new byte[Cmd.CHANNEL_SIZE];
        }
        devices.setChannelNo(bChannelNoList);
        devices.setIntChannelNo(nChannelNoList);
        // devices.setDevMac(_mac);
        APService.getApRegistedDevList().add(devices);

        return devices;
        // TODO mac도 주면 그걸로 파싱한느걸로 바꿔야함.
    }

    public ScenePlayInfo getScenePlaySingle(ByteBuffer response) {
        ScenePlayInfo sceneplay = new ScenePlayInfo();
        //response.position(ACK_POSITION);
        // response.get(mResultBuff);

        response.get(mSceneName);
        sceneplay.setScenePlayName(new String(mSceneName));
        sceneplay.setByteScenePlayName(mSceneName);

        response.get(mSceneIndex);
        sceneplay.setByteIndexNo(mSceneIndex);
        sceneplay.setIntIndexNo(CmdHelper.byteArrayToInt(mSceneIndex));

        response.get(mSceneIconType);
        sceneplay.setByteIconType(mSceneIconType);
        sceneplay.setIntIconType(mSceneIconType[0]);

        response.get(mSceneHideType);
        sceneplay.setBoolHideFlag((mSceneHideType[0] == 0) ? false : true);
        sceneplay.setByteHideFlag(mSceneHideType);

        response.get(mRepeat);
        sceneplay.setBoolRepeatFlag((mRepeat[0] == 0) ? false : true);
        sceneplay.setByteRepeatFlag(mRepeat);

        response.get(mScenePlayTotalCnt);

        ArrayList<byte[]> bSceneNo = new ArrayList<>();
        ArrayList<Integer> nSceneNo = new ArrayList<>();

        ArrayList<byte[]> bHoldingTime = new ArrayList<>();
        ArrayList<Integer> nHoldingTime = new ArrayList<>();
        ArrayList<SceneInfo> sceneList = new ArrayList<>();
        for (int sceneCnt = 0; sceneCnt < mScenePlayTotalCnt[0]; sceneCnt++) {
            response.get(mSceneNo);
            bSceneNo.add(mSceneNo);
            nSceneNo.add(CmdHelper.byteArrayToInt(mSceneNo));

            response.get(mHoldingTime);
            bHoldingTime.add(mHoldingTime);
            nHoldingTime.add((int) mHoldingTime[0]);

            SceneInfo scene = new SceneInfo();
            scene.setIntHoldingTime((int) mHoldingTime[0]);
            scene.setByteHoldintTime(mHoldingTime);
            scene.setIntSceneNo(CmdHelper.byteArrayToInt(mSceneNo));
            scene.setByteSceneNo(mSceneNo);
            sceneList.add(scene);

            mHoldingTime = new byte[1];
            mSceneNo = new byte[2];

        }
        sceneplay.setSceneList(sceneList);
        sceneplay.setByteSceneNo(bSceneNo);
        sceneplay.setIntSceneNo(nSceneNo);

        sceneplay.setByteHoldingTime(bHoldingTime);
        sceneplay.setIntHoldingTime(nHoldingTime);
        return sceneplay;
    }


    public SceneInfo getSceneSingle(ByteBuffer response) {
        SceneInfo sceneInfo = new SceneInfo();
        //response.position(ACK_POSITION);
        //response.get(mResultBuff);

        response.get(mSceneName);
        sceneInfo.setByteSceneName(mSceneName);
        sceneInfo.setSceneName(new String(mSceneName));

        response.get(mSceneIndex);
        sceneInfo.setByteIndex(mSceneIndex);
        sceneInfo.setIntIndex(CmdHelper.byteArrayToInt(mSceneIndex));

        response.get(mSceneIconType);
        sceneInfo.setByteIconType(mSceneIconType);
        sceneInfo.setIntIconType(mSceneIconType[0]);

        response.get(mSceneHideType);
        sceneInfo.setHideFlag((mSceneHideType[0] == 0) ? false : true);
        sceneInfo.setmByteHideFlag(mSceneHideType);

        response.get(mSceneFadeTime);
        sceneInfo.setByteFadeTime(mSceneFadeTime);
        sceneInfo.setIntFadeTime(mSceneFadeTime[0]);

        response.get(mChannelCnt);

        ArrayList<byte[]> bChannelNoList = new ArrayList<>();
        ArrayList<Integer> nChannelNoList = new ArrayList<>();

        ArrayList<byte[]> bDimingLevelList = new ArrayList<>();
        ArrayList<Integer> nDimingLevelList = new ArrayList<>();

        ArrayList<byte[]> bChannelNameList = new ArrayList<>();
        ArrayList<String> sChannelNameList = new ArrayList<>();

        List<Channel> channels = new ArrayList<>();
        for (int cnt = 0; cnt < mChannelCnt[0]; cnt++) {
            response.get(mChannelNo);
            bChannelNoList.add(mChannelNo);
            nChannelNoList.add((int) mChannelNo[0]);


            response.get(mDimingLevel);
            bDimingLevelList.add(mDimingLevel);
            nDimingLevelList.add((int) mDimingLevel[0]);


            //response.get(mChannelName);
            //bChannelNameList.add(mChannelName);
            //sChannelNameList.add(new String(mChannelName));

            channels.add(new Channel("", (int) mDimingLevel[0], true, (int) mChannelNo[0]));

            //mChannelName = new byte[Cmd.CHANNEL_NAME_SIZE];
            mDimingLevel = new byte[1];
            mChannelNo = new byte[Cmd.CHANNEL_SIZE];
        }

        //response.get(mDmxUseflag);
        sceneInfo.setDmxUseFlag(false);
        // response.get(mDmxArrayInfo);
/*
        DmxInfo[] dmxArray = new DmxInfo[Cmd.MAX_DMX_SIZE];
        for (int i = 0; i < Cmd.MAX_DMX_SIZE; i++) {
            dmxArray[i] = new DmxInfo();

            if( mDmxArrayInfo[i] < 0)
                dmxArray[i].mIntValue = (int) mDmxArrayInfo[i] + 256;
            else
                dmxArray[i].mIntValue = (int) mDmxArrayInfo[i];

            dmxArray[i].mNo = i + 1;
            dmxArray[i].mUseFlag = (mDmxArrayInfo[i] > 0) ? 2 : 0; // 2 :saved or used, 0 : not use
        }*/
        //sceneInfo.setDmxList(dmxArray);
        sceneInfo.setChannelList(channels);
        sceneInfo.setByteChannelNo(bChannelNoList);
        sceneInfo.setIntChannelNo(nChannelNoList);

        sceneInfo.setByteChannelDiming(bDimingLevelList);
        sceneInfo.setIntChannelDiing(nDimingLevelList);

        //sceneInfo.setByteChannelName(bChannelNameList);
       // sceneInfo.setChannelName(sChannelNameList);

        return sceneInfo;
    }

    public SceneScheduleInfo getScheduleSceneInfo(ByteBuffer response) {
        SceneScheduleInfo sceneInfo = new SceneScheduleInfo();

        //response.position(ACK_POSITION);
        //response.get(mResultBuff);

        response.get(mSceneName);
        sceneInfo.setSceneScheduleName(new String(mSceneName));
        sceneInfo.setByteSceneScheduleName(mSceneName);

        response.get(mSceneIndex);
        sceneInfo.setByteIndexNo(mSceneIndex);
        sceneInfo.setIntIndexNo(CmdHelper.byteArrayToInt(mSceneIndex));

        response.get(mSceneIconType);
        sceneInfo.setByteIconType(mSceneIconType);
        sceneInfo.setIntIconType(mSceneIconType[0]);

        response.get(mSceneHideType);
        sceneInfo.setByteHideFlag(mSceneHideType);
        sceneInfo.setBoolHideFlag((mSceneHideType[0] == 0) ? false : true);

        response.get(mWeek);
        sceneInfo.setByteWeek(mWeek);

        response.get(mCnt);

        ArrayList<byte[]> bSceneNo = new ArrayList<>();
        ArrayList<Integer> nSceneNo = new ArrayList<>();

        ArrayList<byte[]> bFadeTime = new ArrayList<>();
        ArrayList<Integer> nFadeTime = new ArrayList<>();

        ArrayList<byte[]> bStartTime = new ArrayList<>();
        ArrayList<Integer> nStartTime = new ArrayList<>();

        ArrayList<byte[]> bEndTime = new ArrayList<>();
        ArrayList<Integer> nEndTime = new ArrayList<>();


        ArrayList<SceneIndividualInfo> sceneList = new ArrayList<>();
        for (int cnt = 0; cnt < mCnt[0]; cnt++) {
            SceneIndividualInfo scene = new SceneIndividualInfo();
            response.get(mSceneNo);
            bSceneNo.add(mSceneNo);
            nSceneNo.add(CmdHelper.byteArrayToInt(mSceneNo));

            scene.setIntSceneNo(CmdHelper.byteArrayToInt(mSceneNo));
            scene.setByteSceneNo(mSceneNo);
            mSceneNo = new byte[2];

            response.get(mStartTime);
            bStartTime.add(mStartTime);
            nStartTime.add(CmdHelper.byteArrayToInt(mStartTime));

            scene.setIntStartHour(mStartTime[0]);
            scene.setIntStartMin(mStartTime[1]);
            scene.setByteStartMin(mStartTime[0]);
            scene.setByteStartMin(mStartTime[1]);
            mStartTime = new byte[2];

            response.get(mEndtime);
            bEndTime.add(mEndtime);
            nEndTime.add(CmdHelper.byteArrayToInt(mEndtime));

            scene.setIntEndHour(mEndtime[0]);
            scene.setIntEndMin(mEndtime[1]);
            scene.setByteEndHour(mEndtime[0]);
            scene.setByteEndMin(mEndtime[1]);
            mEndtime = new byte[2];
            sceneList.add(scene);
        }

        sceneInfo.setSceneList(sceneList);
        sceneInfo.setByteSceneNo(bSceneNo);
        sceneInfo.setIntSceneNo(nSceneNo);

        sceneInfo.setByteStartTime(bStartTime);
        sceneInfo.setIntStartTime(nStartTime);

        sceneInfo.setByteEndTime(bEndTime);
        sceneInfo.setIntEndTime(nEndTime);
        return sceneInfo;
    }

    public int getCurrentSceneNo(ByteBuffer response) {
        //response.position(ACK_POSITION);
        response.get(mSceneNo);
        return CmdHelper.byteArrayToInt(mSceneNo);
    }

    public int getCurrentSceneDiming(ByteBuffer response) {
        //response.position(ACK_POSITION);
        response.get(mDimingLevel);
        return (int) mDimingLevel[0];
    }

    public ArrayList<SwitchInfo> parseSwitchInfo(ByteBuffer response) {
        //response.position(ACK_POSITION);
        response.get(mCnt);
        ArrayList<SwitchInfo> switchInfoArray = new ArrayList<>();
        boolean oldLevel = APService.getUserLevel();
        APService.setUserLevel(true);
        for (int i = 0; i < mCnt[0]; i++) {
            // response.get(mButtonNo);

            SwitchInfo info = new SwitchInfo();
            mMac = new byte[8];
            response.get(mMac);
            info.setByteSwitchMac(mMac);
            info.setByteSwitchType( new byte[]{(byte) 0x00, (byte) 0x22});

            mDevName = new byte[16];
            response.get(mDevName);
            info.setmByteswitchName(mDevName);

            //byte[] text_name = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11,(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11};
            //info.setmByteswitchName(text_name);


            mMac = new byte[8];
            response.get(mMac);
            info.setmByteDevMac(mMac);
            //byte[] text_mac = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11,(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00,(byte) 0x11};
            //info.setmByteDevMac(text_mac);

            ArrayList<byte[]> bSceneNo = new ArrayList<>();
            for(int cnt = 0; cnt< Cmd.SIWTCH_MAX_CNT; cnt++){
            //    mSceneNo = new byte[2];
            ///    response.get(mSceneNo);
            //   bSceneNo.add(mSceneNo);
            }

            info.setSceneList(bSceneNo);
            switchInfoArray.add(info);
        }

        APService.setApSwitchInfoList(switchInfoArray);
        APService.addApRegistedDevList(switchInfoArray);
        APService.setUserLevel(oldLevel);
        return switchInfoArray;

    }

    public boolean getDmxUseStatsus(ByteBuffer response) {
        //response.position(ACK_POSITION);
        response.get(mDmxUseflag);
        if (mDmxUseflag[0] == 0)
            APService.setDmxUseFlag(false);
        else
            APService.setDmxUseFlag(true);

        return true;
    }

    public NetworkInfo setNetworkInfo(ByteBuffer response) {
        //response.position(ACK_POSITION);
        response.get(mNetworkMode);
        response.get(mSsidName);
        response.get(mNetPassword); // a1 : apmode , c1 : client mode
        NetworkInfo info = new NetworkInfo(mNetworkMode[0] == 0xa1? false : true, new String(mSsidName), new String(mNetPassword));
        APService.setNetworkInfo(info);
        return info;

    }

    public boolean setSettionId(ByteBuffer responseAck) {
        //responseAck.position(ACK_POSITION);
        responseAck.get(mSessionBuf);
        APService.setConnectionId(mSessionBuf);
        APService.setUserLevel(false);
        return true;
    }

    public boolean setAdminMode(ByteBuffer responseAck) {
        APService.setUserLevel(true);
        return true;
    }

    public SensorInfo getSensorInfo(ByteBuffer response) {
        SensorInfo info = new SensorInfo();

        byte[] curScene = new byte[2];
        response.get(curScene);

        info.setByteSceneNo(curScene);
        info.setIntSceneNo(CmdHelper.byteArrayToInt(curScene));

        byte[] detectSensorEnable = new byte[1];
        response.get(detectSensorEnable);
        boolean status = CmdHelper.byteArrayToInt(detectSensorEnable) ==  0 ? false : true;
        info.setDetectSensorStatus(status);

        byte[] detectSensorValue = new byte[1];
        response.get(detectSensorValue);
        boolean value1 = CmdHelper.byteArrayToInt(detectSensorValue) ==  0 ? false : true;
        info.setDetectValue(value1);

        byte[] lightSensorEnable = new byte[1];
        response.get(lightSensorEnable);
        boolean status2 = CmdHelper.byteArrayToInt(lightSensorEnable) ==  0 ? false : true;
        info.setLightSensorStatus(status2);

        byte[] lightSensorvalue= new byte[1];
        response.get(lightSensorvalue);
        int value2 = CmdHelper.byteArrayToInt(lightSensorvalue);
        info.setLightSensorValue(value2);

        return  info;
    }

    public SensorInfo setSensorStatus(ByteBuffer response) {

        SensorInfo info = new SensorInfo();
        return  info;
    }


    public boolean setDeviceDiming(ByteBuffer responseAck) {
        return true;
    }

    public Dimmer getDeviceDiming(ByteBuffer responseAck) {
        responseAck.get(mDIming);
        responseAck.get(mPowerStatus);
        Dimmer dimmer  = new Dimmer();
        boolean stat = (mPowerStatus[0] == 0x00 ) ? false: true ;
        dimmer.setPowerStatus(stat);
        dimmer.setDiming(CmdHelper.byteArrayToInt(mDIming));
        return dimmer;
    }
    public Object setDevicePower(ByteBuffer responseAck) {
        return true;
    }

    public Object setDeviceName(ByteBuffer responseAck) {
        //responseAck.get(mDIming);
        //boolean stat = (mPowerStatus[0] == 0x00 ) ? false: true ;
        return true;
    }
}
