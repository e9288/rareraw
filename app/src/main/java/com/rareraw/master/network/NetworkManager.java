package com.rareraw.master.network;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.Toast;
import com.neoromax.feelmaster.R;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Enumeration;

import static android.content.Context.WIFI_SERVICE;
import static com.rareraw.master.network.Cmd.CMD_CONNECT;
import static com.rareraw.master.network.Cmd.CMD_SET_DEVICE_NAME;
import static com.rareraw.master.network.Cmd.CMD_SET_DIMMER_ROOM;
import static com.rareraw.master.network.Cmd.CONNECT_ID_NOT_MATCH;
import static com.rareraw.master.network.Cmd.EMPTY_DATA;
import static com.rareraw.master.network.Cmd.FAIL_CODE;
import static com.rareraw.master.network.Cmd.FAIL_CONNECT;
import static com.rareraw.master.network.Cmd.OK_CODE;

/**
 * Created by lch on 2017-02-10.
 */

public class NetworkManager {

    private static NetworkManager _instance = null;
    private Context mContext;
    private String mIpAddress = Cmd.DEFAULT_IP;
    private String mGateWay;

    public static NetworkManager getInstance() {
        if (_instance == null) {
            synchronized (NetworkManager.class) {
                _instance = new NetworkManager();
            }
        }
        return _instance;
    }

    public void postMsgForPing(byte[] cmdConnect, NetworkResultLisnter listener, ByteBuffer sendMsg, String ip) {
        DataHandler handler = new DataHandler(cmdConnect, listener);
        try {
            new ApPoster(handler, mIpAddress).execute(sendMsg);
            //new ApPoster(handler, mIpAddress, APService.getNetworkInfo().getNetworkMode()).execute(sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postMsg(byte[] cmdConnect, NetworkResultLisnter listener, ByteBuffer sendMsg) {
        DataHandler handler = new DataHandler(cmdConnect, listener);
        try {

            new ApPoster(handler, mIpAddress).execute(sendMsg);
            //new ApPoster(handler, mIpAddress, APService.getNetworkInfo().getNetworkMode()).execute(sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void postMsg(byte[] cmdConnect, ByteBuffer sendMsg) {
        DataHandler handler = new DataHandler(cmdConnect);
        try {
            new ApPoster(handler, mIpAddress).execute(sendMsg);
            //new ApPoster(handler, mIpAddress, APService.getNetworkInfo().getNetworkMode()).execute(sendMsg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setContext(Context _context) {
        this.mContext = _context;
    }

    public void getIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        String ip = Formatter.formatIpAddress(inetAddress.hashCode());
                        Log.i("error", "***** IP=" + ip);
                        mIpAddress = ip;
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("error", ex.toString());
        }
    }

    public void getWifipAddr() {
        WifiManager wm = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        DhcpInfo dhcpInfo = wm.getDhcpInfo() ;
        int nIpAddress = dhcpInfo.serverAddress;
        mIpAddress = Formatter.formatIpAddress(nIpAddress);
    }

    public String getGateWay(){
        WifiManager wm = (WifiManager) mContext.getSystemService(WIFI_SERVICE);
        DhcpInfo dhcpInfo = wm.getDhcpInfo() ;
        int gateway = dhcpInfo.gateway;

        String sGateWay = Formatter.formatIpAddress(gateway);
        mGateWay =  sGateWay.substring(0,sGateWay.length()-1);
        return mGateWay;
    }

    public void setWifipAddr(String ip) {
        mIpAddress = ip;
    }

    public class DataHandler extends Handler {

        NetworkResultLisnter _listener = null;
        byte[] cmd;

        public DataHandler(byte[] cmd, NetworkResultLisnter listener) {
            super();
            this._listener = listener;
            this.cmd = cmd;
        }

        public DataHandler(byte[] cmd) {
            super();
            this.cmd = cmd;
        }

        @Override
        public void handleMessage(Message message) {
            if (_listener == null)
                return;

            switch (message.what) {
                case FAIL_CODE:
                    MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_connect_fail), Toast.LENGTH_SHORT);
                    _listener.onError(FAIL_CODE);
                    break;
                case FAIL_CONNECT:
                    _listener.onError(FAIL_CODE);
                    break;
                case OK_CODE:
                    try {
                        AnalyzeAck((ByteBuffer) message.obj);
                    } catch (Exception e) {
                        e.printStackTrace();
                        MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_connect_fail), Toast.LENGTH_SHORT);
                        _listener.onError(FAIL_CODE);
                    }
                    break;
            }
        }

        private void AnalyzeAck(ByteBuffer responseAck) {
            if (!Analyzers.getInstance().nullChecker(responseAck)) {
                MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_connect_fail), Toast.LENGTH_SHORT);
                _listener.onError(FAIL_CODE);
                return;
            }


            int errCode = Analyzers.getInstance().ErrorChceker(responseAck);
            //if (errCode != OK_CODE && errCode == CONNECT_ID_NOT_MATCH) {
            if (errCode != OK_CODE && errCode == CONNECT_ID_NOT_MATCH) {

                MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_retry_connect), Toast.LENGTH_SHORT);
                ByteBuffer mDefaultConnectionId = ByteBuffer.allocate(4);
                ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_CONNECT, mDefaultConnectionId, EMPTY_DATA);
                try {
                    postMsg(CMD_CONNECT, _listener, sendMsg);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("send error : ", "connect requst");
                }
                return;
            }

            if (Arrays.equals(cmd, Cmd.CMD_CONNECT)) {
                _listener.onSuccress(Analyzers.getInstance().setSettionId(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SCENE)) {
                _listener.onSuccress(Analyzers.getInstance().getSceneSingle(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SCENE_PLAY)) {
                _listener.onSuccress(Analyzers.getInstance().getScenePlaySingle(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SCENE_SCHE)) {
                _listener.onSuccress(Analyzers.getInstance().getScheduleSceneInfo(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_REG_DEV_LIST)) {
                _listener.onSuccress(Analyzers.getInstance().setRegistedDevList(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_UNREG_DEV_LIST)) {
                _listener.onSuccress(Analyzers.getInstance().setUnRegistedDevList(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_SCENE_LIST) || Arrays.equals(cmd, Cmd.CMD_ADMIN_SCENE_LIST)) {
                _listener.onSuccress(Analyzers.getInstance().setSceneList(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_ALARM_LIST)) {
                _listener.onSuccress(Analyzers.getInstance().setAlarmList(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_REG_DEVICE)) {
                _listener.onSuccress(Analyzers.getInstance().setSingleDevice(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_CURRENT_SCENE)) {
                _listener.onSuccress(Analyzers.getInstance().getCurrentSceneNo(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SCENE_DIMING_LEVEL)) {
                _listener.onSuccress(Analyzers.getInstance().getCurrentSceneDiming(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SIWTCH_LIST)) {
                _listener.onSuccress(Analyzers.getInstance().parseSwitchInfo(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_DMX_USE)) {
                _listener.onSuccress(Analyzers.getInstance().getDmxUseStatsus(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_NETWORK_INFO)) {
                _listener.onSuccress(Analyzers.getInstance().setNetworkInfo(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_SENSOR_STATUS)) {
                _listener.onSuccress(Analyzers.getInstance().getSensorInfo(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_SET_DETECT_SENSOR_ON)) {
                _listener.onSuccress(Analyzers.getInstance().setSensorStatus(responseAck));
            }else if (Arrays.equals(cmd, Cmd.CMD_SET_DETECT_SENSOR_OFF)) {
                _listener.onSuccress(Analyzers.getInstance().setSensorStatus(responseAck));
            }else if (Arrays.equals(cmd, Cmd.CMD_SET_LIGHT_SENSOR_ON)) {
                _listener.onSuccress(Analyzers.getInstance().setSensorStatus(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_SET_LIGHT_SENSOR_OFF)) {
                _listener.onSuccress(Analyzers.getInstance().setSensorStatus(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_SET_DEVICE_DIMING)) {
                _listener.onSuccress(Analyzers.getInstance().setDeviceDiming(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_GET_DEVICE_DIMING)) {
                _listener.onSuccress(Analyzers.getInstance().getDeviceDiming(responseAck));
            } else if (Arrays.equals(cmd, Cmd.CMD_SET_DEVICE_POWER)) {
                _listener.onSuccress(Analyzers.getInstance().setDevicePower(responseAck));
            }else if (Arrays.equals(cmd, Cmd.CMD_SET_DEVICE_NAME)) {
                _listener.onSuccress(Analyzers.getInstance().setDeviceName(responseAck));
            }else if (Arrays.equals(cmd, Cmd.CMD_SET_DIMMER_ROOM)) {
                _listener.onSuccress(Analyzers.getInstance().setDeviceName(responseAck));
            }


            else if (Arrays.equals(cmd, Cmd.CMD_CONNECT_ADMIN)) {
                if (Analyzers.getInstance().ErrorChceker(responseAck) != OK_CODE) {
                    _listener.onError(FAIL_CODE);
                    return;
                }else
                    _listener.onSuccress(Analyzers.getInstance().setAdminMode(responseAck));
            } else
                _listener.onSuccress(1);
        }
    }
}
