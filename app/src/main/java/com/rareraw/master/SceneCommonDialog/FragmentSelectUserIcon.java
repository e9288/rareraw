package com.rareraw.master.SceneCommonDialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;

import java.util.ArrayList;

public class FragmentSelectUserIcon extends Fragment {
    public UserIconHolder mExsitingHolder = null;
    private GridView mGridView;
    private View mParent;
    private Context mContext;
    private String mMyIconType;
    private int mSelectedIndex = -1;
    private ArrayList<UserIconRes> mUserIconListDay = new ArrayList<>();
    private ArrayList<UserIconRes> mUserIconListSeason = new ArrayList<>();
    private ArrayList<UserIconRes> mUserIconListPlace = new ArrayList<>();
    private AdapterUserIcon mUserIconAdapter;

    public FragmentSelectUserIcon() {

    }

    public static FragmentSelectUserIcon newInstance() {
        FragmentSelectUserIcon fragment = new FragmentSelectUserIcon();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void resetSelectedIcon() {
        if (mExsitingHolder != null) {
            mExsitingHolder.selectStat = false;
            mUserIconAdapter.notifyDataSetChanged();
        }
        mExsitingHolder = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mParent = inflater.inflate(R.layout.fragment_select_user_icon, container, false);
        mGridView = (GridView) mParent.findViewById(R.id.grieview_user_icon);

        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUserIconRes();
        setAdapter();
    }

    private void setAdapter() {
        if (mMyIconType == Config.USER_ICON_DAY)
            mUserIconAdapter = new AdapterUserIcon(getActivity(), mMyIconType, mUserIconListDay);
        if (mMyIconType == Config.USER_ICON_SEASON)
            mUserIconAdapter = new AdapterUserIcon(getActivity(), mMyIconType, mUserIconListSeason);
        if (mMyIconType == Config.USER_ICON_PLACE)
            mUserIconAdapter = new AdapterUserIcon(getActivity(), mMyIconType, mUserIconListPlace);

        mGridView.setAdapter(mUserIconAdapter);
    }

    private void getUserIconRes() {

        // 1 : day , 2: season, 3: place 아이콘 타입이 추가 될 경우 config 에서 MAX사이즈조절하고 img 파일을 기존 이미지 파일이름에 + 1시켜서 생성한다. string table도 마찬가지.
        mUserIconListDay.clear();
        for (int index = 0; index < Config.USER_ICON_DAY_MAX_CNT; index++) {
            int imageResource = getResources().getIdentifier("icon_user_icon_day" + String.format("%02d", index), "mipmap", mContext.getPackageName());
            String stringResource = getResources().getString(R.string.user_icon_msg_day01 + index);
            mUserIconListDay.add(new UserIconRes(imageResource, stringResource));
        }
        mUserIconListSeason.clear();
        for (int index = 0; index < Config.USER_ICON_SEASON_MAX_CNT; index++) {
            int imageResource = getResources().getIdentifier("icon_user_icon_season" + String.format("%02d", index), "mipmap", mContext.getPackageName());
            String stringResource = getResources().getString(R.string.user_icon_msg_season01 + index);
            mUserIconListSeason.add(new UserIconRes(imageResource, stringResource));
        }
        mUserIconListPlace.clear();
        for (int index = 0; index < Config.USER_ICON_PLACE_MAX_CNT; index++) {
            int imageResource = getResources().getIdentifier("icon_user_icon_place" + String.format("%02d", index), "mipmap", mContext.getPackageName());
            String stringResource = getResources().getString(R.string.user_icon_msg_place01 + index);
            mUserIconListPlace.add(new UserIconRes(imageResource, stringResource));
        }
    }

    public void setIconType(String _iconType) {
        mMyIconType = _iconType;
    }

    public class AdapterUserIcon extends BaseAdapter implements View.OnClickListener {

        private LayoutInflater mLayoutInflater;
        private ArrayList<UserIconRes> mUserIconList = new ArrayList<>();
        private String mIconType;

        public AdapterUserIcon(Context context, String _icontype, ArrayList<UserIconRes> _jconList) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mContext = context;
            mIconType = _icontype;
            mUserIconList = _jconList;
        }

        @Override
        public int getCount() {
            return mUserIconList.size();
        }

        @Override
        public Object getItem(int i) {

            return mUserIconList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            UserIconHolder userIconHolder = new UserIconHolder();

            if (view == null) {
                view = mLayoutInflater.inflate(R.layout.item_gridview_user_icon, viewGroup, false);
                view.findViewById(R.id.item_grid_view_layout).setLayoutParams(getSquareLayoutParams(viewGroup));
                userIconHolder.mImgIcon = (ImageView) view.findViewById(R.id.img_user_icon);
                userIconHolder.mTextName = (TextView) view.findViewById(R.id.text_icon_name);
                userIconHolder.mIndex = i;
                view.setOnClickListener(this);
            } else
                userIconHolder = (UserIconHolder) view.getTag();

            if (userIconHolder.selectStat)
                view.setBackgroundResource(R.drawable.background_user_icon_selector);
            else {
                userIconHolder.mImgIcon.setImageResource(mUserIconList.get(i).Iconname);
                view.setBackgroundColor(getResources().getColor(R.color.transparent));
            }
            userIconHolder.mTextName.setText(mUserIconList.get(i).ImgName);

            view.setTag(userIconHolder);
            return view;
        }

        @Override
        public void onClick(View view) {
            if (mExsitingHolder != null)
                mExsitingHolder.selectStat = false;
            UserIconHolder userIconHolder = (UserIconHolder) view.getTag();

            mSelectedIndex = userIconHolder.mIndex;
            if (mIconType.equals(Config.USER_ICON_SEASON))
                mSelectedIndex += 10;
            if (mIconType.equals(Config.USER_ICON_PLACE))
                mSelectedIndex += 20;
            userIconHolder.selectStat = true;
            mExsitingHolder = userIconHolder;
            notifyDataSetChanged();
        }

    }

    private void checkSelectedIcon() {

    }

    private AbsListView.LayoutParams getSquareLayoutParams(ViewGroup viewGroup) {
        int size = ((GridView) viewGroup).getColumnWidth();
        return new AbsListView.LayoutParams(size, size);
    }

    public String getSelectedIconType() {
        return mMyIconType;
    }

    public int getSelectedIndex() {
        return mSelectedIndex;
    }

    public class UserIconHolder {
        int mIndex;
        String mName;
        boolean selectStat;
        TextView mTextName;
        ImageView mImgIcon;
    }

    public class UserIconRes {
        int Iconname;
        String ImgName;

        public UserIconRes(int _iconname, String _imgName) {
            this.Iconname = _iconname;
            this.ImgName = _imgName;
        }
    }
}
