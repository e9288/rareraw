package com.rareraw.master.SceneCommonDialog;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by lch on 2016-10-11.
 */

public class UserIconTabPagerAdapter extends FragmentStatePagerAdapter {

    private int mTabCount;
    public static FragmentSelectUserIcon mFragmentDay = new FragmentSelectUserIcon().newInstance();
    public static FragmentSelectUserIcon mFragmentSeacon = new FragmentSelectUserIcon().newInstance();
    public static FragmentSelectUserIcon mFragmentPlace = new FragmentSelectUserIcon().newInstance();

    public UserIconTabPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mFragmentDay;
            case 1:
                return mFragmentSeacon;
            case 2:
                return mFragmentPlace;
            default:
                return mFragmentDay;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }
}