package com.rareraw.master.SceneCommonDialog;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;


public class DialogSelectUserIcon extends android.support.v4.app.DialogFragment {

    private TabLayout mTabLayout;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private TextView mBtnOk, mBtnCancel;
    private View mParent;
    private OnActionListener mListener;
    private FragmentManager mFragmentManager;
    private  UserIconTabPagerAdapter mPagerAdapter;
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);

    }
    public void setLisetener(OnActionListener lisetener) {
        mListener = lisetener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent =  inflater.inflate(R.layout.dialog_user_icon, container, false);
        mBtnCancel = (TextView) mParent.findViewById(R.id.dialog_user_icon_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mBtnOk = (TextView) mParent.findViewById(R.id.dialog_user_icon_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentSelectUserIcon fragment = (FragmentSelectUserIcon) mPagerAdapter.getItem(mViewPager.getCurrentItem());
                if(fragment.mExsitingHolder == null){
                    Toast.makeText(getActivity(),"선택된 아이콘이 없습니다." ,Toast.LENGTH_SHORT).show();
                    return;
                }

                mListener.onActionListener(fragment.getSelectedIndex(), fragment.getSelectedIconType());
                dismiss();
            }
        });
        mTabLayout = (TabLayout) mParent.findViewById(R.id.dialog_tablayout_select_user_icon);
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.day)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.season)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.place)));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Initializing ViewPager
        mViewPager = (ViewPager) mParent.findViewById(R.id.dialog_select_icon_viewpager);

        // Creating TabPagerAdapter adapter
        mPagerAdapter = new UserIconTabPagerAdapter(getChildFragmentManager(), mTabLayout.getTabCount());
        mPagerAdapter.mFragmentDay.setIconType(Config.USER_ICON_DAY);
        mPagerAdapter.mFragmentSeacon.setIconType(Config.USER_ICON_SEASON);
        mPagerAdapter.mFragmentPlace.setIconType(Config.USER_ICON_PLACE);

        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        // Set TabSelectedListener
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                FragmentSelectUserIcon fragment = (FragmentSelectUserIcon) mPagerAdapter.getItem(mViewPager.getCurrentItem());
                fragment.resetSelectedIcon();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                int d = tab.getPosition();
                mViewPager.setCurrentItem(tab.getPosition());
                FragmentSelectUserIcon fragment = (FragmentSelectUserIcon) mPagerAdapter.getItem(mViewPager.getCurrentItem());
                fragment.resetSelectedIcon();
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int d = tab.getPosition();
              //  mViewPager.setCurrentItem(tab.getPosition());
               // FragmentSelectUserIcon fragment = (FragmentSelectUserIcon) mPagerAdapter.getItem(mViewPager.getCurrentItem());
              //  fragment.resetSelectedIcon();
            }
        });
        return mParent;
    }

    public void setFragmentManager(FragmentManager _fragmentManager) {
        this.mFragmentManager = _fragmentManager;
    }


    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance() {
            PlaceholderFragment fragment = new PlaceholderFragment();
            /*Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);*/
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_select_user_icon, container, false);
            //TextView textView = (TextView) rootView.findViewById(R.id.dialog_select_icon_viewpager);
            // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance();
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}