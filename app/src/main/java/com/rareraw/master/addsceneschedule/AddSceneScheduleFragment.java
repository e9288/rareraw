package com.rareraw.master.addsceneschedule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SimpleCursorTreeAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnTimsetListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.addscene.MoodListAdapter;
import com.rareraw.master.addsceneplay.AddScenePlayListAdapter;
import com.rareraw.master.alarm.TimePicker;
import com.rareraw.master.data.Scene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.SceneIndividualInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Lch on 2016-12-26.
 */

public class AddSceneScheduleFragment extends BaseFragment implements View.OnClickListener {
    private AddSceneScheduleListAdapter mAddSceneScheduleListAdapter;
    private AddMoodSchedulListAdapter mMoodSchedulListAdapter;
    private SchedulResultlListAdapter mSchedulResultAdapter;
    private ListView mListViewSceneList, mListViewMoodList, mListViewSchedulResult;

    private ImageView mBtnIConType;
    private TextView mImgMon, mImgTue, mImgWed, mImgThu, mImgFri, mImgSat, mImgSun, mBtnNameEdit;
    private TimePicker mStartPicker, mEndPicker;
    private RelativeLayout mBtnAddScene;
    private EditText mScheduleSceneName;
    private Context mContext;
    private CheckBox mCheckboxHide;
    private SceneScheduleInfo mSceneScheduleInfo = new SceneScheduleInfo();
    private int mScenenoForModify;
    public boolean MODIFY_FLAG = false; // false : new create, true : modify
    private int mSelectedSceneNo = -1;
    private byte[] mDefaultWeek = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
    private ScheduleTimesetInfo mTimesetInfo;
    private View mParent;
    private NestedScrollView mLayStep2;
    private LinearLayout mLayStep1, mLayStep3;
    private EditText mSchedulName;
    private TextView mTextDesceiption, mTextSave, mTextPrev;
    private RelativeLayout mPrevPage, mNextPage;
    public final int STEP1 = 0, STEP2 = 1, STEP3 = 2;
    public int NOW_PAGE = STEP1;
    public boolean mSaveComplete;
    private OnActionListener mDeleteComfirmListener = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {
            try {

                mSceneScheduleInfo.getSceneList().remove(mSchedulResultAdapter.getDeletePosition());
                mSchedulResultAdapter.setResultMoodList(mSceneScheduleInfo);
                mSchedulResultAdapter.notifyDataSetChanged();

            } catch (Exception e) {
                Log.e("error", "delete scene");
            }
        }
    };

    public static final String START_HOUR = "stringhour", START_MIN = "startmin", END_HOUR = "endhour", END_MIN = "endmin",
            SCENE_NO = "sceneno", START_AMPM = "start_ampm", END_AMPM = "end_apmp";
    private OnActionListener mOnActionListener = new OnActionListener() {
        // Icon type ( day, season, place )
        // Icon index depends on the Type.
        @Override
        public void onActionListener(int _iconIndex, String _IconType) {
            mSceneScheduleInfo.setIntIconType(_iconIndex);
            setUserIconType(_iconIndex);
        }
    };

    private OnTimsetListener mOnTimeListener = new OnTimsetListener() {
        @Override
        public void onTimesetLisnster(ScheduleTimesetInfo info, int position) {
            try {
                deleteDefaultScene();
                SceneIndividualInfo individualInfo = new SceneIndividualInfo();
                individualInfo.setmSceneName(info.getmSceneName());
                individualInfo.setIntFadeTime(Config.DEFAULT_FADE_TIME);
                individualInfo.setIntSceneNo(info.getmSceneNo());

                individualInfo.setIntStartHour(info.getmStartHour());
                individualInfo.setIntStartMin(info.getmStartMin());

                individualInfo.setIntEndHour(info.getmEndHour());
                individualInfo.setIntEndMin(info.getmEndMin());

                mSceneScheduleInfo.getSceneList().add(individualInfo);
                mAddSceneScheduleListAdapter.notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("error", "timeset casting");
            }
        }
    };

    public void deleteDefaultScene() {
        for (int i = 0; i < mSceneScheduleInfo.getSceneList().size(); i++) {
            if (mSceneScheduleInfo.getSceneList().get(i).getmSceneName().equals(getString(R.string.default_scene_name)))
                mSceneScheduleInfo.getSceneList().remove(i);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        // getScheduleSceneInfo();
        mAddSceneScheduleListAdapter = new AddSceneScheduleListAdapter(getContext(), mSceneScheduleInfo, mDeleteComfirmListener);
        mSaveComplete = false;
        NOW_PAGE = STEP1;
    }

    private void getScheduleSceneInfo() {
        if (MODIFY_FLAG)
            getExistingSceneInfo();
        else
            createSceneInfo();
    }

    public void updateUi() {
        getScheduleSceneInfo();
        if (mSceneScheduleInfo == null)
            return;

    }

    private void updateSceneInfo() {
        mCheckboxHide.setChecked(mSceneScheduleInfo.getBoolHideFlag());
        mScheduleSceneName.setText(removeNullChar(mSceneScheduleInfo.getSceneScheduleName()));
        //mScheduleSceneName
        setUserIconType(mSceneScheduleInfo.getIntIconType());
        // setWeekInfo(mSceneScheduleInfo.getByteWeek());
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    public void setModiftStatus(int _sceneNo) {
        if (_sceneNo == -1)
            MODIFY_FLAG = false;
        else {
            MODIFY_FLAG = true;
            mScenenoForModify = _sceneNo;
        }
    }

    private void createSceneInfo() {
        SceneScheduleInfo sceneSchedule = new SceneScheduleInfo();
        try {
            // sceneSchedule.setSceneScheduleName("ScenePlay" + CmdHelper.getEmptySceneNoInt());
            sceneSchedule.setSceneScheduleName("");
            sceneSchedule.setIntScheduleSceneNo(CmdHelper.getEmptySceneNoInt());
            sceneSchedule.setIntIndexNo(CmdHelper.getEmptyIndexNoInt());
            sceneSchedule.setIntIconType(Config.USER_DEFAULT_ICON);
            sceneSchedule.setBoolHideFlag(false);
            sceneSchedule.setByteWeek(mDefaultWeek);

            // sceneSchedule.setMoodInfo(APService.getApSceneList());
            //ArrayList<SceneIndividualInfo> defaultSceneArray = new ArrayList<>();
            //SceneIndividualInfo defaultScene = new SceneIndividualInfo();
            //defaultScene.setmSceneName(getString(R.string.default_scene_name));
            //defaultScene.setIntFadeTime(-1);
            //defaultSceneArray.add(defaultScene);
            // sceneSchedule.setSceneList(defaultSceneArray);
        } catch (Exception e) {
            Log.e("eror", "add sceneplay");
        }


        mSceneScheduleInfo = sceneSchedule;
        //mMoodSchedulListAdapter.setMoodList(mSceneScheduleInfo);
        mListViewMoodList.setAdapter(mMoodSchedulListAdapter);
        //setListViewHeightBasedOnChildren(mListViewMoodList);
        //mMoodSchedulListAdapter.notifyDataSetChanged();
        updateSceneInfo();
        mScheduleSceneName.requestFocus();
        mScheduleSceneName.setSelection(0);
        /*mAddSceneScheduleListAdapter.setSceneScheduleList(mSceneScheduleInfo);
        mAddSceneScheduleListAdapter.notifyDataSetChanged();*/
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getExistingSceneInfo() {
        try {
            ((ContainActivity) getActivity()).createDialog(1300);
            if (Config.DEBUG)
                mSceneScheduleInfo = getTestData();
            else
                APService.getSceneScheduleSingle(mScenenoForModify, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        SceneScheduleInfo sceneSchedule = new SceneScheduleInfo();
                        try {
                            sceneSchedule = (SceneScheduleInfo) retValue;
                            if (sceneSchedule == null)
                                return;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            sceneSchedule.setIntScheduleSceneNo(mScenenoForModify);
                            sceneSchedule.setByteScheduleSceneNo(CmdHelper.intToByteArray(mScenenoForModify));
                            boolean oldLevel = APService.getUserLevel();
                            APService.setUserLevel(true);
                            for (int i = 0; i < sceneSchedule.getSceneList().size(); i++)
                                sceneSchedule.getSceneList().get(i).setmSceneName(CmdHelper.getSceneName(sceneSchedule.getSceneList().get(i).getByteSceneNo()));
                            APService.setUserLevel(oldLevel);

                            ArrayList<SceneInfo> sceneInfoes = makeSceneList(sceneSchedule.getSceneList());
                            sceneSchedule.setMoodInfo(sceneInfoes);
                            mSceneScheduleInfo = sceneSchedule;

                            for (int i = 0; i < mSceneScheduleInfo.getSceneList().size(); i++) {

                                SceneIndividualInfo individualInfo = mSceneScheduleInfo.getSceneList().get(i);

                                //  mSceneScheduleInfo.getSceneList().add(individualInfo);
                            }

                            mListViewMoodList.setAdapter(mMoodSchedulListAdapter);
                            updateSceneInfo();
                            mAddSceneScheduleListAdapter.setSceneScheduleList(mSceneScheduleInfo);


                            mMoodSchedulListAdapter.setMoodList(mSceneScheduleInfo);
                            setListViewHeightBasedOnChildren(mListViewMoodList);
                            mMoodSchedulListAdapter.notifyDataSetChanged();

                            showNowPage(STEP3);
                            // mAddSceneScheduleListAdapter.notifyDataSetChanged();
                        } catch (Exception e) {

                        }
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();
                    }
                });
        } catch (Exception e) {
            Log.e("eror", "add sceneplay");
            Logout();
        }
    }

    private ArrayList<SceneInfo> makeSceneList(ArrayList<SceneIndividualInfo> sceneList) {
        ArrayList<SceneInfo> retInfos = new ArrayList<>();

        // 01049147330 노말씬만 되도록 수정
        // ArrayList<SceneInfo> allSceneinfos = APService.getApSceneList();
        ArrayList<SceneInfo> allSceneinfos = APService.getApNormalSceneList();
        for (SceneInfo allSceneinfo : allSceneinfos) {

            for (SceneIndividualInfo oldInfo : sceneList) {
                if (allSceneinfo.getIntSceneNo() == oldInfo.getIntSceneNo())
                    retInfos.add(allSceneinfo);
            }
        }

        return retInfos;
    }

    private SceneScheduleInfo getTestData() {
        SceneScheduleInfo sceneSchedule = new SceneScheduleInfo();
        sceneSchedule.setSceneScheduleName("ScenePlay" + CmdHelper.getEmptySceneNoInt());
        sceneSchedule.setIntScheduleSceneNo(CmdHelper.getEmptySceneNoInt());
        sceneSchedule.setIntIndexNo(Cmd.NORMAL_SCENE);
        sceneSchedule.setIntIconType(Config.USER_ICON_AFTERNOON);
        sceneSchedule.setBoolHideFlag(false);
        byte[] week = new byte[]{(byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
        sceneSchedule.setByteWeek(week);

        ArrayList<SceneIndividualInfo> defaultSceneArray = new ArrayList<>();

        SceneIndividualInfo defaultScene = new SceneIndividualInfo();
        defaultScene.setIntSceneNo(1);
        defaultScene.setmSceneName("scene1");
        defaultScene.setIntFadeTime(10);
        defaultScene.setIntStartHour(10);
        defaultScene.setIntStartMin(20);
        defaultScene.setIntEndHour(10);
        defaultScene.setIntEndMin(20);

        SceneIndividualInfo defaultScene1 = new SceneIndividualInfo();
        defaultScene1.setIntSceneNo(2);
        defaultScene1.setmSceneName("scene2");
        defaultScene1.setIntFadeTime(9);
        defaultScene1.setIntStartHour(9);
        defaultScene1.setIntStartMin(19);
        defaultScene1.setIntEndHour(9);
        defaultScene1.setIntEndMin(19);

        SceneIndividualInfo defaultScene2 = new SceneIndividualInfo();
        defaultScene2.setmSceneName("scene3");
        defaultScene2.setIntSceneNo(3);
        defaultScene2.setIntFadeTime(8);
        defaultScene2.setIntStartHour(8);
        defaultScene2.setIntStartMin(18);
        defaultScene2.setIntEndHour(8);
        defaultScene2.setIntEndMin(18);

        defaultSceneArray.add(defaultScene);
        defaultSceneArray.add(defaultScene1);
        defaultSceneArray.add(defaultScene2);
        // sceneSchedule.setSceneList(defaultSceneArray);
        return sceneSchedule;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_add_scene_schedule, container, false);
        // mBtnIConType = (ImageView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_user_icon);
        mListViewMoodList = (ListView) mParent.findViewById(R.id.listview_mood_list);
        mListViewSchedulResult = (ListView) mParent.findViewById(R.id.schedul_result_listview);


        mMoodSchedulListAdapter = new AddMoodSchedulListAdapter(getContext(), mSceneScheduleInfo, -1);

        mSchedulResultAdapter = new SchedulResultlListAdapter(getContext(), mSceneScheduleInfo, mDeleteComfirmListener);
        mListViewSchedulResult.setAdapter(mSchedulResultAdapter);
        mBtnAddScene = (RelativeLayout) mParent.findViewById(R.id.fragment_add_scene_play_layout_add_scene);
        mScheduleSceneName = (EditText) mParent.findViewById(R.id.fragment_add_scene_edit_scene_name);

        mImgMon = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_mon);
        mImgTue = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_tue);
        mImgWed = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_wed);
        mImgThu = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_thu);
        mImgFri = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_fri);
        mImgSat = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_sat);
        mImgSun = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_sun);
        mCheckboxHide = (CheckBox) mParent.findViewById(R.id.fragment_add_schedule_hide_image);
        mBtnNameEdit = (TextView) mParent.findViewById(R.id.btn_edit_name);
        mStartPicker = (TimePicker) mParent.findViewById(R.id.timepicker_start);
        mStartPicker.Create(getContext());
        mEndPicker = (TimePicker) mParent.findViewById(R.id.timepicker_end);
        mEndPicker.Create(getContext());
        mSchedulName = (EditText) mParent.findViewById(R.id.text_schedul_name);
        mTextSave = (TextView) mParent.findViewById(R.id.text_schedul_save);
        mTextPrev = (TextView) mParent.findViewById(R.id.text_prev);

        mLayStep1 = (LinearLayout) mParent.findViewById(R.id.layer_step1);
        mLayStep2 = (NestedScrollView) mParent.findViewById(R.id.layer_step2);
        mLayStep3 = (LinearLayout) mParent.findViewById(R.id.layer_step3);

        mPrevPage = (RelativeLayout) mParent.findViewById(R.id.prev_page);
        mBtnNameEdit.setTag(R.id.btn_edit_name, false);
        mBtnNameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean tag = (boolean) v.getTag(R.id.btn_edit_name);
                if (tag) {
                    String name = mSchedulName.getText().toString();
                    if (name == null || name.isEmpty()) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.frag_schedul_underflow_name), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }

                    if (name.getBytes().length > Config.NAME_MAX_LENGTH) {
                        MyToast.getInstance(getActivity()).showToast("스캐줄무드 제목이 너무 깁니다.\n다시 시도해주세요.", com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }

                    mBtnNameEdit.setText("편집");
                    mBtnNameEdit.setTextColor(getResources().getColor(R.color.black_color));
                    mBtnNameEdit.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
                    mBtnNameEdit.setTag(R.id.btn_edit_name, false);
                    mSchedulName.setEnabled(false);
                    mScheduleSceneName.setText(name);
                    mSceneScheduleInfo.setSceneScheduleName(name);
                } else {
                    mBtnNameEdit.setText("저장");
                    mBtnNameEdit.setTextColor(getResources().getColor(R.color.whit_color));
                    mBtnNameEdit.setBackgroundColor(getResources().getColor(R.color.select_item));
                    mBtnNameEdit.setTag(R.id.btn_edit_name, true);
                    mSchedulName.setEnabled(true);
                    mSchedulName.requestFocus();
                    mSchedulName.setSelection(mSchedulName.getText().length());
                    InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);


                }
            }
        });
        mPrevPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NOW_PAGE == STEP1)
                    getActivity().onBackPressed();
                else if (NOW_PAGE == STEP2)
                    if(MODIFY_FLAG)
                        getActivity().onBackPressed();
                    else
                        showNowPage(STEP1);
                else if (NOW_PAGE == STEP3) {
                 /*   if (MODIFY_FLAG) // 추가할때
                        getActivity().onBackPressed();
                    else*/
                        showNowPage(STEP2);
                }
            }
        });
        mNextPage = (RelativeLayout) mParent.findViewById(R.id.next_page);
        mNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NOW_PAGE == STEP1) {
                    String name = mScheduleSceneName.getText().toString();
                    if (name == null || name.isEmpty()) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.frag_schedul_underflow_name), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }
                    if (!checkValidation(name))
                        return;

                    showNowPage(STEP2);

                } else if (NOW_PAGE == STEP2) {
                    if (!mMoodSchedulListAdapter.isSelectMood()) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_channel), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }
                    if (!checkSelectWeek()) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_week_mood), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        return;
                    }
                    showNowPage(STEP3);
                    addSchedulInfo();
                } else if (NOW_PAGE == STEP3)
                    saveSceneScheduleInfo();
            }
        });

        return mParent;
    }

    private boolean checkSelectWeek() {
        for (int i = 0; i < mSceneScheduleInfo.getByteWeek().length; i++) {
            if (mSceneScheduleInfo.getByteWeek()[i] > 0)
                return true;
        }
        return false;
    }

    private void addSchedulInfo() {

        int shour = mStartPicker.getCurrentHour();
        int sMin = mStartPicker.getCurrentMinute();
        int eHour = mEndPicker.getCurrentHour();
        int eMin = mEndPicker.getCurrentMinute();

        SceneInfo info = mMoodSchedulListAdapter.getSelectedMoodInfo();
        deleteDefaultScene();
        SceneIndividualInfo individualInfo = new SceneIndividualInfo();
        individualInfo.setmSceneName(info.getSceneName());
        individualInfo.setIntFadeTime(Config.DEFAULT_FADE_TIME);
        individualInfo.setIntSceneNo(info.getIntSceneNo());

        individualInfo.setIntStartHour(shour);
        individualInfo.setIntStartMin(sMin);

        individualInfo.setIntEndHour(eHour);
        individualInfo.setIntEndMin(eMin);
        individualInfo.setSelected(true);
        mSceneScheduleInfo.getSceneList().add(individualInfo);

    }

    public void forceNextPage() {
        mNextPage.performClick();
    }

    public void forcePrevPage() {
        mPrevPage.performClick();
    }

    public void showNowPage(int nowPage) {
        NOW_PAGE = nowPage;
        if (nowPage == STEP1) {
            mLayStep1.setVisibility(View.VISIBLE);
            mLayStep2.setVisibility(View.GONE);
            mLayStep3.setVisibility(View.GONE);
            mTextSave.setText("다음");

            mSchedulName.requestFocus();
           // InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
           // imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        } else if (nowPage == STEP2) {
            mLayStep1.setVisibility(View.GONE);
            mLayStep2.setVisibility(View.VISIBLE);
            mLayStep3.setVisibility(View.GONE);
            mLayStep2.scrollTo(0,0);
            mTextPrev.setText("이전");
            mTextSave.setText("다음");

            // 01049147330 노말씬만되도록 수정
            mSceneScheduleInfo.setMoodInfo(APService.getApNormalSceneList());
            //mSceneScheduleInfo.setMoodInfo(APService.getApSceneList());
            mMoodSchedulListAdapter.setMoodList(mSceneScheduleInfo);
            setListViewHeightBasedOnChildren(mListViewMoodList);
            mMoodSchedulListAdapter.resetFlag();
            mMoodSchedulListAdapter.notifyDataSetChanged();
            if (!MODIFY_FLAG) {
            } else {
                setWeekInfo(mSceneScheduleInfo.getByteWeek());
            }
            mLayStep2.scrollTo(0,0);
        } else if (nowPage == STEP3) {
            String name = mScheduleSceneName.getText().toString();
            mSchedulName.setText(name);
            mLayStep1.setVisibility(View.GONE);
            mLayStep2.setVisibility(View.GONE);
            mLayStep3.setVisibility(View.VISIBLE);
            mTextPrev.setText("추가");
            mTextSave.setText("저장");

            mSchedulResultAdapter.setResultMoodList(mSceneScheduleInfo);
            mSchedulResultAdapter.notifyDataSetChanged();

        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((ListView) view.findViewById(R.id.fragment_add_scene_schedule_list)).setAdapter(mAddSceneScheduleListAdapter);
        /*mBtnIConType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogSelectUserIcon dialogSelectUserIcon = new DialogSelectUserIcon();
                dialogSelectUserIcon.setFragmentManager(getActivity().getSupportFragmentManager());
                dialogSelectUserIcon.setLisetener(mOnActionListener);
                dialogSelectUserIcon.show(getFragmentManager(), "dialogSelectUserIcon");
            }
        });*/
        mBtnAddScene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* DialogScheduleTimeset dialogScheduleTimeset = new DialogScheduleTimeset();
                dialogScheduleTimeset.setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light );
                dialogScheduleTimeset.setLisetener(mOnTimeListener);
                dialogScheduleTimeset.show(getFragmentManager(), "dialogScheduleTimeset");*/
            }
        });

        mImgMon.setOnClickListener(this);
        mImgMon.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[0], 0, mImgMon));

        mImgTue.setOnClickListener(this);
        mImgTue.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[1], 1, mImgTue));

        mImgWed.setOnClickListener(this);
        mImgWed.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[2], 2, mImgWed));

        mImgThu.setOnClickListener(this);
        mImgThu.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[3], 3, mImgThu));

        mImgFri.setOnClickListener(this);
        mImgFri.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[4], 4, mImgFri));

        mImgSat.setOnClickListener(this);
        mImgSat.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[5], 5, mImgSat));

        mImgSun.setOnClickListener(this);
        mImgSun.setTag(R.id.id_schedule_scene_week_info, new WeekInfo(mSceneScheduleInfo.mByteWeek[6], 6, mImgSun));


        // setting default or exsting info
//        if(!APService.getUserLevel() && MODIFY_FLAG){
//            mImgMon.setEnabled(false);
//            mImgTue.setEnabled(false);
//            mImgWed.setEnabled(false);
//            mImgThu.setEnabled(false);
//            mImgFri.setEnabled(false);
//            mImgSat.setEnabled(false);
//            mImgSun.setEnabled(false);
//            mBtnAddScene.setVisibility(View.GONE);
//            mScheduleSceneName.setEnabled(false);
//            // mBtnIConType.setEnabled(false);
//            mCheckboxHide.setEnabled(false);
//        }

    }

    private void showAddSchedul() {
        DialogScheduleTimeset dialogScheduleTimeset = new DialogScheduleTimeset();
        dialogScheduleTimeset.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
        dialogScheduleTimeset.setLisetener(mOnTimeListener, -1);
        dialogScheduleTimeset.show(getFragmentManager(), "dialogScheduleTimeset");
    }

    private void setUserIconType(int _iconType) {
        try {
            // mBtnIConType.setImageResource(getIconId(_iconType));
        } catch (Exception e) {
            Log.e("error", "set user icon");
        }
    }

    private void setWeekInfo(byte[] byteWeek) {
        if (byteWeek[0] == 0x01) {
            WeekInfo weekinfo = (WeekInfo) mImgMon.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgMon.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }
        if (byteWeek[1] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgTue.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgTue.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }

        if (byteWeek[2] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgWed.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgWed.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }

        if (byteWeek[3] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgThu.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgThu.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }

        if (byteWeek[4] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgFri.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgFri.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }

        if (byteWeek[5] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgSat.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgSat.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }

        if (byteWeek[6] == 0x01){
            WeekInfo weekinfo = (WeekInfo) mImgSun.getTag(R.id.id_schedule_scene_week_info);
            int weekArrayIndex = weekinfo.weekNo;

            weekinfo.stat = true;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
            mImgSun.setTag(R.id.id_schedule_scene_week_info, weekinfo);
        }
    }

    @Override
    public void onClick(View v) {
        WeekInfo weekinfo = (WeekInfo) v.getTag(R.id.id_schedule_scene_week_info);
        int weekArrayIndex = weekinfo.weekNo;
        if (weekinfo.stat) {
            weekinfo.stat = false;
            mSceneScheduleInfo.getByteWeek()[weekArrayIndex] = 0x00;
            //  weekinfo.img.setTextColor(getResources().getColor(R.color.schedule_week_text_color_false));

            weekinfo.img.setTextColor(getResources().getColor(R.color.black_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        } else {
            weekinfo.stat = true;
            mSceneScheduleInfo.getByteWeek()[weekArrayIndex] = 0x01;
            // weekinfo.img.setTextColor(getResources().getColor(R.color.schedule_week_text_color_true));

            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));

        }
        v.setTag(R.id.id_schedule_scene_week_info, weekinfo);
    }


    public void saveSceneScheduleInfo() {
        String name = mScheduleSceneName.getText().toString();
        name = name.trim();
        if (!checkValidation(name))
            return;
        /*if (!mSchedulResultAdapter.getSelect()) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_scene), com.rareraw.master.Common.Toast.LENGTH_SHORT);
            return;
        }*/
        mSceneScheduleInfo.setSceneScheduleName(name);
        mSceneScheduleInfo.setBoolHideFlag(mCheckboxHide.isChecked());
        ByteBuffer buffer = mSceneScheduleInfo.makeBuffer();

        if (buffer == null) {
            Log.e("register schedule scene", "error(null)");
            return;
        }
        APService.registerSceneSchedule(buffer, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                if (Config.DEBUG) {
                    Toast.makeText(mContext, "씬 스케줄이 추가되었습니다.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(mContext, "씬번호 : " + mSceneScheduleInfo.getIntSceneNo(), Toast.LENGTH_SHORT).show();
                }
                Log.e("register schedule scene", "ok");
                mSaveComplete = true;
                getActivity().onBackPressed();
            }

            @Override
            public void onError(int errorCode) {
                mSaveComplete = false;
                Log.e("register schedule scene", "error");
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                mSaveComplete = false;
                Logout();
            }
        });

    }

    static public class WeekInfo {
        boolean stat;
        TextView img;
        int weekNo;

        public WeekInfo(byte _stat, int _weekNo, TextView _img) {
            if (_stat == 0x00)
                stat = false;
            else
                stat = true;
            img = _img;
            this.weekNo = _weekNo;
        }
    }
}
