package com.rareraw.master.addsceneschedule;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnTimsetListener;
import com.neoromax.feelmaster.R;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.alarm.TimePicker;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SceneIndividualInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;

/**
 * Created by Lch on 2016-10-21.
 */

public class DialogScheduleTimeset extends DialogFragment implements View.OnClickListener {
    private ListView mListViewMoodList;
    private OnTimsetListener mListener;
    SceneIndividualInfo sceneIndividualInfo;
    private int mSelectedSceneNo;
    private final int HOUR_MAX = 24, HOUR_MIN = 1;
    private final int MIN_MAX = 59, MIN_MIN = 0;
    private EditText mTextStartHour, mTextStartMin, mTextEndHour, mTextEndMin;
    private TextView mBtnStartAm, mBtnStartPm, mBtnEndAm, mBtnEndPm;
    private TextView mTextSceneName;
    private TimePicker mStartPicker, mEndPicker;
    private SceneScheduleInfo mSceneScheduleInfo = new SceneScheduleInfo();
    private TextView mImgMon, mImgTue, mImgWed, mImgThu, mImgFri, mImgSat, mImgSun, mBtnNameEdit;
    private int mStartPickerMin, mStartPickerHour, mEndPickerMin, mEndPickerHour;
    private RelativeLayout mBtnOk, mBtnCancel;
    private View mParent;
    private RelativeLayout mBtnSelectScene;
    private boolean mSelectSceneFlag = false;
    private int editPosition;
    private int mActiveMoodNo;
    private AddMoodSchedulListAdapter mMoodSchedulListAdapter;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    public void setActiveMoodNo(int no) {
        this.mActiveMoodNo = no;
    }

    private OnActionListener mOnActionListner = new OnActionListener() {
        @Override
        public void onActionListener(int _sceneNo, String _name) {
            mSelectedSceneNo = _sceneNo;
            mTextSceneName.setText(_name);
            mSelectSceneFlag = true;
        }
    };

    public static DialogScheduleTimeset newInstance(byte[] _channelNo) {
        DialogScheduleTimeset channelChangeDialog = new DialogScheduleTimeset();
        return channelChangeDialog;
    }


    public void setLisetener(OnTimsetListener lisetener, int editPos) {
        editPosition = editPos;
        mListener = lisetener;
    }


    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_edit_schedule_time_set, container, false);
        mBtnOk = (RelativeLayout) mParent.findViewById(R.id.btn_edit_save);
        mBtnCancel = (RelativeLayout) mParent.findViewById(R.id.btn_edit_cancel);
        mListViewMoodList = (ListView) mParent.findViewById(R.id.listview_mood_list);

        // 01049147330 노말씬만되도록 수정
        // mSceneScheduleInfo.setMoodInfo(APService.getApSceneList());
        mSceneScheduleInfo.setMoodInfo(APService.getApNormalSceneList());
        mMoodSchedulListAdapter = new AddMoodSchedulListAdapter(getContext(), mSceneScheduleInfo, mActiveMoodNo);
        mListViewMoodList.setAdapter(mMoodSchedulListAdapter);
        setListViewHeightBasedOnChildren(mListViewMoodList);

        mStartPicker = (TimePicker) mParent.findViewById(R.id.timepicker_start);
        mEndPicker = (TimePicker) mParent.findViewById(R.id.timepicker_end);
        mImgMon = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_mon);
        mImgTue = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_tue);
        mImgWed = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_wed);
        mImgThu = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_thu);
        mImgFri = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_fri);
        mImgSat = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_sat);
        mImgSun = (TextView) mParent.findViewById(R.id.fragment_add_scene_schedule_image_sun);

        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mStartPicker.Create(getContext());
        mStartPicker.setOnTimeChangedListener(new android.widget.TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(android.widget.TimePicker view, int hourOfDay, int minute) {

                mStartPickerMin = minute;
                mStartPickerHour = hourOfDay;
            }
        });


        mEndPicker.Create(getContext());
        mEndPicker.setOnTimeChangedListener(new android.widget.TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(android.widget.TimePicker view, int hourOfDay, int minute) {
                mEndPickerHour = hourOfDay;
                mEndPickerMin = minute;
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SceneInfo info = mMoodSchedulListAdapter.getSelectedMoodInfo();
                if (info == null) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_scene), Toast.LENGTH_SHORT);
                    return;
                }

          /*      if (!checkSelectWeek()) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_week_mood), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                    return;
                }*/

                ScheduleTimesetInfo timeInfo = new ScheduleTimesetInfo();
                timeInfo.setmStartHour(mStartPicker.getCurrentHour());
                timeInfo.setmStartMin(mStartPicker.getCurrentMinute());
                timeInfo.setmEndHour(mEndPicker.getCurrentHour());
                timeInfo.setmEndMin(mEndPicker.getCurrentMinute());

                timeInfo.setmStartAmPm(true);
                timeInfo.setmEndAmPm(true);


                timeInfo.setmSceneNo(info.getIntSceneNo());
                timeInfo.setmSceneName(info.getSceneName());
                mListener.onTimesetLisnster(timeInfo, editPosition);
                dismiss();
            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mImgMon.setOnClickListener(this);
        mImgMon.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[0], 0, mImgMon));

        mImgTue.setOnClickListener(this);
        mImgTue.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[1], 1, mImgTue));

        mImgWed.setOnClickListener(this);
        mImgWed.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[2], 2, mImgWed));

        mImgThu.setOnClickListener(this);
        mImgThu.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[3], 3, mImgThu));

        mImgFri.setOnClickListener(this);
        mImgFri.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[4], 4, mImgFri));

        mImgSat.setOnClickListener(this);
        mImgSat.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[5], 5, mImgSat));

        mImgSun.setOnClickListener(this);
        mImgSun.setTag(R.id.id_schedule_scene_week_info, new AddSceneScheduleFragment.WeekInfo(mSceneScheduleInfo.mByteWeek[6], 6, mImgSun));
     //   updateUi();

    }

    private void updateUi() {
        updateWeekUi();
    }

    private void drawActiveDay(TextView day, boolean active) {

        if(active){
            day.setTextColor(getResources().getColor(R.color.whit_color));
            day.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        }else{
            day.setTextColor(getResources().getColor(R.color.black_color));
            day.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
    }

    private void updateWeekUi() {
        if (mSceneScheduleInfo.getByteWeek()[0] == 0x01) {
            drawActiveDay(mImgMon, true);
            //mTextMon.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgMon, false);
            //mTextMon.setTextColor(getResources().getColor(R.color.black_color));
            //mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
        if (mSceneScheduleInfo.getByteWeek()[1] == 0x01) {
            drawActiveDay(mImgTue, true);
            //mTextTue.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgTue, false);
            //mTextTue.setTextColor(getResources().getColor(R.color.black_color));
            //mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
        if (mSceneScheduleInfo.getByteWeek()[2] == 0x01) {
            drawActiveDay(mImgWed, true);
            //mTextWed.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgTue, false);
            //mTextWed.setTextColor(getResources().getColor(R.color.black_color));
           // mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
        if (mSceneScheduleInfo.getByteWeek()[3] == 0x01) {
            drawActiveDay(mImgThu, true);
            //mTextThu.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));

        } else {
            drawActiveDay(mImgThu, false);
            //mTextThu.setTextColor(getResources().getColor(R.color.black_color));
            //mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
        if (mSceneScheduleInfo.getByteWeek()[4] == 0x01) {
            drawActiveDay(mImgFri, true);
            //mTextFri.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgFri, false);
            //mTextFri.setTextColor(getResources().getColor(R.color.black_color));
            //mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

        }
        if (mSceneScheduleInfo.getByteWeek()[5] == 0x01) {
            drawActiveDay(mImgSat, true);
            //mTextSat.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgSat, false);
            //mTextSat.setTextColor(getResources().getColor(R.color.black_color));
            //mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
        if (mSceneScheduleInfo.getByteWeek()[6] == 0x01) {
            drawActiveDay(mImgSun, true);
            //mTextSun.setTextColor(getResources().getColor(R.color.whit_color));
            //mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            drawActiveDay(mImgSun, false);
            //mTextSun.setTextColor(getResources().getColor(R.color.black_color));
            //mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        }
    }

    private boolean checkSelectWeek() {
        for (int i = 0; i < mSceneScheduleInfo.getByteWeek().length; i++) {
            if (mSceneScheduleInfo.getByteWeek()[i] > 0)
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        int value = 0;
        AddSceneScheduleFragment.WeekInfo weekinfo = (AddSceneScheduleFragment.WeekInfo) v.getTag(R.id.id_schedule_scene_week_info);
        int weekArrayIndex = weekinfo.weekNo;
        if (weekinfo.stat) {
            weekinfo.stat = false;
            mSceneScheduleInfo.getByteWeek()[weekArrayIndex] = 0x00;

            weekinfo.img.setTextColor(getResources().getColor(R.color.black_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
        } else {
            weekinfo.stat = true;
            mSceneScheduleInfo.getByteWeek()[weekArrayIndex] = 0x01;
            weekinfo.img.setTextColor(getResources().getColor(R.color.whit_color));
            weekinfo.img.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));

        }
        v.setTag(R.id.id_schedule_scene_week_info, weekinfo);
    }


    public void setSceneSchedulInfo( SceneScheduleInfo info){
        mSceneScheduleInfo = info;
    }
    public void setMoodInfo(SceneIndividualInfo sceneIndividualInfo) {
        this.sceneIndividualInfo = sceneIndividualInfo;
    }
}