package com.rareraw.master.addsceneschedule;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SceneIndividualInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;


/**
 * Created by Lch on 2016-12-26.
 */

public class AddSceneScheduleListAdapter extends BaseAdapter implements View.OnClickListener{
    private LayoutInflater mLayoutInflater;
    private SceneScheduleInfo mScheduleList;
    private Context mContext;
    private OnActionListener mListener;
    private int mDeletePostion;
    public AddSceneScheduleListAdapter(Context context, SceneScheduleInfo _scheduleList, OnActionListener _listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mScheduleList = _scheduleList;
        mContext = context;
        this.mListener = _listener;
    }

    @Override
    public int getCount() {
        return mScheduleList.getSceneList().size();
    }

    @Override
    public Object getItem(int i) {
        return mScheduleList.getSceneList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        AddSceneScheduleListHolder addSceneScheduleListHolder;
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_add_scene_schedule, viewGroup, false);
            addSceneScheduleListHolder = new AddSceneScheduleListHolder();
            addSceneScheduleListHolder.mTextSceneName = (TextView) view.findViewById(R.id.item_list_fragment_add_scene_schedule_text_name);
            addSceneScheduleListHolder.mTextTime = (TextView) view.findViewById(R.id.item_list_fragment_add_scene_schedule_text_time);
            addSceneScheduleListHolder.mTextDefaultName = (TextView) view.findViewById(R.id.item_list_fragment_add_scene_schedule_text_default_name);
            addSceneScheduleListHolder.mListIndex = i;
            addSceneScheduleListHolder.mImgDeleteBtn = (ImageView) view.findViewById(R.id.item_list_fragment_add_scene_schedule_image_delete);
            addSceneScheduleListHolder.mImgDeleteBtn.setOnClickListener(this);
            addSceneScheduleListHolder.mImgDeleteBtn.setTag(R.id.item_list_fragment_add_scene_schedule_image_delete, i);
            // view.setOnClickListener(this);
        } else {
            addSceneScheduleListHolder = (AddSceneScheduleListHolder) view.getTag();
        }
        if (mScheduleList.getSceneList().get(i).getmSceneName().equals(mContext.getString(R.string.default_scene_name))){
            addSceneScheduleListHolder.mTextSceneName.setVisibility(View.GONE);
            addSceneScheduleListHolder.mTextTime.setVisibility(View.GONE);
            addSceneScheduleListHolder.mTextDefaultName.setVisibility(View.VISIBLE);
            addSceneScheduleListHolder.mTextDefaultName.setText(mScheduleList.getSceneList().get(i).getmSceneName());
            addSceneScheduleListHolder.mImgDeleteBtn.setVisibility(View.GONE);

        }else{
            addSceneScheduleListHolder.mTextSceneName.setVisibility(View.VISIBLE);
            addSceneScheduleListHolder.mTextTime.setVisibility(View.VISIBLE);
            addSceneScheduleListHolder.mTextDefaultName.setVisibility(View.GONE);
            addSceneScheduleListHolder.mImgDeleteBtn.setVisibility(View.VISIBLE);
        }

        addSceneScheduleListHolder.mTextSceneName.setText(mScheduleList.getSceneList().get(i).getmSceneName());
        addSceneScheduleListHolder.mTextTime.setText(getFullTimeMsg(mScheduleList.getSceneList().get(i)));
        view.setTag(addSceneScheduleListHolder);

        if(!APService.getUserLevel()){
            addSceneScheduleListHolder.mImgDeleteBtn.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    @Override
    public void onClick(View v) {

        mDeletePostion = (int) v.getTag(R.id.item_list_fragment_add_scene_schedule_image_delete);

        FragmentManager manager = ((Activity) mContext).getFragmentManager();
        DialogDeleteConfirm delete = new DialogDeleteConfirm();
        delete.setListener(mListener);
        delete.show(manager, "delete");
        return;

    }
    public int getDeletePosition(){
        return mDeletePostion;
    }
    private String getFullTimeMsg(SceneIndividualInfo _timeInfo){
        String sTime = "";
        if(_timeInfo.getIntStartHour() > 11)
            sTime = mContext.getString(R.string.frag_add_alarm_pm) + " " + String.format("%02d", _timeInfo.getIntStartHour()) +" : "+String.format("%02d", _timeInfo.getIntStartMin()) + " ~ ";
        else
            sTime = mContext.getString(R.string.frag_add_alarm_am) + " " + String.format("%02d", _timeInfo.getIntStartHour()) +" : "+ String.format("%02d", _timeInfo.getIntStartMin()) + " ~ ";
        if(_timeInfo.getIntEndHour() > 11)
            sTime += mContext.getString(R.string.frag_add_alarm_pm) + " " + String.format("%02d", _timeInfo.getIntEndHour()) +" : "+ String.format("%02d", _timeInfo.getIntEndMin());
        else
            sTime += mContext.getString(R.string.frag_add_alarm_am) + " " + String.format("%02d", _timeInfo.getIntEndHour()) +" : "+ String.format("%02d", _timeInfo.getIntEndMin());
        return sTime;
    }

    public void setSceneScheduleList(SceneScheduleInfo _sceneScheduleInfo) {
        this.mScheduleList = _sceneScheduleInfo;
    }

    public boolean checkSceneCnt() {
        if(mScheduleList.getSceneList().size() == 0)
            return false;
        for (int i = 0; i < mScheduleList.getSceneList().size(); i++) {
            if (mScheduleList.getSceneList().get(i).getmSceneName().equals(mContext.getString(R.string.default_scene_name)))
                return false;
        }
        return true;
    }


    static class AddSceneScheduleListHolder {
        private TextView mTextSceneName;
        private TextView mTextTime;
        private TextView mTextDefaultName;
        private ImageView mImgDeleteBtn;
        private int mListIndex;
    }
}
