package com.rareraw.master.addsceneschedule;

/**
 * Created by lch on 2017-01-13.
 */

public class ScheduleTimesetInfo {
    private int mStartHour;
    private int mStartMin;
    private int mEndHour;
    private int mEndMin;
    private int mSceneNo;
    private boolean mStartAmPm;
    private boolean mEndAmPm;
    private String mSceneName;

    public String getmSceneName() {
        return mSceneName;
    }

    public void setmSceneName(String mSceneName) {
        this.mSceneName = mSceneName;
    }

    public int getmStartHour() {
        return mStartHour;
    }

    public void setmStartHour(int mStartHour) {
        this.mStartHour = mStartHour;
    }

    public int getmStartMin() {
        return mStartMin;
    }

    public void setmStartMin(int mStartMin) {
        this.mStartMin = mStartMin;
    }

    public int getmEndHour() {
        return mEndHour;
    }

    public void setmEndHour(int mEndHour) {
        this.mEndHour = mEndHour;
    }

    public int getmEndMin() {
        return mEndMin;
    }

    public void setmEndMin(int mEndMin) {
        this.mEndMin = mEndMin;
    }

    public int getmSceneNo() {
        return mSceneNo;
    }

    public void setmSceneNo(int mSceneNo) {
        this.mSceneNo = mSceneNo;
    }

    public boolean ismStartAmPm() {
        return mStartAmPm;
    }

    public void setmStartAmPm(boolean mStartAmPm) {
        this.mStartAmPm = mStartAmPm;
    }

    public boolean ismEndAmPm() {
        return mEndAmPm;
    }

    public void setmEndAmPm(boolean mEndAmPm) {
        this.mEndAmPm = mEndAmPm;
    }
}
