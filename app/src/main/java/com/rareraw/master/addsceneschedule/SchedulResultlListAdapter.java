package com.rareraw.master.addsceneschedule;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnTimsetListener;
import com.rareraw.master.Config;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SceneIndividualInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;

/**
 * Created by Lch on 2016-12-26.
 */

public class SchedulResultlListAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;
    private SceneScheduleInfo mSceneSchedulInfo;
    private OnActionListener mListner;
    private Context mContext;
    private int mDeletePosition;
    private int editPosition;
    private OnTimsetListener mOnTimeListener = new OnTimsetListener() {
        @Override
        public void onTimesetLisnster(ScheduleTimesetInfo info, int editPosition) {
            try {
                //deleteDefaultScene();
                SceneIndividualInfo individualInfo = new SceneIndividualInfo();
                individualInfo.setmSceneName(info.getmSceneName());
                individualInfo.setIntFadeTime(Config.DEFAULT_FADE_TIME);
                individualInfo.setIntSceneNo(info.getmSceneNo());

                individualInfo.setIntStartHour(info.getmStartHour());
                individualInfo.setIntStartMin(info.getmStartMin());

                individualInfo.setIntEndHour(info.getmEndHour());
                individualInfo.setIntEndMin(info.getmEndMin());

                mSceneSchedulInfo.getSceneList().set(editPosition, individualInfo);
                notifyDataSetChanged();

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("error", "timeset casting");
            }
        }
    };

    public SchedulResultlListAdapter(Context context, SceneScheduleInfo _sceneplaylist, OnActionListener listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mSceneSchedulInfo = _sceneplaylist;
        this.mListner = listener;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mSceneSchedulInfo.getSceneList().size();
    }

    @Override
    public Object getItem(int i) {
        return mSceneSchedulInfo.getSceneList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    TextView oldcheckedTextView;
    ImageView oldImagViee;

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        AddScenePlayListHolder addScenePlayListHolder;
        if (view == null) {
            addScenePlayListHolder = new AddScenePlayListHolder();
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_schedul_result, viewGroup, false);
            addScenePlayListHolder.schedulTime = (TextView) view.findViewById(R.id.item_list_schedul_name);
            addScenePlayListHolder.delteBt = (RelativeLayout) view.findViewById(R.id.item_list_delete);
            addScenePlayListHolder.editBtn = (RelativeLayout) view.findViewById(R.id.item_list_edit);
        } else {
            addScenePlayListHolder = (AddScenePlayListHolder) view.getTag();
        }

        try {

            addScenePlayListHolder.delteBt.setTag(R.id.item_list_delete, i);
            String startAmpm = mSceneSchedulInfo.getSceneList().get(i).getIntStartHour() < 12 ? "AM " : "PM ";
            String startHour = String.format("%02d", mSceneSchedulInfo.getSceneList().get(i).getIntStartHour());
            String startMin = String.format("%02d", mSceneSchedulInfo.getSceneList().get(i).getIntStartMin());

            String startTime = startHour +":"+ startMin;

            String endAmpm = mSceneSchedulInfo.getSceneList().get(i).getIntEndHour() < 12 ? "AM " : "PM ";
            String endHour = String.format("%02d", mSceneSchedulInfo.getSceneList().get(i).getIntEndHour());
            String enMin = String.format("%02d", mSceneSchedulInfo.getSceneList().get(i).getIntEndMin());

            String endTime = endHour + ":" + enMin;

            addScenePlayListHolder.schedulTime.setText(startAmpm + startTime + " ~ " + endAmpm + endTime);

            addScenePlayListHolder.editBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DialogScheduleTimeset dialogScheduleTimeset = new DialogScheduleTimeset();
                    dialogScheduleTimeset.setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light );
                    dialogScheduleTimeset.setLisetener(mOnTimeListener, i);

                    // dialogScheduleTimeset.setMoodInfo(mSceneSchedulInfo.getSceneList().get(i));

                    // 01049147330 노말씬만되도록 수정
                    //mSceneSchedulInfo.setMoodInfo(APService.getApSceneList());
                    mSceneSchedulInfo.setMoodInfo(APService.getApNormalSceneList());
                    dialogScheduleTimeset.setSceneSchedulInfo(mSceneSchedulInfo);
                    dialogScheduleTimeset.setActiveMoodNo(mSceneSchedulInfo.getSceneList().get(i).getIntSceneNo());
                    android.support.v4.app.FragmentManager manager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                    dialogScheduleTimeset.show( manager, "dialogScheduleTimeset");
                }
            });

            addScenePlayListHolder.delteBt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int pos = (int) v.getTag(R.id.item_list_delete);
                    mDeletePosition = pos;
                    FragmentManager manager = ((Activity) mContext).getFragmentManager();
                    DialogDeleteConfirm delete = new DialogDeleteConfirm();
                    delete.setListener(mListner );
                    delete.show(manager, "delete");
                    return;
                 /*
                    mSceneSchedulInfo.getSceneList().remove(i);
                    notifyDataSetChanged();*/
                }
            });


        } catch (Exception e) {
            Log.e("error", "play scene getview");
        }
        view.setTag(addScenePlayListHolder);
        return view;
    }

    public void setResultMoodList(SceneScheduleInfo sceneScheduleInfo) {
        this.mSceneSchedulInfo = sceneScheduleInfo;
    }

    public int getDeletePosition() {
        return  mDeletePosition;
    }

    static class AddScenePlayListHolder {
        private TextView schedulTime;
        private RelativeLayout editBtn;
        private RelativeLayout delteBt;

    }
}
