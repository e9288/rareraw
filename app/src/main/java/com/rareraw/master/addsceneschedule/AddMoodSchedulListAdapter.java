package com.rareraw.master.addsceneschedule;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.addscene.MoodListAdapter;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.DataClass.ScenePlayInfo;
import com.rareraw.master.network.DataClass.SceneScheduleInfo;

/**
 * Created by Lch on 2016-12-26.
 */

public class AddMoodSchedulListAdapter extends BaseAdapter{

    private LayoutInflater mLayoutInflater;
    private SceneScheduleInfo mSceneSchedulInfoList;
    private OnActionListener mListner;
    private Context mContext;
    private int mDeletePosition;
    public AddMoodSchedulListAdapter(Context context, SceneScheduleInfo _sceneplaylist, int actNo) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mSceneSchedulInfoList = _sceneplaylist;
        resetFlag();
        setActiveMoodNo(actNo);
        mContext = context;
    }

    public void setActiveMoodNo(int no){
        for(int i = 0; i < mSceneSchedulInfoList.getMoodList().size(); i++){
            if(mSceneSchedulInfoList.getMoodList().get(i).getIntSceneNo() == no){
                mSceneSchedulInfoList.getMoodList().get(i).setSelectedFlag(true);
            }
        }
    }

    @Override
    public int getCount() {
        return mSceneSchedulInfoList.getMoodList().size();
    }

    @Override
    public Object getItem(int i) {
        return mSceneSchedulInfoList.getMoodList().get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    TextView oldcheckedTextView;
    ImageView oldImagViee;
    RelativeLayout oldSelectLayout;
    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        AddScenePlayListHolder addScenePlayListHolder;
        if (view == null) {
            addScenePlayListHolder = new AddScenePlayListHolder();
            view = mLayoutInflater.inflate(R.layout.item_list_fragment_add_scene_play, viewGroup, false);
            addScenePlayListHolder.mTextName = (TextView) view.findViewById(R.id.item_list_fragment_play_mood_name);

        } else {
            addScenePlayListHolder = (AddScenePlayListHolder) view.getTag();
        }

        final ImageView moodTypeImg = (ImageView) view.findViewById(R.id.list_view_mood_type);

        final RelativeLayout item = (RelativeLayout) view.findViewById(R.id.moode_layout);
        final TextView textchecked = (TextView) view.findViewById(R.id.textchecked);

        final RelativeLayout selectLayout = (RelativeLayout) view.findViewById(R.id.layout_selete);

        // selectLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
        if(mSceneSchedulInfoList.getMoodList().get(i).isSelectedFlag()){



            oldcheckedTextView = textchecked;
            oldImagViee = moodTypeImg;
            oldSelectLayout = selectLayout;

            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood_act);
            textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
            selectLayout.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));

          //  textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));

        }else{
            moodTypeImg.setImageResource(R.mipmap.icon_normal_mood);
            textchecked.setTextColor(mContext.getResources().getColor(R.color.black_color));
            selectLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
           // textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
        }

        item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetFlag();
                mSceneSchedulInfoList.getMoodList().get(i).setSelectedFlag(true);
                if(oldImagViee != null)
                    oldImagViee.setImageResource(R.mipmap.icon_normal_mood);

                if(oldcheckedTextView != null) {
                    oldcheckedTextView.setTextColor(mContext.getResources().getColor(R.color.black_color));
                 //   oldcheckedTextView.setBackgroundColor(mContext.getResources().getColor(R.color.whit_color));
                }

                if(oldSelectLayout != null) {
                    oldSelectLayout.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.drawable_edit_text_left_line));
                }


                oldcheckedTextView = textchecked;
                oldImagViee = moodTypeImg;
                oldSelectLayout = selectLayout;

               // textchecked.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                textchecked.setTextColor(mContext.getResources().getColor(R.color.whit_color));
                selectLayout.setBackgroundColor(mContext.getResources().getColor(R.color.select_item));
                oldImagViee.setImageResource(R.mipmap.icon_normal_mood_act);

            }
        });


        try {
            addScenePlayListHolder.mTextName.setText(mSceneSchedulInfoList.getMoodList().get(i).getSceneName());

        } catch (Exception e) {
            Log.e("error", "play scene getview");
        }
        view.setTag(addScenePlayListHolder);
        return view;
    }

    public void resetFlag() {
        for (SceneInfo info : mSceneSchedulInfoList.getMoodList())
            info.setSelectedFlag(false);
    }

    public int getDeletPosition(){
        return mDeletePosition;
    }

    public boolean isSelectMood(){

        for (SceneInfo info : mSceneSchedulInfoList.getMoodList()){
            if(info.isSelectedFlag())
                return true;
        }
        return  false;
    }
    public void setMoodList(SceneScheduleInfo mSceneScheduleInfo) {
        this.mSceneSchedulInfoList = mSceneScheduleInfo;
    }

    public SceneInfo getSelectedMoodInfo() {
        for(int i = 0; i < mSceneSchedulInfoList.getMoodList().size(); i++){
            if(mSceneSchedulInfoList.getMoodList().get(i).isSelectedFlag())
                return mSceneSchedulInfoList.getMoodList().get(i);
        }
        return null;
    }

  /*  public int getDeletePosition() {

    }*/


    static class AddScenePlayListHolder {
        private TextView mTextHoldingTime;
        private TextView mTextName;
        private ImageView mImgAdd;
        private ImageView mImgSub;
        private ImageView mImgDelete;
    }
}
