package com.rareraw.master;

/**
 * Created by Lch on 2016-11-22.
 */

public class Config {

    /**************debug flag*****************/
    public final static boolean DEBUG = false;
    /**************debug flag*****************/

    public final static String USER_ICON_DAY = "USER_ICON_DAY";
    public final static String USER_ICON_SEASON = "USER_ICON_SEASON";
    public final static String USER_ICON_PLACE = "USER_ICON_PLACE";

    public final static int USER_ICON_DAY_MAX_CNT = 7;
    public final static int USER_ICON_SEASON_MAX_CNT = 4;
    public final static int USER_ICON_PLACE_MAX_CNT = 9;


    public final static int USER_DEFAULT_ICON = -1;
    public final static int USER_ICON_AFTERNOON = 20;
    public final static int DEFAULT_FADE_TIME = 1;

    public final static int NO_SELECT = 0;

    /*icon index define*/
    public final static int CATEGORY1_MIN  = 0;
    public final static int CATEGORY1_MAX  = 9; // 0~9 icon category 1 ( default icon  + day )

    public final static int CATEGORY2_MIN  = 10;
    public final static int CATEGORY2_MAX  = 19; // 10 ~ 19 icon category 2 ( season )

    public final static int CATEGORY3_MIN  = 20;
    public final static int CATEGORY3_MAX  = 29; // 20 ~ 29 icon category 3 ( place )

    public final static int DMX_MAX_VALUE = 255 ;
    public final static int NAME_MAX_LENGTH= 16 ;

    public final static int PASSWORD_MAX_LENGTH= 8 ;
}
