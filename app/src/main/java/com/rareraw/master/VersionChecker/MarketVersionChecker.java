package com.rareraw.master.VersionChecker;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.support.v7.widget.AppCompatDrawableManager.get;

/**
 * Created by lch on 2017-01-30.
 */

public class MarketVersionChecker {
   /* public static String getMarketVersion(String packageName) {

        Log.d("input pakage", packageName);

        try {

            // com.nexon.nsc.maplem&hl=ko
            //Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + packageName).get();
            Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + "com.nexon.nsc.maplem&hl=ko").get();
            Elements Version = document.select(".content");
            for (Element element : Version) {
                if (element.attr("itemprop").equals("softwareVersion")) {
                    return element.text().trim();
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }*/

    public static String getMarketVersionFast(String packageName) {

        String mData = "", mVer = null;
        try {
            // com.nexon.nsc.maplem&hl=ko
            //Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + packageName).get();
            //Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=" + "com.nexon.nsc.maplem&hl=ko").get();

            // URL mUrl = new URL("https://play.google.com/store/apps/details?id=" + packageName);
            URL mUrl = new URL("https://play.google.com/store/apps/details?id=" + "com.nexon.nsc.maplem");
            HttpURLConnection mConnection = (HttpURLConnection) mUrl.openConnection();
            if (mConnection == null) {
                return null;
            }

            mConnection.setConnectTimeout(1500);
            mConnection.setUseCaches(false);
            mConnection.setDoOutput(true);

            if (mConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(mConnection.getInputStream()));
                while (true) {
                    String line = bufferedReader.readLine();
                    if (line == null)
                        break;
                    mData += line;
                }
                bufferedReader.close();
            }
            mConnection.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }

        String startToken = "softwareVersion\">";
        String endToken = "<";
        int index = mData.indexOf(startToken);

        if (index == -1) {
            mVer = null;
        } else {
            mVer = mData.substring(index + startToken.length(), index + startToken.length() + 100);
            mVer = mVer.substring(0, mVer.indexOf(endToken)).trim();
        }
        return mVer;
    }
}
