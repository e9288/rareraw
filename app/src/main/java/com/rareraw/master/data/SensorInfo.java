package com.rareraw.master.data;

/**
 * Created by Lch on 2016-12-26.
 */

public class SensorInfo  {

    public int getIntSceneNo() {
        return mIntSceneNo;
    }

    public void setIntSceneNo(int mIntSceneNo) {
        this.mIntSceneNo = mIntSceneNo;
    }

    public byte[] getByteSceneNo() {
        return mByteSceneNo;
    }

    public void setByteSceneNo(byte[] mByteSceneNo) {
        this.mByteSceneNo = mByteSceneNo;
    }

    int mIntSceneNo ;
    byte[] mByteSceneNo;

    boolean mDetectSensorStatus;
    boolean mLightSensorStatuf;

    boolean mDetectValue;
    int mLightSensorValue = 0;

    public boolean isDetectValue() {
        return mDetectValue;
    }

    public void setDetectValue(boolean mDetectValue) {
        this.mDetectValue = mDetectValue;
    }


    public int getLightSensorValue() {
        return mLightSensorValue;
    }

    public void setLightSensorValue(int mLightSensorValue) {
        this.mLightSensorValue = mLightSensorValue;
    }

    public boolean isDetectSensorStatus() {
        return mDetectSensorStatus;
    }

    public void setDetectSensorStatus(boolean mDetectSensorStatus) {
        this.mDetectSensorStatus = mDetectSensorStatus;
    }

    public boolean isLightSensorStatus() {
        return mLightSensorStatuf;
    }

    public void setLightSensorStatus(boolean mLightSensorStatuf) {
        this.mLightSensorStatuf = mLightSensorStatuf;
    }



}
