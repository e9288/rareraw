package com.rareraw.master.data;

/**
 * Created by Lch on 2016-11-22.
 */

public class SimpleSceneInfo {
    //시스템에 설정한 씬의 상태 0 - 일반, 1 - 선택
    private int mSceneStatus;
    //시스템에 설정한 씬의 구분 0 - 일반, 1 - 히든
    private int mSceneHide;
    //시스템에 설정한 씬의 종류 0 - Scene, 1 - ScenePlay, 2 - SceneSchedule
    private int mSceneType;
    //사용자가 설정한 씬의 정보
    private int mSceneIcon;
    private String mSceneName;
    //선택한 씬의 재생 상태
    private boolean mCurrentSceneIsPlay;

    // 선택한 씬플레이 재생상태
    //삭제 여부
    private boolean mDeleteSelected;

    private int mDimingLevel;

    private int mSceneIndex;

    public int getSceneIndex() {
        return mSceneIndex;
    }

    public void setSceneIndex(int mSceneIndex) {
        this.mSceneIndex = mSceneIndex;
    }

    public int getDimingLevel() {
        return mDimingLevel;
    }

    public void setDimingLevel(int mDimingLevel) {
        this.mDimingLevel = mDimingLevel;
    }

    public int getSceneNo() {
        return mSceneNo;
    }

    public void setSceneNo(int mSceneNo) {
        this.mSceneNo = mSceneNo;
    }

    private int mSceneNo ;
    public SimpleSceneInfo(){

    }

    public SimpleSceneInfo(int status, int type, int icon, String name, int hide, int scneNo, int scneeIndex) {
        mSceneStatus = status;
        if(status == 1)
            setCurrentSceneIsPlay(true);
        mSceneType = type;
        mSceneIcon = icon;
        mSceneName = name;
        mSceneHide = hide;
        mSceneNo = scneNo;
        mSceneIndex = scneeIndex;
    }

    public void setSceneStatus(int value) {
        //선택된 아이템은 현재 재생 상태를 true 로 변경해줍니다. -> 선택된 것과 재생중인 것은 별개입니다. 선택은 재생에 영향을 주지만 재생은 선택에 영향을 주지 않습니다.
        if (value == 1)
            setCurrentSceneIsPlay(true);
        else
            setCurrentSceneIsPlay(false);
        mSceneStatus = value;
    }

    public int getSceneStatus() {
        return mSceneStatus;
    }

    public void setSceneType(int value) {
        mSceneType = value;
    }

    public int getSceneType() {
        return mSceneType;
    }

    public void setSceneIcon(int value) {
        mSceneIcon = value;
    }

    public int getSceneIcon() {
        return mSceneIcon;
    }

    public void setSceneName(String name) {
        mSceneName = name;
    }

    public String getSceneName() {
        return mSceneName;
    }

    public boolean getCurrentSceneIsPlay(){
        return mCurrentSceneIsPlay;
    }

    public void setCurrentSceneIsPlay(boolean value){
        mCurrentSceneIsPlay = value;
    }

    public int getSceneHide(){
        return mSceneHide;
    }

    public void setDeleteSelected(boolean value){
        this.mDeleteSelected = value;
    }

    public boolean getDeleteSelected(){
        return this.mDeleteSelected;
    }
}
