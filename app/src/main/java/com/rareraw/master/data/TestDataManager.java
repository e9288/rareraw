package com.rareraw.master.data;

import com.rareraw.master.network.Cmd;

import java.util.ArrayList;

/**
 * Created by Lch on 2016-11-22.
 */

public class TestDataManager {
    private ArrayList<SimpleSceneInfo> simpleSceneInfos;

    public TestDataManager(){
        simpleSceneInfos = new ArrayList<>();
        simpleSceneInfos.add(new SimpleSceneInfo(0, Cmd.SCHEDULE_SCENE, 5, "영화 볼때 조명", 1,1,1));
        simpleSceneInfos.add(new SimpleSceneInfo(0, Cmd.PLAY_SCENE, 6, "아침 기상시간", 1,1, 1));
        simpleSceneInfos.add(new SimpleSceneInfo(0,Cmd.PLAY_SCENE, 21, "책 읽을 때 조명", 0,1, 1));
        simpleSceneInfos.add(new SimpleSceneInfo(0, Cmd.NORMAL_SCENE, 0, "운동할 때 조명", 1,1, 1));
        simpleSceneInfos.add(new SimpleSceneInfo(0,Cmd.NORMAL_SCENE, 1, "자기직전 조명", 0,1, 1));
        simpleSceneInfos.add(new SimpleSceneInfo(0,Cmd.NORMAL_SCENE, 2, "TV 볼때 조명", 0,1, 1));
    }

    public ArrayList<SimpleSceneInfo> getSimpleSceneInfos(){
        return simpleSceneInfos;
    }

}
