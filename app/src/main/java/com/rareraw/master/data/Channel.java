package com.rareraw.master.data;

/**
 * Created by Lch on 2016-12-26.
 */

public class Channel {
    private String name;
    private int dimming;
    private boolean select;

    public int getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(int channelNo) {
        this.channelNo = channelNo;
    }

    private int channelNo;
    public Channel(String name, int dimming){
        this.name = name;
        this.dimming = dimming;
    }
    public Channel(String name, int dimming, boolean stat, int _chno){
        this.name = name;
        this.dimming = dimming;
        this.select = stat;
        this.channelNo = _chno;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setDimming(int dimming){
        this.dimming = dimming;
    }

    public int getDimming(){
        return this.dimming;
    }

    public void setSelect(boolean value){
        select = value;
    }

    public boolean getSelect(){
        return select;
    }
}
