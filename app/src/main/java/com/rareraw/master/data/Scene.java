package com.rareraw.master.data;

import com.rareraw.master.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lch on 2016-12-26.
 */

public class Scene extends SimpleSceneInfo {

    private List<Channel> mChannelList = new ArrayList<>();
    private int mIconType = Config.USER_ICON_AFTERNOON;
    private String mName;
    private boolean mHideCheck = false;
    private int mFadeTime = 1;

    public int getSceneNo() {
        return mSceneNo;
    }

    public void setSceneNo(int mSceneNo) {
        this.mSceneNo = mSceneNo;
    }

    private int mSceneNo = 0;
    public int getIndex() {
        return mIndex;
    }

    public void setIndex(int mIndex) {
        this.mIndex = mIndex;
    }

    private int mIndex = 0;
    public int getIconType() {
        return mIconType;
    }

    public void setIconType(int mIconType) {
        this.mIconType = mIconType;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public boolean getHideCheck() {
        return mHideCheck;
    }

    public void setHideCheck(boolean mHideCheck) {
        this.mHideCheck = mHideCheck;
    }

    public int getFadeTime() {
        return mFadeTime;
    }

    public void setFadeTime(int mFadeTime) {
        this.mFadeTime = mFadeTime;
    }

    public void setChannelList(List<Channel> channelList){
        this.mChannelList = channelList;
    }

    public List<Channel> getChannelList(){
        return mChannelList;
    }
}
