package com.rareraw.master.channel;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnRenameListener;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;

/**
 * Created by Lch on 2016-10-21.
 */

public class DialogChannelChange extends DialogFragment {

    private TextView mBtnOk, mBtnCCancel;
    private EditText mEditTextChannelName;
    private static byte[] mChannelId;
    OnRenameListener mListener;

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    public static DialogChannelChange newInstance(byte[] _channelNo) {
        DialogChannelChange channelChangeDialog = new DialogChannelChange();
        mChannelId = _channelNo;
        return channelChangeDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return inflater.inflate(R.layout.dialog_change_channel_name, container, false);

    }

    public void setLisetener(OnRenameListener lisetener) {
        mListener = lisetener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mEditTextChannelName = (EditText) view.findViewById(R.id.change_channel_name);
        mEditTextChannelName.setHint(getTag().toString());
        mBtnCCancel = (TextView) view.findViewById(R.id.dialog_change_name_cancel);
        mBtnCCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (TextView) view.findViewById(R.id.dialog_change_name_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mEditTextChannelName.getText().toString();
                if (name.getBytes().length > Config.NAME_MAX_LENGTH) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_channel_name_length), Toast.LENGTH_SHORT);
                    return;
                }
                if (name.length() <= 0) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_channel_name_length), Toast.LENGTH_SHORT);
                    return;
                }
                String tempName = name;
                int length = 16 - name.getBytes().length;
                byte[] empty = new byte[length];
                tempName = tempName + new String(empty);
                ByteBuffer buffer  = ByteBuffer.allocate(17);
                buffer.put(mChannelId);
                buffer.put(tempName.getBytes());
                APService.setChannelName(buffer, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Toast.makeText(getActivity(), "채널 이름이 변경되었습니다.", Toast.LENGTH_SHORT).show();
                        mListener.onCompleteRename(1, getTag());
                        dismiss();
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.e("error", "change ChannelName");
                        dismiss();
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Log.e("error", "change ChannelName");
                        dismiss();
                    }
                }) ;
            }
        });
    }


}