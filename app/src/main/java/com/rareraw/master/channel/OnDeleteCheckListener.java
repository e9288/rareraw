package com.rareraw.master.channel;

/**
 * Created by lch on 2016-10-23.
 */

public interface OnDeleteCheckListener {

    void onDeleteCheck(byte[] channelId);
    void onDeleteOk();
}
