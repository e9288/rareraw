package com.rareraw.master.channel;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.OnRenameListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.DeviceInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Lch on 2016-12-14.
 */

public class ChannelManagerFragment extends BaseFragment implements SeekBar.OnSeekBarChangeListener, View.OnLongClickListener, OnRenameListener {
    private ArrayList<DeviceInfo> mDevList;
    private ArrayList<ChannelInfo> mChannelList = new ArrayList<>();
    private OnDeleteCheckListener listener;
    private static ChannelAdapter adapter;
    private static final String TAG = "ChannelManagerFragment";
    public static final String EDIT_CH_NAME = "editname", EDIT_COMPLETE = "edit complete";
    private SeekBar mMasterSeeker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // load channel info.
        mDevList = APService.mRegistedDevList;


        // set channel list in devicelist
//        mChannelList = getChannelList();
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.setUserLevel(APService.getUserLevel());
        updateUi();
    }

    // TODO test code must have delete
    private void testgetChannelInfo() {
        if (Config.DEBUG) {
            mDevList.add(new DeviceInfo(1));
            byte[] c1 = new byte[]{(byte) 0x00, (byte) 0x01};
            byte[] c2 = new byte[]{(byte) 0x00, (byte) 0x02};
            byte[] c3 = new byte[]{(byte) 0x00, (byte) 0x03};
            ArrayList<byte[]> mByteChannelNo = new ArrayList<>();
            mByteChannelNo.add(c1);
            mByteChannelNo.add(c2);
            // mByteChannelNo.add(c3);

            ArrayList<String> mChannelName = new ArrayList<>();
            mChannelName.add("mychannel1");
            mChannelName.add("mychannel2");
            // mChannelName.add("mychannel3");

            mDevList.get(0).setChannelNo(mByteChannelNo);
            mDevList.get(0).setChannelName(mChannelName);

           // mDevList.get(1).setChannelNo(mByteChannelNo);
          //  mDevList.get(1).setChannelName(mChannelName);

            mChannelList = getChannelList();
            adapter.setChannelList(mChannelList);
            adapter.notifyDataSetChanged();
        }

    }

    private ArrayList<ChannelInfo> getChannelList() {
        ArrayList<ChannelInfo> channelList = new ArrayList<>();
        for (DeviceInfo device : mDevList) {
            for (int cnt = 0; cnt < device.getChannelNo().size(); cnt++) {
                String name = (CmdHelper.checkNullName(device.getChannelName().get(cnt)) == 0) ? "Ch"+ String.format("%02d", device.getIntChannelNo().get(cnt)) : device.getChannelName().get(cnt);
                channelList.add(new ChannelInfo(name, device.getChannelNo().get(cnt)));
            }
        }
        Log.e("channellist size : ", Integer.toString(channelList.size()));
        return channelList;
    }

    //레이아웃 리소스 생성
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_channel_manager, container, false);
    }

    //onCreateView에서 생성된 View를 받아서 Layout내부의 객체들을 정의
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView listViewChannels = (ListView) view.findViewById(R.id.fragment_channelcontrol_list_view);
        adapter = new ChannelAdapter(getContext(), mChannelList, this);
        listViewChannels.setAdapter(adapter);

        TextView textViewMasterValue = (TextView) view.findViewById(R.id.fragment_add_channel_control_master_value);
        textViewMasterValue.setText("0%");

        mMasterSeeker = (SeekBar) view.findViewById(R.id.fragment_channel_control_seek_master);
        mMasterSeeker.setOnSeekBarChangeListener(this);
        mMasterSeeker.setTag(R.id.id_fragment_channel_list_view, listViewChannels);
        mMasterSeeker.setTag(R.id.id_fragment_channelcontrol_text_master_value, textViewMasterValue);

        //mMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
        //mMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));

        // setSeekberThumb();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        TextView textView = (TextView) seekBar.getTag(R.id.id_fragment_channelcontrol_text_master_value);
        textView.setText(String.valueOf(progress) + "%");

        adapter.onProgressChanged(progress);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        ListView listView = (ListView) seekBar.getTag(R.id.id_fragment_channel_list_view);
        int count = listView.getCount();
        for (int i = 0; i < count; i++)
            mChannelList.get(i).mIntChannelDiming = seekBar.getProgress();

        int val = mMasterSeeker.getProgress();
        byte[] bVal = new byte[1];
        bVal[0] = (byte) val;

        APService.setMasterDiming(new String(bVal), new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                adapter.setChannelList(mChannelList);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }


    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    public void setSeekberThumb() {
        ViewTreeObserver vto = mMasterSeeker.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                try {
                    Resources res = getActivity().getResources();
                    Drawable thumb = res.getDrawable(R.mipmap.img_master_seekbar_thumb);
                    int h = (int) (mMasterSeeker.getMeasuredHeight() * 0.58);
                    int w = h;
                    Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                    Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                    Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                    newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                    mMasterSeeker.setThumb(newThumb);
                    mMasterSeeker.getViewTreeObserver().removeOnPreDrawListener(this);

                } catch (Exception e) {

                }
                return true;
            }
        });
    }

    private void updateUi() {
        if (APService.mRegistedDevList == null)
            return;
        APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                mDevList = APService.mRegistedDevList;
                mChannelList = getChannelList();
                adapter.setChannelList(mChannelList);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onError(int errorCode) {
                Log.e(TAG, "getDeviceErr");
                testgetChannelInfo();
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });

    }

    @Override
    public void onActionRename(byte[] channelId, String oldName) {
        oldName = oldName.replace("\0", "" );

        DialogChannelChange channelChangeDialog = DialogChannelChange.newInstance(channelId);
        channelChangeDialog.setLisetener(this);
        channelChangeDialog.show(getFragmentManager(), oldName);
    }

    @Override
    public void onCompleteRename(int nValue, String sValue) {
        updateUi();
    }

    public static class ChannelAdapter extends BaseAdapter implements View.OnClickListener, AdapterView.OnItemLongClickListener, SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {

        private final Context mContext;
        private LayoutInflater mLayoutInflater;
        private ArrayList<DeviceInfo> mDeviceList;
        private ArrayList<ChannelInfo> mChannList;
        private boolean mUsreLevel;
        OnRenameListener mListener;

        public ChannelAdapter(Context _context, ArrayList<ChannelInfo> _channList, OnRenameListener listener) {
            mLayoutInflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mContext = _context;
            this.mChannList = _channList;
            this.mListener = listener;
        }

        public void setChannelList(ArrayList<ChannelInfo> _list) {
            this.mChannList = _list;
        }

        @Override
        public int getCount() {
            return mChannList.size();
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Object getItem(int i) {
            return mChannList.get(i);
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {
            ChannelHolder chHolder = new ChannelHolder();
            if (view == null) {
                view = mLayoutInflater.inflate(R.layout.item_list_view_channel_list, viewGroup, false);
                chHolder.imageRename = (ImageView) view.findViewById(R.id.change_channelname_btn);
                chHolder.touchRename = (RelativeLayout) view.findViewById(R.id.layout_change_channelname_btn);

                chHolder.seekDimming = (SeekBar) view.findViewById(R.id.item_list_view_channel_control_seek_bar);
                chHolder.seekDimming.setOnSeekBarChangeListener(this);
                resizeSeekbarthumb(chHolder.seekDimming, 1.4);

                chHolder.textValue = (TextView) view.findViewById(R.id.item_list_view_channel_control_text_value);
                chHolder.textName = (TextView) view.findViewById(R.id.item_list_view_channelcontrol_text_name);

            } else {
                chHolder = (ChannelHolder) view.getTag();
            }

            if (mUsreLevel) {
                chHolder.touchRename.setOnClickListener(this);
                chHolder.imageRename.setVisibility(View.VISIBLE);
            } else {
                chHolder.imageRename.setVisibility(View.GONE);
            }

            chHolder.touchRename.setTag(R.id.change_channelname_btn, i);

            if (CmdHelper.checkNullName(mChannList.get(i).mChannelName) != 0) {
                chHolder.textName.setText(mChannList.get(i).mChannelName);
            } else {
                String no = String.format("%02d", mChannList.get(i).mIntChannelNo);
                chHolder.textName.setText("Ch" + no);
            }
            int valDiming = mChannList.get(i).mIntChannelDiming;
            chHolder.textValue.setText(Integer.toString(valDiming) + "%");


            chHolder.seekDimming.setTag(R.id.item_list_view_channel_control_text_value, chHolder.textValue);
            chHolder.seekDimming.setTag(R.id.item_list_view_channel_control_seek_bar, i);
            chHolder.seekDimming.setProgress(valDiming);
            view.setTag(chHolder);
            return view;
        }

        public void onProgressChanged(int value) {
            for (int i = 0; i < mChannList.size(); i++)
                mChannList.get(i).mIntChannelDiming = value;
            notifyDataSetChanged();
        }

        public void resizeSeekbarthumb(final SeekBar mySeekbar, final double size) {
            ViewTreeObserver vto = mySeekbar.getViewTreeObserver();
            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    Resources res = mContext.getResources();
                    Drawable thumb = res.getDrawable(R.mipmap.img_single_seekbar_thumb);
                    int h = (int) (mySeekbar.getMeasuredHeight() * size);
                    int w = h;
                    Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                    Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                    Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                    newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                    mySeekbar.setThumb(newThumb);
                    mySeekbar.getViewTreeObserver().removeOnPreDrawListener(this);
                    return true;
                }
            });
        }


        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

        }

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            TextView textView = (TextView) seekBar.getTag(R.id.item_list_view_channel_control_text_value);
            textView.setText(String.valueOf(seekBar.getProgress()) + "%");

        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

            int channelindex = (int) seekBar.getTag(R.id.item_list_view_channel_control_seek_bar);

            ByteBuffer buffer = ByteBuffer.allocate(2);
            buffer.put((byte) mChannList.get(channelindex).mIntChannelNo);
            buffer.put((byte) seekBar.getProgress());

            APService.setSingleDiming(buffer);
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            return false;
        }

        @Override
        public void onClick(View v) {

            if (!APService.getUserLevel()) {
                new AlertDialog.Builder(mContext)
                        .setTitle("Access denied ")
                        .setMessage("You do not have permission.")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return;
            }
            int index = (int) v.getTag(R.id.change_channelname_btn);
            mListener.onActionRename(mChannList.get(index).mByteChannelNo,  mChannList.get(index).mChannelName);
        }

        public void setUserLevel(boolean userLevel) {
            mUsreLevel = userLevel;
        }

        static class ChannelHolder {
            ImageView imageRename;
            TextView textName;
            TextView textValue;
            SeekBar seekDimming;
            RelativeLayout touchRename;
        }
    }

    public static class ChannelInfo {
        private String mChannelName;

        private String mSChannelNo;
        private int mIntChannelNo;
        private byte[] mByteChannelNo;

        private byte mByteChannelDiming;
        private int mIntChannelDiming;

        public ChannelInfo(String _channame, byte[] _channelno) {
            this.mChannelName = _channame;

            this.mSChannelNo = String.format("%02d", CmdHelper.byteArrayToInt(_channelno));
            this.mByteChannelNo = _channelno;
            this.mIntChannelNo = CmdHelper.byteArrayToInt(_channelno);

            this.mIntChannelDiming = 0;
            this.mByteChannelDiming = 0x00;


        }
    }
}