package com.rareraw.master.addsceneold;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;
import com.rareraw.master.addscene.DialogSetDmxValue;
import com.rareraw.master.network.Cmd;

import java.util.ArrayList;

public class SceneDmx512 extends Fragment implements OnActionListener {

    private final int DMX_MAX_SIZE = 42;
    private final int DMX_ARRAY_SIZE = 546;
    private final int START_PAGE = 1, END_PAGE = 13;
    private int CURRENT_PAGE = 1;
    private int DMX_NO = 0;
    private DmxInfo[] mDmxInfoArray =  new DmxInfo[Cmd.MAX_DMX_SIZE];
    private ArrayList<DmxInfo> mDmxList = new ArrayList<>();
    private AdapterDmx512 mAdapter;
    private GridView mGridView;
    private ImageView mBtnPreview, mBtnNext, mBtnSave;
    private TextView mTextCurPage;
    private OnFragmentInteractionListener mListener;
    private View mParent;
    private View mGridViewHeight;
    private int mHeightSize = 0;
    private Context mContext;

    public SceneDmx512() {

    }

    public static SceneDmx512 newInstance() {
        SceneDmx512 fragment = new SceneDmx512();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
        Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mParent = inflater.inflate(R.layout.fragment_scene_dmx512, container, false);
        initDmxInfo();
        getDeviceDmxInfo();
        initView();
        return mParent;
    }

    private void initDmxInfo() {
        for(int index = 0; index < Cmd.MAX_DMX_SIZE; index++){
            mDmxInfoArray[index] = new DmxInfo();
            mDmxInfoArray[index].mNo = index+1;
        }
    }

    private void updateDmxUi() {
        mAdapter.setDmxList(getDmxInitValue());
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void initView() {
        mGridView = (GridView) mParent.findViewById(R.id.grieview_dmxinfo);
        mTextCurPage = (TextView) mParent.findViewById(R.id.textview_curpage);
        mBtnPreview = (ImageView) mParent.findViewById(R.id.btn_prev);

        mAdapter = new AdapterDmx512(getContext(), this);
        mAdapter.setDmxList(getDmxInitValue());
        mGridView.setAdapter(mAdapter);

        mBtnPreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("current page : ", Integer.toString(CURRENT_PAGE));
                if(CURRENT_PAGE == START_PAGE)
                    return;

                CURRENT_PAGE--;
                mTextCurPage.setText(Integer.toString(CURRENT_PAGE));
                updateDmxUi();
            }
        });

        mBtnNext = (ImageView) mParent.findViewById(R.id.btn_next);
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("current page : ", Integer.toString(CURRENT_PAGE));
                if(CURRENT_PAGE == END_PAGE)
                    return;
                CURRENT_PAGE++;
                mTextCurPage.setText(Integer.toString(CURRENT_PAGE));
                updateDmxUi();
            }
        });

        mBtnSave = (ImageView) mParent.findViewById(R.id.btn_save);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private ArrayList<DmxInfo> getDmxInitValue() {
        int startNo = (CURRENT_PAGE *42) - 41;
        mDmxList.clear();
        for (int i = startNo-1; i < (DMX_MAX_SIZE+startNo)-1; i++) {
            mDmxList.add(mDmxInfoArray[i]);
        }
        return mDmxList;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     /*   if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getDeviceDmxInfo() {
        // TODO 디바이스에서 dmx 정보를 읽어오도록 변경 해야 함.

    }

    @Override
    public void onActionListener(int _arrayIndex, String _value) {
        try {
            // Because dmx real start no at -1
            mDmxInfoArray[_arrayIndex-1].mUseFlag = true;
            mDmxInfoArray[_arrayIndex-1].mSVaue = _value;
            updateDmxUi();
        }catch (IndexOutOfBoundsException e){
            Log.e("error","indexoutofbound");
        }catch (Exception e){
            Log.e("error","error");
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public class AdapterDmx512 extends BaseAdapter implements View.OnClickListener{

        private LayoutInflater mLayoutInflater;
        private ArrayList<DmxInfo> mDmxInfoList = new ArrayList<>();
        private OnActionListener mLinstener;
        public AdapterDmx512(Context context, OnActionListener _listner) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mLinstener = _listner;
            //mDmxInfoList = _dmxlist;
        }

        @Override
        public int getCount() {
            return mDmxInfoList.size();
        }

        @Override
        public Object getItem(int i) {

            return mDmxInfoList.get(1);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if(view == null)
                view = mLayoutInflater.inflate(R.layout.item_gridview_dmx512, viewGroup, false);

            TextView no = (TextView) view.findViewById(R.id.text_no);
            TextView value = (TextView) view.findViewById(R.id.text_value);
            no.setText(Integer.toString(mDmxInfoList.get(i).mNo));
            value.setText(mDmxInfoList.get(i).mSVaue);

            if(mDmxInfoList.get(i).mUseFlag)
                view.setBackgroundColor(getResources().getColor(R.color.dmx_used_item_color));
            else
                view.setBackgroundColor(getResources().getColor(R.color.dmx_not_used_item_color));

            view.setTag(R.id.text_no, mDmxInfoList.get(i).mNo);
            view.setTag(R.id.text_value, mDmxInfoList.get(i).mSVaue);
            view.setOnClickListener(this);
            return view;
        }

        public void setDmxList(ArrayList<DmxInfo> _dmxInitValue) {
            mDmxInfoList = _dmxInitValue;
        }

        @Override
        public void onClick(View view) {

            int indexNo = (int)view.getTag(R.id.text_no);
            String sDmxValue = (String) view.getTag(R.id.text_value);

            Log.e("no : ",Integer.toString(indexNo));
            Log.e("val : ",sDmxValue);

            DialogSetDmxValue setDmxValue = new DialogSetDmxValue();
            setDmxValue.show(getActivity().getFragmentManager(), "setdmxValue");
        }
    }

    public class DmxInfo {
        public int mNo = 0;
        public String mSVaue = "0xFF";
        public int mIntValue = 0xff;
        public boolean mUseFlag = false;
    }
}
