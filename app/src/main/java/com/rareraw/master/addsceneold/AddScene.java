package com.rareraw.master.addsceneold;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.neoromax.feelmaster.R;


public class AddScene extends AppCompatActivity {
    public final static String PAGE_TITLE = "Settings";
    public final static int PAGE_VALUE = 110;

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addscene_old);
        mContext = getBaseContext();
        mTabLayout = (TabLayout) findViewById(R.id.add_scene_tablayout);
        mTabLayout.addTab(mTabLayout.newTab().setText("Wireless"));
        mTabLayout.addTab(mTabLayout.newTab().setText("DMX 512"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        // Initializing ViewPager
        mViewPager = (ViewPager) findViewById(R.id.add_scene_viewpager);

        // Creating TabPagerAdapter adapter
        TabPagerAdapter pagerAdapter = new TabPagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(pagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        // Set TabSelectedListener
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}