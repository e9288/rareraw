package com.rareraw.master.addsceneold;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by lch on 2016-10-11.
 */

public class TabPagerAdapter extends FragmentStatePagerAdapter {

    // Count number of tabs
    private int mTabCount;
    private static SceneWireless mFragmentWireless = new SceneWireless().newInstance();
    private static SceneDmx512 mFragmentDmx512 = new SceneDmx512().newInstance();

    public TabPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mTabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mFragmentWireless;
            case 1:
                return mFragmentDmx512;
            default:
                return mFragmentWireless;
        }
    }

    @Override
    public int getCount() {
        return mTabCount;
    }
}