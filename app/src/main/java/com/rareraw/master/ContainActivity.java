package com.rareraw.master;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.Toast;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSceneEditListener;
import com.rareraw.master.Common.DialogNotification;
import com.neoromax.feelmaster.R;
import com.rareraw.master.Dimmer.DimmerFragment;
import com.rareraw.master.Switch.SwitchManagerFragment;
import com.rareraw.master.Switch.SwitchSettingFragment;
import com.rareraw.master.addscene.AddSceneFragment;
import com.rareraw.master.addsceneplay.AddScenePlayFragment;
import com.rareraw.master.addsceneschedule.AddSceneScheduleFragment;
import com.rareraw.master.alarm.AddAlarmFragment;
import com.rareraw.master.alarm.AlarmFragment;
import com.rareraw.master.dimmersetting.DimmerManagerFragment;
import com.rareraw.master.main.MainUserFragment;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.channel.ChannelManagerFragment;
import com.rareraw.master.Common.OnFragmentCallbackListener;
import com.rareraw.master.device.DevManagerFragment;
import com.rareraw.master.help.HelpFragment;
import com.rareraw.master.mode.ModeChangeFragment;
import com.rareraw.master.Settings.SettingFragment;
import com.rareraw.master.main.MainFragment;
import com.rareraw.master.sensor.SensorFragment;

public class ContainActivity extends AppCompatActivity implements OnFragmentCallbackListener, OnSceneEditListener {

    public static String MAIN_USER = "main_user", MAIN = "main", ALARM = "alarm", ADD_ALARM = "add_alarm", DEVICE_MANAGER = "device_manager",
            HELP = "help", SETTING = "setting", CHANNEL_MANAGER = "channel_manager", SWITCH_MANAGER = "switch_manager", SWITCH_SETTING = "switch_setting", MODE_CHANGE = "mode_change",
            ADD_SCENE_FRAGMENT = "addscene", ADD_SCENE_FRAGMENT2 = "addscene2", ADD_SCENE_PLAY_FRAGMENT = "ADDSCENE_PLAY_FRAGMENT", ADD_SCENE_PLAY_FRAGMENT2 = "ADDSCENE_PLAY_FRAGMENT2", ADD_SCENE_PLAY_FRAGMENT3 = "ADDSCENE_PLAY_FRAGMENT3",
            ADD_SCHEDULE_SCENE_FRAGMENT = "ADD_SCHEDULE_SCENE", ADD_SCHEDULE_SCENE_FRAGMENT2 = "ADD_SCHEDULE_SCENE2", ADD_SCHEDULE_SCENE_FRAGMENT3 = "ADD_SCHEDULE_SCENE3", VISIBLE_DELETE_BTN = "VISIBLE_DELETE_BTN",
            SENSOR_FRAGMENT = "SENSOR_FRAGMENT", DIMMER_FRAGMENT = "DIMMER_FRAGMENT", DIMMER_SETTING_FRAGMENT = "DIMMER_SETTING_FRAGMENT";

    public static String SCENENO = "SCENENO";
    private DimmerFragment mDimmerFragment;
    private MainFragment mMainFragment;
    private SensorFragment mSensorFragment;
    private MainUserFragment mMainUserFragment;
    private AlarmFragment mAlarmFragment;
    private DimmerManagerFragment mDimmerSettingFragment;
    private DevManagerFragment mDevManagerFragment;
    private AddAlarmFragment mAddAlarmFragment;
    private HelpFragment mHelpFragment;
    private SettingFragment mSettingFragment;
    private ChannelManagerFragment mChManagerFragment;
    private SwitchManagerFragment mSwitchManagerFragment;
    private ModeChangeFragment mModeChangeFragment;
    private SwitchSettingFragment mSwitchSettingFragment;
    private FrameLayout mShadowLayout;
    private Fragment mCurretFragment;
    private AddSceneFragment mAddSceneFragment;
    private AddScenePlayFragment mAddScenePlayFragment;

    private AddSceneScheduleFragment mAddScheduleSceneFragment;


    private CustomProgressDialog customProgressDialog;

    private TextView mImgEdit;
    private TextView mImgDelete;
    private ImageView mImgMenu;
    private int CURRENT_MODE = 0;
    private PopupWindowHolder mPopupWindowHolder;
    private final int WAITING = 0, EDIT = 1, SAVE = 2, BACK = 3;
    private final int FAIL = -1;
    public static final int REQUEST_DELETE_CONFIRM = 11;
    private final int ADD_SCENE = 0, ADD_SCENE_PLAY = 1, ADD_SCHEDULE_SCENE = 2, LOGOUT_OK = 3;
    final ViewGroup nullParent = null;  //warning을 제거하기 위해서 선언했음,, popupwindow를 만드는데 사용됨.
    private int mCurrentFragment = 0;
    private Toast mToast;
    private long mBackPressedTime = 0;
    private long FINSH_INTERVAL_TIME = 3000, RETURN_TIME = 600;
    private OnActionListener mDeleteBtnVisibleLisnter = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {

            // 20190626 에디트모드에서 딜리트플래그가 생기면 DELETE 버튼 생기는거 제거
            // -> 딜리트는 각자 하는걸로 변경 함
                /*if(nValue == -1)
                    findViewById(R.id.activity_contain_image_delete).setVisibility(View.INVISIBLE);
                else
                    findViewById(R.id.activity_contain_image_delete).setVisibility(View.VISIBLE);*/

        }
    };

    private OnActionListener mRemoveSettingFragmentListner = new OnActionListener() {

        @Override
        public void onActionListener(int nValue, String sValue) {
            onBackPressed();
        }
    };


    @Override
    public void onFragmentCallback(String requestCode, int response) {
        if (requestCode.equals(DIMMER_FRAGMENT)) {
            // called mainfragment
            updateFloatingbuttonUi();
            setCurrentMode(WAITING);
            mCurretFragment = mDimmerFragment;
      /*      if (response == -1) {
                mCurretFragment = mDimmerFragment;
                transFragment(mDimmerFragment, DIMMER_FRAGMENT);
                String text = getString(R.string.msg_change_user_mode);
                android.widget.Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
            }*/
        } else if (requestCode.equals(MAIN)) {
            // called mainfragment
            //updateFloatingbuttonUi();
            setCurrentMode(BACK);
            mCurretFragment = mMainFragment;

            if (response == 10) { // add mood
                mAddSceneFragment = null;
                mAddSceneFragment = new AddSceneFragment();
                mAddSceneFragment.setModiftStatus(-1);
                transFragment(mAddSceneFragment, ADD_SCENE_FRAGMENT);
            } else if (response == 11) { // add mood play
                mAddScenePlayFragment = null;
                mAddScenePlayFragment = new AddScenePlayFragment();
                mAddScenePlayFragment.setModiftStatus(-1);
                transFragment(mAddScenePlayFragment, ADD_SCENE_PLAY_FRAGMENT);
            } else if (response == 12) { // add mood schedul
                mAddScheduleSceneFragment = null;
                mAddScheduleSceneFragment = new AddSceneScheduleFragment();
                mAddScheduleSceneFragment.setModiftStatus(-1);
                transFragment(mAddScheduleSceneFragment, ADD_SCHEDULE_SCENE_FRAGMENT);
            }

            // transFragment(mAddAlarmFragment, ADD_ALARM);
         /*   if (response == -1) {
                mCurretFragment = mMainUserFragment;
                transFragment(mMainUserFragment, MAIN_USER);
                String text = getString(R.string.msg_change_user_mode);
                android.widget.Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
            }*/
        } /*else if (requestCode.equals(MAIN_USER)) {
            updateFloatingbuttonUi();
            setCurrentMode(WAITING);
            mCurretFragment = mMainUserFragment;
            if (response == -1) {
                mCurretFragment = mMainFragment;
                transFragment(mMainFragment, MAIN);
                String text = getString(R.string.msg_change_admin_mode);
                android.widget.Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
            }
        }*/ else if (requestCode.equals(ALARM)) {
            mAddAlarmFragment = null;
            mAddAlarmFragment = new AddAlarmFragment();

            if (response == -1)
                mAddAlarmFragment.setModiftStatus(-1);
            else
                mAddAlarmFragment.setModiftStatus(response); // response is alarm key
            transFragment(mAddAlarmFragment, ADD_ALARM);
        } else if (requestCode.equals(ADD_ALARM)) {
            if (Config.DEBUG) {
                if (response != FAIL)
                    Toast.makeText(this, "알람이 추가되었습니다.", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "알람 추가에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
            transFragment(mAlarmFragment, ALARM);
        } else if (requestCode.equals(MODE_CHANGE)) {
            transFragment(mMainFragment, MAIN);
        } else if (requestCode.equals(ADD_SCENE_FRAGMENT) || requestCode.equals(ADD_SCHEDULE_SCENE_FRAGMENT) || requestCode.equals(ADD_SCENE_PLAY_FRAGMENT)) {
            transFragment(mMainFragment, MAIN);
            mMainFragment.refreshUi();
            setCurrentMode(WAITING);
        } else if (requestCode.equals(SWITCH_MANAGER)) { // added lch 09190609
            mSwitchSettingFragment = null;
            mSwitchSettingFragment = new SwitchSettingFragment();
            mSwitchSettingFragment.setNowSwitchIndex(response);
            transFragment(mSwitchSettingFragment, SWITCH_SETTING);
        }
    }

    @Override
    public void onSceneEditListener(int _sceneType, int _sceneNo, int nValue3) {
        //updateFloatingbuttonUi();
        if (_sceneType == Cmd.NORMAL_SCENE) {
            mAddSceneFragment.setModiftStatus(_sceneNo);
            transFragment(mAddSceneFragment, ADD_SCENE_FRAGMENT);
        } else if (_sceneType == Cmd.PLAY_SCENE) {
            mAddScenePlayFragment.setModiftStatus(_sceneNo);
            transFragment(mAddScenePlayFragment, ADD_SCENE_PLAY_FRAGMENT);
        } else if (_sceneType == Cmd.SCHEDULE_SCENE) {
            mAddScheduleSceneFragment.setModiftStatus(_sceneNo);
            transFragment(mAddScheduleSceneFragment, ADD_SCHEDULE_SCENE_FRAGMENT);
        }
    }

    private void setCurrentMode(int mode) {
        CURRENT_MODE = mode;
        updateTitlebarUi();
    }

    private int getCurrentMode() {
        return CURRENT_MODE;
    }

    private void updateTitlebarUi() {

        findViewById(R.id.activity_contain_bottom_menu).setVisibility(View.GONE);
        if (CURRENT_MODE == WAITING) {
            // mImgEdit.setText(getString(R.string.edit));

            // 01049147330 menu

            //mImgMenu.setText(getString(R.string.menu));

            //mImgMenu.setImageResource(APService.getUserLevel() ? R.mipmap.pop_logout : R.mipmap.pop_login);
            // findViewById(R.id.activity_contain_image_delete).setVisibility(View.GONE);
            //findViewById(R.id.activity_contain_image_edit).setVisibility(View.VISIBLE);
            //if (APService.getUserLevel())
            //   findViewById(R.id.activity_contain_bottom_menu).setVisibility(View.VISIBLE);
            mImgMenu.setImageResource(APService.getUserLevel() ? R.mipmap.pop_login : R.mipmap.pop_logout);
            resetHolder();
        } else if (CURRENT_MODE == SAVE) {
            //mImgEdit.setText(getString(R.string.save));
            // 01049147330 menu
            // mImgMenu.setText(getString(R.string.back));
            mImgMenu.setImageResource(android.R.color.transparent);
            //findViewById(R.id.activity_contain_image_edit).setVisibility(View.VISIBLE);
            // findViewById(R.id.activity_contain_image_delete).setVisibility(View.GONE);
        } else if (CURRENT_MODE == BACK) {
            //findViewById(R.id.activity_contain_image_delete).setVisibility(View.GONE);
            //findViewById(R.id.activity_contain_image_edit).setVisibility(View.GONE);
            // 01049147330 menu
            mImgMenu.setImageResource(R.mipmap.icon_sub_back);
            //mImgMenu.setText(getString(R.string.back));
        } else if (CURRENT_MODE == EDIT) {
            // 01049147330 menu
            // mImgMenu.setText(getString(R.string.menu));
            // 01049147330
       /*     if (mMainFragment.getEditMode()) {
                //findViewById(R.id.activity_contain_image_delete).setVisibility(View.VISIBLE);
                mImgEdit.setText(getString(R.string.complete));
            } else {
                findViewById(R.id.activity_contain_image_delete).setVisibility(View.GONE);
                mImgEdit.setText(getString(R.string.edit));
                if (APService.getUserLevel())
                    findViewById(R.id.activity_contain_bottom_menu).setVisibility(View.VISIBLE);
            }*/
        }
    }

    private View.OnClickListener onActionItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
            /*if (mFloatingActionMenu.isOpened())
                mFloatingActionMenu.close(true);*/

            switch (view.getId()) {
                case R.id.activity_contain_image_delete:

                    if (!mMainFragment.getSelectedSceneCount()) {
                        MyToast.getInstance(getApplicationContext()).showToast(getString(R.string.msg_non_select_scene), Toast.LENGTH_SHORT);
                        return;
                    }
                    Intent intent = null;
                    intent = new Intent(getApplicationContext(), DialogNotification.class);
                    intent.putExtra(DialogNotification.TITLE, getString(R.string.delete_scene_title_msg));
                    intent.putExtra(DialogNotification.CONTENTS, getString(R.string.delete_scene_content_msg));
                    intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.WARNING);
                    startActivityForResult(intent, REQUEST_DELETE_CONFIRM);
                    break;

                case R.id.activity_contain_image_sub_menu:
                    // edit mode exit
                    // 다른 메뉴로 이동 시에는 편집모드는 강제 종료
                    //if (!APService.getUserLevel()) {
                    // 01049147330
                    /*
                        if (mMainFragment.getEditMode()) {
                            mMainFragment.setEditMode(false);
                            setCurrentMode(WAITING);
                        }*/
                    /*}else{
                        // mMainFragment.setEditMode(false);
                        setCurrentMode(WAITING);

                    }*/

                    if (getCurrentMode() == BACK || getCurrentMode() == SAVE) {
                        //    setCurrentMode(WAITING);
                        if (mCurretFragment == mAddAlarmFragment) {
                            setCurrentMode(BACK);
                        }
                        onBackPressed();
                    } else {
                        if (mPopupWindowLayout == null)
                            setPopupWindow(view);
                        mPopupWindow.showAsDropDown(view);
                        updateCurrentUserInfo();
                        updateUserLevelMenuUi();
                    }
                    break;
                case R.id.activity_contain_image_edit:
                    if (checkSaveMode())
                        return;
                    checkEditMode();
                    break;
                case R.id.activity_contain_image_title:

                    if (mPopupWindowLayout == null)
                        setPopupWindow(view);
                    try {
                        resetHolder();
                        transFragment(mDimmerFragment, DIMMER_FRAGMENT);
                    } catch (Exception e) {
                        Log.e("error", "back home ");
                    }
                    break;
            }
        }
    };

    private void resetHolder() {
        if (mPopupWindowLayout != null) {
            PopupWindowHolder popupWindowHolder = (PopupWindowHolder) mPopupWindowLayout.getTag();

            popupWindowHolder.viewMood.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewChannel.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewSensor.setVisibility(View.INVISIBLE);

            popupWindowHolder.viewSwitch.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewDevice.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewAlarm.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewDimmer.setVisibility(View.INVISIBLE);

            popupWindowHolder.viewSettings.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewHelp.setVisibility(View.INVISIBLE);
        }
    }

    private boolean checkSaveMode() {
        if (mCurretFragment != null && getSupportFragmentManager().findFragmentByTag(ADD_SCENE_PLAY_FRAGMENT) == mCurretFragment) {
            mAddScenePlayFragment.saveScenePlayInfo();
            return true;
        } else if (mCurretFragment != null && getSupportFragmentManager().findFragmentByTag(ADD_SCENE_FRAGMENT) == mCurretFragment) {
            mAddSceneFragment.saveSceneInfo();
            return true;
        } else if (mCurretFragment != null && getSupportFragmentManager().findFragmentByTag(ADD_SCHEDULE_SCENE_FRAGMENT) == mCurretFragment) {
            mAddScheduleSceneFragment.saveSceneScheduleInfo();
            return true;
        } else if (mCurretFragment != null && getSupportFragmentManager().findFragmentByTag(ADD_ALARM) == mCurretFragment) {
            mAddAlarmFragment.registAlarm();
            return true;
        }
        return false;
    }

    private void checkEditMode() {
        // 01049147330
      /*  if (getSupportFragmentManager().findFragmentByTag(MAIN) == mCurretFragment) {
            boolean editMode = !mMainFragment.getEditMode();
            mMainFragment.setEditMode(editMode);
            setCurrentMode(EDIT);
        }*/
    }

    private PopupWindow mPopupWindow;
    private View mPopupWindowLayout;
    private View.OnClickListener onPopupWindowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            PopupWindowHolder popupWindowHolder = (PopupWindowHolder) mPopupWindowLayout.getTag();
            // popupWindowHolder.viewChannel.setVisibility(View.INVISIBLE);
            //popupWindowHolder.viewSensor.setVisibility(View.INVISIBLE);

            //popupWindowHolder.viewSwitch.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewDevice.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewAlarm.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewDimmer.setVisibility(View.INVISIBLE);
            //popupWindowHolder.viewSettings.setVisibility(View.INVISIBLE);
            popupWindowHolder.viewHelp.setVisibility(View.INVISIBLE);

            switch (view.getId()) {

                case R.id.layout_pop_up_window_login:
                    if (checkUserLevel())
                        transFragment(mModeChangeFragment, MODE_CHANGE);
                    else {

                    }
                    // 01049147330
                    //else
                    //    transFragment(mMainUserFragment, MAIN_USER);
                    break;

                case R.id.layout_pop_up_window_mood:
                    popupWindowHolder.viewMood.setVisibility(View.VISIBLE);
                    transFragment(mMainFragment, MAIN);
                    break;

                case R.id.layout_pop_up_window_channel:
                    popupWindowHolder.viewChannel.setVisibility(View.VISIBLE);
                    transFragment(mChManagerFragment, CHANNEL_MANAGER);
                    break;
                case R.id.layout_pop_up_window_sensor:
                    popupWindowHolder.viewSensor.setVisibility(View.VISIBLE);
                    transFragment(mSensorFragment, SENSOR_FRAGMENT);
                    break;

                case R.id.layout_pop_up_window_switch:
                    popupWindowHolder.viewSwitch.setVisibility(View.VISIBLE);
                    transFragment(mSwitchManagerFragment, SWITCH_MANAGER);
                    break;

                case R.id.layout_pop_up_window_device:
                    popupWindowHolder.viewDevice.setVisibility(View.VISIBLE);
                    transFragment(mDevManagerFragment, DEVICE_MANAGER);
                    break;
                case R.id.layout_pop_up_window_alarm:
                    popupWindowHolder.viewAlarm.setVisibility(View.VISIBLE);
                    transFragment(mAlarmFragment, ALARM);
                    break;
                case R.id.layout_pop_up_window_dimmer:
                    popupWindowHolder.viewDimmer.setVisibility(View.VISIBLE);
                    transFragment(mDimmerSettingFragment, DIMMER_SETTING_FRAGMENT);
                    break;

                case R.id.layout_pop_up_window_settings:
                    popupWindowHolder.viewSettings.setVisibility(View.VISIBLE);
                    mSettingFragment = null;
                    mSettingFragment = new SettingFragment();
                    mSettingFragment.setListener(mRemoveSettingFragmentListner);
                    transFragment(mSettingFragment, SETTING);
                    break;
                case R.id.layout_pop_up_window_help:
                    Uri uri = Uri.parse("http://www.feelux.com/2013/product/03.asp?now=3");
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(it);
                    /*
                    popupWindowHolder.viewHelp.setVisibility(View.VISIBLE);
                    transFragment(mHelpFragment, HELP);*/
                    break;
                case R.id.layout_pop_up_window_admin:
                    break;
            }
        }
    };

    private boolean checkUserLevel() {
        if (!APService.getUserLevel())
            return true; // normal mode
        else {
            //Intent intent = new Intent(ContainActivity.this, LogoutDialog.class);
            //startActivityForResult(intent, LOGOUT_OK);

            // 20170401 로그아웃 물어보지 않고 바로 하도록 변경
            APService.setUserLevel(false);

            // 20190703 수정
            // 01049147330
            // mMainFragment.resetList();
            // mMainUserFragment.refreshUi();
            MyToast.getInstance(getApplicationContext()).showToast(getString(R.string.msg_change_user_mode), Toast.LENGTH_SHORT);
            if (mPopupWindow != null)
                mPopupWindow.dismiss();
            updateFloatingbuttonUi();
            return false; // admin mode
        }
    }

    // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
    // private FloatingActionMenu mFloatingActionMenu;
    private View.OnClickListener onFloatingMenuClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
          /*  new Thread(new Runnable() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if(mFloatingActionMenu.isOpened())
                                    mFloatingActionMenu.close(true);
                            } catch (Exception e) {
                            }
                        }
                    });
                }
            }).start();*/

            Intent intent = null;
            switch (view.getId()) {
                // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴 id는 그대로 사용함
                case R.id.activity_contain_floating_scene:
                    mAddSceneFragment = null;
                    mAddSceneFragment = new AddSceneFragment();
                    mAddSceneFragment.setModiftStatus(-1);
                    transFragment(mAddSceneFragment, ADD_SCENE_FRAGMENT);
                    break;
                // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴 id는 그대로 사용함
                case R.id.activity_contain_floating_scene_play:
                    mAddScenePlayFragment = null;
                    mAddScenePlayFragment = new AddScenePlayFragment();
                    mAddScenePlayFragment.setModiftStatus(-1);
                    transFragment(mAddScenePlayFragment, ADD_SCENE_PLAY_FRAGMENT);
                    break;

                // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴 id는 그대로 사용함
                case R.id.activity_contain_floating_scene_schedule:
                    mAddScheduleSceneFragment = null;
                    mAddScheduleSceneFragment = new AddSceneScheduleFragment();
                    mAddScheduleSceneFragment.setModiftStatus(-1);
                    transFragment(mAddScheduleSceneFragment, ADD_SCHEDULE_SCENE_FRAGMENT);
                    break;
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_DELETE_CONFIRM:
                    mMainFragment.deleteScene();
                    break;
                case ADD_SCENE_PLAY:
                    break;
                case ADD_SCHEDULE_SCENE:
                    break;
                case LOGOUT_OK:
                    APService.setUserLevel(false);
                    mMainFragment.refreshUi();
                    MyToast.getInstance(getApplicationContext()).showToast(getString(R.string.msg_logout), Toast.LENGTH_SHORT);
                    if (mPopupWindow != null)
                        mPopupWindow.dismiss();
                    updateFloatingbuttonUi();
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contain);
        if (Config.DEBUG)
            APService.setUserLevel(true);
        setCustomToolbar();
        setFloatingMenu();
        makeFragments();

        customProgressDialog = new CustomProgressDialog(ContainActivity.this); // 위에서 테두리를 둥글게 했지만 다이얼로그 자체가 네모라 사각형 여백이 보입니다. 아래 코드로 다이얼로그 배경을 투명처리합니다.
        customProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    public void createDialog(int time) {
        CustomProgressDialog dialog = new CustomProgressDialog(ContainActivity.this, time); // 위에서 테두리를 둥글게 했지만 다이얼로그 자체가 네모라 사각형 여백이 보입니다. 아래 코드로 다이얼로그 배경을 투명처리합니다.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }
    public void createDialog() {
        CustomProgressDialog dialog = new CustomProgressDialog(ContainActivity.this); // 위에서 테두리를 둥글게 했지만 다이얼로그 자체가 네모라 사각형 여백이 보입니다. 아래 코드로 다이얼로그 배경을 투명처리합니다.
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void showDialog() {
        if (!customProgressDialog.isShowing())
            customProgressDialog.show(); // 보여주기
    }

    public void hideDialog() {
        if(customProgressDialog.isShowing())
            customProgressDialog.dismiss(); // 없애기
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    private void updateFloatingbuttonUi() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mImgMenu.setImageResource(APService.getUserLevel() ? R.mipmap.pop_login : R.mipmap.pop_logout);

                            if (APService.getUserLevel()) {
                                // 플로팅 버튼 삭제하고 아래 메뉴로 고정시킴
                                //mFloatingActionMenu.setVisibility(View.VISIBLE);
                                // findViewById(R.id.activity_contain_image_edit).setVisibility(View.VISIBLE);

                            } else {
                                // 플로팅 버튼 삭제하고 아래 메뉴로 고정시킴
                                // mFloatingActionMenu.setVisibility(View.GONE);
                                // findViewById(R.id.activity_contain_image_edit).setVisibility(View.GONE);
                            }
                            if (Config.DEBUG) {
                                // 플로팅 버튼 삭제하고 아래 메뉴로 고정시킴
                                // mFloatingActionMenu.setVisibility(View.VISIBLE);
                                //  findViewById(R.id.activity_contain_image_edit).setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                        }
                    }
                });
            }
        }).start();
    }

    private void updateUserLevelMenuUi() {
        if (APService.getUserLevel()) {

            //mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_settings).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_mood).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_alarm).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_dimmer).setVisibility(View.VISIBLE);



            ((TextView) mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login_text)).setText("로그아웃");
            ((ImageView) mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login_image)).setImageResource(R.mipmap.pop_logout);

            //mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_switch).setVisibility(View.VISIBLE);
            //mImgEdit.setVisibility(View.VISIBLE);
            // findViewById(R.id.activity_contain_image_edit).setVisibility(View.VISIBLE);
        } else {
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device).setVisibility(View.GONE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login).setVisibility(View.VISIBLE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_mood).setVisibility(View.GONE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_alarm).setVisibility(View.GONE);
            mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_dimmer).setVisibility(View.GONE);
            ((TextView) mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login_text)).setText("로그인");
            ((ImageView) mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login_image)).setImageResource(R.mipmap.pop_login);
            //mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_settings).setVisibility(View.GONE);
            // mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device).setVisibility(View.GONE);
            // mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_switch).setVisibility(View.GONE);
            // mImgEdit.setVisibility(View.GONE);
            // findViewById(R.id.activity_contain_image_edit).setVisibility(View.GONE);
        }

        if (Config.DEBUG) {
            //mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_settings).setVisibility(View.VISIBLE);
            //mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device).setVisibility(View.VISIBLE);
        }
    }


    private void makeFragments() {
        mMainFragment = new MainFragment();
        mMainUserFragment = new MainUserFragment();

        mMainFragment.setListener(mDeleteBtnVisibleLisnter);
        mAlarmFragment = new AlarmFragment();
        mDimmerSettingFragment= new DimmerManagerFragment();
        mAddAlarmFragment = new AddAlarmFragment();
        mDevManagerFragment = new DevManagerFragment();
        mHelpFragment = new HelpFragment();
        mSettingFragment = new SettingFragment();
        mChManagerFragment = new ChannelManagerFragment();
        mSensorFragment = new SensorFragment();
        mSensorFragment = new SensorFragment();
        mSwitchManagerFragment = new SwitchManagerFragment();
        mModeChangeFragment = new ModeChangeFragment();
        mAddSceneFragment = new AddSceneFragment();
        mAddScenePlayFragment = new AddScenePlayFragment();
        mAddScheduleSceneFragment = new AddSceneScheduleFragment();
        mSwitchSettingFragment = new SwitchSettingFragment();
        mDimmerFragment = new DimmerFragment();
        //if (APService.getUserLevel()) {
        setDefaultFragment(mDimmerFragment, DIMMER_FRAGMENT);
        // } else {
        //    setDefaultFragment(mMainUserFragment, MAIN_USER);
        // }
    }

    private void setDefaultFragment(Fragment fragment, String tag) {
        mCurretFragment = fragment;
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.activity_contain_layout_fragment_container, fragment, tag);
        transaction.commit();
    }

    private void setFloatingMenu() {
        // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
        //mFloatingActionMenu = (FloatingActionMenu) findViewById(R.id.activity_contain_floating_menu);
        // findViewById(R.id.activity_contain_floating_scene).setOnClickListener(onFloatingMenuClickListener);
        //findViewById(R.id.activity_contain_floating_scene_play).setOnClickListener(onFloatingMenuClickListener);
        //findViewById(R.id.activity_contain_floating_scene_schedule).setOnClickListener(onFloatingMenuClickListener);

        // 20190623 플로팅 버튼없앳으나, 플로팅 버튼 id를 그대로 사용해서 리스너 생성함
        findViewById(R.id.activity_contain_floating_scene).setOnClickListener(onFloatingMenuClickListener);
        findViewById(R.id.activity_contain_floating_scene_play).setOnClickListener(onFloatingMenuClickListener);
        findViewById(R.id.activity_contain_floating_scene_schedule).setOnClickListener(onFloatingMenuClickListener);

        //플로팅 버튼이 Open 상태일때 뒷배경의 클릭을 막고 close 메소드를 실행합니다.20190623 삭제함.
        /*findViewById(R.id.activity_contain_floating_below).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    mFloatingActionMenu.close(true);
                    return true;
                }
                return mFloatingActionMenu.isOpened();
            }
        });*/
    }

    private void setCustomToolbar() {
        findViewById(R.id.activity_contain_image_sub_menu).setOnClickListener(onActionItemClickListener);
        findViewById(R.id.activity_contain_image_title).setOnClickListener(onActionItemClickListener);
        // findViewById(R.id.activity_contain_image_delete).setOnClickListener(onActionItemClickListener);
        //findViewById(R.id.activity_contain_image_edit).setOnClickListener(onActionItemClickListener);
        mImgEdit = (TextView) findViewById(R.id.btn_contain_image_edit);
        mImgMenu = (ImageView) findViewById(R.id.btn_contain_image_menu);

        mImgMenu.setImageResource(APService.getUserLevel() ? R.mipmap.pop_login : R.mipmap.pop_logout);
    }

    private void setPopupWindow(View view) {
        mShadowLayout = (FrameLayout) findViewById(R.id.layout_shadow);
        // mShadowLayout.setVisibility(View.VISIBLE);
        mPopupWindowLayout = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_pop_up_window, nullParent);
        mPopupWindowHolder = new PopupWindowHolder();

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_mood).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewMood = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_mood_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_channel).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewChannel = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_channel_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_sensor).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewSensor = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_sensor_layout);


        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_switch).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewSwitch = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_switch_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewDevice = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_device_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_alarm).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewAlarm = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_alarm_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_dimmer).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewDimmer = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_dimmer_layout);

        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_settings).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewSettings = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_settings_layout);
        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_help).setOnClickListener(onPopupWindowClickListener);
        mPopupWindowHolder.viewHelp = mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_help_layout);
        mPopupWindowLayout.setTag(mPopupWindowHolder);
        mPopupWindowLayout.findViewById(R.id.layout_pop_up_window_login).setOnClickListener(onPopupWindowClickListener);

        mPopupWindow = new PopupWindow(view);
        mPopupWindow.setContentView(mPopupWindowLayout);
        mPopupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setOutsideTouchable(true);
    }

    private void transFragment(Fragment fragment, String tag) {

        // if (fragment != mMainFragment && fragment != mMainUserFragment) {
        if (fragment != mDimmerFragment) {
            // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
            // mFloatingActionMenu.setVisibility(View.GONE);
            // mFloatingActionMenu.close(true);
           /* if (fragment == mAddSceneFragment || fragment == mAddScenePlayFragment || fragment == mAddScheduleSceneFragment || fragment == mAddAlarmFragment)
                setCurrentMode(SAVE);
            else*/
            setCurrentMode(BACK);
        } else
            setCurrentMode(WAITING);

        // 다른 메뉴로 이동 시에는 편집모드는 강제 종료
        // mMainFragment.setEditMode(false);
        if (mPopupWindow != null)
            mPopupWindow.dismiss();

        mCurretFragment = fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.activity_contain_layout_fragment_container, fragment, tag);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    // 20190623 플로팅 버튼 없애고 하단에 메뉴 고정시킴
   /* public void closeFloatingMenu(){
        if (mFloatingActionMenu.isOpened()) {
            mFloatingActionMenu.close(true);
        }
    }*/
    @Override
    public void onBackPressed() {
        try {
            // if (getSupportFragmentManager().getBackStackEntryCount() == 0 || mCurretFragment == mMainUserFragment || mCurretFragment == mMainFragment) {
            if (getSupportFragmentManager().getBackStackEntryCount() == 0 || mCurretFragment == mDimmerFragment) {
                long tempTime = System.currentTimeMillis();
                long intervalTime = tempTime - mBackPressedTime;
                if (0 <= intervalTime && RETURN_TIME >= intervalTime) {
                    return;
                }
                if (0 <= intervalTime && FINSH_INTERVAL_TIME >= intervalTime) {
                    // super.onBackPressed();
                    finish();
                } else {
                    mBackPressedTime = tempTime;
                    MyToast.getInstance(getApplicationContext()).showToast(getString(R.string.ask_exit_app), Toast.LENGTH_SHORT);
                }
            } else if (mCurretFragment == mAddScheduleSceneFragment) {
                if (mAddScheduleSceneFragment.NOW_PAGE == mAddScheduleSceneFragment.STEP1) {
                    super.onBackPressed();
                } else if (mAddScheduleSceneFragment.NOW_PAGE == mAddScheduleSceneFragment.STEP3 && mAddScheduleSceneFragment.mSaveComplete) {
                    super.onBackPressed();
                } else if (mAddScheduleSceneFragment.NOW_PAGE == mAddScheduleSceneFragment.STEP3 && mAddScheduleSceneFragment.MODIFY_FLAG) {
                    super.onBackPressed();
                }  else if (mAddScheduleSceneFragment.NOW_PAGE == mAddScheduleSceneFragment.STEP2 && mAddScheduleSceneFragment.MODIFY_FLAG) {
                    super.onBackPressed();
                } else
                    mAddScheduleSceneFragment.forcePrevPage();
                return;
            } else if (mCurretFragment == mAddScenePlayFragment) {
                if (mAddScenePlayFragment.NOW_PAGE == mAddScenePlayFragment.STEP1) {
                    super.onBackPressed();
                } else if (mAddScenePlayFragment.NOW_PAGE == mAddScenePlayFragment.STEP3 && mAddScenePlayFragment.mSaveComplete) {
                    super.onBackPressed();
                } else
                    mAddScenePlayFragment.forcePrevPage();

                return;
            } else
                super.onBackPressed();
        } catch (Exception e) {

        }
    }


    private void updateCurrentUserInfo() {
        if (APService.getUserLevel()) {
            ((ImageView) mPopupWindowLayout.findViewById(R.id.img_current_user_icon)).setImageResource(R.mipmap.pop_admin);
            ((TextView) mPopupWindowLayout.findViewById(R.id.text_current_userinfo)).setText(getString(R.string.admin));
        } else {
            ((ImageView) mPopupWindowLayout.findViewById(R.id.img_current_user_icon)).setImageResource(R.mipmap.pop_admin);
            ((TextView) mPopupWindowLayout.findViewById(R.id.text_current_userinfo)).setText(getString(R.string.normal));
        }
    }


    private static class PopupWindowHolder {
        View viewMood;
        View viewChannel;
        View viewSensor;
        View viewSwitch;
        View viewDevice;
        View viewAlarm;
        View viewDimmer;
        View viewSettings;
        View viewHelp;
        ImageView userIcon;
        TextView userLevel;
    }
}
