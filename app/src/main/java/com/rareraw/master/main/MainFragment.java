package com.rareraw.master.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSelecetedSceneInfo;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.data.SimpleSceneInfo;
import com.rareraw.master.data.TestDataManager;
import com.rareraw.master.network.NetworkResultLisnter;
import com.woxthebox.draglistview.DragItem;
import com.woxthebox.draglistview.DragItemAdapter;
import com.woxthebox.draglistview.DragListView;

/**
 * Created by Lch on 2016-12-14.
 */

public class MainFragment extends BaseFragment implements View.OnClickListener {
    private ArrayList<Pair<Long, SimpleSceneInfo>> mItemArray = new ArrayList<Pair<Long, SimpleSceneInfo>>();
    private DragListView mDragListView;
    private MainListAdapter mListAdapter;
    // 20190626 최상단 레이아웃 추가됨
    private SeekBar mMainMasterSeeker;
    private TextView mSelectedSceneName;
    private ImageView mBtnDelete;
    private LinearLayout mBtnDefaultScene0percent;
    private LinearLayout mBtnDefaultScene50percent;
    private LinearLayout mBtnDefaultScene100percent;
    private CheckBox mSwitchNormalScene, mSwitchPlayScene, mSwitchSchedScene;
    private OnActionListener mDeleteBtnVisibleLisnter;
    private ArrayList<SimpleSceneInfo> simpleSceneInfos;
    private ImageView mImgDefault100, mImgDefault50, mImgDefault0;
    private TextView mTextDefault100, mTextDefault50, mTextDefault0;
    private RelativeLayout mAddMood, mAddMoodPlay, mAddMoodSchedul;
    private OnSelecetedSceneInfo onSelecetedSceneInfo = new OnSelecetedSceneInfo() {

        @Override
        public void onSelectedSceneInfo(SimpleSceneInfo _info, int _sceneType) {
            String sceneName = "";
            if (_info != null) {
                sceneName = _info.getSceneName();
            }

            switch (_sceneType) {
                case Cmd.NORMAL_SCENE:
                    mSwitchNormalScene.setVisibility(View.VISIBLE);
                    mSwitchPlayScene.setVisibility(View.GONE);
                    mSwitchSchedScene.setVisibility(View.GONE);
                    if (!_info.getCurrentSceneIsPlay()) {
                        mSwitchNormalScene.setChecked(false);
                        mMainMasterSeeker.setEnabled(false);
                        mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                        mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    } else {
                        mSwitchNormalScene.setChecked(true);
                        mMainMasterSeeker.setEnabled(true);
                        mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb));
                        mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master));
                    }

                    // resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), _info.getCurrentSceneIsPlay());
                    mMainMasterSeeker.setTag(R.id.fragment_main_master_seeker, _info);
                    mSwitchNormalScene.setTag(R.id.fragment_switch_normal, _info);
                    mMainMasterSeeker.setProgress(_info.getDimingLevel());

                    break;

                case Cmd.PLAY_SCENE:
                    mSwitchNormalScene.setVisibility(View.GONE);
                    mSwitchPlayScene.setVisibility(View.VISIBLE);
                    mSwitchSchedScene.setVisibility(View.GONE);
                    mMainMasterSeeker.setEnabled(false);
                    mMainMasterSeeker.setProgress(0);
                    mSwitchPlayScene.setTag(R.id.fragment_switch_play, _info);
                    mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                    mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    //  mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    //resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), false);
                    break;

                case Cmd.SCHEDULE_SCENE:
                    mSwitchNormalScene.setVisibility(View.GONE);
                    mSwitchPlayScene.setVisibility(View.GONE);
                    mSwitchSchedScene.setVisibility(View.VISIBLE);
                    mMainMasterSeeker.setEnabled(false);
                    mMainMasterSeeker.setProgress(0);
                    mSwitchSchedScene.setTag(R.id.fragment_switch_schedul, _info);
                    mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                    mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    //   mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    // resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), false);
                    break;
            }

            if (sceneName.contains("\n")) {
                sceneName = sceneName.replace("\n", "");
            }
            mSelectedSceneName.setText(sceneName);
        }
    };


    private View.OnClickListener onSceneOnOffListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_normal);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬 on of off 실패");
                return;
            }

            final int progressLevel = mMainMasterSeeker.getProgress();
            if (simpleSceneInfo.getCurrentSceneIsPlay()) { // on 상태
                mMainMasterSeeker.setEnabled(false);
                mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), 0, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
            } else { // off 상태
                mMainMasterSeeker.setEnabled(true);
                mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb));
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
                APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), progressLevel, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onScenePlayStopStartListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_play);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬플레이 on of off 실패");
                return;
            }
            if (!simpleSceneInfo.getCurrentSceneIsPlay()) {
                // 20190623 씬플레이 선택시 이미지 제거
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_play);
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
            } else {
                // 20190623 씬플레이 선택시 이미지 제거
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_stop);
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onSceneScheduleStopStartListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_play);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬스케줄 on of off 실패");
                return;
            }

            if (simpleSceneInfo.getCurrentSceneIsPlay()) {
                // imageViewSceneScheduleOnOff = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_schedule_image_on_off);
                // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_off);
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
            } else {
                // 20190623 스케줄씬 선택시 플레이 이미지 제거
                // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_on);
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setSceneList();
        if (Config.DEBUG)
            APService.setUserLevel(true);

        // 4 step, set adapter
        mListAdapter = new MainListAdapter(mItemArray, R.layout.item_list_fragment_main, R.id.item_list_fragment_main_title_image_move, false, onSceneEditListener, onSelecetedSceneInfo, getActivity(), mDeleteBtnVisibleLisnter);

    }

    private void setSceneList() {

        if (Config.DEBUG) {
            TestDataManager testDataManager = new TestDataManager();
            simpleSceneInfos = testDataManager.getSimpleSceneInfos();

            // 3 step, casting simpleSceneinfo to listArray ( for dragList class)
            mItemArray = new ArrayList<>();
            for (int i = 0; i < simpleSceneInfos.size(); i++) {
                mItemArray.add(new Pair<>(Long.valueOf(i), simpleSceneInfos.get(i)));
            }
            ArrayList<Pair<Long, SimpleSceneInfo>> initItem = new ArrayList<Pair<Long, SimpleSceneInfo>>();
            mListAdapter.resetItemList(initItem);
            mListAdapter.setItemList(mItemArray);
        }else{
            // 1 step, get scene list in ap
            APService.getSceneList(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    getSimpleSceneInfos();

                }

                @Override
                public void onError(int errorCode) {
                    hideDialog();
                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    LogoutMain();
                    hideDialog();
                }
            });
        }
    }

    public void resetList() {
        ArrayList<Pair<Long, SimpleSceneInfo>> initItem = new ArrayList<Pair<Long, SimpleSceneInfo>>();
        mListAdapter.resetItemList(initItem);
        mListAdapter.notifyDataSetChanged();
    }

    public void refreshUi() {
        setSceneList();
    }

    private void getSimpleSceneInfos() {
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

                ArrayList<SimpleSceneInfo> infos = new ArrayList<>();
                CmdHelper.sortByIndexSceneList();
                try {
                    for (SceneInfo sceneinfo : APService.getApSceneList())
                        if (sceneinfo.getIntSceneNo() >= Cmd.MIN_SCENE_NO)
                            infos.add(new SimpleSceneInfo(Config.NO_SELECT, sceneinfo.getIntSceneType(), sceneinfo.getIntIconType(), sceneinfo.getSceneName().trim(), (sceneinfo.getHideFlag()) ? 1 : 0, sceneinfo.getIntSceneNo(), sceneinfo.getIntIndex()));
                    //else if (sceneinfo.getIntSceneNo() == Cmd.DEFAULT_SCENE_100) {
                    //    infos.add(new SimpleSceneInfo(Config.NO_SELECT, sceneinfo.getIntSceneType(), sceneinfo.getIntIconType(), getString(R.string.scene_all), (sceneinfo.getHideFlag()) ? 1 : 0, sceneinfo.getIntSceneNo(), sceneinfo.getIntIndex()));
                    // }
                } catch (Exception e) {
                    Log.e("error", "scenelist casting");
                }

                simpleSceneInfos = infos;
                // 3 step, casting simpleSceneinfo to listArray ( for dragList class)
                mItemArray = new ArrayList<>();
                for (int i = 0; i < simpleSceneInfos.size(); i++) {
                    mItemArray.add(new Pair<>(Long.valueOf(i), simpleSceneInfos.get(i)));
                }

                ArrayList<Pair<Long, SimpleSceneInfo>> initItem = new ArrayList<Pair<Long, SimpleSceneInfo>>();
                mListAdapter.resetItemList(initItem);
                mListAdapter.setItemList(mItemArray);

                APService.getCurrentActionScene(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        int sceneNo = (int) retValue;
                        if (sceneNo != -1) {
                            if (sceneNo == Cmd.DEFAULT_SCENE_0 || sceneNo == Cmd.DEFAULT_SCENE_50 || sceneNo == Cmd.DEFAULT_SCENE_100)
                                updateDefaultScene(sceneNo);

                            mListAdapter.selectDoingScene(sceneNo);

                        }
                        mListAdapter.notifyDataSetChanged();
                        hideDialog();
                    }

                    @Override
                    public void onError(int errorCode) {
                        hideDialog();
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        hideDialog();
                        LogoutMain();
                    }
                });
            }

            @Override
            public void onError(int errorCode) {
                hideDialog();
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                hideDialog();
                LogoutMain();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_main, container, false);
        mSwitchNormalScene = (CheckBox) parent.findViewById(R.id.fragment_switch_normal);
        mSwitchNormalScene.setOnClickListener(onSceneOnOffListener);

        mSwitchPlayScene = (CheckBox) parent.findViewById(R.id.fragment_switch_play);
        mSwitchPlayScene.setOnClickListener(onScenePlayStopStartListener);

        mSwitchSchedScene = (CheckBox) parent.findViewById(R.id.fragment_switch_schedul);
        mSwitchSchedScene.setOnClickListener(onSceneScheduleStopStartListener);

        mMainMasterSeeker = (SeekBar) parent.findViewById(R.id.fragment_main_master_seeker);
        mMainMasterSeeker.setEnabled(false);
        mMainMasterSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                try {
                    SimpleSceneInfo simpleSceneInfo = (SimpleSceneInfo) seekBar.getTag(R.id.fragment_main_master_seeker);
                    APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), seekBar.getProgress(), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {

                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("error", "diming");
                }
                // Toast.makeText(seekBar.getContext(), "선택한 아이템의 번호 : " + viewHolder.getAdapterPosition() + "\n최종 디밍 값 : " + seekBar.getProgress() + "", Toast.LENGTH_SHORT).show();
            }
        });
        mSelectedSceneName = (TextView) parent.findViewById(R.id.fragment_main_selected_scene_name);


        mAddMood = (RelativeLayout) parent.findViewById(R.id.fragment_main_add_mood);
        mAddMood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFragmentCallbackListener.onFragmentCallback(getTag(), 10);
            }
        });
        mAddMoodPlay = (RelativeLayout) parent.findViewById(R.id.fragment_main_add_play_mood);
        mAddMoodPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFragmentCallbackListener.onFragmentCallback(getTag(), 11);
            }
        });
        mAddMoodSchedul = (RelativeLayout) parent.findViewById(R.id.fragment_main_add_schdul_mood);
        mAddMoodSchedul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFragmentCallbackListener.onFragmentCallback(getTag(), 12);
            }
        });

        return parent;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mDragListView = (DragListView) view.findViewById(R.id.fragment_main_list_simple_scene);
        // mDragListView.getRecyclerView().setVerticalScrollBarEnabled(false);
        mDragListView.setDragListListener(new DragListView.DragListListenerAdapter() {
            @Override
            public void onItemDragStarted(int position) {
                int sNo = simpleSceneInfos.get(position).getSceneNo();

                //Toast.makeText(mDragListView.getContext(), "Start - position: " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onItemDragEnded(int fromPosition, int toPosition) {
                if (fromPosition != toPosition) {
                    int fromSceneNo = simpleSceneInfos.get(fromPosition).getSceneNo();
                    int endSceneNo = simpleSceneInfos.get(toPosition).getSceneNo();

                    int count = mItemArray.size();
                    ByteBuffer buffer = ByteBuffer.allocate((count * 4) + 2); // count 2byte + sceneno 2byte
                    buffer.put(CmdHelper.intToByteArray(count));
                    for (int i = 0, index = 4; i < mItemArray.size(); i++, index++) {
                        buffer.put(CmdHelper.intToByteArray(mItemArray.get(i).second.getSceneNo()));
                        buffer.put(CmdHelper.intToByteArray(index));
                    }

                    APService.setSortedSceneList(buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            // simpleSceneInfos.clear();
                            //  mItemArray.clear();
                            //  setSceneList();
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            LogoutMain();
                        }
                    });
                }
            }
        });

        setupListRecyclerView();
    }

    @Override
    public void onStart() {
        super.onStart();
        // if (APService.getUserLevel()) {
        showDialogLoop();
        refreshUi();
        onFragmentCallbackListener.onFragmentCallback(getTag(), 0);
    }
    public void showDialog() {
        ((ContainActivity) getActivity()).createDialog(700);
    }

    public void hideDialog() {
        ((ContainActivity) getActivity()).hideDialog();
    }

    public void showDialogLoop(){
        ((ContainActivity) getActivity()).createDialog();
    }

    private void setupListRecyclerView() {
        mDragListView.setLayoutManager(new LinearLayoutManager(getContext()));
        mDragListView.setAdapter(mListAdapter, true);
        mDragListView.setCanDragHorizontally(false);

        mDragListView.setCustomDragItem(new MyDragItem(getContext(), R.layout.item_list_fragment_main));
    }

    public void setEditMode(boolean value) {
        //mBtnDelete.setVisibility(value ? View.VISIBLE : View.GONE);
        mListAdapter.setEditMode(value);
        mListAdapter.notifyDataSetChanged();
    }

    public boolean getEditMode() {
        return mListAdapter.getEditMode();
    }

    public boolean getSelectedSceneCount() {
        return mListAdapter.getSelectedSceneCnt();
    }

    private ImageView mPrevImg;
    private TextView mPrevText;
    private LinearLayout mPrevLayout;


    private void updateDefaultScene(int _sceneType) {
    }

    public void deleteScene() {
        SimpleSceneInfo simpleSceneInfo;
        for (int i = 0; i < simpleSceneInfos.size(); i++) {
            simpleSceneInfo = simpleSceneInfos.get(i);
            if (simpleSceneInfo.getDeleteSelected()) {
                APService.deleteScene(simpleSceneInfo.getSceneNo(), new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Log.e("ok", "delete scene");
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_complete_delete_scene), com.rareraw.master.Common.Toast.LENGTH_SHORT);
                        refreshUi();
                        if (Config.DEBUG)
                            Toast.makeText(getActivity(), "씬이 삭제되었습니다.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(int errorCode) {

                        Log.e("error", "delete scene");
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Log.e("error", "delete scene");
                        LogoutMain();
                    }
                });
            }
        }
    }

    public void setListener(OnActionListener _listener) {
        mDeleteBtnVisibleLisnter = _listener;
    }

    private class MyDragItem extends DragItem {

        MyDragItem(Context context, int layoutId) {
            super(context, layoutId);
        }

        @Override
        public void onBindDragView(View clickedView, View dragView) {
            SimpleSceneInfo simpleSceneInfo = mItemArray.get(((DragItemAdapter.ViewHolder) clickedView.getTag()).getAdapterPosition()).second;

            dragView.findViewById(R.id.item_list_fragment_main_content_layout_parent).setAlpha(0.6f);
            //헤더
            // 20190623 선택표시 이미지 제거
            // ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_select)).setImageResource(R.mipmap.icon_select_off);

            switch (simpleSceneInfo.getSceneType()) {
                case Cmd.NORMAL_SCENE: //Scene
                    ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_type_icon)).setImageResource(R.mipmap.icon_scene_non);

                    // 20190623 씬 편집 아이콘 통일로 인해 주석
                    // ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_control)).setImageResource(R.mipmap.icon_item_title_control);

                    // 20190623 씬 타입 텍스트 삭제함
                    // ((TextView) dragView.findViewById(R.id.item_list_fragment_main_title_text_type_name)).setText(getString(R.string.scene));
                    break;
                case Cmd.PLAY_SCENE: //ScenePlay
                    ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_type_icon)).setImageResource(R.mipmap.icon_play_off);
                    // 20190623 씬 편집 아이콘 통일로 인해 주석
                    // ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_control)).setImageResource(R.mipmap.icon_item_title_control_play);
                    // 20190623 씬 타입 텍스트 삭제함
                    // ((TextView) dragView.findViewById(R.id.item_list_fragment_main_title_text_type_name)).setText(getString(R.string.scene_play));
                    break;
                case Cmd.SCHEDULE_SCENE: //SceneSchedule
                    ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_type_icon)).setImageResource(R.mipmap.icon_schedul_off);
                    // 20190623 씬 편집 아이콘 통일로 인해 주석
                    // ((ImageView) dragView.findViewById(R.id.item_list_fragment_main_title_image_control)).setImageResource(R.mipmap.icon_item_title_control_sche);
                    // 20190623 씬 타입 텍스트 삭제함
                    // ((TextView) dragView.findViewById(R.id.item_list_fragment_main_title_text_type_name)).setText(getString(R.string.scene_schedule));
                    break;
            }
            // 20190626 드래그 할시에 표시에 되는 씬 타입 텍스트 제거함
            //((TextView) dragView.findViewById(R.id.item_list_fragment_main_title_text_type_name)).setTextColor(0xFF8F8F90);
            dragView.findViewById(R.id.item_list_fragment_main_title_image_control).setVisibility(View.INVISIBLE);
            //            viewHolder.imageViewTitleMove.setVisibility(View.VISIBLE); -> 원래는 색상있는걸로 바꿔줘야하는데 안바꿔도 될듯? -> 사용자가 누르고 있기 때문에 안보임


            ((TextView) dragView.findViewById(R.id.item_list_fragment_main_content_text_title)).setText(simpleSceneInfo.getSceneName());
            ((TextView) dragView.findViewById(R.id.item_list_fragment_main_content_text_title)).setTextColor(0xFFFFFFFF);


        }
    }

    private int getImageResource(int iconIndex) {
        if (iconIndex == Config.USER_DEFAULT_ICON)
            return getResources().getIdentifier("icon_user_default_icon_press", "mipmap", getActivity().getPackageName());
        else if (iconIndex >= Config.CATEGORY1_MIN && iconIndex <= Config.CATEGORY1_MAX) // category 1 day
            return getResources().getIdentifier("icon_user_icon_press_day" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());
        else if (iconIndex >= Config.CATEGORY2_MIN && iconIndex < Config.CATEGORY2_MAX) // category 2 season
            return getResources().getIdentifier("icon_user_icon_press_season" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());
        else if (iconIndex > Config.CATEGORY3_MIN && iconIndex < Config.CATEGORY3_MAX)  // category 3 place
            return getResources().getIdentifier("icon_user_icon_press_place" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());

        return 0;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }
    }
}