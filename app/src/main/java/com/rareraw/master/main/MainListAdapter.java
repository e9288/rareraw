/**
 * Copyright 2014 Magnus Woxblom
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rareraw.master.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.util.Pair;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rareraw.master.Common.DialogNotification;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSceneEditListener;
import com.rareraw.master.Common.OnSelecetedSceneInfo;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.data.SimpleSceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;

import static com.rareraw.master.Config.CATEGORY2_MIN;
import static com.rareraw.master.Config.CATEGORY3_MIN;
import static com.rareraw.master.ContainActivity.REQUEST_DELETE_CONFIRM;

public class MainListAdapter extends DragItemAdapter<Pair<Long, SimpleSceneInfo>, MainListAdapter.ViewHolder> {

    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;
    private boolean mEditMode = false;
    private Context mContext;
    private OnSceneEditListener mListener;


    // 20190626 리스너 이름만 변경함 mActionListner -> onSelectSceneLayoutListner
    // private OnActionListener mActionListener;
    private OnSelecetedSceneInfo onSelectSceneLayoutListner;
    private OnActionListener mDeleteListener;

    private View.OnClickListener onSceneScheduleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;

            if (simpleSceneInfo.getCurrentSceneIsPlay()) {
                // imageViewSceneScheduleOnOff = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_schedule_image_on_off);
                // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_off);
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(false);
            } else {
                // 20190623 스케줄씬 선택시 플레이 이미지 제거
               // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_on);
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onScenePlayClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
            if (!simpleSceneInfo.getCurrentSceneIsPlay()) {
                // 20190623 씬플레이 선택시 이미지 제거
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_play);
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(true);
            } else {
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_stop);
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onViewClickListener = new View.OnClickListener() {

        // 리스트뷰 호출 부
        @Override
        public void onClick(View view) {
            if (!mEditMode) {
                try {
                    //  Activity act = (Activity) mContext; showDialog
                    if( ((ContainActivity) mContext) != null)
                        ((ContainActivity) mContext).createDialog(500);


                    // mActionListener.onActionListener(-1, "off default scene");
                    ViewHolder viewHolder = (ViewHolder) view.getTag();
                    final SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
                    APService.setScene(simpleSceneInfo.getSceneNo(), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            Log.d("", "ok" );
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.d("", "error" );
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });

                  /*  APService.changeSceneAction(true, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            Log.d("", "ok" );
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.d("", "error" );
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });
*/
                    if (simpleSceneInfo.getSceneStatus() == 1) {//이미 선택된 상태

                    } else {
                        for (int i = 0; i < mItemList.size(); i++)
                            mItemList.get(i).second.setSceneStatus(0);

                    }
                    simpleSceneInfo.setSceneStatus(1);

                    if (simpleSceneInfo.getSceneType() == Cmd.NORMAL_SCENE) {
                        APService.getCurrentSceneDimingLevel(new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {
                                int level = (int) retValue;
                                simpleSceneInfo.setDimingLevel(level);
                                notifyDataSetChanged();
                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });
                    }

                    notifyDataSetChanged();
                } catch (Exception e) {
                    Log.d("error", "onitemClick");
                }
            } else {
                // 20190626 에디트 모드일때는 터치하면 딜리트플래그셋하고 삭제 준비를 한ㄷ.
                // -> 변경함. 에디트 모드 일때는 터치해도 아무 동작없음.

                /*
                ViewHolder viewHolder = (ViewHolder) view.getTag();
                SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
                simpleSceneInfo.setDeleteSelected(!simpleSceneInfo.getDeleteSelected());
                notifyDataSetChanged();
                boolean deleteFlag = false;
                for(int i = 0; i < mItemList.size(); i++){
                    if(mItemList.get(i).second.getDeleteSelected())
                        deleteFlag = true;
                }
                if(deleteFlag)
                    mDeleteListener.onActionListener(1,"visible delete button");
                else
                    mDeleteListener.onActionListener(-1, "invisible delete button");*/
            }
        }
    };

    public void Allstop() {
        for (int i = 0; i < mItemList.size(); i++)
            mItemList.get(i).second.setSceneStatus(0);
        notifyDataSetChanged();
    }

    private View.OnClickListener onDeleteCheckClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

            ViewHolder viewHolder = (ViewHolder) view.getTag();
            SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
            simpleSceneInfo.setDeleteSelected(!simpleSceneInfo.getDeleteSelected());

            // 20190626 체크박스 삭제됐으므로 플래그만 셋팅한다.
            // notifyDataSetChanged();

            Intent intent = null;
            intent = new Intent(mContext, DialogNotification.class);
            intent.putExtra(DialogNotification.TITLE, mContext.getString(R.string.delete_scene_title_msg));
            intent.putExtra(DialogNotification.CONTENTS, mContext.getString(R.string.delete_scene_content_msg));
            intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.WARNING);
            Activity act = (Activity) mContext;
            act.startActivityForResult(intent, REQUEST_DELETE_CONFIRM);
        }
    };

    private View.OnClickListener onControlClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
            mListener.onSceneEditListener(simpleSceneInfo.getSceneType(), simpleSceneInfo.getSceneNo(), -1);
        }
    };

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            ViewHolder viewHolder = (ViewHolder) seekBar.getTag();
            viewHolder.textViewDimmingValue.setText(i + "%");
            /*SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
            APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), i);
            mItemList.get(viewHolder.getAdapterPosition()).second.setDimingLevel(i);*/
            //((ViewHolder) seekBar.getTag()).textViewDimmingValue.setText(i + "%");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            ViewHolder viewHolder = (ViewHolder) seekBar.getTag();
            //viewHolder.textViewDimmingValue.setText(seekBar.getProgress() + "%");
            try {
                SimpleSceneInfo simpleSceneInfo = mItemList.get(viewHolder.getAdapterPosition()).second;
                APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), seekBar.getProgress(), new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                mItemList.get(viewHolder.getAdapterPosition()).second.setDimingLevel(seekBar.getProgress());
            } catch (Exception e) {
                Log.d("error", "diming");
            }
            // Toast.makeText(seekBar.getContext(), "선택한 아이템의 번호 : " + viewHolder.getAdapterPosition() + "\n최종 디밍 값 : " + seekBar.getProgress() + "", Toast.LENGTH_SHORT).show();
        }
    };

    MainListAdapter(ArrayList<Pair<Long, SimpleSceneInfo>> list, int layoutId, int grabHandleId, boolean dragOnLongPress, OnSceneEditListener listner, OnSelecetedSceneInfo _actionLis, Context context, OnActionListener _deleteBtnLinstener) {
        mLayoutId = layoutId;
        mGrabHandleId = grabHandleId;
        mDragOnLongPress = dragOnLongPress;
        mListener = listner;
        // 20190626 리스너 이름만 변경함 mActionListner -> onSelectSceneLayoutListner
        //mActionListener = _actionLis;
        onSelectSceneLayoutListner = _actionLis;
        mDeleteListener = _deleteBtnLinstener;
        mContext = context;
        setHasStableIds(true);
        setItemList(list);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        super.onBindViewHolder(viewHolder, position);
        SimpleSceneInfo simpleSceneInfo = mItemList.get(position).second;

        //씬 플레이
        // 20190623 씬플레이 선택시 이미지 제거
       //  viewHolder.imageViewScenePlayPlay.setTag(viewHolder);

        //씬 스케줄
        // viewHolder.textViewSceneScheduleName.setTag(viewHolder);

        // 20190623 스케줄씬 선택시 플레이 이미지 제거
        // viewHolder.imageViewSceneScheduleOnOff.setTag(viewHolder);

        // 씬 스케줄 씨커 주석 20190623
        // viewHolder.seekBarDimming.setTag(viewHolder);

        //모든 레이아웃을 안보이게 처리하고 뒤에 스위치문에서 필요한 레이아웃만 VISIBLE 합니다.

        // 20190623 기본레이아웃 4개 삭제함
        // viewHolder.layoutBase.setVisibility(View.GONE);
        // viewHolder.layoutScene.setVisibility(View.GONE);
        // viewHolder.layoutScenePlay.setVisibility(View.GONE);
        // viewHolder.layoutSceneSchedule.setVisibility(View.GONE);

        // viewHolder.imageViewTitleControl.setTag(viewHolder);
        viewHolder.textViewTitleControl.setTag(viewHolder);
        //편집모드
        if (mEditMode) {
            // 20190623 기본레이아웃 4개 삭제함
            // viewHolder.layoutBase.setVisibility(View.VISIBLE);
            // 20190623 씬 선택 표시 아이콘 주석
            // viewHolder.imageViewTitleSelect.setImageResource(R.mipmap.icon_select_off);
            switch (simpleSceneInfo.getSceneType()) {
                case Cmd.NORMAL_SCENE:
                    // 20190623 에디트모드에서 씬아이콘 변경되지 않음
                    //viewHolder.imageViewTitleTypeIcon.setImageResource(R.mipmap.icon_scene);

                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene));
                    break;
                case Cmd.PLAY_SCENE:
                    // 20190623 에디트모드에서 씬아이콘 변경되지 않음
                    // viewHolder.imageViewTitleTypeIcon.setImageResource(R.mipmap.icon_scene_play_off);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_play);

                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_play));
                    break;
                case Cmd.SCHEDULE_SCENE:
                    // 20190623 에디트모드에서 씬아이콘 변경되지 않음
                    // viewHolder.imageViewTitleTypeIcon.setImageResource(R.mipmap.icon_scene_schedule_off);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_sche);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_schedule));
                    break;
            }
            switch (simpleSceneInfo.getSceneStatus()) {
                case 0:// 일반 아이템 레이아웃
                    // 20190623 기본레이아웃 4개 삭제함
                    // viewHolder.layoutBase.setVisibility(View.VISIBLE);

                    // 20190623 하이드씬일때랑 일반씬일때랑 아이콘 동일하게 변경함
                    if (simpleSceneInfo.getSceneHide() == 0) { //일반상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.INVISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, false, false);
                        setBaseLayout(viewHolder, simpleSceneInfo, false);
                    } else { //숨김상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.VISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, true, false);
                        setBaseLayout(viewHolder, simpleSceneInfo, true);
                    }
                    break;

                case 1:// 선택된 아이템
                    if (simpleSceneInfo.getSceneHide() == 0) {//일반상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.INVISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, false, true);
                    } else {//숨김상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.VISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, true, true);
                    }
                    switch (simpleSceneInfo.getSceneType()) {
                        case Cmd.NORMAL_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutScene.setVisibility(View.VISIBLE);
                            setSceneLayout(viewHolder, simpleSceneInfo);
                            //onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.NORMAL_SCENE);
                            break;
                        case Cmd.PLAY_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutScenePlay.setVisibility(View.VISIBLE);
                            setScenePlayLayout(viewHolder, simpleSceneInfo);
                            //onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.PLAY_SCENE);
                            break;
                        case Cmd.SCHEDULE_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutSceneSchedule.setVisibility(View.VISIBLE);
                            setSceneScheduleLayout(viewHolder, simpleSceneInfo);
                            //onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.SCHEDULE_SCENE);
                            break;
                    }

                   // if(simpleSceneInfo.getCurrentSceneIsPlay() == false)
                   //     viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_off);
            }

//            viewHolder.textViewTitleTypeName.setTextColor(0xFF8F8F90);
            // 20190623 씬 구분 설명 텍스트 주석
            // viewHolder.textViewTitleTypeName.setTextColor(0xFFFFFFFF);
           // viewHolder.imageViewTitleMove.setVisibility(View.VISIBLE);

            // 20190626 무브 버튼에 홀더 추가
            viewHolder.imageViewTitleMove.setTag(viewHolder);

            // 20190623 씬 편집버튼 상시 노출로 변경함
            // viewHolder.imageViewTitleControl.setVisibility(View.INVISIBLE);
            //viewHolder.imageViewTitleHide.setVisibility((simpleSceneInfo.getSceneHide() == 0) ? View.INVISIBLE : View.VISIBLE);
            //TODO Delete

            String name = simpleSceneInfo.getSceneName().trim();
            if(name.contains("\n") ){
                name = name.replace("\n", "");
            }
            viewHolder.textViewName.setText(name);
//            viewHolder.textViewName.setTextColor(0xFF8F8F90);
            //viewHolder.textViewName.setTextColor(0xFFFFFFFF);

            // 20190626 삭제버튼에 홀더 추가
            viewHolder.textViewTitleDelete.setTag(viewHolder);
            viewHolder.textViewTitleDelete.setVisibility(View.VISIBLE);
            viewHolder.textViewTitleControl.setVisibility(View.GONE);

            //TODO Hide전용 이미지로 교체해야됩니다.
            // 20190623 씬 대표 이미지 주석
            // viewHolder.imageViewIcon.setImageResource(getImageResource(simpleSceneInfo.getSceneIcon()));

            // 20190623 씬 편집모드 체크박스 주석
            // viewHolder.imageViewCheck.setVisibility(View.VISIBLE);
            // viewHolder.imageViewCheck.setImageResource(simpleSceneInfo.getDeleteSelected() ? R.mipmap.icon_check_on : R.mipmap.icon_check_off);
            // viewHolder.imageViewCheck.setTag(viewHolder);
        } else {
            //viewHolder.textViewTitleDelete.setVisibility(View.GONE);

            // 01049147330
            viewHolder.textViewTitleDelete.setTag(viewHolder);


            if(simpleSceneInfo.getSceneNo() == Cmd.DEFAULT_SCENE_100) {
                viewHolder.textViewTitleControl.setVisibility(View.GONE);
                viewHolder.textViewTitleDelete.setVisibility(View.GONE);
            } else
                viewHolder.textViewTitleControl.setVisibility(View.VISIBLE);

            switch (simpleSceneInfo.getSceneStatus()) {
                case 0:// 일반 아이템 레이아웃
                    // 20190623 기본레이아웃 4개 삭제함
                    // viewHolder.layoutBase.setVisibility(View.VISIBLE);

                    // 20190623 하이드씬일때랑 일반씬일때랑 아이콘 동일하게 변경함
                    if (simpleSceneInfo.getSceneHide() == 0) { //일반상태
                        //viewHolder.imageViewTitleHide.setVisibility(View.INVISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, false, false);
                        setBaseLayout(viewHolder, simpleSceneInfo, false);
                    } else { //숨김상태
                        //viewHolder.imageViewTitleHide.setVisibility(View.VISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, true, false);
                        setBaseLayout(viewHolder, simpleSceneInfo, true);
                    }
                    break;

                case 1:// 선택된 아이템
                    if (simpleSceneInfo.getSceneHide() == 0) {//일반상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.INVISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, false, true);
                        setBaseLayout(viewHolder, simpleSceneInfo, false);
                    } else {//숨김상태
                       // viewHolder.imageViewTitleHide.setVisibility(View.VISIBLE);
                        setTitleLayout(viewHolder, simpleSceneInfo, true, true);
                        setBaseLayout(viewHolder, simpleSceneInfo, true);
                    }

                    switch (simpleSceneInfo.getSceneType()) {
                        case Cmd.NORMAL_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutScene.setVisibility(View.VISIBLE);
                            setSceneLayout(viewHolder, simpleSceneInfo);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.NORMAL_SCENE);
                            break;
                        case Cmd.PLAY_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutScenePlay.setVisibility(View.VISIBLE);
                           setScenePlayLayout(viewHolder, simpleSceneInfo);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.PLAY_SCENE);
                            break;
                        case Cmd.SCHEDULE_SCENE:
                            // 20190623 기본레이아웃 4개 삭제함
                            // viewHolder.layoutSceneSchedule.setVisibility(View.VISIBLE);
                            setSceneScheduleLayout(viewHolder, simpleSceneInfo);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.SCHEDULE_SCENE);
                            break;
                    }

                    // off 상태 일경우에는 off background color를 셋팅 20190703

                   // if(simpleSceneInfo.getCurrentSceneIsPlay() == false)
                    //    viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_off);
            }
        }


        viewHolder.layoutParent.setTag(viewHolder);
    }


    @Override
    public long getItemId(int position) {
        return mItemList.get(position).first;
    }

    private void setSceneScheduleLayout(ViewHolder viewHolder, SimpleSceneInfo simpleSceneInfo) {
        // 20190625 일반씬 레이아웃 하이라이트 변경추가
        //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_schedul);

        // 20190623 씬스케줄 네임텍스트 제거
        //viewHolder.textViewSceneScheduleName.setText(simpleSceneInfo.getSceneName());

        // 20190623 스케줄씬 선택시 플레이 이미지 제거
        // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_on);
    }

    private void setScenePlayLayout(ViewHolder viewHolder, SimpleSceneInfo simpleSceneInfo) {
        // 20190625 일반씬 레이아웃 하이라이트 변경추가
        //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_playl);

        // 20190623 씬스케줄 네임텍스트 제거
        //viewHolder.textViewScenePlayName.setText(simpleSceneInfo.getSceneName());
        // 20190623 씬플레이 선택시 이미지 제거
        //viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_play);

    }

    private void setSceneLayout(ViewHolder viewHolder, SimpleSceneInfo simpleSceneInfo) {
        // 20190625 일반씬 레이아웃 하이라이트 변경추가
       // viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_normal);

        // viewHolder.textViewSceneName.setText(simpleSceneInfo.getSceneName());

        // viewHolder.textViewDimmingValue.setText(String.valueOf(simpleSceneInfo.getDimingLevel()) + "%");
        // 씬 스케줄 씨커 주석 20190623
        // viewHolder.seekBarDimming.setProgress(simpleSceneInfo.getDimingLevel());
    }

    private void setBaseLayout(ViewHolder viewHolder, SimpleSceneInfo simpleSceneInfo, boolean hide) {
        String name = simpleSceneInfo.getSceneName().trim();
        if(name.contains("\n") ){
            name = name.replace("\n", "");
        }

        viewHolder.textViewName.setText(name);
      //  viewHolder.textViewName.setTextColor(hide ? 0xFF8F8F90 : 0xFFFFFFFF);
        // 20190623 씬 대표 이미지 주석
        // viewHolder.imageViewIcon.setImageResource(getImageResource(simpleSceneInfo.getSceneIcon(), hide));

        //편집모드에서만 사용하는 객체입니다.
        // 20190623 씬 편집모드 체크박스 주석
        // viewHolder.imageViewCheck.setVisibility(View.GONE);
    }

    private int getImageResource(int _index, boolean _hide) {
        if (_index == Config.USER_DEFAULT_ICON) {
            if (_hide)
                return mContext.getResources().getIdentifier("icon_user_default_icon_press", "mipmap", mContext.getPackageName());
            else
                return mContext.getResources().getIdentifier("icon_user_default_icon", "mipmap", mContext.getPackageName());
        } else if (_index >= Config.CATEGORY1_MIN && _index <= Config.CATEGORY1_MAX) { // category 1 day

            if (_hide)
                return mContext.getResources().getIdentifier("icon_user_icon_press_day" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
            else
                return mContext.getResources().getIdentifier("icon_user_icon_day" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
        } else if (_index >= CATEGORY2_MIN && _index <= Config.CATEGORY2_MAX) { // category 2 season
            _index = _index - CATEGORY2_MIN;
            if (_hide)
                return mContext.getResources().getIdentifier("icon_user_icon_press_season" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
            else
                return mContext.getResources().getIdentifier("icon_user_icon_season" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
        } else if (_index >= CATEGORY3_MIN && _index <= Config.CATEGORY3_MAX) { // category 3 place
            _index = _index - CATEGORY3_MIN;
            if (_hide)
                return mContext.getResources().getIdentifier("icon_user_icon_press_place" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
            else
                return mContext.getResources().getIdentifier("icon_user_icon_place" + String.format("%02d", _index), "mipmap", mContext.getPackageName());
        }
        return 0;
    }

    private int getImageResource(int iconIndex) {
        if (iconIndex == Config.USER_DEFAULT_ICON)
            return mContext.getResources().getIdentifier("icon_user_default_icon", "mipmap", mContext.getPackageName());
        else if (iconIndex >= Config.CATEGORY1_MIN && iconIndex <= Config.CATEGORY1_MAX) // category 1 day
            return mContext.getResources().getIdentifier("icon_user_icon_press_day" + String.format("%02d", iconIndex), "mipmap", mContext.getPackageName());
        else if (iconIndex >= CATEGORY2_MIN && iconIndex <= Config.CATEGORY2_MAX) {// category 2 season
            iconIndex = iconIndex - CATEGORY2_MIN;
            return mContext.getResources().getIdentifier("icon_user_icon_press_season" + String.format("%02d", iconIndex), "mipmap", mContext.getPackageName());
        } else if (iconIndex >= CATEGORY3_MIN && iconIndex <= Config.CATEGORY3_MAX) { // category 3 place
            iconIndex = iconIndex - CATEGORY3_MIN;
            return mContext.getResources().getIdentifier("icon_user_icon_press_place" + String.format("%02d", iconIndex), "mipmap", mContext.getPackageName());
        }
        return 0;
    }

    private void setTitleLayout(ViewHolder viewHolder, SimpleSceneInfo simpleSceneInfo, boolean hide, boolean select) {
        //Hide 아이템의 선택 색상이 바뀔때를 대비해서 우선 이렇게 찢어서 작성해놨습니다.

        switch (simpleSceneInfo.getSceneType()) {
            case Cmd.NORMAL_SCENE:

                // 20190624 일반씬은 아이콘없음 수정함
                // viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_scene_on : R.mipmap.icon_scene);
                viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_normal_mood_on : R.mipmap.icon_normal_mood_off);
                //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                //viewHolder.imageViewTitleTypeIcon.setVisibility(View.GONE);
                //viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_normal_scene_long);
                // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control);
                // 20190623 씬 구분 설명 텍스트 주석
                // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene));
                break;
            case Cmd.PLAY_SCENE:
                //TODO hide 전용 이미지가 없습니다.
                //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_play_mood_on : R.mipmap.icon_play_mood_off);
                // viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                //viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_play);
                // 20190623 씬 구분 설명 텍스트 주석
                // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_play));
                break;
            case Cmd.SCHEDULE_SCENE:
                //TODO hide 전용 이미지가 없습니다.
                //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_schedule_mood_on : R.mipmap.icon_schedule_mood_off);
               // viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_sche);
                // 20190623 씬 구분 설명 텍스트 주석
                // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_schedule));
                break;
        }
        viewHolder.imageViewTitleMove.setVisibility(View.INVISIBLE);
        /*
        if (hide) {
            //TODO hide 전용 이미지가 없습니다.
            // 20190623 씬 선택 표시 아이콘 주석
            // viewHolder.imageViewTitleSelect.setImageResource(select ? R.mipmap.icon_select_on : R.mipmap.icon_select_off_hide);
            switch (simpleSceneInfo.getSceneType()) {
                case Cmd.NORMAL_SCENE:

                    // 20190624 일반씬은 아이콘없음 수정함
                    // viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_scene_on : R.mipmap.icon_scene);

                    viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                    viewHolder.imageViewTitleTypeIcon.setVisibility(View.GONE);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_normal_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene));
                    break;
                case Cmd.PLAY_SCENE:
                    //TODO hide 전용 이미지가 없습니다.
                    viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                    viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_play_on : R.mipmap.icon_play_off);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    //viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_play);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_play));
                    break;
                case Cmd.SCHEDULE_SCENE:
                    //TODO hide 전용 이미지가 없습니다.
                    viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                    viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_schedul_on : R.mipmap.icon_schedul_off);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_sche);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_schedule));
                    break;
            }
            // 20190623 씬 구분 설명 텍스트 주석
            // viewHolder.textViewTitleTypeName.setTextColor(select ? 0xFFfdf258 : 0xFF8F8F90); // 0xFFD1E9EC ->yellow

            // 20190623 씬 편집버튼 상시노출루 변경
            //viewHolder.imageViewTitleControl.setVisibility(select ? View.VISIBLE : View.INVISIBLE);
        } else {
            // 20190623 씬 선택 표시 아이콘 주석
            // viewHolder.imageViewTitleSelect.setImageResource(select ? R.mipmap.icon_select_on : R.mipmap.icon_select_off);
            switch (simpleSceneInfo.getSceneType()) {
                case Cmd.NORMAL_SCENE:

                    // viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_scene_on : R.mipmap.icon_scene);
                    // 20190624 일반씬은 아이콘없음 수정함
                    //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_normal);
                    viewHolder.imageViewTitleTypeIcon.setVisibility(View.GONE);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_normal_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene));
                    break;
                case Cmd.PLAY_SCENE:
                   //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_playl);
                    viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_play_on : R.mipmap.icon_play_off);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                   //  viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_play);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_play));
                    break;
                case Cmd.SCHEDULE_SCENE:
                    //viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_schedul);
                    viewHolder.imageViewTitleTypeIcon.setImageResource(select ? R.mipmap.icon_schedul_on : R.mipmap.icon_schedul_off);
                    viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_long);
                    // 20190623 씬 편집버튼 씬 타입에 따라 변경없이 통일함
                    // viewHolder.imageViewTitleControl.setImageResource(R.mipmap.icon_item_title_control_sche);
                    // 20190623 씬 구분 설명 텍스트 주석
                    // viewHolder.textViewTitleTypeName.setText(mContext.getString(R.string.scene_schedule));
                    break;
            }
            // 20190623 씬 구분 설명 텍스트 주석
            // viewHolder.textViewTitleTypeName.setTextColor(select ? 0xFFfdf258 : 0xFFFFFFFF); // 0xFFD1E9EC ->yellow

            // 20190623 씬 편집버튼 상시 노출로 변경함
            // viewHolder.imageViewTitleControl.setVisibility(select ? View.VISIBLE : View.INVISIBLE);
        }
        //편집모드
        viewHolder.imageViewTitleMove.setVisibility(View.INVISIBLE);*/
    }

    void setEditMode(boolean value) {

        //편집모드가 해제될때 기존의 선택되었던 값을 초기화합니다.
        if (!value)
            for (int i = 0; i < mItemList.size(); i++) {
                mItemList.get(i).second.setDeleteSelected(false);
                mItemList.get(i).second.setSceneStatus(0);
            }
        mEditMode = value;
        notifyDataSetChanged();
    }

    boolean getEditMode() {
        return mEditMode;
    }



    public void selectDoingScene(int sceneNo) {
        for (int i = 0; i < mItemList.size(); i++) {
            if (sceneNo == mItemList.get(i).second.getSceneNo()) {
                final SimpleSceneInfo simpleSceneInfo = mItemList.get(i).second;
                simpleSceneInfo.setSceneStatus(1);

                switch (simpleSceneInfo.getSceneType()) {
                    case Cmd.NORMAL_SCENE:
                        onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.NORMAL_SCENE);
                        break;
                    case Cmd.PLAY_SCENE:
                        onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.PLAY_SCENE);
                        break;
                    case Cmd.SCHEDULE_SCENE:
                        onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.SCHEDULE_SCENE);
                        break;
                }
                notifyDataSetChanged();
                if (simpleSceneInfo.getSceneType() == Cmd.NORMAL_SCENE) {
                    APService.getCurrentSceneDimingLevel(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            int level = (int) retValue;
                            simpleSceneInfo.setDimingLevel(level);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });
                }
            }
        }
    }

    public boolean getSelectedSceneCnt() {
        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).second.getDeleteSelected()) {
                return true;
            }
        }
        return false;
    }

    public boolean checkNormalScene(int sceneNo) {
        for (int i = 0; i < mItemList.size(); i++) {
            if (mItemList.get(i).second.getSceneType() == Cmd.NORMAL_SCENE) {
                return true;
            }
        }
        return false;
    }

    class ViewHolder extends DragItemAdapter.ViewHolder {
        View layoutParent;
        //헤더

        // 20190623 씬표시 bg 이미지 추가
        RelativeLayout sceneBoxImg;
        // 20190623 씬 선택 표시 아이콘 주석
        //ImageView imageViewTitleSelect;
        ImageView imageViewTitleTypeIcon;

        // 20190623 씬 구분 설명 텍스트 주석
        // TextView textViewTitleTypeName;

        ImageView imageViewTitleHide;

        // 20190623 편집버튼 이미지에서 텍스트로 변경
        ImageView imageViewTitleControl;
        RelativeLayout textViewTitleControl;
        RelativeLayout textViewTitleDelete;

        ImageView imageViewTitleMove;

        //레이아웃들
        // 기존 레이아웃 4개 제거함
        // View layoutBase;
        // View layoutScene;
        // View layoutScenePlay;
        // View layoutSceneSchedule;

        //기본 객체
        // 20190623 씬 편집모드 체크박스 주석
        // ImageView imageViewCheck;

        // 20190623 씬 대표 이미지 주석
        // ImageView imageViewIcon;
        TextView textViewName;

        //씬 객체
        TextView textViewSceneName;
        TextView textViewDimmingValue;

        // 씬 스케줄 씨커 주석 20190623
        // SeekBar seekBarDimming;

        //씬 플레이 객체
        // 20190623 씬플레이 네임 텍스트 주석
        // TextView textViewScenePlayName;

        // 20190623 씬플레이 선택시 이미지 제거
        //ImageView imageViewScenePlayPlay;

        //씬 스케줄 객체
        // 20190623 씬스케줄 네임 텍스트 주석
        // TextView textViewSceneScheduleName;

        // 20190623 스케줄씬 선택시 플레이 이미지 제거
        //ImageView imageViewSceneScheduleOnOff;

        ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            layoutParent = itemView.findViewById(R.id.item_list_fragment_main_content_layout_parent);
            layoutParent.setOnClickListener(onViewClickListener);
            sceneBoxImg = (RelativeLayout) itemView.findViewById(R.id.item_list_fragment_main_box_bg_img);
            //헤더
            // 20190623 씬 선택 표시 아이콘 주석
            // imageViewTitleSelect = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_title_image_select);
            imageViewTitleTypeIcon = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_title_image_type_icon);
            // 20190623 씬 구분 설명 텍스트 주석
            // textViewTitleTypeName = (TextView) itemView.findViewById(R.id.item_list_fragment_main_title_text_type_name);

            imageViewTitleHide = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_title_image_hide);
            imageViewTitleMove = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_title_image_move);

            // 20190623 편집버튼 이미지에서 텍스트뷰로 변경
            // imageViewTitleControl = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_title_image_control);
            // imageViewTitleControl.setOnClickListener(onControlClickListener);

            textViewTitleControl = (RelativeLayout) itemView.findViewById(R.id.item_list_fragment_main_title_image_control);
            textViewTitleControl.setOnClickListener(onControlClickListener);

            textViewTitleDelete = (RelativeLayout) itemView.findViewById(R.id.item_list_fragment_main_title_image_delete);
            textViewTitleDelete.setOnClickListener(onDeleteCheckClickListener);

            //내부 레이아웃
            // 20190623 기본레이아웃 4개 삭제함
            // layoutBase = itemView.findViewById(R.id.item_list_fragment_main_content_layout_base);
            // layoutScene = itemView.findViewById(R.id.item_list_fragment_main_content_layout_scene);
            // layoutScenePlay = itemView.findViewById(R.id.item_list_fragment_main_content_layout_scene_play);
            // layoutSceneSchedule = itemView.findViewById(R.id.item_list_fragment_main_content_layout_scene_schedule);


            // [START] 선택안된 상태 일반 레이아웃 아이템
            // 20190623 씬 편집모드 체크박스 주석
            // imageViewCheck = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_image_check);

            // 20190623 씬 대표 이미지 주석
            // imageViewIcon = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_image_icon);
            textViewName = (TextView) itemView.findViewById(R.id.item_list_fragment_main_content_text_title);
            // imageViewCheck.setOnClickListener(onDeleteCheckClickListener);
            // [END]일반 레이아웃 아이템

            //[START]씬 레이아웃 아이템

            textViewSceneName = (TextView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_text_name);

            textViewDimmingValue = (TextView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_text_dimming_value);

            // 일반씬 레이아웃 디밍씨커 제거
            //seekBarDimming = (SeekBar) itemView.findViewById(R.id.item_list_fragment_main_content_scene_seek_bar);
            //seekBarDimming.setOnSeekBarChangeListener(onSeekBarChangeListener);
            //[END] 씬 레이아웃 아이템


            // [START]씬 플레이 레이아웃 아이템
            // 20190623 씬스케줄 네임텍스트 제거
            //textViewScenePlayName = (TextView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_play_text_name);
            // 20190623 씬플레이 선택시 이미지 제거
            //imageViewScenePlayPlay = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_play_image_play);
            //imageViewScenePlayPlay.setOnClickListener(onScenePlayClickListener);
            // [END]씬 플레이 레이아웃 아이템


            // [START]씬 스케줄 레이아웃 아이템
            // 20190623 씬스케줄 네임텍스트 제거
            //textViewSceneScheduleName = (TextView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_schedule_text_name);

            // 20190623 스케줄씬 선택시 플레이 이미지 제거
            //textViewSceneScheduleName.setOnClickListener(onSceneScheduleClickListener);
            // 20190623 스케줄씬 선택시 플레이 이미지 제거
            // imageViewSceneScheduleOnOff = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_schedule_image_on_off);
            // imageViewSceneScheduleOnOff.setOnClickListener(onSceneScheduleClickListener);
            // [END]씬 스케줄 레이아웃 아이템
        }

    }
}
