package com.rareraw.master.main;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSelecetedSceneInfo;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;
import com.rareraw.master.data.SimpleSceneInfo;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.ArrayList;

public class MainUserListAdapter extends BaseAdapter implements View.OnClickListener {

    private LayoutInflater mLayoutInflater;
    private ArrayList<SimpleSceneInfo> mSceneList = new ArrayList<>();
    private OnActionListener mLinstener;
    private Context mContext;
    private OnSelecetedSceneInfo onSelectSceneLayoutListner;
    private View.OnClickListener onViewClickListener = new View.OnClickListener() {

        // 리스트뷰 호출 부
        @Override
        public void onClick(View view) {
            try {
                // mActionListener.onActionListener(-1, "off default scene");
                final SimpleSceneInfo simpleSceneInfo  = (SimpleSceneInfo) view.getTag(R.id.fragment_user_main_parent);
                APService.setScene(simpleSceneInfo.getSceneNo(), new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Log.d("setScene", "ok");
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.d("setScene", "fail");
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });

                if (simpleSceneInfo.getSceneStatus() == 1) {//이미 선택된 상태

                } else {
                    for (int i = 0; i < mSceneList.size(); i++)
                        mSceneList.get(i).setSceneStatus(0);

                }
                simpleSceneInfo.setSceneStatus(1);

                if (simpleSceneInfo.getSceneType() == Cmd.NORMAL_SCENE) {
                    APService.getCurrentSceneDimingLevel(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            int level = (int) retValue;
                            simpleSceneInfo.setDimingLevel(level);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });
                }

                notifyDataSetChanged();
            } catch (Exception e) {
                Log.d("error", "onitemClick");
            }
        }
    };


    public MainUserListAdapter(Context context, ArrayList<SimpleSceneInfo> _arrayList, OnSelecetedSceneInfo _listner) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mSceneList = _arrayList;
        onSelectSceneLayoutListner = _listner;
        mContext = context;
    }

    @Override
    public int getCount() {
        return mSceneList.size();
    }

    @Override
    public Object getItem(int i) {

        return mSceneList.get(1);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        userSceneHolder holder = new userSceneHolder();
        SimpleSceneInfo simpleSceneInfo = mSceneList.get(i);
        int width = Prf.readToSharedPreferences(mContext, Prf.WIDTH_SIZE_FILE, Prf.WIDTH_SIZE_TAG, 1080);
        if (view == null) {
            view = mLayoutInflater.inflate(R.layout.item_gridview_scene, viewGroup, false);

            holder.sceneName = (TextView) view.findViewById(R.id.text_scene_name);
            holder.sceneTypeIcon = (ImageView) view.findViewById(R.id.item_list_fragment_normal_main_title_image_type_icon);
            holder.layoutParent = (View) view.findViewById(R.id.fragment_user_main_parent);

            holder.layoutParent.setOnClickListener(onViewClickListener);

            holder.sceneBoxImg = (RelativeLayout) view.findViewById(R.id.item_list_fragment_normal_main_box_bg_img);

        } else
            holder = (userSceneHolder) view.getTag();

            ViewGroup.LayoutParams params = (ViewGroup.LayoutParams)holder.layoutParent.getLayoutParams();

            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = width;
            holder.layoutParent.setLayoutParams(params);

            holder.layoutParent.setTag(R.id.fragment_user_main_parent,  simpleSceneInfo);
        try {
            holder.position = i;
            holder.sceneName.setText(simpleSceneInfo.getSceneName());
            switch (simpleSceneInfo.getSceneStatus()) {
                case 0:// 일반 아이템 레이아웃
                    drawSceneContents(holder, simpleSceneInfo, false);
                    break;

                case 1:// 선택된 아이템
                    drawSceneContents(holder, simpleSceneInfo, true);

                    switch (simpleSceneInfo.getSceneType()) {
                        case Cmd.NORMAL_SCENE:
                            holder.layoutParent.setBackgroundResource(R.drawable.bg_normal);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.NORMAL_SCENE);
                            break;
                        case Cmd.PLAY_SCENE:
                            holder.layoutParent.setBackgroundResource(R.drawable.bg_playl);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.PLAY_SCENE);
                            break;
                        case Cmd.SCHEDULE_SCENE:
                            holder.layoutParent.setBackgroundResource(R.drawable.bg_schedul);
                            onSelectSceneLayoutListner.onSelectedSceneInfo(simpleSceneInfo, Cmd.SCHEDULE_SCENE);
                            break;
                    }

                    // off 상태 일경우에는 off background color를 셋팅 20190703
                    if(simpleSceneInfo.getCurrentSceneIsPlay() == false)
                        holder.layoutParent.setBackgroundResource(R.drawable.bg_off);

            }
        } catch (Exception e) {
            e.printStackTrace();
            return view;
        }
        view.setTag(holder);
        // view.textViewTitleControl.setTag(viewHolder);
        return view;
    }


    public void selectDoingScene(int sceneNo) {
        for (int i = 0; i < mSceneList.size(); i++) {
            if (sceneNo == mSceneList.get(i).getSceneNo()) {
                final SimpleSceneInfo simpleSceneInfo = mSceneList.get(i);
                simpleSceneInfo.setSceneStatus(1);
                notifyDataSetChanged();
                if (simpleSceneInfo.getSceneType() == Cmd.NORMAL_SCENE) {
                    APService.getCurrentSceneDimingLevel(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            int level = (int) retValue;
                            simpleSceneInfo.setDimingLevel(level);
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });
                }
            }
        }
    }

    public void setUserSceneList(ArrayList<SimpleSceneInfo> _info) {
        mSceneList = _info;
    }

    private void drawSceneContents(userSceneHolder viewHolder, SimpleSceneInfo simpleSceneInfo, boolean select) {
        switch (simpleSceneInfo.getSceneType()) {
            case Cmd.NORMAL_SCENE:
                viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                viewHolder.sceneTypeIcon.setVisibility(View.GONE);
                viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_normal_scene_short);
                break;
            case Cmd.PLAY_SCENE:
                //TODO hide 전용 이미지가 없습니다.
                viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                viewHolder.sceneTypeIcon.setImageResource(select ? R.mipmap.icon_play_on : R.mipmap.icon_play_off);
                viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_short);
                break;
            case Cmd.SCHEDULE_SCENE:
                //TODO hide 전용 이미지가 없습니다.
                viewHolder.layoutParent.setBackgroundResource(R.drawable.bg_default);
                viewHolder.sceneTypeIcon.setImageResource(select ? R.mipmap.icon_schedul_on : R.mipmap.icon_schedul_off);
                viewHolder.sceneBoxImg.setBackgroundResource(R.mipmap.bg_special_scene_short);
                break;
        }
    }

    @Override
    public void onClick(View view) {

        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        notifyDataSetChanged();
    }


    public class userSceneHolder {
        TextView sceneName;
        View layoutParent;
        int position;
        RelativeLayout sceneBoxImg;
        ImageView sceneTypeIcon;
    }
}

