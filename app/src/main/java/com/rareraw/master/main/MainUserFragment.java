package com.rareraw.master.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSelecetedSceneInfo;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.data.SimpleSceneInfo;
import com.rareraw.master.data.TestDataManager;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.ArrayList;

/**
 * Created by Lch on 2016-12-14.
 */

public class MainUserFragment extends BaseFragment implements View.OnClickListener {
    private ArrayList<SimpleSceneInfo> mItemArray = new ArrayList();
    private GridView mGridListView;
    private MainUserListAdapter mListAdapter;
    // 20190626 최상단 레이아웃 추가됨
    private SeekBar mMainMasterSeeker;
    private TextView mSelectedSceneName;
    private CheckBox mSwitchNormalScene, mSwitchPlayScene, mSwitchSchedScene;
    private ArrayList<SimpleSceneInfo> simpleSceneInfos;

    private OnActionListener onActionListener = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {

        }
    };

    private OnSelecetedSceneInfo onSelecetedSceneInfo = new OnSelecetedSceneInfo() {

        @Override
        public void onSelectedSceneInfo(SimpleSceneInfo _info, int _sceneType) {
            String sceneName = "";
            if (_info != null) {
                sceneName = _info.getSceneName();
            }
            switch (_sceneType) {
                case Cmd.NORMAL_SCENE:
                    mSwitchNormalScene.setVisibility(View.VISIBLE);
                    mSwitchPlayScene.setVisibility(View.GONE);
                    mSwitchSchedScene.setVisibility(View.GONE);

                    if(!_info.getCurrentSceneIsPlay()) {
                        mSwitchNormalScene.setChecked(false);
                        mMainMasterSeeker.setEnabled(false);
                        mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                        mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    } else {
                        mSwitchNormalScene.setChecked(true);
                        mMainMasterSeeker.setEnabled(true);
                        mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb));
                        mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master));
                    }
                    resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), _info.getCurrentSceneIsPlay());
                    mMainMasterSeeker.setTag(R.id.fragment_main_master_seeker, _info);
                    mSwitchNormalScene.setTag(R.id.fragment_switch_normal, _info);
                   //  mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb));

                    mMainMasterSeeker.setProgress(_info.getDimingLevel());
                   //  mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master));

                    break;

                case Cmd.PLAY_SCENE:
                    mSwitchNormalScene.setVisibility(View.GONE);
                    mSwitchPlayScene.setVisibility(View.VISIBLE);
                    mSwitchSchedScene.setVisibility(View.GONE);
                    mMainMasterSeeker.setEnabled(false);
                    mMainMasterSeeker.setProgress(0);
                    mSwitchPlayScene.setTag(R.id.fragment_switch_play, _info);
                    mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                    mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), false);
                    if(_info.getCurrentSceneIsPlay())
                        mSwitchPlayScene.setChecked(true);
                    else
                        mSwitchPlayScene.setChecked(false);
                   //   mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));

                    break;

                case Cmd.SCHEDULE_SCENE:
                    mSwitchNormalScene.setVisibility(View.GONE);
                    mSwitchPlayScene.setVisibility(View.GONE);
                    mSwitchSchedScene.setVisibility(View.VISIBLE);
                    mMainMasterSeeker.setEnabled(false);
                    mMainMasterSeeker.setProgress(0);
                    mSwitchSchedScene.setTag(R.id.fragment_switch_schedul, _info);
                    mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                    mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    resizeSeekbarthumb(mMainMasterSeeker, 1.0, getContext(), false);
                    if(_info.getCurrentSceneIsPlay())
                        mSwitchSchedScene.setChecked(true);
                    else
                        mSwitchSchedScene.setChecked(false);
                       //mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));
                    break;
            }
            if(sceneName.contains("\n") ){
                sceneName = sceneName.replace("\n", "");
            }
            mSelectedSceneName.setText(sceneName);
        }
    };


    private View.OnClickListener onSceneOnOffListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_normal);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬 on of off 실패");
                return;
            }

            final int progressLevel = simpleSceneInfo.getDimingLevel();
            if (simpleSceneInfo.getCurrentSceneIsPlay()) { // on 상태
                mMainMasterSeeker.setEnabled(false);
                mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb_false));
                //mMainMasterSeeker.setProgressDrawable(getResources().getDrawable(R.drawable.seekbar_master_off));


                APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), 0, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
            } else { // off 상태
                mMainMasterSeeker.setEnabled(true);
                mMainMasterSeeker.setThumb(getResources().getDrawable(R.mipmap.img_master_seekbar_thumb));
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
                APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), progressLevel, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onScenePlayStopStartListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_play);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬플레이 on of off 실패");
                return;
            }
            if (!simpleSceneInfo.getCurrentSceneIsPlay()) {
                // 20190623 씬플레이 선택시 이미지 제거
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_play);
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
            } else {
                // 20190623 씬플레이 선택시 이미지 제거
                // viewHolder.imageViewScenePlayPlay.setImageResource(R.mipmap.icon_sceneplay_stop);
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };

    private View.OnClickListener onSceneScheduleStopStartListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            SimpleSceneInfo simpleSceneInfo = null;
            try {
                simpleSceneInfo = (SimpleSceneInfo) view.getTag(R.id.fragment_switch_schedul);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (simpleSceneInfo == null) {
                Log.d("normal", "씬스케줄 on of off 실패");
                return;
            }

            if (simpleSceneInfo.getCurrentSceneIsPlay()) {
                // imageViewSceneScheduleOnOff = (ImageView) itemView.findViewById(R.id.item_list_fragment_main_content_scene_schedule_image_on_off);
                // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_off);
                APService.changeSceneAction(false, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
                simpleSceneInfo.setCurrentSceneIsPlay(false);
                mListAdapter.notifyDataSetChanged();
            } else {
                // 20190623 스케줄씬 선택시 플레이 이미지 제거
                // viewHolder.imageViewSceneScheduleOnOff.setImageResource(R.mipmap.icon_schedule_on);
                simpleSceneInfo.setCurrentSceneIsPlay(true);
                mListAdapter.notifyDataSetChanged();
                APService.changeSceneAction(true, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setSceneList();
        if (Config.DEBUG)
            APService.setUserLevel(true);
        mListAdapter = new MainUserListAdapter(getActivity(), mItemArray, onSelecetedSceneInfo);
    }

    private void setSceneList() {
        // 1 step, get scene list in ap
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                getSimpleSceneInfos();
            }

            @Override
            public void onError(int errorCode) {
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                LogoutMain();
            }
        });
        if (Config.DEBUG) {
            TestDataManager testDataManager = new TestDataManager();
            simpleSceneInfos = testDataManager.getSimpleSceneInfos();

            // 3 step, casting simpleSceneinfo to listArray ( for dragList class)
            mItemArray = new ArrayList<>();
            for (int i = 0; i < simpleSceneInfos.size(); i++) {
                mItemArray.add(simpleSceneInfos.get(i));
            }

            mListAdapter.setUserSceneList(mItemArray);
        }
    }

    public void refreshUi() {
        setSceneList();
    }

    private void getSimpleSceneInfos() {
        APService.getSceneList(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

                ArrayList<SimpleSceneInfo> infos = new ArrayList<>();
                CmdHelper.sortByIndexSceneList();
                try {
                    for (SceneInfo sceneinfo : APService.getApSceneList())
                        if (sceneinfo.getIntSceneNo() >= Cmd.MIN_SCENE_NO) {
                            infos.add(new SimpleSceneInfo(Config.NO_SELECT, sceneinfo.getIntSceneType(), sceneinfo.getIntIconType(), sceneinfo.getSceneName().trim(), (sceneinfo.getHideFlag()) ? 1 : 0, sceneinfo.getIntSceneNo(), sceneinfo.getIntIndex()));
                        }else if(sceneinfo.getIntSceneNo() == Cmd.DEFAULT_SCENE_100) {
                            infos.add(new SimpleSceneInfo(Config.NO_SELECT, sceneinfo.getIntSceneType(), sceneinfo.getIntIconType(), getString(R.string.scene_all), (sceneinfo.getHideFlag()) ? 1 : 0, sceneinfo.getIntSceneNo(), sceneinfo.getIntIndex()));
                        }
                } catch (Exception e) {
                    Log.e("error", "scenelist casting");
                }

                simpleSceneInfos = infos;
                // 3 step, casting simpleSceneinfo to listArray ( for dragList class)
                mItemArray = new ArrayList<>();
                for (int i = 0; i < simpleSceneInfos.size(); i++) {
                    mItemArray.add(simpleSceneInfos.get(i));
                }
               // mListAdapter.resetItemList(initItem);
                mListAdapter.setUserSceneList(mItemArray);

                APService.getCurrentActionScene(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        int sceneNo = (int) retValue;
                        if (sceneNo != -1) {
                            //if (sceneNo == Cmd.DEFAULT_SCENE_0 || sceneNo == Cmd.DEFAULT_SCENE_50 || sceneNo == Cmd.DEFAULT_SCENE_100)
                                //    updateDefaultScene(sceneNo);

                                mListAdapter.selectDoingScene(sceneNo);
                        }
                        mListAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        LogoutMain();
                    }
                });
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                LogoutMain();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View parent = inflater.inflate(R.layout.fragment_user_main, container, false);

        mGridListView = (GridView) parent.findViewById(R.id.grieview_scene_normal);
        mGridListView.setAdapter(mListAdapter);
        mSwitchNormalScene = (CheckBox) parent.findViewById(R.id.fragment_switch_normal);
        mSwitchNormalScene.setOnClickListener(onSceneOnOffListener);

        mSwitchPlayScene = (CheckBox) parent.findViewById(R.id.fragment_switch_play);
        mSwitchPlayScene.setOnClickListener(onScenePlayStopStartListener);

        mSwitchSchedScene = (CheckBox) parent.findViewById(R.id.fragment_switch_schedul);
        mSwitchSchedScene.setOnClickListener(onSceneScheduleStopStartListener);

        mMainMasterSeeker = (SeekBar) parent.findViewById(R.id.fragment_main_master_seeker);
        mMainMasterSeeker.setEnabled(false);
        mMainMasterSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                try {
                    SimpleSceneInfo simpleSceneInfo = (SimpleSceneInfo) seekBar.getTag(R.id.fragment_main_master_seeker);
                    APService.setCurrentSceneDimingLevel(simpleSceneInfo.getSceneNo(), seekBar.getProgress(), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            Log.d("seek", "ok");
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.d("seek", "fail");
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {

                        }
                    });

                } catch (Exception e) {
                    Log.d("error", "diming");
                }
                // Toast.makeText(seekBar.getContext(), "선택한 아이템의 번호 : " + viewHolder.getAdapterPosition() + "\n최종 디밍 값 : " + seekBar.getProgress() + "", Toast.LENGTH_SHORT).show();
            }
        });
        mSelectedSceneName = (TextView) parent.findViewById(R.id.fragment_main_selected_scene_name);

        return parent;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onStart() {
        super.onStart();

        if (!APService.getUserLevel()) {
            refreshUi();
            onFragmentCallbackListener.onFragmentCallback(getTag(), 0);
        } else
            onFragmentCallbackListener.onFragmentCallback(getTag(), -1);
    }


    private ImageView mPrevImg;
    private TextView mPrevText;
    private LinearLayout mPrevLayout;

    @Override
    public void onClick(View v) {
        //  mListAdapter.Allstop();
    }
}