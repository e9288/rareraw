package com.rareraw.master.alarm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.DataClass.AlarmItem;

import java.util.ArrayList;

/**
 * Created by lch on 2016-12-13.
 */

public class AlarmAdapter extends BaseAdapter implements View.OnClickListener {
    private LayoutInflater mLayoutInflater;
    private ArrayList<AlarmItem> mAlarmList;
    private Context mContext;
    private boolean mUserLevel = false;
    private OnActionListener mListener;

    AlarmAdapter(Context context, ArrayList<AlarmItem> _alarm, OnActionListener _listener) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mAlarmList = _alarm;
        this.mContext = context;
        mListener = _listener;
    }

    @Override
    public int getCount() {
        return mAlarmList.size();
    }

    @Override
    public Object getItem(int position) {
        return mAlarmList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        if (view == null)
            view = mLayoutInflater.inflate(R.layout.item_list_view_alarm_list, parent, false);

        try {

            RelativeLayout layoutModify = (RelativeLayout) view.findViewById(R.id.layout_alarm_modify);
            layoutModify.setTag(R.id.layout_alarm_modify, position);
            layoutModify.setOnClickListener(this);
            TextView textTitle = (TextView) view.findViewById(R.id.list_view_alarm_list_title);

            textTitle.setText(mContext.getString(R.string.alarm) + " " + String.format("%d", mAlarmList.get(position).mIndex));

            TextView textApmp = (TextView) view.findViewById(R.id.list_view_alarm_list_ampm);
            textApmp.setText(mAlarmList.get(position).mAmPm);

            TextView textTimeHour = (TextView) view.findViewById(R.id.list_view_alarm_list_time_hour);
            if (mAlarmList.get(position).mHour > 12)
                textTimeHour.setText(String.format("%02d", mAlarmList.get(position).mHour - 12));
            else
                textTimeHour.setText(String.format("%02d", mAlarmList.get(position).mHour));
            TextView textTimeMin = (TextView) view.findViewById(R.id.list_view_alarm_list_time_min);
            textTimeMin.setText(String.format("%02d", mAlarmList.get(position).mMin));

            TextView textScemeName = (TextView) view.findViewById(R.id.list_view_alarm_list_scene_name);
            textScemeName.setText(mAlarmList.get(position).mSceneName);

            TextView textWeek = (TextView) view.findViewById(R.id.list_view_alarm_list_week);
            textWeek.setText(mAlarmList.get(position).mWeek);

            ImageView alarmAction = (ImageView) view.findViewById(R.id.list_view_alarm_list_ararm_action_btn);
           //  alarmAction.setOnClickListener(this);


            if (mAlarmList.get(position).mAction)
                alarmAction.setImageResource(R.mipmap.icon_alarm_on);
            else
                alarmAction.setImageResource(R.mipmap.icon_alarm_off);

            RelativeLayout imgDelete = (RelativeLayout) view.findViewById(R.id.list_view_alarm_list_ararm_delete_btn);
            imgDelete.setOnClickListener(this);
            imgDelete.setTag(R.id.list_view_alarm_list_ararm_delete_btn, position);

            layoutModify.setTag(R.id.layout_alarm_modify, position);

            /*if (mUserLevel) {
                layoutModify.setOnClickListener(this);
                imgDelete.setVisibility(View.VISIBLE);
            } else {
                imgDelete.setVisibility(View.GONE);
                if (Config.DEBUG)
                    imgDelete.setVisibility(View.VISIBLE);
            }*/

            LinearLayout item_layout = (LinearLayout) view.findViewById(R.id.item_layout);
            item_layout.setTag(R.id.list_view_alarm_list_ararm_action_btn, position);
            item_layout.setOnClickListener(this);

            if (Config.DEBUG)
                layoutModify.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        int position = 0;
        switch (view.getId()) {
            case R.id.list_view_alarm_list_ararm_delete_btn:
                position = (int) view.getTag(R.id.list_view_alarm_list_ararm_delete_btn);
                mListener.onActionListener(position, AlarmFragment.DELETE_ALARM);
                break;
            case R.id.item_layout : // list_view_alarm_list_ararm_action_btn:
                position = (int) view.getTag(R.id.list_view_alarm_list_ararm_action_btn);
                mListener.onActionListener(position, AlarmFragment.ACTION_CHANGE);
                break;
            case R.id.layout_alarm_modify:
                position = (int) view.getTag(R.id.layout_alarm_modify);
                mListener.onActionListener(position, AlarmFragment.ACTION_MODIFY);
                break;
        }
    }

    public void setAlarmList(ArrayList<AlarmItem> _alarmItemList) {
        this.mAlarmList = _alarmItemList;
    }

    public void setUserLevel(boolean userLevel) {
        mUserLevel = userLevel;
    }
}
