package com.rareraw.master.alarm;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.AlarmInfo;
import com.rareraw.master.network.DataClass.AlarmItem;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;


public class AlarmFragment extends BaseFragment implements OnActionListener {
    public static final String DELETE_ALARM = "delete_alarm", ACTION_CHANGE = "action_change", ACTION_MODIFY = "modidfy", SHOW_DIG = "SHOW_DIG", HIDE_DIG = "HIDE_DIG";
    private ArrayList<AlarmItem> mAlarmItemList = new ArrayList<>();
    private RelativeLayout mBtnAddAlarm;
    private AlarmAdapter mAdapter;
    private ListView mListviewAlarmList;
    private OnFragmentInteractionListener mListener;
    private View mParent;
    private byte ACTION_ON = 0x01, ACTION_OFF = 0x00;
    private Context mContext;
    private int mDeletePosition = -1;
    private OnActionListener mDeleteListener = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {
            //((ContainActivity) getActivity()).createDialog(900);
            deleteAlarm(mDeletePosition);
        }
    };

    public AlarmFragment() {
        // Required empty public constructor
    }

    public static AlarmFragment newInstance() {
        AlarmFragment fragment = new AlarmFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mParent = inflater.inflate(R.layout.fragment_alarm, container, false);
        mListviewAlarmList = (ListView) mParent.findViewById(R.id.listview_alarmlist);
        mBtnAddAlarm = (RelativeLayout) mParent.findViewById(R.id.btn_add_alarm);

        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnAddAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onFragmentCallbackListener.onFragmentCallback(getTag(), -1);
            }
        });
      //  getAlarmList();
      //  addAlarmList();
        drawListView();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUserLevelMenu();
        updateAlarmList();
    }

    private void updateUserLevelMenu() {
        if (APService.getUserLevel()) {
            mBtnAddAlarm.setVisibility(View.VISIBLE);
        } else {
            mBtnAddAlarm.setVisibility(View.GONE);
        }
    }


    private void drawListView() {
        mAdapter = new AlarmAdapter(getContext(), mAlarmItemList, this);
        mListviewAlarmList.setAdapter(mAdapter);
    }

    private void addAlarmList() {
        mAlarmItemList.clear();
        int index = 1;
        boolean oldLevel = APService.getUserLevel();
        APService.setUserLevel(true);
        for (AlarmInfo info : APService.getApAlramList()) {
            mAlarmItemList.add(new AlarmItem(index++, AlarmItem.getAmPm(info.getIntAlarmTimeHour(), mContext), info.getIntAlarmTimeHour(),
                    info.getIntAlarmTimeMin(), info.getNSceneNo(), CmdHelper.getSceneName(info.getmBSceneNo()),
                    AlarmItem.getWeek(info.getAlarmWeek(),mContext), info.getAlarmWeek(), info.getBooleanAction(), info.getAlarmKey()));
        }
        APService.setUserLevel(oldLevel);
    }

    private void testDataList() {
        // TODO TEST CODE

        if (Config.DEBUG) {
            byte[] dd = new byte[]{(byte) 0x00, (byte) 0x00};
            byte[] week1 = new byte[]{(byte) 0x01, (byte) 0x01, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
            byte[] week2 = new byte[]{(byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01, (byte) 0x01};
            mAlarmItemList.add(new AlarmItem(1, AlarmItem.getAmPm(5, mContext), 12, 10, 0, "name1", AlarmItem.getWeek(week1, mContext), week1, false, dd));
            mAlarmItemList.add(new AlarmItem(2, AlarmItem.getAmPm(20, mContext), 20, 10, 0, "name2", AlarmItem.getWeek(week1,mContext), week1, false, dd));
        }
    }

    private void getAlarmList() {
        try {
            ((ContainActivity) getActivity()).createDialog(700);
            APService.getAlramList(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    addAlarmList();
                    mAdapter.setAlarmList(mAlarmItemList);
                    mAdapter.setUserLevel(APService.getUserLevel());
                    mAdapter.notifyDataSetChanged();
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Logout();
                }
            });
            if(Config.DEBUG)
                testDataList();
        } catch (Exception e) {
            Logout();
            Log.e("error", "get alarmlist");
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onActionListener(int position, String tag) {
        if (tag.equals(DELETE_ALARM)) {
            // TODO delete alarm list using position
            mDeletePosition = position;
            confirmDelete();
        } else if (tag.equals(ACTION_CHANGE)) {
            ((ContainActivity) getActivity()).createDialog(700);
            changeAlarmAction(position);
        }else if (tag.equals(ACTION_MODIFY)){
            modifyExistingAlarm(position);
        }
    }

    private void modifyExistingAlarm(int pos) {
        int nKey = CmdHelper.byteArrayToInt(mAlarmItemList.get(pos).mbKey);
        onFragmentCallbackListener.onFragmentCallback(getTag(), nKey);
    }

    private void confirmDelete() {
        DialogDeleteAlarm dialogDeleteAlarm = new DialogDeleteAlarm();
        dialogDeleteAlarm.setListener(mDeleteListener);
        dialogDeleteAlarm.show(getActivity().getFragmentManager(), "dialogDeleteAlarm");
    }

    private void changeAlarmAction(int position) {
        ByteBuffer buffer = ByteBuffer.allocate(Cmd.ALARM_MODIFY_DATA_SIZE);
        try {
            byte hour = (byte) mAlarmItemList.get(position).mHour;
            byte min = (byte) mAlarmItemList.get(position).mMin;
            byte[] week = mAlarmItemList.get(position).mBweek;
            byte[] sceneNo = CmdHelper.intToByteArray(mAlarmItemList.get(position).mSceneNo);
            byte action = 0x00;
            if (mAlarmItemList.get(position).mAction)
                action = ACTION_OFF;
            else
                action = ACTION_ON;

            byte repeat = 0x00;
            Log.e("Hour", Byte.toString(hour));
            Log.e("Min", Byte.toString(min));

            Log.e("scene no", Byte.toString(sceneNo[0]));
            Log.e("scene no", Byte.toString(sceneNo[1]));
            Log.e("action", Byte.toString(action));
            Log.e("repeat", Byte.toString(repeat));

            buffer.put(mAlarmItemList.get(position).mbKey);
            buffer.put(hour);
            buffer.put(min);
            buffer.put(week);
            buffer.put(sceneNo);
            buffer.put(action);

            // TODO check repeat
            //buffer.put(repeat);

            APService.modifyAlram(buffer, new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    Log.e("ok", "change alarm");
                    updateAlarmList();
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Log.e("error", "change alarm");
                    Logout();
                }
            });
        } catch (Exception e) {
            Log.e("error", "change alarm");
            Logout();
        }
    }

    private void deleteAlarm(int position) {
        ByteBuffer buf = ByteBuffer.allocate(2);
        buf.put(mAlarmItemList.get(position).mbKey);
        APService.deleteAlram(buf, new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                updateAlarmList();
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }

    private void updateAlarmList() {
        getAlarmList();
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
