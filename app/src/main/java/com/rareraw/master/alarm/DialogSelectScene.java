package com.rareraw.master.alarm;

import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.SceneInfo;

import java.util.ArrayList;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogSelectScene extends DialogFragment {

    private OnActionListener mListner;
    private TextView mBtnCancel, mBtnOk;
    private ListView mListviewSceneList;
    private SceneListAdapter mAdapter;
    private View mParent;
    private int mDeletePosition;
    private ArrayList<SceneInfomation> mSceneInfoList = new ArrayList<>();

    public DialogSelectScene() {
    }

    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        initSceneList();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_select_scene, container, false);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnCancel = (TextView) mParent.findViewById(R.id.btn_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (TextView) mParent.findViewById(R.id.btn_confirm);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SceneInfomation info = mAdapter.getSelectedScene();
                if (info != null)
                    mListner.onActionListener(info.mSceneNo, info.mSceneName);
                dismiss();
            }
        });
        mListviewSceneList = (ListView) mParent.findViewById(R.id.listview_scene_list);

    }

    private void initSceneList() {
        getSceneList();
        mSceneInfoList = getSceneListInfo();
        mAdapter = new SceneListAdapter(getActivity(), mSceneInfoList);
        mListviewSceneList.setAdapter(mAdapter);
        mAdapter.setDefaultCheck();
    }

    private ArrayList<SceneInfomation> getSceneListInfo() {
        ArrayList<SceneInfomation> list = new ArrayList<>();

        // scene
        for (SceneInfo info : APService.getApSceneList()) {
            if(info.getIntSceneType() != Cmd.NORMAL_SCENE)
                continue;

            list.add(new SceneInfomation(info.getIntSceneNo(), info.getSceneName()));
        }
        // scene play list delete
        // for (ScenePlayInfo info : APService.mScenePlayList)
        //    list.add(new SceneInfomation(info.getIntScenePlayNo(), info.getScenePlayName()));

        if (Config.DEBUG) {
            byte[] test = new byte[]{(byte) 0x00, (byte) 0x00};
            list.add(new SceneInfomation(1, "name1"));
            list.add(new SceneInfomation(1, "name2"));
            list.add(new SceneInfomation(1, "name3"));
            list.add(new SceneInfomation(1, "name4"));
            list.add(new SceneInfomation(1, "name5"));
            list.add(new SceneInfomation(1, "name6"));
            list.add(new SceneInfomation(1, "name7"));
            list.add(new SceneInfomation(1, "name8"));
            list.add(new SceneInfomation(1, "name9"));
            list.add(new SceneInfomation(1, "name10"));
        }
        return list;
    }


    private void getSceneList() {
        // TODO apservice 에서 리스트 가져오기
        // Apservice.getSceneList();
    }

    public void setposition(int _pos) {
        this.mDeletePosition = _pos;
    }

    public class SceneListAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private ArrayList<SceneInfomation> mSceneList;
        private Context mContext;

        SceneListAdapter(Context context, ArrayList<SceneInfomation> _scenelist) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mSceneList = _scenelist;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return mSceneList.size();
        }

        @Override
        public Object getItem(int position) {
            return mSceneList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null)
                view = mLayoutInflater.inflate(R.layout.item_list_view_scene_list, parent, false);
            RadioButton sceneName = (RadioButton) view.findViewById(R.id.radio_scenename);
            sceneName.setText(mSceneList.get(position).mSceneName);
            sceneName.setChecked(mSceneList.get(position).mCheckStat);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Toast.makeText(mContext, "position" + Integer.toString(position), Toast.LENGTH_SHORT).show();
                    for (SceneInfomation info : mSceneList) {
                        info.mCheckStat = false;
                    }
                    mSceneList.get(position).mCheckStat = true;
                    notifyDataSetChanged();
                }
            });
            return view;
        }

        public SceneInfomation getSelectedScene() {

            for (SceneInfomation info : mSceneList) {
                if (info.mCheckStat)
                    return info;
            }
            return null;
        }

        public void setDefaultCheck() {
            try {
                mSceneList.get(0).mCheckStat = true;
                notifyDataSetChanged();
            } catch (Exception e) {
                Log.e("error", "empty list");
            }
        }
    }

    public class SceneInfomation {
        public int mSceneNo;
        public String mSceneName;
        public boolean mCheckStat = false;

        public SceneInfomation(int _sceneno, String _name) {
            this.mSceneNo = _sceneno;
            this.mSceneName = _name;
        }
    }
}

