package com.rareraw.master.alarm;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.addscene.MoodListAdapter;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.AlarmInfo;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;


public class AddAlarmFragment extends BaseFragment implements View.OnClickListener {
    private ImageView mBtnSave;
    private TimePicker mTimePicker;

    private MoodListAdapter mAdapter;
    private ListView mMoodListView;
    private ArrayList<MoodListAdapter.moodInfomation> mMoodList = new ArrayList<>();
    private RelativeLayout mBtnsaveArlarm;
    private int mPickerHour, mPickerMin;
    private EditText mTextMin, mTextHour;
    private TextView mTextAm, mTextPm, mSelectedSceneName;
    private TextView mTextMon, mTextThu, mTextWed, mTextTue, mTextFri, mTextSat, mTextSun, mTextSave;
    private CheckBox mCheckRepeat;
    private ImageView mBtnHourUp, mBtnHourDown, mBtnMinUp, mBtnMinDown;
    private RelativeLayout mBtnSelectScene;
    private OnFragmentInteractionListener mListener;
    private Context mContext;
    private boolean MODIFY_FLAG = false; // false : new create, true : modify
    private View mParent;
    private String mPrevHour, mPrevMin;
    private boolean mTimeStatus = false; // false am, true pm
    private boolean[] mWeekArray = new boolean[7];
    private int mHourIndex = 0, mMinIndex = 0;
    private int mSelectedSceneNo = -1;
    private byte[] mAlarmKey = new byte[]{(byte) 0x00, (byte) 0x00};
    private final int MAX_HOUR_INDEX = 11, MAX_HOUR = 12;
    private final int MAX_MIN_INDEX = 59, MAX_MIN = 59;
    private final int MINIMUM_INDEX = 0, MIN_MIN = 0, MIN_HOUR = 1;
    private final byte ACTION_ON = 0x01;
    private AlarmInfo mModifyAlarmInfo;
    private int mHourArray[] = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    private int mMinArray[] = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
            40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59};
    private OnActionListener mOnActionListner = new OnActionListener() {
        @Override
        public void onActionListener(int _sceneNo, String _name) {
            mSelectedSceneNo = _sceneNo;
            mSelectedSceneName.setText(_name);
        }
    };

    public AddAlarmFragment() {
        // Required empty public constructor

    }

    public static AddAlarmFragment newInstance() {
        AddAlarmFragment fragment = new AddAlarmFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        mSelectedSceneNo = -1;
        setWeekData();
        setSelectMood();
    }

    private void setSelectMood() {

    }

    public void setModiftStatus(int _alarmKey) {
        if (_alarmKey == -1)
            MODIFY_FLAG = false;
        else {
            MODIFY_FLAG = true;
            mAlarmKey = CmdHelper.intToByteArray(_alarmKey);
        }
    }


    private void setWeekData() {
        if (MODIFY_FLAG) {
            getAlarmInfo(mAlarmKey);
        } else {
            mWeekArray[0] = false;
            mWeekArray[1] = false;
            mWeekArray[2] = false;
            mWeekArray[3] = false;
            mWeekArray[4] = false;
            mWeekArray[5] = false;
            mWeekArray[6] = false;
        }
    }

    private void getAlarmInfo(byte[] _Key) {
        mModifyAlarmInfo = CmdHelper.getAlarmSingle(_Key);
        if (mModifyAlarmInfo != null) {
            // am pm
            if (mModifyAlarmInfo.getIntAlarmTimeHour() > 12)
                mTimeStatus = true;
            else
                mTimeStatus = false;
            updateUi();

            // time
            try {
                String hour = "", min = "";
                /*if (mModifyAlarmInfo.getIntAlarmTimeHour() > 12)
                    hour = Integer.toString(mModifyAlarmInfo.getIntAlarmTimeHour() - 12);
                else
                    hour = Integer.toString(mModifyAlarmInfo.getIntAlarmTimeHour());
                mTextHour.setText(hour);
                mTextMin.setText(Integer.toString(mModifyAlarmInfo.getIntAlarmTimeMin()));*/
                mTimePicker.setCurrentHour(mModifyAlarmInfo.getIntAlarmTimeHour());
                mTimePicker.setCurrentMinute(mModifyAlarmInfo.getIntAlarmTimeMin());

                // week
                mWeekArray[0] = (mModifyAlarmInfo.getAlarmWeek()[0] == 0x01) ? true : false;
                mWeekArray[1] = (mModifyAlarmInfo.getAlarmWeek()[1] == 0x01) ? true : false;
                mWeekArray[2] = (mModifyAlarmInfo.getAlarmWeek()[2] == 0x01) ? true : false;
                mWeekArray[3] = (mModifyAlarmInfo.getAlarmWeek()[3] == 0x01) ? true : false;
                mWeekArray[4] = (mModifyAlarmInfo.getAlarmWeek()[4] == 0x01) ? true : false;
                mWeekArray[5] = (mModifyAlarmInfo.getAlarmWeek()[5] == 0x01) ? true : false;
                mWeekArray[6] = (mModifyAlarmInfo.getAlarmWeek()[6] == 0x01) ? true : false;
                updateWeekUi();

                // setscene info
                mSelectedSceneNo = mModifyAlarmInfo.getNSceneNo();
                mSelectedSceneName.setText(CmdHelper.getSceneName(mModifyAlarmInfo.getmBSceneNo()));

                for(MoodListAdapter.moodInfomation info : mMoodList){
                    if(info.mSceneNo == mSelectedSceneNo){
                        info.mCheckStat = true;
                    }
                }
                mAdapter.setMoodList(mMoodList);
                mAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void updateWeekUi() {
        if (mWeekArray[0]) {
            //  mTextMon.setTextColor(getResources().getColor(alarm_text_week_selected_color));

            mTextMon.setTextColor(getResources().getColor(R.color.whit_color));
            mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            // mTextMon.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));
            mTextMon.setTextColor(getResources().getColor(R.color.black_color));
            mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));


        }
        if (mWeekArray[1]) {
            // mTextTue.setTextColor(getResources().getColor(alarm_text_week_selected_color));
            mTextTue.setTextColor(getResources().getColor(R.color.whit_color));
            mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));


        } else {
            // mTextTue.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));
            mTextTue.setTextColor(getResources().getColor(R.color.black_color));
            mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

        }
        if (mWeekArray[2]) {
            //mTextWed.setTextColor(getResources().getColor(alarm_text_week_selected_color));

            mTextWed.setTextColor(getResources().getColor(R.color.whit_color));
            mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            //mTextWed.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));

            mTextWed.setTextColor(getResources().getColor(R.color.black_color));
            mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));


        }
        if (mWeekArray[3]) {
            // mTextThu.setTextColor(getResources().getColor(alarm_text_week_selected_color));
            mTextThu.setTextColor(getResources().getColor(R.color.whit_color));
            mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));

        } else {
            // mTextThu.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));

            mTextThu.setTextColor(getResources().getColor(R.color.black_color));
            mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

        }
        if (mWeekArray[4]) {
            // mTextFri.setTextColor(getResources().getColor(alarm_text_week_selected_color));

            mTextFri.setTextColor(getResources().getColor(R.color.whit_color));
            mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            // mTextFri.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));


            mTextFri.setTextColor(getResources().getColor(R.color.black_color));
            mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

        }
        if (mWeekArray[5]) {
            //mTextSat.setTextColor(getResources().getColor(alarm_text_week_selected_color));

            mTextSat.setTextColor(getResources().getColor(R.color.whit_color));
            mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            //mTextSat.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));

            mTextSat.setTextColor(getResources().getColor(R.color.black_color));
            mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

        }
        if (mWeekArray[6]) {
            // mTextSun.setTextColor(getResources().getColor(alarm_text_week_selected_color));

            mTextSun.setTextColor(getResources().getColor(R.color.whit_color));
            mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
        } else {
            //mTextSun.setTextColor(getResources().getColor(alarm_text_week_not_selected_color));
            mTextSun.setTextColor(getResources().getColor(R.color.black_color));
            mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mParent = inflater.inflate(R.layout.fragment_add_alarm, container, false);
        return mParent;
    }

    private void initMoodList() {
        mMoodList = APService.getMoodList();
        mAdapter = new MoodListAdapter(mContext, mMoodList, mOnActionListner);
        mMoodListView.setAdapter(mAdapter);
      //  mAdapter.setDefaultCheck();
        setListViewHeightBasedOnChildren(mMoodListView);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getViewInlayout();
        updateUi();
        initMoodList();
    }

    private void updateUi() {
        if(MODIFY_FLAG)
            mTextSave.setText("알람 수정");
        else
            mTextSave.setText("알람 추가");
      /*  if (mTimeStatus) {
            mTextAm.setBackgroundResource(R.drawable.add_alarm_round_not_selcted);
            mTextAm.setTextColor(getResources().getColor(R.color.alarm_add_not_selected_color));

            mTextPm.setBackgroundResource(R.drawable.add_alarm_round_selected);
            mTextPm.setTextColor(getResources().getColor(R.color.alarm_add_selected_color));
        } else {
            mTextAm.setBackgroundResource(R.drawable.add_alarm_round_selected);
            mTextAm.setTextColor(getResources().getColor(R.color.alarm_add_selected_color));

            mTextPm.setBackgroundResource(R.drawable.add_alarm_round_not_selcted);
            mTextPm.setTextColor(getResources().getColor(R.color.alarm_add_not_selected_color));
        }*/
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0,0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight+ (listView.getDividerHeight() * (listAdapter.getCount() - 1));

        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    private void getViewInlayout() {
        mMoodListView = (ListView) mParent.findViewById(R.id.listview_scene_list);
        mTimePicker = (TimePicker) mParent.findViewById(R.id.timepicker);
        mTimePicker.Create(getContext());
        mTimePicker.setOnTimeChangedListener(new android.widget.TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(android.widget.TimePicker view, int hourOfDay, int minute) {
                mPickerHour = hourOfDay;
                mPickerMin = minute;
            }
        });

        mSelectedSceneName = (TextView) mParent.findViewById(R.id.btn_selecet_scene);
        mSelectedSceneName = (TextView) mParent.findViewById(R.id.btn_selecet_scene);
        mBtnsaveArlarm = (RelativeLayout) mParent.findViewById(R.id.layer_btn_save);
        mBtnsaveArlarm.setOnClickListener(this);

        mTextMon = (TextView) mParent.findViewById(R.id.text_week_mon);
        mTextMon.setOnClickListener(this);

        mTextThu = (TextView) mParent.findViewById(R.id.text_week_thu);
        mTextThu.setOnClickListener(this);

        mTextWed = (TextView) mParent.findViewById(R.id.text_week_wed);
        mTextWed.setOnClickListener(this);

        mTextTue = (TextView) mParent.findViewById(R.id.text_week_tue);
        mTextTue.setOnClickListener(this);

        mTextFri = (TextView) mParent.findViewById(R.id.text_week_fri);
        mTextFri.setOnClickListener(this);

        mTextSat = (TextView) mParent.findViewById(R.id.text_week_sat);
        mTextSat.setOnClickListener(this);

        mTextSun = (TextView) mParent.findViewById(R.id.text_week_sun);
        mTextSun.setOnClickListener(this);

        mTextSave = (TextView) mParent.findViewById(R.id.text_save);
        // mCheckRepeat = (CheckBox) mParent.findViewById(R.id.checkbox_repeat);
        mBtnSelectScene = (RelativeLayout) mParent.findViewById(R.id.layout_select_scene);
        mBtnSelectScene.setOnClickListener(this);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     /*   if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mSelectedSceneNo = -1;
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        int value = 0;
        switch (view.getId()) {
            case R.id.layer_btn_save:
                registAlarm();
                break;
            case R.id.layout_select_scene:
                //Toast.makeText(mContext, "씬선택 팝업~~", Toast.LENGTH_SHORT).show();
                DialogSelectScene dialogSelectScene = new DialogSelectScene();
                dialogSelectScene.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light);
                dialogSelectScene.setListener(mOnActionListner);
                dialogSelectScene.show(getActivity().getFragmentManager(), "dialogSelectScene");
                break;
            case R.id.text_week_mon:
                if (!mWeekArray[0]) {
                    mTextMon.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[0] = true;
                } else {
                    mTextMon.setTextColor(getResources().getColor(R.color.black_color));
                    mTextMon.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[0] = false;
                }
                break;
            case R.id.text_week_tue:
                if (!mWeekArray[1]) {
                    mTextTue.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[1] = true;
                } else {
                    mTextTue.setTextColor(getResources().getColor(R.color.black_color));
                    mTextTue.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[1] = false;
                }
                break;
            case R.id.text_week_wed:
                if (!mWeekArray[2]) {
                    mTextWed.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[2] = true;
                } else {
                    mTextWed.setTextColor(getResources().getColor(R.color.black_color));
                    mTextWed.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[2] = false;
                }
                break;
            case R.id.text_week_thu:
                if (!mWeekArray[3]) {

                    mTextThu.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[3] = true;
                } else {
                    mTextThu.setTextColor(getResources().getColor(R.color.black_color));
                    mTextThu.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));

                    mWeekArray[3] = false;
                }
                break;
            case R.id.text_week_fri:
                if (!mWeekArray[4]) {
                    mTextFri.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[4] = true;
                } else {
                    mTextFri.setTextColor(getResources().getColor(R.color.black_color));
                    mTextFri.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[4] = false;
                }

                break;
            case R.id.text_week_sat:
                if (!mWeekArray[5]) {
                    mTextSat.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[5] = true;
                } else {
                    mTextSat.setTextColor(getResources().getColor(R.color.black_color));
                    mTextSat.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[5] = false;
                }
                break;
            case R.id.text_week_sun:
                if (!mWeekArray[6]) {
                    mTextSun.setTextColor(getResources().getColor(R.color.whit_color));
                    mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_accent));
                    mWeekArray[6] = true;
                } else {
                    mTextSun.setTextColor(getResources().getColor(R.color.black_color));
                    mTextSun.setBackground(getResources().getDrawable(R.drawable.background_all_round_white));
                    mWeekArray[6] = false;
                }
                break;
        }
    }

    public void registAlarm() {

        MoodListAdapter.moodInfomation info = mAdapter.getSelectedScene();
        //if (mSelectedSceneNo == -1) {
        if (info == null) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_scene), com.rareraw.master.Common.Toast.LENGTH_SHORT);
            return;
        }

        if (!isSelectWeek()) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_non_select_week), com.rareraw.master.Common.Toast.LENGTH_SHORT);
            return;
        }
        // TODO check alarm regist
        ByteBuffer buffer = null;
        if (MODIFY_FLAG)
            buffer = ByteBuffer.allocate(Cmd.ALARM_MODIFY_DATA_SIZE);
        else
            buffer = ByteBuffer.allocate(Cmd.ALARM_DATA_SIZE);
        try {

            byte hour = (byte) getHourByte();
            byte min = (byte) getMinByte();
            byte[] week = getWeekByte();
            byte[] sceneNo = CmdHelper.intToByteArray(mSelectedSceneNo);
            byte action = ACTION_ON;
            // byte repeat = (mCheckRepeat.isChecked() == true) ? (byte) 0x01 : (byte) 0x00;
            Log.e("Hour", Byte.toString(hour));
            Log.e("Min", Byte.toString(min));

            Log.e("scene no", Byte.toString(sceneNo[0]));
            Log.e("scene no", Byte.toString(sceneNo[1]));
            Log.e("action", Byte.toString(action));
            // Log.e("repeat", Boolean.toString(mCheckRepeat.isChecked()));
            if (MODIFY_FLAG)
                buffer.put(mAlarmKey);
            buffer.put(hour);
            buffer.put(min);
            buffer.put(week);
            buffer.put(sceneNo);
            buffer.put(action);

            if (!MODIFY_FLAG) {
                APService.registerAlram(buffer, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        if (Config.DEBUG)
                            Toast.makeText(getActivity(), "알람이 등록되었습니다.", Toast.LENGTH_SHORT).show();

                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onError(int errorCode) {
                        try {
                            if (Config.DEBUG)
                                Toast.makeText(getActivity(), "알람 등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();

                    }
                });
            } else {
                APService.modifyAlram(buffer, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        if (Config.DEBUG)
                            Toast.makeText(getActivity(), "알람이 등록되었습니다.", Toast.LENGTH_SHORT).show();

                        getActivity().onBackPressed();
                    }

                    @Override
                    public void onError(int errorCode) {
                        try {
                            if (Config.DEBUG)
                                Toast.makeText(getActivity(), "알람 등록에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();

                    }
                });
            }


        } catch (Exception e) {
            Logout();
            if (Config.DEBUG)
                Toast.makeText(getActivity(), "Alarm registration failed.", Toast.LENGTH_SHORT).show();
            onFragmentCallbackListener.onFragmentCallback(getTag(), -1);
            e.printStackTrace();
        }
    }

    private int getMinByte() {
        return mPickerMin;
   /*     if (mTextMin.getText().toString().length() == 0) {
            return Integer.parseInt(mPrevMin.toString());
        } else
            return Integer.parseInt(mTextMin.getText().toString());*/
    }

    private boolean isSelectWeek() {
        boolean flag = false;
        if (mWeekArray[0])
            flag = true;
        if (mWeekArray[1])
            flag = true;
        if (mWeekArray[2])
            flag = true;
        if (mWeekArray[3])
            flag = true;
        if (mWeekArray[4])
            flag = true;
        if (mWeekArray[5])
            flag = true;
        if (mWeekArray[6])
            flag = true;


        if (flag)
            return true;
        else
            return false;
    }

    private int getHourByte() {
        return mPickerHour;
    }

    private byte[] getWeekByte() {
        byte[] buffer = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
        if (mWeekArray[0])
            buffer[0] = 0x01;
        if (mWeekArray[1])
            buffer[1] = 0x01;
        if (mWeekArray[2])
            buffer[2] = 0x01;
        if (mWeekArray[3])
            buffer[3] = 0x01;
        if (mWeekArray[4])
            buffer[4] = 0x01;
        if (mWeekArray[5])
            buffer[5] = 0x01;
        if (mWeekArray[6])
            buffer[6] = 0x01;

        for (int i = 0; i < 7; i++) {
            Log.e("week", Byte.toString(buffer[i]));
        }
        return buffer;
    }

    private void updateTimeUi() {
        try {
            mTextHour.setText(String.format("%02d", mHourArray[mHourIndex]));
            mTextMin.setText(String.format("%02d", mMinArray[mMinIndex]));
        } catch (Exception e) {
            Log.e("error", "time ui update!");
        }
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
