package com.rareraw.master.alarm;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.widget.NumberPicker;

import java.lang.reflect.Field;

public class TimePicker extends android.widget.TimePicker {
    private final int m_iColor = 0x00000000;
    public TimePicker(Context context) {
        super(context);
    }

    public TimePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    public void Create( Context clsContext)
    {
        try
        {
            Class<?> clsParent = Class.forName( "com.android.internal.R$id" );
            NumberPicker clsAmPm = (NumberPicker)findViewById( clsParent.getField( "amPm" ).getInt( null ) );

            NumberPicker clsHour = (NumberPicker)findViewById( clsParent.getField( "hour" ).getInt( null ) );
            NumberPicker clsMin = (NumberPicker)findViewById( clsParent.getField( "minute" ).getInt( null ) );
            Class<?> clsNumberPicker = Class.forName( "android.widget.NumberPicker" );
            Field clsSelectionDivider = clsNumberPicker.getDeclaredField( "mSelectionDivider" );

            clsSelectionDivider.setAccessible( true );
            ColorDrawable clsDrawable = new ColorDrawable( m_iColor );
            clsSelectionDivider.set( clsAmPm, clsDrawable );
            clsSelectionDivider.set( clsHour, clsDrawable );
            clsSelectionDivider.set( clsMin, clsDrawable );
        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }
    }


}
