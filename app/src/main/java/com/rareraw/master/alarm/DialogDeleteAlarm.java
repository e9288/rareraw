package com.rareraw.master.alarm;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogDeleteAlarm extends DialogFragment {

    private OnActionListener mListner;
    private LinearLayout mBtnCancel, mBtnOk;
    private View mParent;

    public DialogDeleteAlarm() {
    }

    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_delete_alarm, container, false);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnCancel = (LinearLayout) mParent.findViewById(R.id.dialog_delete_alarm_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (LinearLayout) mParent.findViewById(R.id.dialog_delete_alarm_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListner.onActionListener(1,"delete");
                dismiss();
            }
        });
    }
}

