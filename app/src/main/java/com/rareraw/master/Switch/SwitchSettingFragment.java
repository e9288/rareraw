package com.rareraw.master.Switch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.DataClass.SwitchInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;

public class SwitchSettingFragment extends BaseFragment implements View.OnClickListener {
    private static String TAG = "SwitchSetting";
    private TextView mTextSwitch1, mTextSwitch2, mTextSwitch3, mTextSwitch4;
    private static Context mContext;
    private DialogSelectScene mDialogSelectScene;
    private int NOW_SWITCH_INDEX = 0;
    private SwitchInfo mSwitchInfo = null;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    private void updateUi() {
        getRegSwitchList();
    }

    private void getRegSwitchList() {

        if (Config.DEBUG) {
            getTestData();
        } else {

        }
    }

    private void getTestData() {

    }

    private OnActionListener mOnActionListner = new OnActionListener() {
        @Override
        public void onActionListener(int _sceneNo, String _name) {

            int index = 0;
            if(mDialogSelectScene != null){
                if(mDialogSelectScene.getTag().equals("switch1")){
                    mTextSwitch1.setText(_name);
                    index = 0;
                }else if(mDialogSelectScene.getTag().equals("switch2")){
                    mTextSwitch2.setText(_name);
                    index = 1;
                }else if(mDialogSelectScene.getTag().equals("switch3")){
                    mTextSwitch3.setText(_name);
                    index = 2;
                }else if(mDialogSelectScene.getTag().equals("switch4")){
                    mTextSwitch4.setText(_name);
                    index = 3;
                }
                try {
                    if(mSwitchInfo == null) {
                        Log.e("empty switch info","error");
                        return;
                    }

                    ByteBuffer buffer = ByteBuffer.allocate(8 + 1 + 2 ); // mac 8 byte + index 1byte + scene 2byte
                    buffer.put(mSwitchInfo.getByteSwitchMac());
                    buffer.put(new byte[]{(byte) index});
                    buffer.put(CmdHelper.intToByteArray(_sceneNo));

                    APService.setSwitchButtonWithScene(buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            Log.e("setSwitchButton", "ok");
                        }

                        @Override
                        public void onError(int errorCode) {
                            Log.e("setSwitchButton", "error");
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Log.e("setSwitchButton", "error");
                            Logout();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("setSwitchButton", "error");
                    Logout();
                }
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_switch_setting, container, false);

        mTextSwitch1 = (TextView) view.findViewById(R.id.text_swich1);
        mTextSwitch1.setOnClickListener(this);

        mTextSwitch2 = (TextView) view.findViewById(R.id.text_swich2);
        mTextSwitch2.setOnClickListener(this);

        mTextSwitch3 = (TextView) view.findViewById(R.id.text_swich3);
        mTextSwitch3.setOnClickListener(this);

        mTextSwitch4 = (TextView) view.findViewById(R.id.text_swich4);
        mTextSwitch4.setOnClickListener(this);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<SwitchInfo>  switchInfos = APService.getApSwitchInfoList();

        try{
            mSwitchInfo =  switchInfos.get(NOW_SWITCH_INDEX);
            if(mSwitchInfo != null){
                if(CmdHelper.byteArrayToInt(mSwitchInfo.getSceneList().get(0)) == 0)
                    mTextSwitch1.setText(R.string.default_scene_name);
                    // mTextSwitch1.setText(CmdHelper.getSceneName(new byte[]{(byte) 0x00, (byte) 0x01}));
                else
                    mTextSwitch1.setText(CmdHelper.getSceneName(mSwitchInfo.getSceneList().get(0)));

                if(CmdHelper.byteArrayToInt(mSwitchInfo.getSceneList().get(1)) == 0)
                    mTextSwitch2.setText(R.string.default_scene_name);
                    //mTextSwitch2.setText(CmdHelper.getSceneName(new byte[]{(byte) 0x00, (byte) 0x02}));
                else
                    mTextSwitch2.setText(CmdHelper.getSceneName(mSwitchInfo.getSceneList().get(1)));

                if(CmdHelper.byteArrayToInt(mSwitchInfo.getSceneList().get(2)) == 0)
                    mTextSwitch3.setText(R.string.default_scene_name);
                    // mTextSwitch3.setText(CmdHelper.getSceneName(new byte[]{(byte) 0x00, (byte) 0x03}));
                else
                    mTextSwitch3.setText(CmdHelper.getSceneName(mSwitchInfo.getSceneList().get(2)));

                if(CmdHelper.byteArrayToInt(mSwitchInfo.getSceneList().get(3)) == 0)
                    mTextSwitch4.setText(R.string.default_scene_name);
                    // mTextSwitch4.setText(CmdHelper.getSceneName(new byte[]{(byte) 0x00, (byte) 0x04}));
                else
                    mTextSwitch4.setText(CmdHelper.getSceneName(mSwitchInfo.getSceneList().get(3)));
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {

        mDialogSelectScene = new DialogSelectScene();
        mDialogSelectScene.setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light );
        mDialogSelectScene.setListener(mOnActionListner);

        switch (v.getId()){
            case R.id.text_swich1:
                mDialogSelectScene.show(getActivity().getFragmentManager(), "switch1");
                break;

            case R.id.text_swich2:
                mDialogSelectScene.show(getActivity().getFragmentManager(), "switch2");
                break;

            case R.id.text_swich3:
                mDialogSelectScene.show(getActivity().getFragmentManager(), "switch3");
                break;

            case R.id.text_swich4:
                mDialogSelectScene.show(getActivity().getFragmentManager(), "switch4");
                break;

        }
    }

    public void setNowSwitchIndex(int response) {
        this.NOW_SWITCH_INDEX = response;
    }
}
