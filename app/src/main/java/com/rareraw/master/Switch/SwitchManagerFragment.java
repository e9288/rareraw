package com.rareraw.master.Switch;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.DataClass.SwitchInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.ArrayList;

public class SwitchManagerFragment extends BaseFragment implements OnSwitchStatusChangedListener {
    private static String TAG = "SwitchManager";
    ArrayList<SwitchInfo> mSwitchList = new ArrayList<SwitchInfo>();
    SwitchListAdapter mSwtichListAdapter;
    private static Context mContext;
    private ListView listView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        mSwtichListAdapter = new SwitchListAdapter(getContext(), mSwitchList, this);

    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();
    }

    private void updateUi() {
        getRegSwitchList();
    }

    private void getRegSwitchList() {
        final ArrayList<SwitchInfo> list = new ArrayList<>();
        if (Config.DEBUG) {
            getTestData();
        } else {
            try {
                APService.getSwitchInfo(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                        mSwitchList =  (ArrayList<SwitchInfo>) retValue;
                        boolean oldLevel = APService.getUserLevel();
                        APService.setUserLevel(true);

                        mSwtichListAdapter.setSwitchList(mSwitchList);

                        mSwtichListAdapter.notifyDataSetChanged();
                        //     list.get(i).sceneName = CmdHelper.getSceneName(CmdHelper.intToByteArray(list.get(i).index));
                        APService.setUserLevel(oldLevel);

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });

            } catch (Exception e) {

            }
        }
    }

    private void getTestData() {

        if(mSwitchList != null){
            for(int i =0; i <mSwitchList.size() ;i++)
                mSwitchList.remove(i);
        }


        SwitchInfo switch1 = new SwitchInfo();
        switch1.setSceneNo(0);
        byte[] data1 = new byte[6];
        data1[0] = (byte)1; data1[1] = (byte)2; data1[2] = (byte)3; data1[3] = (byte)4;
        switch1.setByteSwitchMac(data1);
        mSwitchList.add(switch1);

        SwitchInfo switch2 = new SwitchInfo();
        switch1.setSceneNo(1);
        byte[] data2 = new byte[6];
        data2[0] = (byte)4; data2[1] = (byte)3; data2[2] = (byte)2; data2[3] = (byte)1;
        switch2.setByteSwitchMac(data2);
        mSwitchList.add(switch2);

        mSwtichListAdapter.setSwitchList(mSwitchList);
        mSwtichListAdapter.notifyDataSetChanged();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_switch, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listView = (ListView) view.findViewById(R.id.listview_switch);
        listView.setAdapter(mSwtichListAdapter);
    }

    @Override
    public void onSwitchStatusChangedListener(int index, int type) {
        // TODO jump switch 4 type page

/*   try {
            APService.deleteDevice(mSwitchList.get(index).getSceneNo() , new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    updateUi();
                }

                @Override
                public void onError(int errorCode) {

                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Logout();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Logout();
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class SwitchListAdapter extends BaseAdapter implements View.OnClickListener {
        private OnSwitchStatusChangedListener onSwitchChangedListener;
        private LayoutInflater mLayoutInflater;
        private ArrayList<SwitchInfo> dataSet;
        private int type;

        SwitchListAdapter(Context context, ArrayList<SwitchInfo> data, OnSwitchStatusChangedListener listener) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.dataSet = data;
            this.type = type;
            this.onSwitchChangedListener = listener;
        }

        public void setSwitchList(ArrayList<SwitchInfo> _dataSet) {
            dataSet = _dataSet;
        }

        @Override
        public int getCount() {
            return dataSet.size();
        }

        @Override
        public Object getItem(int position) {
            return dataSet.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            switchHolder holder = new switchHolder();
            if (convertView == null) {
                convertView = mLayoutInflater.inflate(R.layout.item_list_view_switch_manager, parent, false);
                holder.switchName = (TextView) convertView.findViewById(R.id.item_list_view_switch_manager_text_name);
                try {


                }catch (Exception e){
                    e.printStackTrace();
                }

            } else
                holder = (switchHolder) convertView.getTag();

            // holder.switchName.setText(dataSet.get(position).getSwitchMac());

            holder.switchName.setText(dataSet.get(position).getSwitchMac());

            convertView.setOnClickListener(this);
            convertView.setTag(R.id.id_select_switch_position, position);
            convertView.setTag(holder);
            return convertView;
        }

        @Override
        public void onClick(View v) {
            int position = (int) v.getTag(R.id.id_select_switch_position);
            Log.d(TAG , Integer.toString(position));
            onFragmentCallbackListener.onFragmentCallback(getTag(), position);
            // onSwitchChangedListener.onSwitchStatusChangedListener(dataSet.get(position).getSeqNo(), dataSet.get(position).getSceneNo());
        }
    }

    static class switchHolder {

        TextView switchName;
    }
}

interface OnSwitchStatusChangedListener {
    void onSwitchStatusChangedListener(int index, int type);
}