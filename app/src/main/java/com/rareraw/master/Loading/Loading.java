package com.rareraw.master.Loading;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.DhcpInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.widget.TextView;


import com.rareraw.master.Common.DialogNotification;
import com.rareraw.master.Common.ErrorManager;
import com.rareraw.master.Config;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.CmdHelper;
import com.rareraw.master.network.NetworkManager;
import com.rareraw.master.network.NetworkResultLisnter;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static com.rareraw.master.network.Cmd.CMD_BROADCASR;
import static com.rareraw.master.network.Cmd.EMPTY_DATA;


/**
 * Created by lch on 2016-10-02.
 */

public class Loading extends Activity {
    private boolean mNetStat = false;
    private TextView mTextVersion;
    private Context mContext;
    private final String APP_MARKET_ADD = "https://play.google.com/store/apps/details?id=com.nexon.nsc.maplem&hl=ko";
    public APService test;
    private final int REQUEST_N_PERMISSION = 33, REQUEST_CONNECT_FAIL = 44, REQUEST_UPDATE = 45;
    private Runnable mRunnable, mNetRunnable, mExitRunnable, mConnectFail;
    private Handler mHandler = new Handler();
    private long DELAY_MILITIME = 2000, DELAY_1400 = 1400;
    private String deviceVersion;
    private static final int UPDATE = 0, NON_UPDATE = 1;
    private String mCurrnetVersion = "";
    DatagramSocket mSocket = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(android.R.style.Theme_Holo_Light_NoActionBar_TranslucentDecor);
        setContentView(R.layout.activity_loading2);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mContext = getApplicationContext();
        setVersionInfo();

    }

    @Override
    public void onStart() {
        super.onStart();
        initData();
    }

    private void initData() {
        setLoadingDelay();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // android.widget.Toast.makeText(mContext, "1111111111111111111111111", Toast.LENGTH_SHORT).show();
                getFeelMasterInfo();
            }
        }, 000);
    }

    private void receiveBroadCast() {
        mHandler.postDelayed(new Runnable() {
            public void run() {
                int server_port = 40300;
                byte[] message = new byte[1200];
                try {
                    DatagramPacket p = new DatagramPacket(message, message.length);
                    // DatagramSocket s = new DatagramSocket(server_port);


                    mSocket = new DatagramSocket(null);
                    mSocket.setReuseAddress(true);
                    mSocket.setBroadcast(true);
                    mSocket.bind(new InetSocketAddress(server_port));


                    mSocket.receive(p);
                    String sIp = p.getAddress().toString();
                    sIp = sIp.substring(1, sIp.length());
                    Log.d("rockman", "message:" + p.getAddress().toString());
                    Log.d("rockman", "message:" + p.getData().toString());
                    NetworkManager.getInstance().setWifipAddr(sIp);


                    mSocket.close();
            /*        mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //android.widget.Toast.makeText(mContext, "1111111111111111111111111", Toast.LENGTH_SHORT).show();
                        }
                    },200);*/
                    // getFeelMasterInfo();
                } catch (Exception e) {
                    Log.d("rockman", "error  " + e.toString());
                    mHandler.postDelayed(mExitRunnable, DELAY_1400);
                }
            }
        }, 000);
    }


    public void sendBroadcast() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {

            //Open a random port to send the package
            DatagramSocket socket = new DatagramSocket();
            socket.setBroadcast(true);

            ByteBuffer mDefaultConnectionId = ByteBuffer.allocate(4);
            ByteBuffer sendMsg = CmdHelper.makeMessage(CMD_BROADCASR, mDefaultConnectionId, EMPTY_DATA);

            byte[] sendData = sendMsg.array();

            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, getBroadcastAddress(), 40200);
            socket.send(sendPacket);
            System.out.println(getClass().getName() + "Broadcast packet sent to: " + getBroadcastAddress().getHostAddress());
        } catch (IOException e) {
            Log.e("send err", "IOException: " + e.getMessage());
        }
    }

    private InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
            quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    }

   /* private void AppUpdate() {
        try {
            Intent intent = null;
            intent = new Intent(mContext, DialogNotification.class);
            intent.putExtra(DialogNotification.TITLE, getString(R.string.msg_update));
            intent.putExtra(DialogNotification.CONTENTS, getString(R.string.ask_update));
            intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.CINFORM);
            startActivityForResult(intent, REQUEST_UPDATE);
        } catch (Exception e) {

        }
    }
*/

    private void getFeelMasterInfo() {

        APService.getInstance(getBaseContext());
        NetworkManager.getInstance().setContext(mContext);
        // NetworkManager.getInstance().getWifipAddr();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
        Date currentTime = new Date();
        String dTime = formatter.format(currentTime);
        if (Config.DEBUG) {
            mHandler.postDelayed(mRunnable, DELAY_1400);
            return;
        }

        // NetworkManager.getInstance().getIpAddress();
        APService.connectRequst(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

                APService.getRegistedDeviceInfo(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        APService.synchronizeTime(getCurrentTime(), new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {

                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });

                        APService.getUnRegistedDeviceInfo(new NetworkResultLisnter() {

                            @Override
                            public void onSuccress(Object retValue) {
                            }

                            @Override
                            public void onError(int errorCode) {
                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {
                            }
                        });

                        // 20200426 dmx 값 가져오는거 없어짐 (rareraw 버전 )
                      /*  APService.getDmxUsage(new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {

                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });*/

                        APService.getNetworkMode(new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {
                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });

                        ErrorManager.getInstance().setContext(getApplicationContext());
                        Log.e("welcom to feelmaster", "connect ok");
                        mHandler.postDelayed(mRunnable, DELAY_1400);
                    }

                    @Override
                    public void onError(int errorCode) {
                        ErrorManager.getInstance().setContext(getApplicationContext());
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        ErrorManager.getInstance().setContext(getApplicationContext());
                    }
                });
            }

            @Override
            public void onError(int errorCode) {
                mHandler.postDelayed(mExitRunnable, DELAY_MILITIME);
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                mHandler.postDelayed(mExitRunnable, DELAY_MILITIME);
            }
        });
    }

    private String getCurrentTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss", Locale.KOREA);
        Date currentTime = new Date();
        String dTime = formatter.format(currentTime);
        return dTime;
    }

    private void setVersionInfo() {
        mTextVersion = (TextView) findViewById(R.id.loading_textview_version);
        PackageInfo pi = null;

        try {
            String version = this.getPackageName();
            pi = this.getPackageManager().getPackageInfo(version, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }

        mCurrnetVersion = "V " + pi.versionName;
        mTextVersion.setText(mCurrnetVersion);
    }

    private void setLoadingDelay() {
        mRunnable = new Runnable() {
            @Override
            public void run() {

           /*     if (!checkTutorial()) {
                    mNetStat = true;
                    Intent intent = new Intent(mContext, TutorialManager.class);
                    startActivity(intent);
                    finish();
                } else {*/
                mNetStat = true;
                Intent intent = new Intent(mContext, ContainActivity.class);
                startActivity(intent);
                finish();
                //}
            }
        };

        mExitRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                intent = new Intent(mContext, DialogNotification.class);
                intent.putExtra(DialogNotification.TITLE, getString(R.string.connect_feelmaster_fail_title_msg));
                intent.putExtra(DialogNotification.CONTENTS, getString(R.string.connect_feelmaster_fail_content_msg));
                intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.WARNING);
                startActivityForResult(intent, REQUEST_CONNECT_FAIL);
            }
        };
        mConnectFail = new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                intent = new Intent(mContext, DialogNotification.class);
                intent.putExtra(DialogNotification.TITLE, getString(R.string.connect_feelmaster_fail_title_msg));
                intent.putExtra(DialogNotification.CONTENTS, getString(R.string.connect_feelmaster_fail_content_msg));
                intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.WARNING);
                startActivityForResult(intent, REQUEST_CONNECT_FAIL);
            }
        };

        mNetRunnable = new Runnable() {
            @Override
            public void run() {
                Intent intent = null;
                intent = new Intent(mContext, DialogNotification.class);
                intent.putExtra(DialogNotification.TITLE, getString(R.string.connect_network_title_msg));
                intent.putExtra(DialogNotification.CONTENTS, getString(R.string.connect_network_fail_content_msg));
                intent.putExtra(DialogNotification.IMAGE_TYPE, DialogNotification.CINFORM);
                startActivityForResult(intent, REQUEST_N_PERMISSION);
            }
        };
    }

    private boolean checkTutorial() {
        return Prf.readToSharedPreferences(mContext, Prf.TUTORIAL_OK_FILE, Prf.TUTORIAL_OK_TAG, false);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Intent intent;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_N_PERMISSION:
                    intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;
                case REQUEST_CONNECT_FAIL:
                    intent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                    break;
                case REQUEST_UPDATE:
                    Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                    marketLaunch.setData(Uri.parse(APP_MARKET_ADD));
                    try {
                        startActivity(marketLaunch);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        finish();
                    }
                    break;
            }
        }
        if (resultCode == RESULT_CANCELED) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        Log.i("test", "onDstory()");
        if (mNetStat)
            mHandler.removeCallbacks(mRunnable);
        else {
            mHandler.removeCallbacks(mExitRunnable);
            mHandler.removeCallbacks(mNetRunnable);
            mHandler.removeCallbacks(mConnectFail);
        }
        super.onDestroy();
    }

    public class VersionChecker extends AsyncTask<String, String, Boolean> {

        String newVersion;

        @Override
        protected Boolean doInBackground(String... params) {
            String currentVersion = params[0];
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + "com.springsunsoft.unodiary2" + "&hl=en")
                        .timeout(3000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
            } catch (IOException e) {

                e.printStackTrace();
                return false;
            }
            if (newVersion == null || currentVersion == null)
                return false;

            if (newVersion.equals(currentVersion))
                return false;
            else
                return false; // app update


        }
    }
}