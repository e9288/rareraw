package com.rareraw.master.Loading;

import android.util.Log;

import com.rareraw.master.network.NetworkManager;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;

/**
 * Created by wspark on 2018-04-06.
 */

public class BroadCastReceiver implements  Runnable{
    public BroadCastReceiver( ){
    }
    DatagramSocket mSocket;
    @Override
    public void run() {
        int server_port = 40300;
        byte[] message = new byte[1200];
        try {
            DatagramPacket p = new DatagramPacket(message, message.length);
            // DatagramSocket s = new DatagramSocket(server_port);

            if (mSocket == null) {
                mSocket = new DatagramSocket(null);
                mSocket.setReuseAddress(true);
                mSocket.setBroadcast(true);
                mSocket.bind(new InetSocketAddress(server_port));
            }

            mSocket.receive(p);
            String sIp = p.getAddress().toString();
            sIp = sIp.substring(1, sIp.length());
            Log.d("rockman", "message:" + p.getAddress().toString());
            Log.d("rockman", "message:" + p.getData().toString());
            NetworkManager.getInstance().setWifipAddr(sIp);

 /*           Message message2 = Message.obtain(handler, OK_CODE);
            message2.obj = "";
            message2.sendToTarget();*/
            // getFeelMasterInfo();
            mSocket.close();

        } catch (Exception e) {
            Log.d("rockman", "error  " + e.toString());
        }
    }
}
