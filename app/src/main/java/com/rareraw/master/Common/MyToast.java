package com.rareraw.master.Common;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.neoromax.feelmaster.R;

/**
 * Created by lch on 2017-02-01.
 */
public class MyToast extends Toast {
    Context mContext;
    private static MyToast ourInstance = null;

    public static MyToast getInstance(Context _context) {
        if (ourInstance == null)

            synchronized (MyToast.class) {
                ourInstance = new MyToast(_context);
            }
        return ourInstance;
    }

    private MyToast(Context _context) {
        super(_context);
        mContext = _context;
    }

    public void showToast(String body, int duration) {
        if (mContext == null)
            return;

        int timeLength = 0;
        if (duration == LENGTH_SHORT)
            timeLength = LENGTH_SHORT;
        if (duration == LENGTH_LONG)
            timeLength = LENGTH_LONG;
        LayoutInflater inflater;
        View v;
        if (false) {
            Activity act = (Activity) mContext;
            inflater = act.getLayoutInflater();
            v = inflater.inflate(R.layout.toast_layout, null);
        } else {  // same
            inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.toast_layout, null);
        }
        TextView text = (TextView) v.findViewById(R.id.toast_text);
        text.setText(body);

        show(this, v, timeLength);
    }

    private void show(android.widget.Toast toast, View v, int duration) {
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(duration);
        toast.setView(v);
        toast.show();
    }
}
