package com.rareraw.master.Common;

/**
 * Created by lch on 2016-12-11.
 */

public interface OnSceneEditListener {
    void onSceneEditListener(int nValue1, int nValue2, int nValue3);
}
