package com.rareraw.master.Common;

import com.rareraw.master.data.SimpleSceneInfo;

/**
 * Created by lch on 2016-12-11.
 */

public interface OnSelecetedSceneInfo {
    void onSelectedSceneInfo(SimpleSceneInfo _info, int _sceneType);
}
