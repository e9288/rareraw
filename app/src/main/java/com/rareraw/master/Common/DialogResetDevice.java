package com.rareraw.master.Common;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogResetDevice extends DialogFragment {

    private OnActionListener mListner;
    private LinearLayout mBtnCancel, mBtnOk;
    private TextView mContent;
    private View mParent;
    private EditText mPassword;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private OnActionListener mResetListener = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {
            APService.resetDevice(new NetworkResultLisnter() {
                @Override
                public void onSuccress(Object retValue) {
                    Log.e("ok ", "reset");
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_complete_reset_device), Toast.LENGTH_SHORT);
                }

                @Override
                public void onError(int errorCode) {
                    Log.e("error ", "reset");
                    dismiss();
                }

                @Override
                public void onErrorConnectionId(int errorCode) {
                    Log.e("error ", "reset");
                    dismiss();
                }
            });
        }
    };

    public DialogResetDevice() {
    }

    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_reset_device, container, false);
        //mPassword = (EditText) mParent.findViewById(R.id.dialog_reset_password);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnCancel = (LinearLayout) mParent.findViewById(R.id.dialog_reset_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (LinearLayout) mParent.findViewById(R.id.dialog_reset_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                APService.resetDevice(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        Log.e("ok ", "reset");
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dismiss();
                            }
                        }, 1400);
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_complete_reset_device), Toast.LENGTH_SHORT);
                    }

                    @Override
                    public void onError(int errorCode) {
                        Log.e("error ", "reset");
                        dismiss();
                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        dismiss();
                        Log.e("error ", "reset");
                    }
                }) ;
                dismiss();

            }
        });
    }
}

