package com.rareraw.master.Common;

import android.content.Context;

import com.neoromax.feelmaster.R;
import com.rareraw.master.network.Cmd;

/**
 * Created by lch on 2017-02-10.
 */

public class ErrorManager {
    private Context mContext;
    private static ErrorManager _instance = null;
    public static ErrorManager getInstance() {
        if (_instance == null) {
            synchronized (ErrorManager.class) {
                _instance = new ErrorManager();
            }
        }
        return _instance;
    }


    public void setContext(Context context){
        this.mContext = context;
    }
    private void printError(byte bErrorCode){

        if(mContext == null)
            return;

        switch (bErrorCode){
            case Cmd.FAIL_CODE:
                MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_connect_fail), Toast.LENGTH_SHORT);
                break;
            case Cmd.CONNECT_ID_NOT_MATCH:
                MyToast.getInstance(mContext).showToast(mContext.getString(R.string.msg_retry_connect), Toast.LENGTH_SHORT);
                break;
        }
    }
}
