package com.rareraw.master.Common;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;

public class DialogNotification extends Activity {
    private TextView mTitle, mContents ;
    private LinearLayout mBtnOk , mBtnCancel;
    private ImageView  mTitleImage;
    public final static String TITLE = "title", CONTENTS = "contents", IMAGE_TYPE = "imagetype";
    public final static int CINFORM = 0, ERROR = 1, WARNING = 2, NOTIFICATION = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_notification);
        String sTitle = getIntent().getStringExtra(TITLE);
        String sContents = getIntent().getStringExtra(CONTENTS);

       // mTitleImage = (ImageView) findViewById(R.id.notification_title_image);

        mTitle = (TextView) findViewById(R.id.notification_title);
        mTitle.setText(sTitle);

        mContents = (TextView) findViewById(R.id.notification_content);
        mContents.setText(sContents);
        setTitleImage(getIntent().getIntExtra(IMAGE_TYPE, 0));

        mBtnOk = (LinearLayout) findViewById(R.id.dialog_notification_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_OK);
                finish();
            }
        });

        mBtnCancel = (LinearLayout) findViewById(R.id.dialog_notification_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setTitleImage(int intExtra) {
        switch (intExtra) {
            case CINFORM:
              //  mTitleImage.setImageResource(R.mipmap.icon_notication_alter);
                break;
            case ERROR:
             //   mTitleImage.setImageResource(R.mipmap.icon_error_alter);
                break;
            case WARNING:
              //  mTitleImage.setImageResource(R.mipmap.icon_waring_alter);
                break;
            case NOTIFICATION:
             //   mTitleImage.setImageResource(R.mipmap.icon_notication_alter);
                break;
        }
    }
}
