package com.rareraw.master.Common;

/**
 * Created by lch on 2016-12-11.
 */

public interface OnRenameListener {
    void onActionRename(byte[] nValue, String sValue);
    void onCompleteRename(int nValue, String sValue);
}
