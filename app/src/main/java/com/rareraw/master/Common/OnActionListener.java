package com.rareraw.master.Common;

/**
 * Created by lch on 2016-12-11.
 */

public interface OnActionListener {
    void onActionListener(int nValue, String sValue);
}
