package com.rareraw.master.Common;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.rareraw.master.Config;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogChangePw extends DialogFragment {

    private OnActionListener mListner;
    private TextView mBtnCancel, mBtnOk;
    private EditText mNewPassword1, mNewPassword2, mOldPassword;
    private View mParent;

    public DialogChangePw() {
    }

    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_change_password, container, false);
        mNewPassword1 = (EditText) mParent.findViewById(R.id.dialog_change_new_password1);
        mNewPassword2 = (EditText) mParent.findViewById(R.id.dialog_change_new_password2);
        mOldPassword = (EditText) mParent.findViewById(R.id.dialog_change_old_password);
        mBtnOk = (TextView) mParent.findViewById(R.id.dialog_change_ok);
        mBtnCancel = (TextView) mParent.findViewById(R.id.dialog_change_cancel);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {

                    String sCurrentPassword = mOldPassword.getText().toString();
                    String sNewPassword = mNewPassword1.getText().toString();
                    String sConfirmPassword = mNewPassword2.getText().toString();

                    if (sCurrentPassword.length() == 0 || sNewPassword.length() == 0 || sConfirmPassword.length() == 0) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }

                    if (!sConfirmPassword.equals(sNewPassword)) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_password), Toast.LENGTH_SHORT);
                        return;
                        //Toast.makeText(getActivity(), "The new password does not match.", Toast.LENGTH_SHORT).show();
                    }
                    if (sNewPassword.getBytes().length < 4) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }
                    if (sNewPassword.getBytes().length > Config.PASSWORD_MAX_LENGTH) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }
                    if (sConfirmPassword.getBytes().length > Config.PASSWORD_MAX_LENGTH) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_password_length), Toast.LENGTH_SHORT);
                        return;
                    }

                    byte[] nullPoint = new byte[]{(byte) 0x00, (byte) 0x00};

                    int length = 8 - sCurrentPassword.getBytes().length;
                    if (length != 0) {
                        byte[] empty = new byte[length];
                        sCurrentPassword = sCurrentPassword + new String(empty);
                    }

                    int length2 = 8 - sNewPassword.getBytes().length;
                    if (length2 != 0) {
                        byte[] empty = new byte[length2];
                        sNewPassword = sNewPassword + new String(empty);
                    }
                    final String finalPassword = sNewPassword;
                    APService.changePassword(sCurrentPassword + new String(nullPoint) + sNewPassword + new String(nullPoint), new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            String pw = finalPassword.trim();
                            Prf.writeToSharedPreferences(getActivity(), Prf.LOGIN_PASSWORD_FILE, Prf.LOGIN_PASSWORD_TAG, pw);

                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_complete_pw_change), Toast.LENGTH_SHORT);
                            dismiss();
                        }

                        @Override
                        public void onError(int errorCode) {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_pw), Toast.LENGTH_SHORT);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            dismiss();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

