package com.rareraw.master.Common;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.view.ViewTreeObserver;
import android.widget.SeekBar;

import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

/**
 * Created by Lch on 2016-10-04.
 * 기본 Fragment를 대신해서 사용할 Fragment입니다.
 * onFragmentCallbackListener를 공통적으로 사용하도록 만들었습니다.
 */
public class BaseFragment extends Fragment {
    protected OnFragmentCallbackListener onFragmentCallbackListener;
    protected OnSceneEditListener onSceneEditListener;

    protected static Toast mToast;

    /*
        public void showToast(String msg, int _time_length ){
            int timeLength = 0;
            if(_time_length == Toast.LENGTH_SHORT)
                timeLength = Toast.LENGTH_SHORT;
            if(_time_length == Toast.LENGTH_LONG)
                timeLength = Toast.LENGTH_LONG;

            Toast toast = new Toast(mContext);
            toast.showToast(msg, timeLength);
        }
    */
    public String removeNullChar(String str) {
        return str.replace("\0", "");
    }

    public void Logout() {
        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_retry_connect), Toast.LENGTH_LONG);
        APService.setUserLevel(false);
        byte[] nullVal = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
        APService.setConnectionId(nullVal);
        // APService.connectRequst();
        getActivity().onBackPressed();
        APService.connectRequst(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {

            }
        });
    }

    public void LogoutMain() {
        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_retry_connect), Toast.LENGTH_SHORT);
        APService.setUserLevel(false);
        byte[] nullVal = new byte[]{(byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x00};
        APService.setConnectionId(nullVal);
        APService.connectRequst(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {

            }
        });
    }

    public boolean checkValidation(String name) {
        if (name.length() <= 0) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_scene_name_length), Toast.LENGTH_SHORT);
            return false;
        }

        if (name.getBytes().length > Config.NAME_MAX_LENGTH) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_scene_name_length), Toast.LENGTH_SHORT);
            return false;
        }
        return true;
    }


    protected int getIconId(int iconIndex) {
        if (iconIndex == Config.USER_DEFAULT_ICON)
            return getResources().getIdentifier("icon_user_default_icon", "mipmap", getActivity().getPackageName());
        else if (iconIndex >= Config.CATEGORY1_MIN && iconIndex <= Config.CATEGORY1_MAX) // category 1 day
            return getResources().getIdentifier("icon_user_icon_day" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());
        else if (iconIndex >= Config.CATEGORY2_MIN && iconIndex <= Config.CATEGORY2_MAX) { // category 2 season
            iconIndex = iconIndex - Config.CATEGORY2_MIN;
            return getResources().getIdentifier("icon_user_icon_season" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());
        } else if (iconIndex >= Config.CATEGORY3_MIN && iconIndex <= Config.CATEGORY3_MAX) {  // category 3 place
            iconIndex = iconIndex - Config.CATEGORY3_MIN;
            return getResources().getIdentifier("icon_user_icon_place" + String.format("%02d", iconIndex), "mipmap", getActivity().getPackageName());
        }
        return getResources().getIdentifier("icon_user_icon_day01", "mipmap", getActivity().getPackageName());
    }

    public void resizeSeekbarthumb(final SeekBar mySeekbar, final double size, final Context _context) {
        ViewTreeObserver vto = mySeekbar.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Resources res = _context.getResources();
                Drawable thumb = res.getDrawable(R.mipmap.img_single_seekbar_thumb);
                int h = (int) (mySeekbar.getMeasuredHeight() * size);
                int w = h;
                Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                mySeekbar.setThumb(newThumb);
                mySeekbar.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }
    public void resizeSeekbarthumb(final SeekBar mySeekbar, final double size, final Context _context, final boolean flag) {
        ViewTreeObserver vto = mySeekbar.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                Resources res = _context.getResources();
                Drawable thumb = null;
                if(flag)
                    thumb = res.getDrawable(R.mipmap.img_single_seekbar_thumb);
                else
                    thumb = res.getDrawable(R.mipmap.img_master_seekbar_thumb_false );

                int h = (int) (mySeekbar.getMeasuredHeight() * size);
                int w = h;
                Bitmap bmpOrg = ((BitmapDrawable) thumb).getBitmap();
                Bitmap bmpScaled = Bitmap.createScaledBitmap(bmpOrg, w, h, true);
                Drawable newThumb = new BitmapDrawable(res, bmpScaled);
                newThumb.setBounds(0, 0, newThumb.getIntrinsicWidth(), newThumb.getIntrinsicHeight());
                mySeekbar.setThumb(newThumb);
                mySeekbar.getViewTreeObserver().removeOnPreDrawListener(this);
                return true;
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentCallbackListener) {
            onFragmentCallbackListener = (OnFragmentCallbackListener) context;
            onSceneEditListener = (OnSceneEditListener) context;
            if (mToast == null)
                mToast = new Toast(context);
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentCallbackListener");
        }
    }
}
