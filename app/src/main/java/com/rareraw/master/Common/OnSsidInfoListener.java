package com.rareraw.master.Common;

/**
 * Created by lch on 2016-12-11.
 */

public interface OnSsidInfoListener {
    void onSsidInfoListener(String sValue1, String sValue2);
}
