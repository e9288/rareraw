package com.rareraw.master.Common;

import android.app.DialogFragment;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neoromax.feelmaster.R;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogDeleteConfirm extends DialogFragment {

    private OnActionListener mListner;
    private LinearLayout mBtnCancel, mBtnOk;
    private ImageView mImgIcon;
    private TextView mContent;
    private View mParent;
    private int mIconType;
    public static int RESET = 1;
    public DialogDeleteConfirm() {
    }

    public void setDialogType(int type){
        mIconType = type;
    }
    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_delete, container, false);
        mContent = (TextView) mParent.findViewById(R.id.text_content);
        mImgIcon = (ImageView) mParent.findViewById(R.id.image_icon);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(mIconType == RESET){
            mImgIcon.setImageResource(R.mipmap.icon_scene);
            mContent.setText(getString(R.string.reset_device_msg));
        }
        mBtnCancel = (LinearLayout) mParent.findViewById(R.id.dialog_delete_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (LinearLayout) mParent.findViewById(R.id.dialog_delete_ok);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListner.onActionListener(1,"delete");
                dismiss();
            }
        });
    }
}

