package com.rareraw.master.Common;

/**
 * Created by Lch on 2016-10-04.
 */
public interface OnFragmentCallbackListener {
    void onFragmentCallback(String requestCode, int value);
}
