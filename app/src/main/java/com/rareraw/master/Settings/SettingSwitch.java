package com.rareraw.master.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.DialogDeleteConfirm;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Config;
import com.neoromax.feelmaster.R;
import com.rareraw.master.alarm.DialogSelectScene;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.SwitchInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.ArrayList;


public class SettingSwitch extends BaseFragment {
    private ListView mListviewSwitch;
    private Context mContext;
    private View mParent;
    private final int MAX_SIWTCH_CNT = 8;

    private int mSelectedPos = 0;
    private ImageView mAddSwitch;
    private int mDeletedPos = 0;
    private TextView mBtnSave, mBtnCancel;
    private switchAdapter mSwitchAdapter;
    private ArrayList<SwitchInfo> mSwitchList = new ArrayList<>();
    private OnActionListener mDeleteListener = new OnActionListener() {
        @Override
        public void onActionListener(int nValue, String sValue) {
            mSwitchList.remove(mDeletedPos);
            mSwitchAdapter.setSwitchList(mSwitchList);
            mSwitchAdapter.notifyDataSetChanged();
        }
    };

    private OnActionListener mSelectListener = new OnActionListener() {
        @Override
        public void onActionListener(int _sceneNo, String _scenename) {
          //  mSwitchList.get(mSelectedPos).sceneName = _scenename;
          //  mSwitchList.get(mSelectedPos).scneNo = _sceneNo;
            mSwitchAdapter.setSwitchList(mSwitchList);
            mSwitchAdapter.notifyDataSetChanged();
        }
    };

    public SettingSwitch() {
    }

    public static SettingSwitch newInstance() {
        SettingSwitch fragment = new SettingSwitch();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUi();

    }

    public void updateUi() {
        getSwitchList();
        if(mSwitchList == null)
            return;

        mSwitchAdapter = new switchAdapter(getActivity(), mSwitchList);
        mListviewSwitch.setAdapter(mSwitchAdapter);
    }

    private void getSwitchList() {
        final ArrayList<SwitchInfo> list = new ArrayList<>();
        if (Config.DEBUG) {
            for (int i = 0; i < MAX_SIWTCH_CNT; i++)
                list.add(new SwitchInfo());
        } else {
           /* try {
               APService.getSwitchInfo(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        mSwitchList = (ArrayList<SwitchInfo>) retValue;
                        boolean oldLevel = APService.getUserLevel();
                        APService.setUserLevel(true);
                        for (int i = 0; i < list.size(); i++)
                            list.get(i).sceneName = CmdHelper.getSceneName(CmdHelper.intToByteArray(list.get(i).index));
                        APService.setUserLevel(oldLevel);
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });

            } catch (Exception e) {

            }*/
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_setting_switch, container, false);
        mListviewSwitch = (ListView) mParent.findViewById(R.id.listview_setting_switch);
        mBtnSave = (TextView) mParent.findViewById(R.id.btn_switch_save);
        mBtnCancel = (TextView) mParent.findViewById(R.id.btn_switch_cancel);
        mAddSwitch = (ImageView) mParent.findViewById(R.id.btn_add_switch);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int count = mSwitchList.size();
                    ByteBuffer buffer = ByteBuffer.allocate(count * Cmd.SIWTCH__INFO_SIZE + 1);
                    buffer.put((byte) count);
                   /* for (int i = 0; i < mSwitchList.size(); i++) {
                        buffer.put((byte) mSwitchList.get(i).index);
                        buffer.put(CmdHelper.intToByteArray(mSwitchList.get(i).scneNo));
                    }*/
                    APService.setSwitchInfo(buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {

                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_switch_saved), com.rareraw.master.Common.Toast.LENGTH_SHORT );
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Logout();
                        }
                    });
                } catch (Exception e) {
                    Logout();
                    Log.e("error", "set switch info");
                }

            }
        });

        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "취소되었습니다.", Toast.LENGTH_SHORT).show();
            }
        });

        mAddSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*    if (mSwitchList.size() >= SIWTCH_MAX_CNT) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_switch_max_cnt), Toast.LENGTH_SHORT );
                    return;
                }
                SwitchInfo switchObj = new SwitchInfo();
                switchObj.index = getEmptyNo();
                switchObj.sceneName = getString(R.string.none);
                mSwitchList.add(switchObj);
                mSwitchAdapter.setSwitchList(mSwitchList);
                mSwitchAdapter.notifyDataSetChanged();*/
            }
        });
    }

    private int getEmptyNo() {
        if (mSwitchList.size() == 0)
            return 1;
        else {
            boolean flag = false;
            for (int i = 1; i < mSwitchList.size() + 1; i++) {
                for (int j = 0; j < mSwitchList.size(); j++) {
                  //  if (i == mSwitchList.get(j).index)
                  //      flag = true;
                }
                if (flag) {
                    flag = false;
                    continue;
                } else
                    return i;
            }
        }
        return mSwitchList.size() + 1;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public class switchAdapter extends BaseAdapter implements View.OnClickListener {
        private LayoutInflater mLayoutInflater;
        private ArrayList<SwitchInfo> mSwitchList;
        private Context mContext;
        private OnActionListener mDeleteComfirm = new OnActionListener() {
            @Override
            public void onActionListener(int nValue, String sValue) {

            }
        };

        switchAdapter(Context context, ArrayList<SwitchInfo> _list) {
            mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mSwitchList = _list;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return mSwitchList.size();
        }

        @Override
        public Object getItem(int position) {
            return mSwitchList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null)
                view = mLayoutInflater.inflate(R.layout.item_list_view_switch_list, parent, false);
            try {
                TextView textIndex = (TextView) view.findViewById(R.id.switch_index);
              //  textIndex.setText(Integer.toString(mSwitchList.get(position).index));

                TextView textTitle = (TextView) view.findViewById(R.id.switch_scenename);
               // textTitle.setText(mSwitchList.get(position).sceneName);

                LinearLayout selectNameBtn = (LinearLayout) view.findViewById(R.id.layout_switch_name);
                selectNameBtn.setTag(R.id.layout_switch_name, position);
                selectNameBtn.setOnClickListener(this);

                LinearLayout deleteBtn = (LinearLayout) view.findViewById(R.id.layout_switch_delete);
                deleteBtn.setTag(R.id.layout_switch_delete, position);
                deleteBtn.setOnClickListener(this);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return view;
        }

        @Override
        public void onClick(View view) {
            int position = 0;
            switch (view.getId()) {
                case R.id.layout_switch_name:
                    position = (int) view.getTag(R.id.layout_switch_name);
                    mSelectedPos = position;
                    DialogSelectScene dialogScene = new DialogSelectScene();
                    dialogScene.setStyle( DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light );
                    dialogScene.setListener(mSelectListener);
                    dialogScene.show(getActivity().getFragmentManager(), "dialogScene");
                    break;

                case R.id.layout_switch_delete:
                    position = (int) view.getTag(R.id.layout_switch_delete);
                    mDeletedPos = position;
                    DialogDeleteConfirm confirm = new DialogDeleteConfirm();
                    confirm.setListener(mDeleteListener);
                    confirm.show(getActivity().getFragmentManager(), "confirm");
                    break;
            }
        }

        public void setSwitchList(ArrayList<SwitchInfo> _switchList) {
            mSwitchList = _switchList;
        }
    }
}
