package com.rareraw.master.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnActionListener;
import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.Toast;

/**
 * Created by Lch on 2016-12-14.
 */

public class SettingFragment extends BaseFragment {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private Context mContext;
    private OnActionListener mListener ;
    SettingsPagerAdapter mPagerAdapter;
    // private final int WIFI = 0, NETWORK = 1, SWITCH = 2, OTHER = 3;
    private final int WIFI = 0, OTHER = 1;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        mTabLayout = (TabLayout) view.findViewById(R.id.settings_tablayout);
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.wifi)));
        //mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.network)));
        //mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.switch_text)));
        mTabLayout.addTab(mTabLayout.newTab().setText(getString(R.string.others)));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        TextView tv1 = (TextView)(((LinearLayout)((LinearLayout)mTabLayout.getChildAt(0)).getChildAt(0)).getChildAt(1));
        tv1.setScaleY(-1);
        TextView tv2 = (TextView)(((LinearLayout)((LinearLayout)mTabLayout.getChildAt(0)).getChildAt(1)).getChildAt(1));
        tv2.setScaleY(-1);
        /*TextView tv3 = (TextView)(((LinearLayout)((LinearLayout)mTabLayout.getChildAt(0)).getChildAt(2)).getChildAt(1));
        tv3.setScaleY(-1);
        TextView tv4= (TextView)(((LinearLayout)((LinearLayout)mTabLayout.getChildAt(0)).getChildAt(3)).getChildAt(1));
        tv4.setScaleY(-1);
*/
        mViewPager = (ViewPager) view.findViewById(R.id.settings_viewpager);


        ((TextView) view.findViewById(R.id.btn_cancel_setting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_cancel), Toast.LENGTH_SHORT );
                mPagerAdapter.fragmentReset();
                mListener.onActionListener(1,"back");
            }
        });

        ((TextView) view.findViewById(R.id.btn_save_setting)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch(mViewPager.getCurrentItem()){
                    case WIFI:
                        SettingWifi wifi = ( SettingWifi) mPagerAdapter.getItem(WIFI);
                        wifi.saveWifiData();
                        break;
                 /*   case NETWORK:
                        Settingnetwok network= ( Settingnetwok ) mPagerAdapter.getItem(NETWORK);
                        break;
                    case SWITCH:
                        SettingSwitch sSwitch = ( SettingSwitch ) mPagerAdapter.getItem(SWITCH);
                        break;*/
                    case OTHER:
                        SettingOthers others= ( SettingOthers) mPagerAdapter.getItem(OTHER);
                        others.saveOtherSetting();
                        break;
                }
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Creating TabPagerAdapter adapter
        mPagerAdapter = new SettingsPagerAdapter(getActivity().getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));

        // Set TabSelectedListener
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                switch (mViewPager.getCurrentItem()) {
                    case 2: // if switch, get switch info in server
                        try {
                            SettingSwitch fragment = (SettingSwitch) mPagerAdapter.getItem(mViewPager.getCurrentItem());
                            fragment.updateUi();
                        } catch (Exception e) {

                        }
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPagerAdapter.fragmentReset();
    }

    public void setListener(OnActionListener linstener){
        this.mListener = linstener;
    }
}