package com.rareraw.master.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.DialogChangePw;
import com.rareraw.master.Common.DialogResetDevice;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.Toast;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;


public class SettingOthers extends BaseFragment {

    private Context mContext;
    private View mParent;
    private CheckBox mCheckboxDmxUse;

    private TextView mBtnReset, mBtnChangePw, mBtnSave, mBtnCancel;

    public SettingOthers() {
    }

    public static SettingOthers newInstance() {
        SettingOthers fragment = new SettingOthers();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_setting_others, container, false);
        mBtnSave= (TextView) mParent.findViewById(R.id.btn_save_others);
        mBtnCancel = (TextView) mParent.findViewById(R.id.btn_cancel_others);
        mBtnReset  = (TextView) mParent.findViewById(R.id.setting_others_btn_reset);
        mBtnChangePw = (TextView) mParent.findViewById(R.id.setting_others_btn_change_pw);
        mCheckboxDmxUse = (CheckBox) mParent.findViewById(R.id.setting_others_checkbox_use_dmx);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_cancel), Toast.LENGTH_SHORT );
            }
        });
        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                APService.setDmxUsage(mCheckboxDmxUse.isChecked(), new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT );
                        APService.getDmxUsage(new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {

                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();
                    }
                });
            }
        });
        mBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogResetDevice dialogResetDevice = new DialogResetDevice();
                dialogResetDevice.show(getActivity().getFragmentManager(),"dialogResetDevice");
            }
        });
        mBtnChangePw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogChangePw changepw = new DialogChangePw();
                changepw.show(getActivity().getFragmentManager(),"changepw");
            }
        });

   /*     mCheckboxDmxUse.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked == true){
                    //Prf.writeToSharedPreferences(mContext, Prf.DMX_USE_FILE, Prf.DMX_USE_TAG, true);
                } else {
                    //Prf.writeToSharedPreferences(mContext, Prf.DMX_USE_FILE, Prf.DMX_USE_TAG, false);
                }
            }
        });*/
        // mCheckboxDmxUse.setChecked(APService.getDmxUseFlag());
    }

    public void saveOtherSetting(){
        APService.setDmxUsage(mCheckboxDmxUse.isChecked(), new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT );
                APService.getDmxUsage(new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {

                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {

                    }
                });
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                Logout();
            }
        });
    }
    @Override
    public void onStart(){
        super.onStart();
        setDmxUseUi(APService.getDmxUseFlag());
    }

    private void setDmxUseUi(boolean stat) {
        mCheckboxDmxUse.setChecked(stat);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
