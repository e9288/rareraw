package com.rareraw.master.Settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.neoromax.feelmaster.R;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;


public class Settingnetwok extends BaseFragment {

    private EditText mIp, mSubnet, mGateWay, mDns;
    private Spinner mPanid, mChannelId;
    private Context mContext;
    private View mParent;
    private TextView mSaveNet, mSaveZigbee;
    private final int DEFAULT_CH_ID = 10, DEFAULT_PAN_ID = 10;
    private RelativeLayout mBtnPanId, mBtnChannelId;
    public Settingnetwok() {
    }

    public static Settingnetwok newInstance() {
        Settingnetwok fragment = new Settingnetwok();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_setting_network, container, false);
        mIp = (EditText) mParent.findViewById(R.id.net_ip);
        mSubnet= (EditText) mParent.findViewById(R.id.net_subnet);
        mGateWay = (EditText) mParent.findViewById(R.id.net_gateway);
        mDns = (EditText) mParent.findViewById(R.id.net_dns);


        mPanid = (Spinner) mParent.findViewById(R.id.spn_panid);
        mChannelId= (Spinner) mParent.findViewById(R.id.spn_channel_id);
        mSaveNet = (TextView) mParent.findViewById(R.id.btn_save_network);
        mSaveZigbee = (TextView) mParent.findViewById(R.id.btn_save_zigbee);

        mBtnPanId = (RelativeLayout) mParent.findViewById(R.id.layout_spn_panid);
        mBtnChannelId= (RelativeLayout) mParent.findViewById(R.id.layout_spn_channel_id);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnPanId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPanid.performClick();
            }
        });
        mBtnChannelId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // mChannelId.performClick();
            }
        });
        mSaveNet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mIp.getText().toString().length() <= 0){
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_ip_add), Toast.LENGTH_SHORT );
                    return;
                }
                if(mSubnet.getText().toString().length() <= 0){
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_sub_add), Toast.LENGTH_SHORT );
                    return;
                }
                if(mGateWay.getText().toString().length() <= 0){
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_gate_add), Toast.LENGTH_SHORT );
                    return;
                }
                if(mDns.getText().toString().length() <= 0){
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_dns_add), Toast.LENGTH_SHORT );
                    return;
                }
                MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT );
            }
        });
        mSaveZigbee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String sChannelId = mPanid.getSelectedItem().toString().substring(2,4);
                    String sPanId     = mChannelId.getSelectedItem().toString().substring(2,4);
                    int nChannelId =  Integer.parseInt( sChannelId, 16 );
                    int nPanId = Integer.parseInt( sPanId, 16 );
                    byte chanid = (byte) nChannelId;
                    byte panid = (byte) nPanId ;
                    ByteBuffer buf = ByteBuffer.allocate(2);
                    buf.put(chanid);
                    buf.put(panid);
                    //char cChannelId = (char) nChannelId;
                    //char cPanid = (char) nPanId;
                    APService.setZigBeeChannel(buf, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_saved), Toast.LENGTH_SHORT );
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Logout();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        String[] myResArray = getResources().getStringArray(R.array.zigbee_panid_item);
        List<String> zigbeeOption = Arrays.asList(myResArray);
        SpinnerAdapter mAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_dropdown_item, zigbeeOption);
        mPanid.setAdapter(mAdapter);
        mPanid.setSelection(DEFAULT_PAN_ID);
        mAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_dropdown_item, zigbeeOption);
        mChannelId.setAdapter(mAdapter);
        mChannelId.setSelection(DEFAULT_CH_ID);
        mChannelId.setEnabled(false);
        mChannelId.setFocusable(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
