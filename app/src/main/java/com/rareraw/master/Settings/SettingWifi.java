package com.rareraw.master.Settings;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.MyToast;
import com.rareraw.master.Common.OnSsidInfoListener;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.NetworkResultLisnter;

import java.nio.ByteBuffer;


public class SettingWifi extends BaseFragment {

    private OnFragmentInteractionListener mListener;
    private EditText mBtnSelectWifi;
    private EditText mEditSetFeelmasterName;
    private RadioButton mRadioApmode, mRadioClientMode;
    private EditText mApPassword, mApPasswordConfirm, mPasswordClient, mIpAddress;
    private LinearLayout mLayoutApmode, mLayoutClientMode;
    private Context mContext;
    private View mParent;
    private boolean mSelectedFlag = false;
    private TextView mBtnSave, mBtnSaveIp;
    private boolean NETWORK_MODE = false; // true : ap mode, false : client mode
    private boolean AP_MODE = true, CLIENT_MODE = false;
    private String mSsid, mBssid;
    private OnSsidInfoListener mOnSsidListener = new OnSsidInfoListener() {
        @Override
        public void onSsidInfoListener(String _ssid, String _bssid) {
            mSelectedFlag = true;
            mSsid = _ssid;
            mBssid = _bssid;
            mBtnSelectWifi.setText(mSsid);
        }
    };
    private RadioGroup mRadioGroup;

    public SettingWifi() {
    }


    public static SettingWifi newInstance() {
        SettingWifi fragment = new SettingWifi();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }


    private void setNetworkMode(boolean _mode) {
        NETWORK_MODE = _mode;
        Log.e("mode : ", (_mode) ? "ap" : "client");
        Prf.writeToSharedPreferences(mContext, Prf.WIRELESS_MODE_FILE, Prf.WIRELESS_MODE_TAG, _mode);
        updateUi(_mode);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mParent = inflater.inflate(R.layout.fragment_setting_wifi, container, false);
        mRadioGroup = (RadioGroup) mParent.findViewById(R.id.radio_group);
        mRadioApmode = (RadioButton) mParent.findViewById(R.id.setting_radio_ap_mode);
        mRadioClientMode = (RadioButton) mParent.findViewById(R.id.setting_radio_client_mode);

        mEditSetFeelmasterName = (EditText) mParent.findViewById(R.id.apmode_ap_name);
        mApPassword = (EditText) mParent.findViewById(R.id.ap_mode_password);
        mApPasswordConfirm = (EditText) mParent.findViewById(R.id.ap_mode_password_confirm);

        mBtnSelectWifi = (EditText) mParent.findViewById(R.id.client_mode_name);
        mPasswordClient = (EditText) mParent.findViewById(R.id.client_mode_password);

        mLayoutApmode = (LinearLayout) mParent.findViewById(R.id.layout_ap_mode);
        mLayoutClientMode = (LinearLayout) mParent.findViewById(R.id.layout_client_mode);
        mBtnSave = (TextView) mParent.findViewById(R.id.btn_save_network);
        mBtnSaveIp = (TextView) mParent.findViewById(R.id.btn_save_ip);
        mIpAddress = (EditText) mParent.findViewById(R.id.etext_ip_address);
        return mParent;
    }

    public void saveWifiData() {
        if (NETWORK_MODE == AP_MODE) {
            if (!checkInvalideParam())
                return;

            ByteBuffer buffer = makeBuffer();
            if (buffer != null) {
                APService.setNetworkApMode(buffer, new NetworkResultLisnter() {
                    @Override
                    public void onSuccress(Object retValue) {
                        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_change_wifi), Toast.LENGTH_SHORT);
                        APService.getNetworkMode(new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {
                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {

                            }
                        });
                    }

                    @Override
                    public void onError(int errorCode) {

                    }

                    @Override
                    public void onErrorConnectionId(int errorCode) {
                        Logout();
                    }
                });
            }
        } else if (NETWORK_MODE == CLIENT_MODE) {
            if (checkInvalideParam2()) {
                ByteBuffer buffer = makeBuffer2();

                if (buffer != null) {
                    APService.setNetworkClientMode(buffer, new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_connect_cliedt), Toast.LENGTH_SHORT);
                            APService.getNetworkMode(new NetworkResultLisnter() {
                                @Override
                                public void onSuccress(Object retValue) {
                                }

                                @Override
                                public void onError(int errorCode) {

                                }

                                @Override
                                public void onErrorConnectionId(int errorCode) {

                                }
                            });
                        }

                        @Override
                        public void onError(int errorCode) {

                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            Logout();
                        }
                    });
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        /*mBtnSelectWifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TedPermission.with(mContext)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {

                                DialogSelectSSid dialogSelectSSid = new DialogSelectSSid();
                                DialogSelectSSid.setLisetener(mOnSsidListener);
                               //  DialogSelectSSid.setContext(mContext);
                                dialogSelectSSid.show(getActivity().getFragmentManager(), "dialogSelectSSid ");

                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_ip_add), Toast.LENGTH_SHORT);
                            }
                        })
                        .setPermissions(Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_MULTICAST_STATE)
                        .check();

            }
        });*/
        mBtnSaveIp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIpAddress.getText().toString().length() <= 0) {
                    MyToast.getInstance(getActivity()).showToast(getString(R.string.mas_want_ip_add), Toast.LENGTH_SHORT);
                    return;
                }
                // TODO setip adress to ap

            }
        });

       /* mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NETWORK_MODE == AP_MODE) {
                    if (!checkInvalideParam())
                        return;

                    ByteBuffer buffer = makeBuffer();
                    if (buffer != null) {
                        APService.setNetworkApMode(buffer, new NetworkResultLisnter() {
                            @Override
                            public void onSuccress(Object retValue) {
                                MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_change_wifi), Toast.LENGTH_SHORT);
                                APService.getNetworkMode(new NetworkResultLisnter() {
                                    @Override
                                    public void onSuccress(Object retValue) {
                                    }

                                    @Override
                                    public void onError(int errorCode) {

                                    }

                                    @Override
                                    public void onErrorConnectionId(int errorCode) {

                                    }
                                });
                            }

                            @Override
                            public void onError(int errorCode) {

                            }

                            @Override
                            public void onErrorConnectionId(int errorCode) {
                                Logout();
                            }
                        });
                    }
                } else if (NETWORK_MODE == CLIENT_MODE) {
                    if (checkInvalideParam2()) {
                        ByteBuffer buffer = makeBuffer2();

                        if (buffer != null) {
                            APService.setNetworkClientMode(buffer, new NetworkResultLisnter() {
                                @Override
                                public void onSuccress(Object retValue) {
                                    MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_connect_cliedt), Toast.LENGTH_SHORT);
                                    APService.getNetworkMode(new NetworkResultLisnter() {
                                        @Override
                                        public void onSuccress(Object retValue) {
                                        }

                                        @Override
                                        public void onError(int errorCode) {

                                        }

                                        @Override
                                        public void onErrorConnectionId(int errorCode) {

                                        }
                                    });
                                }

                                @Override
                                public void onError(int errorCode) {

                                }

                                @Override
                                public void onErrorConnectionId(int errorCode) {
                                    Logout();
                                }
                            }) ;
                        }
                    }
                }
            }
        });*/
        mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.setting_radio_ap_mode:
                        setNetworkMode(AP_MODE);
                        break;
                    case R.id.setting_radio_client_mode:
                        setNetworkMode(CLIENT_MODE);
                        break;
                }
            }
        });
        setNetworkMode(Prf.readToSharedPreferences(mContext, Prf.WIRELESS_MODE_FILE, Prf.WIRELESS_MODE_TAG, AP_MODE));
    }

    private ByteBuffer makeBuffer2() {
        ByteBuffer buffer = ByteBuffer.allocate(Cmd.CLIENT_MODE_BUFFER_SIZE);

        String fullName = mSsid;
        int length = 16 - mSsid.getBytes().length;
        byte[] empty = new byte[length];
        fullName = fullName + new String(empty);

        String fullPassword = mPasswordClient.getText().toString();
        int length2 = 16 - mPasswordClient.getText().toString().getBytes().length;
        byte[] empty2 = new byte[length2];
        fullPassword = fullPassword + new String(empty2);

        buffer.put(fullName.getBytes());
        buffer.put(fullPassword.getBytes());
        buffer.put(APService.getIpadress().getBytes());

        return buffer;
    }

    private boolean checkInvalideParam2() {
        if (mPasswordClient.getText().length() <= 5) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }

        if (mSsid == null || mBssid == null) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_select_feelmaster), Toast.LENGTH_SHORT);
            return false;
        }

        if (mSsid.length() <= 0) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_select_feelmaster), Toast.LENGTH_SHORT);
            return false;
        }
        if (mSsid.length() > 16) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_name_length), Toast.LENGTH_SHORT);
            return false;
        }
        return true;
    }

    private ByteBuffer makeBuffer() {
        ByteBuffer buffer = ByteBuffer.allocate(Cmd.AP_MODE_BUFFER_SIZE);

        String fullName = mEditSetFeelmasterName.getText().toString();
        int length = 16 - mEditSetFeelmasterName.getText().toString().getBytes().length;
        byte[] empty = new byte[length];
        fullName = fullName + new String(empty);

        String fullPassword = mApPassword.getText().toString();
        int length2 = 16 - mApPassword.getText().toString().getBytes().length;
        byte[] empty2 = new byte[length2];
        fullPassword = fullPassword + new String(empty2);

        buffer.put(fullName.getBytes());
        buffer.put(fullPassword.getBytes());
        buffer.put(APService.getIpadress().getBytes());

        return buffer;
    }

    private boolean checkInvalideParam() {

        if (mEditSetFeelmasterName.getText().length() <= 0) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mEditSetFeelmasterName.getText().toString().getBytes().length > 16) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_overflow_name_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mApPassword.getText().length() <= 5 || mApPasswordConfirm.getText().length() <= 5) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (mApPassword.getText().length() < 8 || mApPasswordConfirm.getText().length() < 8) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_underflow_password_length), Toast.LENGTH_SHORT);
            return false;
        }
        if (!mApPassword.getText().toString().equals(mApPasswordConfirm.getText().toString())) {
            MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_not_match_pw), Toast.LENGTH_SHORT);
            return false;
        }

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        setNetworkMode(Prf.readToSharedPreferences(mContext, Prf.WIRELESS_MODE_FILE, Prf.WIRELESS_MODE_TAG, AP_MODE));
        mSelectedFlag = false;
        mSsid = "";
        mBssid = "";
    }

    private void updateUi(boolean _mode) {
        if (_mode) {
            mLayoutApmode.setVisibility(View.VISIBLE);
            mLayoutClientMode.setVisibility(View.GONE);
            mRadioApmode.setChecked(true);
            mRadioClientMode.setChecked(false);
        } else {
            mLayoutApmode.setVisibility(View.GONE);
            mLayoutClientMode.setVisibility(View.VISIBLE);
            mRadioApmode.setChecked(false);
            mRadioClientMode.setChecked(true);
        }
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
     /*   if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
