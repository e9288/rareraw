package com.rareraw.master.Settings;

import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rareraw.master.Common.OnActionListener;
import com.rareraw.master.Common.OnSsidInfoListener;
import com.neoromax.feelmaster.R;

import java.util.List;

/**
 * Created by lch on 2016-12-14.
 */

public class DialogSelectSSid extends DialogFragment {

    private OnActionListener mListner;
    private TextView mBtnCancel, mBtnOk;
    private ListView mListviewSceneList;
    private SceneListAdapter mAdapter;
    private View mParent;
    // private WifiScanAdapter adapter;
    private  Context mContext;
    private WifiManager wifiManager;
    private List<ScanResult> scanDatas;
    private static OnSsidInfoListener mListener;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                scanDatas = wifiManager.getScanResults();
                if(scanDatas != null && scanDatas.size() == 0){
                    wifiManager.startScan();
                    return;
                }
                mAdapter = new SceneListAdapter(getActivity(), scanDatas);
                mListviewSceneList.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }else if(action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                getActivity().sendBroadcast(new Intent("wifi.ON_NETWORK_STATE_CHANGED"));
            }
        }
    };

    public DialogSelectSSid() {
    }

    public static void setContext(Context mContext) {
      //  this.mContext = mContext;
    }

    public void setListener(OnActionListener _listener) {
        mListner = _listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        //wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        wifiManager = (WifiManager) getActivity().getApplication().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
      //  if(!wifiManager.isWifiEnabled()){
            wifiManager.setWifiEnabled(true);
     //   }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mParent = inflater.inflate(R.layout.dialog_select_ssid, container, false);
        return mParent;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mBtnCancel = (TextView) mParent.findViewById(R.id.btn_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        mBtnOk = (TextView) mParent.findViewById(R.id.btn_confirm);
        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mSelectFlag){
                    Toast.makeText(getActivity(), "원하는 필마스터를 선택해주세요.", Toast.LENGTH_SHORT).show();
                    return;
                }
                mListener.onSsidInfoListener(mAdapter.getSsid(), mAdapter.getBssid());
                dismiss();
            }
        });
        mListviewSceneList = (ListView) mParent.findViewById(R.id.listview_ssid_list);
    }
    @Override
    public void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        getActivity().registerReceiver(receiver, intentFilter);
        wifiManager.startScan();

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    public static void setLisetener(OnSsidInfoListener _listener) {
        mListener = _listener;
    }

    private View mExistingView;
    private boolean mSelectFlag = false;
    public class SceneListAdapter extends BaseAdapter {

        private LayoutInflater mLayoutInflater;
        private List<ScanResult> mSSidList;
        private Context mContext;
        private String mSelectedSsid ;
        private String mSelectedBSsid;
        SceneListAdapter(Context context, List<ScanResult> _sSidList) {
            mLayoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.mSSidList = _sSidList;
            this.mContext = context;
        }

        @Override
        public int getCount() {
            return mSSidList.size();
        }

        @Override
        public Object getItem(int position) {
            return mSSidList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View view, ViewGroup parent) {
            if (view == null)
                view = mLayoutInflater.inflate(R.layout.item_list_view_ssid_list, parent, false);

            TextView Ssid = (TextView) view.findViewById(R.id.ssid_ssid);
            TextView bSsid = (TextView) view.findViewById(R.id.ssid_bssid);
            Ssid.setText(mSSidList.get(position).SSID.toString());
            bSsid.setText(mSSidList.get(position).BSSID.toString());
            view.setTag(R.id.id_select_ssid_position, position);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectFlag = true;
                    view.setBackgroundColor(getResources().getColor(R.color.selected_ssid_back_color));
                    int position =(int) view.getTag(R.id.id_select_ssid_position);
                    if(mExistingView != null)
                        mExistingView.setBackgroundColor(getResources().getColor(R.color.transparent));
                    mSelectedSsid = mSSidList.get(position).SSID.toString();
                    mSelectedBSsid  = mSSidList.get(position).BSSID.toString();
                    mExistingView = view;
                }
            });
            return view;
        }
        public String getSsid(){
            return mSelectedSsid;
        }
        public String getBssid(){
            return mSelectedBSsid;
        }
    }
}

