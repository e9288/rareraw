package com.rareraw.master.Settings;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by lch on 2016-10-11.
 */

public class SettingsPagerAdapter extends FragmentStatePagerAdapter {

    private  SettingWifi wifi = new SettingWifi();
    private  Settingnetwok network = new Settingnetwok();
    private  SettingSwitch switchse = new SettingSwitch();
    private  SettingOthers others = new SettingOthers();


    // Count number of tabs
    private int mTabCount;

    public SettingsPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mTabCount = tabCount;
    }

    public void fragmentReset(){
        wifi = null;
        network = null;
        switchse = null;
        others = null;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (wifi == null) {
                    wifi = new SettingWifi();
                    return wifi;
                }
                return wifi;

            case 1:
                if (others == null) {
                    others = new SettingOthers();
                    return others;
                }
                return others;
            default:
                return new SettingWifi();
        }

       /* switch (position) {
            case 0:
                if (wifi == null) {
                    wifi = new SettingWifi();
                    return wifi;
                }
                return wifi;
            case 1:
                if (network == null) {
                    network = new Settingnetwok();
                    return network;
                }
                return network;
            case 2:
                if (switchse == null) {
                    switchse = new SettingSwitch();
                    return switchse;
                }
                return switchse;
            case 3:
                if (others == null) {
                    others = new SettingOthers();
                    return others;
                }
                return others;
            default:
                return new SettingWifi();
        }*/
    }

    @Override
    public int getCount() {
        return mTabCount;
    }
}