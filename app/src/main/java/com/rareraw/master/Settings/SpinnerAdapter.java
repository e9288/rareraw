package com.rareraw.master.Settings;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.neoromax.feelmaster.R;

import java.util.List;

/**
 * Created by lch on 2016-10-17.
 */

public class SpinnerAdapter extends ArrayAdapter<String>{
    private Activity activity;
    private List<String> data;
    public Resources res;
    LayoutInflater inflater;
    public SpinnerAdapter(Context context, int resource) {
        super(context, resource);
    }
    public SpinnerAdapter(
            Activity activitySpinner,
            int textViewResourceId,
            List<String> objects
    ) {
        super(activitySpinner, textViewResourceId, objects);
        activity = activitySpinner;
        data = objects;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {
        View row = inflater.inflate(R.layout.spinner_dropdown_item, parent, false);
        String sceneName = (String) data.get(position);
        TextView textSceneName = (TextView) row.findViewById(R.id.spinner_dropdown_id);
        textSceneName.setText(sceneName);
        return row;
    }

}
