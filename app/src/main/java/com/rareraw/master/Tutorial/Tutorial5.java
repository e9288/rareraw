package com.rareraw.master.Tutorial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.rareraw.master.Common.MyToast;
import com.rareraw.master.ContainActivity;
import com.rareraw.master.Common.Toast;
import com.rareraw.master.Preference.Prf;
import com.neoromax.feelmaster.R;


/**
 * Created by lch on 2016-10-03.
 */

public class Tutorial5 extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private Activity mParentActivity;
    private View mParent;
    public Tutorial5() {
    }

    public static Tutorial5 newInstance(int sectionNumber) {
        Tutorial5 fragment = new Tutorial5();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity mainActivity) {
        super.onAttach(mainActivity);
        mParentActivity = (TutorialManager)mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_tutorial5, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        Button text = (Button) mParent.findViewById(R.id.text_complete);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                completeTutorial();
                startActivity(new Intent(getActivity(), ContainActivity.class));
                mParentActivity.finish();
            }
        });
        return mParent;
    }

    private void completeTutorial() {
        Prf.writeToSharedPreferences(getContext(), Prf.TUTORIAL_OK_FILE, Prf.TUTORIAL_OK_TAG, true);
        MyToast.getInstance(getActivity()).showToast(getString(R.string.msg_welcome_feelmaster), Toast.LENGTH_SHORT );
    }
}