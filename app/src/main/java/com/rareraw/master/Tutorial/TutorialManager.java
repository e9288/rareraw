package com.rareraw.master.Tutorial;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.rareraw.master.Common.OnFragmentCallbackListener;
import com.rareraw.master.Common.OnSceneEditListener;
import com.neoromax.feelmaster.R;

public class TutorialManager extends AppCompatActivity implements OnFragmentCallbackListener ,OnSceneEditListener {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private TutorialAdapter mAdapder;
    private Context mContext;

    @Override
    public void onFragmentCallback(String requestCode, int response) {

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mAdapder = new TutorialAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.tutorial_viewpager);
        mViewPager.setAdapter(mAdapder);


        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.tutorial_indicator);
        mViewPager.setAdapter(mAdapder);
        indicator.setViewPager(mViewPager);
        // indicator.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        mViewPager.setCurrentItem(0);
    }

    @Override
    public void onSceneEditListener(int nValue1, int nValue2, int nValue3) {

    }
}
