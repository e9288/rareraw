package com.rareraw.master.Tutorial;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by lch on 2016-10-03.
 */

public class TutorialAdapter  extends FragmentPagerAdapter {
    private final int PAGE_COUNT = 5;

    public TutorialAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
       /*      case 0:
                return new DevManagerFragment();
            case 1:
                return new DevManagerFragment();
            case 2:
                return new DevManagerFragment();
            case 3:
                return new ChannelManagerFragment();
            case 4:
                return new AddSceneFragment();*/

            case 0:
                return Tutorial1.newInstance(position + 1);
            case 1:
                return Tutorial2.newInstance(position + 1);
            case 2:
                return Tutorial3.newInstance(position + 1);
            case 3:
                return Tutorial4.newInstance(position + 1);
            case 4:
                return Tutorial5.newInstance(position + 1);
        }
        // return AlarmFragment.newInstance();
        return Tutorial1.newInstance(position + 1);

    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SECTION 1";
            case 1:
                return "SECTION 2";
            case 2:
                return "SECTION 3";
        }
        return null;
    }
}