package com.rareraw.master.Tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neoromax.feelmaster.R;


/**
 * Created by lch on 2016-10-03.
 */

public class Tutorial2 extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private View mParent;
    public Tutorial2() {
    }

    public static Tutorial2 newInstance(int sectionNumber) {
        Tutorial2 fragment = new Tutorial2();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_tutorial2, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        return mParent;
    }
}