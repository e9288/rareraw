package com.rareraw.master.Tutorial;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.neoromax.feelmaster.R;


public class Tutorial4 extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private View mParent;
    public Tutorial4() {
    }

    public static Tutorial4 newInstance(int sectionNumber) {
        Tutorial4 fragment = new Tutorial4();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_tutorial4, container, false);
        //TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
        return mParent;
    }
}