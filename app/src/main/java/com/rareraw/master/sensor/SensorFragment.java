package com.rareraw.master.sensor;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.rareraw.master.Common.BaseFragment;
import com.rareraw.master.Common.Toast;
import com.neoromax.feelmaster.R;
import com.rareraw.master.data.SensorInfo;
import com.rareraw.master.network.APService;
import com.rareraw.master.network.Cmd;
import com.rareraw.master.network.DataClass.SceneInfo;
import com.rareraw.master.network.NetworkResultLisnter;

import java.util.Timer;
import java.util.TimerTask;

public class SensorFragment extends BaseFragment {
    private static String TAG = "SensorFragment";
    private static Context mContext;
    private View mParent;
    private TextView mTextSceneName, mTextDimingLevel, mTextDetectStatus;
    private CheckBox mSensorDetect, mSensorDiming;
    private Timer mTimer;
    Handler mHandler = new Handler(Looper.getMainLooper());
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();

    }

    @Override
    public void onStart() {
        super.onStart();
        // updateUi();

    }

    private void updateUi() {
        // getCurrentScene();
        getSensorInfo();
    }

    private void getCurrentScene() {

        APService.getCurrentSceneDimingLevel(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                int level = (int) retValue;
                mTextDimingLevel.setText(Integer.toString(level));
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {

            }
        });

        APService.getCurrentActionScene(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {

                int sceneNo = (int) retValue;
                SceneInfo curScene = null;
                for (SceneInfo info : APService.getApSceneList()) {
                    if (info.getIntSceneNo() == sceneNo) {
                        curScene = info;
                    }
                }

                if (curScene != null) {
                    String name = "";
                    if (curScene.getIntSceneNo() == Cmd.DEFAULT_SCENE_100) {
                        name = "전체조명";
                    }
                    mTextSceneName.setText(name);
                }
            }

            @Override
            public void onError(int errorCode) {

            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                LogoutMain();
            }
        });
    }

    private void getSensorInfo() {

        APService.getSensorInfo(new NetworkResultLisnter() {
            @Override
            public void onSuccress(Object retValue) {
                SensorInfo sinfo = (SensorInfo) retValue;
                setSensorInfo(sinfo);
            }

            @Override
            public void onError(int errorCode) {

                SensorInfo sinfo = new SensorInfo();
                sinfo.setIntSceneNo(4);

                setSensorInfo(sinfo);
            }

            @Override
            public void onErrorConnectionId(int errorCode) {
                LogoutMain();
            }
        });
    }

    private void setSensorInfo(final SensorInfo sinfo) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    SceneInfo curScene = null;
                    for (SceneInfo info : APService.getApSceneList()) {
                        if (info.getIntSceneNo() == sinfo.getIntSceneNo()) {
                            curScene = info;
                        }
                    }

                    if (curScene != null) {
                        String name = "";
                        if (curScene.getIntSceneNo() == Cmd.DEFAULT_SCENE_100) {
                            name = "전체조명";
                        } else
                            name = curScene.getSceneName().trim();

                        mTextSceneName.setText(name);
                    }


                    mSensorDetect.setChecked(sinfo.isDetectSensorStatus());
                    mSensorDiming.setChecked(sinfo.isLightSensorStatus());

                    mTextDimingLevel.setText(Integer.toString(sinfo.getLightSensorValue()));
                    String onOff = sinfo.isDetectValue() == false ? "OFF" : "ON";
                    mTextDetectStatus.setText(onOff);
                } catch (Exception e) {
                }
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mParent = inflater.inflate(R.layout.fragment_sensor_manager, container, false);
        mTextSceneName = (TextView) mParent.findViewById(R.id.fragment_sensor_cur_scene_text);
        mTextDimingLevel = (TextView) mParent.findViewById(R.id.fragment_sensor_light_text);
        mTextDetectStatus = (TextView) mParent.findViewById(R.id.fragment_sensor_detect_text);

        mSensorDetect = (CheckBox) mParent.findViewById(R.id.fragment_sensor_detect_enable);
        mSensorDiming = (CheckBox) mParent.findViewById(R.id.fragment_sensor_light_enable);

        // mSensorDetect.setEnabled(false);
        // mSensorDiming .setEnabled(false);

        mTimer = new Timer();

        TimerTask TT = new TimerTask() {
            @Override
            public void run() {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        updateUi();
                    }
                },100);
            }

        };

        mTimer.schedule(TT, 0, 4000);
        return mParent;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //mSensorDetect.setChecked(true);
        // mSensorDiming.setChecked(true);
        mSensorDetect.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {

                    APService.setSensorDetectStatus(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {

                            //mSensorDetect.setChecked(false);
                            getSensorInfo();
                        }

                        @Override
                        public void onError(int errorCode) {
                            android.widget.Toast.makeText(getContext(), "센서 설정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            mSensorDetect.setChecked(false);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            mSensorDetect.setChecked(false);
                        }
                    }, true);

                } else {
                    APService.setSensorDetectStatus(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            //mSensorDetect.setChecked(true);
                            getSensorInfo();
                        }

                        @Override
                        public void onError(int errorCode) {

                            android.widget.Toast.makeText(getContext(), "센서 설정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            mSensorDetect.setChecked(true);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            mSensorDetect.setChecked(true);
                        }
                    }, false);
                }
            }
        });

        mSensorDiming.setOnClickListener(new CheckBox.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    APService.setSensorLightStatus(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            // mSensorDiming.setChecked(false);
                            getSensorInfo();
                        }

                        @Override
                        public void onError(int errorCode) {
                            android.widget.Toast.makeText(getContext(), "센서 설정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            mSensorDiming.setChecked(false);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            mSensorDiming.setChecked(false);
                        }
                    }, true);
                } else {
                    APService.setSensorLightStatus(new NetworkResultLisnter() {
                        @Override
                        public void onSuccress(Object retValue) {
                            //mSensorDiming.setChecked(true);
                            getSensorInfo();
                        }

                        @Override
                        public void onError(int errorCode) {
                            android.widget.Toast.makeText(getContext(), "센서 설정에 실패하였습니다.", Toast.LENGTH_SHORT).show();
                            mSensorDiming.setChecked(true);
                        }

                        @Override
                        public void onErrorConnectionId(int errorCode) {
                            mSensorDiming.setChecked(true);
                        }
                    }, false);
                }
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mTimer.cancel();
    }
}